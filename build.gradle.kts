repositories {
    mavenCentral()
    google()
}

plugins {
    alias(libs.plugins.kotlin)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.spring.boot)
    alias(libs.plugins.spring.dependency.management)
    jacoco
}

group = "com.dou"
version = "1.0-SNAPSHOT"

tasks {
    test {
        useJUnitPlatform()
    }

    jacocoTestReport {
        reports {
            xml.required.set(true)
            csv.required.set(false)
            html.required.set(true)
        }
    }
}

jacoco {
    toolVersion = "0.8.11"
}

kotlin {
    jvmToolchain(21)
}

tasks {
    compileKotlin {
        kotlinOptions {
            freeCompilerArgs += "-Xcontext-receivers"
        }
    }
    compileTestKotlin {
        kotlinOptions {
            freeCompilerArgs += "-Xcontext-receivers"
        }
    }
    processResources {
        from("src/main/kotlin/") {
            include("com/dou/advent/year*/*.txt")
        }
    }
}

dependencies {
    implementation(libs.bundles.kotlin)

    implementation(libs.spring.boot.starter)

    implementation(libs.bundles.coroutines)
    implementation(libs.bundles.serialization)
    implementation(libs.bundles.akka)

    implementation(libs.allure)

    implementation(libs.guava)

    implementation(libs.bundles.arrow)

    implementation(libs.bundles.junit)
    implementation(libs.hamcrest)

    implementation(libs.bundles.ktor)

    testImplementation(libs.bundles.testcontainers)
}

springBoot {
    mainClass = "com.dou.modules.ModulesApplication"
}
