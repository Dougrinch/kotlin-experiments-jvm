#!/usr/bin/env bash

mv ./allure-report/history ./allure-results/
rm -r ./allure-report
allure generate ./allure-results
allure open
rm -r allure-results