package com.dou.modules

import jakarta.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.getBean
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@SpringBootApplication
open class ModulesApplication

@Component
class A

@Component
data class B(val a: A)

interface Test

@Scope("prototype")
@Component
data class Foo(val b: B, @Assisted val name: String, @Assisted val ages: List<Int>) : Test {
    @Autowired
    lateinit var a: A

    init {
        println("Foo.<init>")
    }

    @PostConstruct
    fun setup() {
        println("Foo.postConstruct() $a $name")
    }

    override fun toString(): String {
        return "Foo(a=$a, b=$b, name='$name', ages=$ages)"
    }
}

@Component
data class Baz(val a: A, val fooFactory: AssistedBeanFactory2<Test, String, List<Int>>) {
    fun run(name: String) {
        println("---")
        println("Baz.run($name), a=$a")
        println(fooFactory.create(name, listOf(1, 2, 3)))
        println("---")
    }
}

fun main() {
    val ctx = runApplication<ModulesApplication> {
        setDefaultProperties(mapOf("spring.config.name" to "test"))
        setAdditionalProfiles("next one")
    }

    val baz = ctx.getBean<Baz>()
    baz.run("hello")
    baz.run("world")

    println(ctx.getBean<AAA>())
}

interface AAA

@Component
class BBB : AAA
