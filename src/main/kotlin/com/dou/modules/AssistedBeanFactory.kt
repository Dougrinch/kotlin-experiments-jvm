package com.dou.modules

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.AutowireCapableBeanFactory
import org.springframework.beans.factory.config.DependencyDescriptor
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.core.MethodParameter
import java.lang.reflect.Constructor

annotation class Assisted

interface AssistedBeanFactory1<T, P1> {
    fun create(p1: P1): T
}

interface AssistedBeanFactory2<T, P1, P2> {
    fun create(p1: P1, p2: P2): T
}

@Configuration
open class AssistedBeanFactoryConfiguration(private val applicationContext: ApplicationContext) {
    @Scope("prototype")
    @Bean
    open fun <T, P1> assistedBeanFactory1(
        @Suppress("SpringJavaInjectionPointsAutowiringInspection")
        dependencyDescriptor: DependencyDescriptor
    ): AssistedBeanFactory1<T, P1> {
        return getAssistedBeanFactory<T, P1, Nothing>(applicationContext, dependencyDescriptor)
    }

    @Scope("prototype")
    @Bean
    open fun <T, P1, P2> assistedBeanFactory2(
        @Suppress("SpringJavaInjectionPointsAutowiringInspection")
        dependencyDescriptor: DependencyDescriptor
    ): AssistedBeanFactory2<T, P1, P2> {
        return getAssistedBeanFactory(applicationContext, dependencyDescriptor)
    }
}

private fun <T, P1, P2> getAssistedBeanFactory(
    applicationContext: ApplicationContext,
    dependencyDescriptor: DependencyDescriptor
): AssistedBeanFactoryImpl<T, P1, P2> {
    val beanFactoryType = dependencyDescriptor.resolvableType
    val requestedBeanType = beanFactoryType.getGeneric(0)

    val beanName = applicationContext.getBeanNamesForType(requestedBeanType, true, false).single()
    val beanType = applicationContext.getType(beanName)

    val ctors = beanType.declaredConstructors
    val ctor = ctors.singleOrNull() ?: ctors.single { it.isAnnotationPresent(Autowired::class.java) }

    val assistedTypes = beanFactoryType.generics.drop(1)
    var assistedArgsIndex = 0
    val parameters = mutableListOf<Parameter>()

    for ((idx, param) in ctor.parameters.withIndex()) {
        if (param.isAnnotationPresent(Assisted::class.java)) {
            if (assistedTypes[assistedArgsIndex].type != param.parameterizedType) {
                throw IllegalStateException("Wrong assisted parameter type: $beanFactoryType, required: ${ctor.genericParameterTypes.toList()}")
            }

            parameters += AssistedParameter(assistedArgsIndex)
            assistedArgsIndex += 1
        } else {
            parameters += AutomaticParameter(ctor, idx)
        }
    }

    if (assistedArgsIndex != assistedTypes.size) {
        throw IllegalStateException("Wrong assisted parameters count: $beanFactoryType, required: ${ctor.genericParameterTypes.toList()}")
    }

    return AssistedBeanFactoryImpl(applicationContext.autowireCapableBeanFactory, beanName, parameters)
}

private class AssistedBeanFactoryImpl<T, P1, P2>(
    private val beanFactory: AutowireCapableBeanFactory,
    private val beanName: String,
    private val parameters: List<Parameter>
) : AssistedBeanFactory1<T, P1>, AssistedBeanFactory2<T, P1, P2> {

    override fun create(p1: P1): T {
        return realCreate(p1)
    }

    override fun create(p1: P1, p2: P2): T {
        return realCreate(p1, p2)
    }

    private fun realCreate(vararg assistedArgs: Any?): T {
        val argsToCall = parameters.map { it.getDependencyValue(beanFactory, assistedArgs) }
        @Suppress("UNCHECKED_CAST")
        return beanFactory.getBean(beanName, *argsToCall.toTypedArray()) as T
    }
}

private sealed interface Parameter {
    fun getDependencyValue(beanFactory: AutowireCapableBeanFactory, assistedArgs: Array<out Any?>): Any?
}

private class AssistedParameter(private val idx: Int) : Parameter {
    override fun getDependencyValue(beanFactory: AutowireCapableBeanFactory, assistedArgs: Array<out Any?>): Any? {
        return assistedArgs[idx]
    }
}

private class AutomaticParameter(ctor: Constructor<*>, idx: Int) : Parameter {
    private val dependencyDescriptor = DependencyDescriptor(MethodParameter(ctor, idx), true)

    override fun getDependencyValue(beanFactory: AutowireCapableBeanFactory, assistedArgs: Array<out Any?>): Any? {
        return beanFactory.resolveDependency(dependencyDescriptor, null)
    }
}
