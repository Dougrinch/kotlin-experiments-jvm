package com.dou.snapshot

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.time.LocalTime
import kotlin.random.Random.Default.nextInt
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
fun main() {
    val storage = Storage()
    val snapshotEngine = SnapshotEngine(storage, valueProcessingTime = 1.milliseconds)
    val snapshotAsserter = SequentialSnapshotAsserter(storage, emptyMap())

    runBlocking {
        launch {
            while (true) {
                delay(1.milliseconds)
                storage.write(id = nextInt(1000), value = nextInt(100))
            }
        }

        launch {
            while (true) {
                val snapshotInfo = snapshotEngine.take(ids = (0 until 1000).toSet())
                println("${LocalTime.now()} : $snapshotInfo")
                snapshotAsserter.assertPartial(snapshotInfo.snapshot)
            }
        }
    }
}
