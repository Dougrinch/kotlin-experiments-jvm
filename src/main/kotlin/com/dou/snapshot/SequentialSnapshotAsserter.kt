package com.dou.snapshot

class SequentialSnapshotAsserter(storage: Storage, initial: Snapshot) {
    private val snapshots = SequentialSnapshotSubscription(initial).also { storage.subscribe(it) }

    fun assertPartial(snapshot: Snapshot) {
        val checkedSnapshots = ArrayList<Map<Id, Value>>()

        for (pastSnapshot in snapshots) {
            checkedSnapshots.add(pastSnapshot)
            if (snapshot.all { it.value == pastSnapshot[it.key] }) {
                return
            }
        }

        for (checkedSnapshot in checkedSnapshots) {
            println(checkedSnapshot)
        }

        throw AssertionError()
    }
}
