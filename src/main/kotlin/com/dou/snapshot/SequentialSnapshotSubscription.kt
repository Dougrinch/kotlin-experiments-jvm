package com.dou.snapshot

import java.util.concurrent.ConcurrentLinkedDeque

private data class Update(val id: Id, val value: Value)

class SequentialSnapshotSubscription(initial: Snapshot) : Subscription {
    private var currentSnapshot = initial
    private val updates = ConcurrentLinkedDeque<Update>()

    override fun onUpdate(id: Id, value: Value) {
        updates.addLast(Update(id, value))
    }

    operator fun iterator(): Iterator<Snapshot> {
        return iterator {
            while (true) {
                val update = updates.pollFirst() ?: break
                currentSnapshot = currentSnapshot + (update.id to update.value)
                yield(currentSnapshot)
            }
        }
    }
}
