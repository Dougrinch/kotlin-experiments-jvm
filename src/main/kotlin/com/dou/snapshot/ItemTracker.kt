package com.dou.snapshot

import java.util.concurrent.ConcurrentLinkedDeque

class ItemTracker : Subscription {
    private val updates = ConcurrentLinkedDeque<Id>()

    override fun onUpdate(id: Id, value: Value) {
        updates.addLast(id)
    }

    fun getUpdated(): Set<Id> {
        val result = HashSet<Id>()
        while (true) {
            val id = updates.pollFirst() ?: break
            result.add(id)
        }
        return result
    }
}
