package com.dou.snapshot

import kotlinx.coroutines.delay
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

typealias Snapshot = Map<Id, Value>

data class SnapshotInfo(
    val iteration: Int,
    val loadedValues: Int,
    val iterations: List<LoadIteration>,
    val snapshot: Snapshot
)

data class LoadIteration(val update: Set<Id>, val data: Map<Id, Value>)

@OptIn(ExperimentalTime::class)
class SnapshotEngine(private val storage: Storage, private val valueProcessingTime: Duration) {
    suspend fun take(ids: Set<Id>): SnapshotInfo {
        val itemTracker = ItemTracker()
        storage.subscribe(itemTracker).use {
            val iterations = ArrayList<LoadIteration>()

            iterations.add(load(ids))

            var prevUpdated: Set<Id>? = null

            while (true) {
                val updated = itemTracker.getUpdated().filterTo(HashSet()) { it in ids }
                if (updated.isEmpty() || (prevUpdated != null && updated.all { it !in prevUpdated!! })) {
                    return buildSnapshot(iterations)
                }

                iterations.add(load(updated))

                prevUpdated = updated
            }
        }
    }

    private suspend fun load(ids: Set<Id>): LoadIteration {
        val result = HashMap<Id, Value>()
        for (id in ids) {
            storage.read(id)?.also { result[id] = it }
            delay(valueProcessingTime)
        }
        return LoadIteration(ids, result)
    }

    private fun buildSnapshot(iterations: List<LoadIteration>): SnapshotInfo {
        val result = HashMap<Id, Value>()
        for (iteration in iterations) {
            result.putAll(iteration.data)
        }
        return SnapshotInfo(iterations.size, iterations.map { it.update.size }.sum(), iterations, result)
    }
}
