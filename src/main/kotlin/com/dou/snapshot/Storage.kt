package com.dou.snapshot

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList

typealias Id = Int
typealias Value = Int

interface Subscription {
    fun onUpdate(id: Id, value: Value)
}

class Storage {
    private val trackers = CopyOnWriteArrayList<Subscription>()
    private val data = ConcurrentHashMap<Id, Value>()

    fun subscribe(subscription: Subscription): AutoCloseable {
        trackers.add(subscription)
        return AutoCloseable { trackers.remove(subscription) }
    }

    fun write(id: Id, value: Value) {
        data[id] = value
        trackers.forEach { it.onUpdate(id, value) }
    }

    fun read(id: Id): Value? {
        return data[id]
    }
}
