package com.dou.proxy

import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Proxy
import kotlin.coroutines.Continuation
import kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED

interface MethodCall {
    val args: List<Any?>
    suspend operator fun invoke(args: List<Any?>): Any?
}

private val methodCallArgsMethod = MethodCall::class.java.methods.single { it.name == "getArgs" }
private val methodCallInvokeMethod = MethodCall::class.java.methods.single { it.name == "invoke" }

fun aroundSuspendFun(
    srcMethod: (Array<Any?>) -> Any?,
    srcArgs: Array<Any?>,
    block: suspend (MethodCall) -> Any?
): Any? {
    @Suppress("UNCHECKED_CAST")
    val completion = srcArgs.last() as Continuation<Any?>
    val args = srcArgs.take(srcArgs.size - 1)

    val realMethod = realMethodCall(block::class.java.classLoader, srcMethod, args)

    return SuspendFunction.invokeMethod.invoke(object : SuspendFunction {
        override suspend fun invoke() = block.invoke(realMethod)
    }, completion)
}

private fun realMethodCall(
    classLoader: ClassLoader, srcMethod: (Array<Any?>) -> Any?, args: List<Any?>
): MethodCall {
    return Proxy.newProxyInstance(
        classLoader, arrayOf(MethodCall::class.java)
    ) { _, method, argsAndContinuation ->
        when (method) {
            methodCallArgsMethod -> args
            methodCallInvokeMethod -> {
                val newArgs = (argsAndContinuation[0] as List<Any?>).toTypedArray()
                val continuation = argsAndContinuation[1] as Continuation<*>

                try {
                    srcMethod(arrayOf(*newArgs, continuation))
                } catch (e: InvocationTargetException) {
                    continuation.resumeWith(Result.failure(e.targetException))
                    COROUTINE_SUSPENDED
                } catch (e: Throwable) {
                    continuation.resumeWith(Result.failure(e))
                    COROUTINE_SUSPENDED
                }
            }
            else -> throw UnsupportedOperationException("$method")
        }
    } as MethodCall
}
