package com.dou.proxy

import akka.actor.ActorRef
import akka.actor.ActorSystem

interface HelloService : ActorService {
    suspend fun getSelfRef(): ActorRef
    suspend fun sayHello(name: String)
    suspend fun sayHelloTo(name: String, target: ActorRef?)
    suspend fun getHelloText(name: String): String
}

class HelloServiceImpl : HelloService, ActorServiceImpl {
    override suspend fun getSelfRef(): ActorRef {
        return self
    }

    override suspend fun sayHello(name: String) {
        println(getHelloText(name))
    }

    override suspend fun sayHelloTo(name: String, target: ActorRef?) {
        target?.tell(getHelloText(name), sender)
    }

    override suspend fun getHelloText(name: String): String {
        return "Hello, $name!"
    }
}

suspend fun main() {
    val system = ActorSystem.create()
    try {
        val ref = system.createServiceImpl(HelloServiceImpl())
        val service = ref.asService<HelloService>()
        println(ref)
        println(service.getSelfRef())
        println(service.getHelloText("ivan"))
        service.sayHello("petr")
        service.sayHelloTo("hall", ActorRef.noSender())
    } finally {
        system.terminate()
    }
}
