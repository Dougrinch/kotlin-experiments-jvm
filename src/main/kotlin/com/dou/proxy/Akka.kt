package com.dou.proxy

import akka.actor.*
import akka.pattern.Patterns
import kotlinx.coroutines.future.await
import java.lang.reflect.Method
import java.time.Duration

interface ActorService
interface ActorServiceImpl

private val currentActor = ThreadLocal<Actor>()

@Suppress("unused")
val ActorServiceImpl.self: ActorRef
    get() = currentActor.get().self()

@Suppress("unused")
val ActorServiceImpl.sender: ActorRef
    get() = currentActor.get().sender()

@Suppress("unused")
val ActorServiceImpl.context: ActorContext
    get() = currentActor.get().context()

fun <S : ActorServiceImpl> ActorSystem.createServiceImpl(impl: S): ActorRef {
    return actorOf(Props.create(ServiceActor::class.java) { ServiceActor(impl) })
}

inline fun <reified S : ActorService> ActorRef.asService(): S {
    return asService(S::class.java)
}

fun <S : ActorService> ActorRef.asService(clazz: Class<S>): S {
    return proxy(clazz) { _, method, args ->
        this.ask(ServiceActorMsg(method, args))
    }
}

suspend fun ActorRef.ask(msg: Any): Any {
    return Patterns.ask(this, msg, Duration.ofDays(1)).await()
}

private class ServiceActor(private val service: ActorServiceImpl) : AbstractActor() {
    override fun createReceive() = receive {
        matchSuspend(handleAskSuspend(rawProxy<F<ServiceActorMsg, Any>> { _, _, rawArgs, cont ->
            val (method, args) = rawArgs[0] as ServiceActorMsg
            val realArgs = (args + cont).toTypedArray()
            currentActor.set(this@ServiceActor)
            try {
                method.invoke(service, *realArgs)
            } finally {
                currentActor.remove()
            }
        }))
    }
}

private data class ServiceActorMsg(val method: Method, val args: List<Any?>)
