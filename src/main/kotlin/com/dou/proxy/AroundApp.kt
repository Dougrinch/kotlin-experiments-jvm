package com.dou.proxy

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.lang.reflect.Proxy

interface Test {
    suspend fun a(v: Int): Int
    suspend fun b(v: Int): Int
}

class TestImpl : Test {
    override suspend fun a(v: Int): Int {
        println("a: $v")
        return v
    }

    override suspend fun b(v: Int): Int {
        println("b1: $v")
        delay(100)
//        throw IllegalStateException()
        println("b2: $v")
        return v
    }
}

fun wrap(src: Test): Test {
    val clazz = Test::class.java
    return Proxy.newProxyInstance(clazz.classLoader, arrayOf(clazz)) { _, srcMethod, srcArgs ->
        aroundSuspendFun({ srcMethod(src, *it) }, srcArgs) {
            println("aspect: ${srcMethod.name}(${it.args}) = ?")
            try {
                val r = it(it.args)
                println("aspect: ${srcMethod.name}(${it.args}) = $r")
                r
            } catch (e: Throwable) {
                println(e)
                666
            }
        }
    } as Test
}

fun main() {
    val test = wrap(TestImpl())

    runBlocking {
        println(test.a(1))
        println("---")
        println(test.b(2))
    }
}
