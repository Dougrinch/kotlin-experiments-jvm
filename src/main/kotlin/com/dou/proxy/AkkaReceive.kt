package com.dou.proxy

import akka.actor.*
import akka.japi.pf.ReceiveBuilder
import kotlinx.coroutines.*
import scala.compat.java8.FutureConverters
import scala.concurrent.duration.FiniteDuration
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume

sealed class SuspendableReceiveBuilder : ReceiveBuilder() {
    internal class WithData(
        val context: ActorContext,
        var usesSuspend: Boolean = false
    ) : SuspendableReceiveBuilder()
}

internal data class RunInActorContext(val action: () -> Unit) : NotInfluenceReceiveTimeout

fun Actor.receive(init: SuspendableReceiveBuilder.() -> Unit): AbstractActor.Receive {
    val builder = SuspendableReceiveBuilder.WithData(context())
    return receive0(builder, init)
}

private fun receive0(builder: SuspendableReceiveBuilder, init: SuspendableReceiveBuilder.() -> Unit): AbstractActor.Receive {
    builder as SuspendableReceiveBuilder.WithData
    builder.init()
    return if (builder.usesSuspend) {
        suspendBehaviour.orElse(builder.build())
    } else {
        builder.build()
    }
}

val suspendBehaviour: AbstractActor.Receive = ReceiveBuilder().addSuspendBehaviour().build()

fun ReceiveBuilder.addSuspendBehaviour(): ReceiveBuilder {
    match<RunInActorContext> { (action) -> action() }
    return this
}


inline fun <reified T> ReceiveBuilder.match(noinline handler: (T) -> Unit) {
    this.match(T::class.java, handler)
}

inline fun <reified T> SuspendableReceiveBuilder.matchSuspend(noinline handler: suspend (T) -> Unit) {
    this.match(T::class.java, matchSuspendHandler<T>(handler, "on${T::class.java.simpleName}"))
}

interface F<T, R> {
    suspend operator fun invoke(t: T): R
}

fun <T, R> Actor.handleAskSuspend(run: F<T, R>): suspend (T) -> Unit = { request: T ->
    try {
        sender().tell(run(request), self())
    } catch (ex: Exception) {
        ex.printStackTrace()
        sender().tell(ex, self())
    }
}

fun <T> SuspendableReceiveBuilder.matchSuspendHandler(handler: suspend (T) -> Unit, coroutineName: String? = null): (T) -> Unit {
    this as SuspendableReceiveBuilder.WithData
    usesSuspend = true
    return { message ->
        val context = coroutineName?.let { actorCoroutine(context) + CoroutineName(coroutineName) } ?: actorCoroutine(context)
        @OptIn(DelicateCoroutinesApi::class)
        GlobalScope.launch(context, CoroutineStart.UNDISPATCHED) { handler(message) }
    }
}

private fun actorCoroutine(context: ActorContext) =
    actorCoroutine(context.self(), context.system().scheduler(), context.sender())

fun actorCoroutine(self: ActorRef, scheduler: Scheduler, sender: ActorRef?) =
    ActorCoroutineDispatcher(self, scheduler, sender) +
            ActorCoroutineExceptionHandler(self, sender)

@OptIn(InternalCoroutinesApi::class)
private class ActorCoroutineDispatcher(val self: ActorRef, val scheduler: Scheduler, val originalSender: ActorRef?): CoroutineDispatcher(), Delay {
    companion object {
        private val sameThreadExecutionContext = FutureConverters.fromExecutor { it.run() }
    }

    override fun dispatch(context: CoroutineContext, block: Runnable) {
        self.tell(RunInActorContext { block.run() }, originalSender)
    }

    override fun scheduleResumeAfterDelay(timeMillis: Long, continuation: CancellableContinuation<Unit>) {
        invokeOnTimeout(timeMillis, Runnable { continuation.resume(Unit) }, continuation.context)
    }

    override fun invokeOnTimeout(timeMillis: Long, block: Runnable, context: CoroutineContext): DisposableHandle {
        val job = scheduler.scheduleOnce(
            timeMillis.millis, self, RunInActorContext { block.run() }, sameThreadExecutionContext, originalSender
        )
        return object: DisposableHandle {
            override fun dispose() {
                job.cancel()
            }
        }
    }
}

private class ActorCoroutineExceptionHandler(val self: ActorRef, val originalSender: ActorRef?): CoroutineExceptionHandler {
    override val key: CoroutineContext.Key<*> = CoroutineExceptionHandler.Key

    override fun handleException(context: CoroutineContext, exception: Throwable) {
        self.tell(RunInActorContext { throw exception }, originalSender)
    }
}

val Long.millis: FiniteDuration; get() = FiniteDuration.create(this, TimeUnit.MILLISECONDS)
