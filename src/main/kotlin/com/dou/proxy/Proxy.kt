package com.dou.proxy

import java.lang.reflect.Method
import java.lang.reflect.Proxy
import kotlin.coroutines.Continuation

//fun interface SuspendableInvocationHandler {
//    suspend fun invoke(proxy: Any, method: Method, args: List<Any?>?): Any?
//}

typealias SuspendableInvocationHandler = suspend (Any, Method, List<Any?>) -> Any?

typealias RawInvocationHandler = (Any, Method, List<Any?>, Continuation<Any?>) -> Any?

@Suppress("UNCHECKED_CAST")
inline fun <reified S : Any> proxy(noinline handler: SuspendableInvocationHandler): S {
    return proxy(S::class.java, handler)
}

@Suppress("UNCHECKED_CAST")
inline fun <reified S : Any> rawProxy(noinline handler: RawInvocationHandler): S {
    return rawProxy(S::class.java, handler)
}

fun <S : Any> proxy(clazz: Class<S>, handler: SuspendableInvocationHandler): S {
    @Suppress("UNCHECKED_CAST")
    return Proxy.newProxyInstance(clazz.classLoader, arrayOf(clazz)) { proxy, method, arguments ->
        val continuation = arguments.last() as Continuation<*>
        val argumentsWithoutContinuation = arguments.take(arguments.size - 1)
        SuspendFunction.invokeMethod.invoke(object : SuspendFunction {
            override suspend fun invoke() = handler.invoke(proxy, method, argumentsWithoutContinuation)
        }, continuation)
    } as S
}

fun <S : Any> rawProxy(clazz: Class<S>, handler: RawInvocationHandler): S {
    @Suppress("UNCHECKED_CAST")
    return Proxy.newProxyInstance(clazz.classLoader, arrayOf(clazz)) { proxy, method, arguments ->
        val continuation = arguments.last() as Continuation<Any?>
        val argumentsWithoutContinuation = arguments.take(arguments.size - 1)
        RawFunction.invokeMethod.invoke(object : RawFunction {
            override fun invoke() = handler.invoke(proxy, method, argumentsWithoutContinuation, continuation)
        })
    } as S
}

interface SuspendFunction {
    suspend fun invoke(): Any?

    companion object {
        val invokeMethod: Method = SuspendFunction::class.java.methods.single { it.name == "invoke" }
    }
}

private interface RawFunction {
    fun invoke(): Any?

    companion object {
        val invokeMethod: Method = RawFunction::class.java.methods.single { it.name == "invoke" }
    }
}