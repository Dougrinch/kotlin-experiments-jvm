package com.dou.proxy

suspend fun work() {}

suspend fun call(f: suspend () -> String): String {
    return f()
}