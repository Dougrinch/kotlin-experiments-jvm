package com.dou.typed

@DslMarker
annotation class WhenBuilderMarker

interface WhenBuilderContext

@WhenBuilderMarker
interface WhenBuilderContext1<R1, M1, C, V> : WhenBuilderContext

private val WhenBuilderContext1<*, *, *, *>.context: WhenBuilderContextImpl
    get() = this as WhenBuilderContextImpl

@WhenBuilderMarker
interface WhenBuilderContext2<R1, M1, R2, M2, M, C, V> : WhenBuilderContext

private val WhenBuilderContext2<*, *, *, *, *, *, *>.context: WhenBuilderContextImpl
    get() = this as WhenBuilderContextImpl

@WhenBuilderMarker
interface WhenBuilderContext3<R1, M1, R2, M2, R3, M3, M, C, V> : WhenBuilderContext

private val WhenBuilderContext3<*, *, *, *, *, *, *, *, *>.context: WhenBuilderContextImpl
    get() = this as WhenBuilderContextImpl

@Suppress("UNCHECKED_CAST")
fun <R1 : Any, M1 : Any, C : Any, V : Any> buildWhen(
    keyMatcher: (R1, M1) -> Boolean,
    block: WhenBuilderContext1<R1, M1, C, V>.() -> Unit
): TypedWhen<R1, M1, C, V> {
    val context = WhenBuilderContextImpl(0, listOf(keyMatcher) as List<(Any, Any) -> Boolean>)
    (context as WhenBuilderContext1<R1, M1, C, V>).block()
    return TypedWhenImpl(
        context.children as List<MatchTree<V>>,
        { r1 -> listOf(r1) },
        { (r1) -> r1 as R1 },
        { (m1) -> m1 as M1 }
    ) { f, (p1, p2) ->
        (f as suspend Any.(Any) -> V?).invoke(p1, p2)
    }
}

@Suppress("UNCHECKED_CAST")
fun <R1 : Any, M1 : Any, R2 : Any, M2 : Any, C : Any, V : Any> buildWhen(
    key1Matcher: (R1, M1) -> Boolean,
    key2Matcher: (R2, M2) -> Boolean,
    block: WhenBuilderContext2<R1, M1, R2, M2, Pair<M1, M2>, C, V>.() -> Unit
): TypedWhen<Pair<R1, R2>, Pair<M1, M2>, C, V> {
    val context = WhenBuilderContextImpl(0, listOf(key1Matcher, key2Matcher) as List<(Any, Any) -> Boolean>)
    (context as WhenBuilderContext2<R1, M1, R2, M2, Pair<M1, M2>, C, V>).block()
    return TypedWhenImpl(
        context.children as List<MatchTree<V>>,
        { (r1, r2) -> listOf(r1, r2) },
        { (r1, r2) -> (Pair(r1, r2)) as Pair<R1, R2> },
        { (m1, m2) -> (Pair(m1, m2)) as Pair<M1, M2> }
    ) { f, (p1, p2, p3) ->
        (f as suspend Any.(Any, Any) -> V?).invoke(p1, p2, p3)
    }
}

@Suppress("UNCHECKED_CAST")
fun <R1 : Any, M1 : Any, R2 : Any, M2 : Any, R3 : Any, M3 : Any, C : Any, V : Any> buildWhen(
    key1Matcher: (R1, M1) -> Boolean,
    key2Matcher: (R2, M2) -> Boolean,
    key3Matcher: (R3, M3) -> Boolean,
    block: WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Triple<M1, M2, M3>, C, V>.() -> Unit
): TypedWhen<Triple<R1, R2, R3>, Triple<M1, M2, M3>, C, V> {
    val context = WhenBuilderContextImpl(0, listOf(key1Matcher, key2Matcher, key3Matcher) as List<(Any, Any) -> Boolean>)
    (context as WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Triple<M1, M2, M3>, C, V>).block()
    return TypedWhenImpl(
        context.children as List<MatchTree<V>>,
        { (r1, r2, r3) -> listOf(r1, r2, r3) },
        { (r1, r2, r3) -> (Triple(r1, r2, r3)) as Triple<R1, R2, R3> },
        { (m1, m2, m3) -> Triple(m1, m2, m3) as Triple<M1, M2, M3> }
    ) { f, (p1, p2, p3, p4) ->
        (f as suspend Any.(Any, Any, Any) -> V?).invoke(p1, p2, p3, p4)
    }
}


fun <R1, M1, C, V> WhenBuilderContext1<R1, M1, C, V>.match(
    key: M1, block: suspend C.(R1) -> V?
): Unit = context.collectLeaf(key, block)

fun <R1, M1, C, V> WhenBuilderContext1<R1, M1, C, V>.match(
    vararg keys: M1, block: suspend C.(R1) -> V?
): Unit = context.collectLeaf(keys, block)

fun <R1, M1, C, V> WhenBuilderContext1<R1, M1, C, V>.match(
    keys: Set<M1>, block: suspend C.(Set<R1>) -> V?
): Unit = context.collectLeaf(keys, block)


fun <R1, M1, R2, M2, C, V> WhenBuilderContext2<R1, M1, R2, M2, Pair<M1, M2>, C, V>.match(
    key: M1, block: WhenBuilderContext2<R1, M1, R2, M2, M2, C, V>.() -> Unit
): Unit = context.collectNode(key, block)

fun <R1, M1, R2, M2, C, V> WhenBuilderContext2<R1, M1, R2, M2, Pair<M1, M2>, C, V>.match(
    vararg keys: M1, block: WhenBuilderContext2<R1, M1, R2, M2, M2, C, V>.() -> Unit
): Unit = context.collectNode(keys, block)

fun <R1, M1, R2, M2, C, V> WhenBuilderContext2<R1, M1, R2, M2, Pair<M1, M2>, C, V>.match(
    keys: Set<M1>, block: WhenBuilderContext2<Set<R1>, M1, R2, M2, M2, C, V>.() -> Unit
): Unit = context.collectNode(keys, block)

fun <R1, M1, R2, M2, C, V> WhenBuilderContext2<R1, M1, R2, M2, M2, C, V>.match(
    key: M2, block: suspend C.(R1, R2) -> V?
): Unit = context.collectLeaf(key, block)

fun <R1, M1, R2, M2, C, V> WhenBuilderContext2<R1, M1, R2, M2, M2, C, V>.match(
    vararg keys: M2, block: suspend C.(R1, R2) -> V?
): Unit = context.collectLeaf(keys, block)

fun <R1, M1, R2, M2, C, V> WhenBuilderContext2<R1, M1, R2, M2, M2, C, V>.match(
    keys: Set<M2>, block: suspend C.(R1, Set<R2>) -> V?
): Unit = context.collectLeaf(keys, block)


@JvmName("match31")
fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Triple<M1, M2, M3>, C, V>.match(
    key: M1, block: WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Pair<M2, M3>, C, V>.() -> Unit
): Unit = context.collectNode(key, block)

@JvmName("match31")
fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Triple<M1, M2, M3>, C, V>.match(
    vararg keys: M1, block: WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Pair<M2, M3>, C, V>.() -> Unit
): Unit = context.collectNode(keys, block)

@JvmName("match31")
fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Triple<M1, M2, M3>, C, V>.match(
    keys: Set<M1>, block: WhenBuilderContext3<Set<R1>, M1, R2, M2, R3, M3, Pair<M2, M3>, C, V>.() -> Unit
): Unit = context.collectNode(keys, block)

@JvmName("match32")
fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Pair<M2, M3>, C, V>.match(
    key: M2, block: WhenBuilderContext3<R1, M1, R2, M2, R3, M3, M3, C, V>.() -> Unit
): Unit = context.collectNode(key, block)

@JvmName("match32")
fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Pair<M2, M3>, C, V>.match(
    vararg keys: M2, block: WhenBuilderContext3<R1, M1, R2, M2, R3, M3, M3, C, V>.() -> Unit
): Unit = context.collectNode(keys, block)

@JvmName("match32")
fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, Pair<M2, M3>, C, V>.match(
    keys: Set<M2>, block: WhenBuilderContext3<R1, M1, Set<R2>, M2, R3, M3, M3, C, V>.() -> Unit
): Unit = context.collectNode(keys, block)

fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, M3, C, V>.match(
    key: M3, block: suspend C.(R1, R2, R3) -> V?
): Unit = context.collectLeaf(key, block)

fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, M3, C, V>.match(
    vararg keys: M3, block: suspend C.(R1, R2, R3) -> V?
): Unit = context.collectLeaf(keys, block)

fun <R1, M1, R2, M2, R3, M3, C, V> WhenBuilderContext3<R1, M1, R2, M2, R3, M3, M3, C, V>.match(
    keys: Set<M3>, block: suspend C.(R1, R2, Set<R3>) -> V?
): Unit = context.collectLeaf(keys, block)


private class WhenBuilderContextImpl(
    val keyDepth: Int,
    val keyMatchers: List<(Any, Any) -> Boolean>
) : WhenBuilderContext1<Any, Any, Any, Any>,
    WhenBuilderContext2<Any, Any, Any, Any, Any, Any, Any>,
    WhenBuilderContext3<Any, Any, Any, Any, Any, Any, Any, Any, Any> {

    val children: MutableList<MatchTree<*>> = ArrayList()

    fun collectLeaf(key: Any?, blockFunction: Function<*>) {
        collectLeaf(createMatch(key), blockFunction)
    }

    fun collectLeaf(keys: Array<out Any?>, blockFunction: Function<*>) {
        collectLeaf(createMatch(keys), blockFunction)
    }

    fun collectLeaf(keys: Set<Any?>, blockFunction: Function<*>) {
        collectLeaf(createMatch(keys), blockFunction)
    }

    fun <T : WhenBuilderContext> collectNode(key: Any?, blockFunction: T.() -> Unit) {
        collectNode(createMatch(key), blockFunction)
    }

    fun <T : WhenBuilderContext> collectNode(keys: Array<out Any?>, blockFunction: T.() -> Unit) {
        collectNode(createMatch(keys), blockFunction)
    }

    fun <T : WhenBuilderContext> collectNode(keys: Set<Any?>, blockFunction: T.() -> Unit) {
        collectNode(createMatch(keys), blockFunction)
    }

    private fun createMatch(key: Any?): Match {
        return Match(setOf(key as Any), false, keyDepth, keyMatchers[keyDepth])
    }

    private fun createMatch(keys: Array<out Any?>): Match {
        @Suppress("UNCHECKED_CAST")
        return Match(keys.toSet() as Set<Any>, false, keyDepth, keyMatchers[keyDepth])
    }

    private fun createMatch(keys: Set<Any?>): Match {
        @Suppress("UNCHECKED_CAST")
        return Match(keys as Set<Any>, true, keyDepth, keyMatchers[keyDepth])
    }

    private fun collectLeaf(match: Match, blockFunction: Function<*>) {
        children += MatchTreeLeaf(match, blockFunction)
    }

    private fun <T : WhenBuilderContext> collectNode(match: Match, blockFunction: T.() -> Unit) {
        val nextContext = WhenBuilderContextImpl(keyDepth + 1, keyMatchers)
        @Suppress("UNCHECKED_CAST")
        (nextContext as T).blockFunction()
        @Suppress("UNCHECKED_CAST")
        children += MatchTreeNode(match, nextContext.children as List<MatchTree<Any>>)
    }
}
