package com.dou.typed

interface TypedWhen<R : Any, M : Any, C : Any, V : Any> {
    val supportedKeys: Set<M>
    suspend fun run(keys: Set<R>, context: C): Pair<List<V>, Set<R>>

    suspend fun run(key: R, context: C): V? {
        val keys = setOf(key)
        val (result, _) = run(keys, context)
        return result.singleOrNull()
    }
}

class TypedWhenImpl<R : Any, M : Any, C : Any, V : Any>(
    private val roots: List<MatchTree<V>>,
    private val keyExtractor: (R) -> RequestKey,
    private val keyConstructor: (RequestKey) -> R,
    private val matchKeyConstructor: (MatchKey) -> M,
    private val functionCaller: suspend (Function<V?>, List<Any>) -> V?
) : TypedWhen<R, M, C, V> {
    override val supportedKeys: Set<M> = roots.flatMap { buildSupportedKeys(it, emptyList()) }.mapTo(HashSet(), matchKeyConstructor)

    private fun buildSupportedKeys(node: MatchTree<*>, parentKeys: List<Any>): List<MatchKey> {
        val keys = node.match.supportedKeys.map { parentKeys + it }
        return when (node) {
            is MatchTreeNode -> keys.flatMap { key -> node.children.flatMap { n -> buildSupportedKeys(n, key) } }
            is MatchTreeLeaf -> keys
        }
    }

    override suspend fun run(keys: Set<R>, context: C): Pair<List<V>, Set<R>> {
        val matchResult = match(keys.mapTo(HashSet(), keyExtractor), roots, emptyList())
        return callFunctions(matchResult, context)
    }

    private fun match(keys: Set<RequestKey>, nodes: List<MatchTree<V>>, parentMatches: List<Match>): FunctionMatchResult<V> {
        val resultMatched = HashMap<List<Match>, Pair<Set<RequestKey>, Function<V?>>>()
        val resultUnmatched = HashSet<RequestKey>()

        var unprocessedKeys = keys
        for (node in nodes) {
            if (unprocessedKeys.isEmpty()) break
            val (matched, unmatched) = match(unprocessedKeys, node, parentMatches)
            resultMatched += matched
            unprocessedKeys = unmatched
        }
        resultUnmatched += unprocessedKeys

        return FunctionMatchResult(resultMatched, resultUnmatched)
    }

    private fun match(keys: Set<RequestKey>, node: MatchTree<V>, parentMatches: List<Match>): FunctionMatchResult<V> {
        val resultMatched = HashMap<List<Match>, Pair<Set<RequestKey>, Function<V?>>>()
        val resultUnmatched = HashSet<RequestKey>()

        val (matchedKeys, unmatched) = node.match.match(keys)
        resultUnmatched += unmatched

        if (matchedKeys.isNotEmpty()) {
            val matches = parentMatches + node.match
            when (node) {
                is MatchTreeNode -> {
                    val (nestedMatched, nestedUnmatched) = match(matchedKeys, node.children, matches)
                    resultMatched += nestedMatched
                    resultUnmatched += nestedUnmatched
                }
                is MatchTreeLeaf -> {
                    resultMatched[matches] = Pair(matchedKeys, node.function)
                }
            }
        }

        return FunctionMatchResult(resultMatched, resultUnmatched)
    }

    private suspend fun callFunctions(matchResult: FunctionMatchResult<V>, context: C): Pair<List<V>, Set<R>> {
        val result = ArrayList<V>()
        val unmatched = HashSet<R>()

        unmatched += matchResult.unmatched.asSequence().map(keyConstructor)

        for ((matches, k) in matchResult.matched) {
            val (keys, function) = k

            var groupedKeys = listOf(keys as Collection<RequestKey>)
            for (match in matches) {
                if (!match.argumentsAsSet) {
                    groupedKeys = groupedKeys.flatMap { it.groupBy { it[match.keyDepth] }.values }
                }
            }

            val keysToCall = groupedKeys.map { originalKeys ->
                val keyToCall = matches.map { m ->
                    if (m.argumentsAsSet)
                        originalKeys.mapTo(HashSet()) { it[m.keyDepth] }
                    else
                        originalKeys.mapTo(HashSet()) { it[m.keyDepth] }.single()
                }
                Pair(keyToCall, originalKeys)
            }

            for ((keyToCall, originalKeys) in keysToCall) {
                val v = functionCaller(function, listOf(context) + keyToCall)
                if (v != null) {
                    result += v
                } else {
                    unmatched += originalKeys.asSequence().map(keyConstructor)
                }
            }
        }

        return Pair(result, unmatched)
    }
}

sealed class MatchTree<V : Any> {
    abstract val match: Match
}

class MatchTreeNode<V : Any>(override val match: Match, val children: List<MatchTree<V>>) : MatchTree<V>()
class MatchTreeLeaf<V : Any>(override val match: Match, val function: Function<V?>) : MatchTree<V>()

typealias RequestKey = List<Any>
typealias MatchKey = List<Any>

class Match(
    val supportedKeys: Set<Any>,
    val argumentsAsSet: Boolean,
    val keyDepth: Int,
    private val keyMatcher: (Any, Any) -> Boolean
) {
    fun match(keys: Set<RequestKey>): SimpleMatchResult {
        val (matched, unmatched) = keys.partitionToSets { key -> supportedKeys.any { keyMatcher(key[keyDepth], it) } }
        return SimpleMatchResult(matched, unmatched)
    }
}

data class SimpleMatchResult(val matched: Set<RequestKey>, val unmatched: Set<RequestKey>)
data class FunctionMatchResult<V : Any>(val matched: Map<List<Match>, Pair<Set<RequestKey>, Function<V?>>>, val unmatched: Set<RequestKey>)

inline fun <T> Iterable<T>.partitionToSets(predicate: (T) -> Boolean): Pair<Set<T>, Set<T>> {
    val first = HashSet<T>()
    val second = HashSet<T>()
    for (element in this) {
        if (predicate(element)) {
            first.add(element)
        } else {
            second.add(element)
        }
    }
    return Pair(first, second)
}
