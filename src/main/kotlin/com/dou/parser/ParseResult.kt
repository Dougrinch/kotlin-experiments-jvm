package com.dou.parser

sealed interface ParseResult<out A>

data class Success<A>(val consumed: Int, val attachedError: Failure? = null, val value: () -> A) : ParseResult<A>

data class Failure(
    val errors: List<Pair<ParseError, Context>>, val committed: Boolean
) : ParseResult<Nothing> {
    constructor(error: ParseError, context: Context, committed: Boolean) : this(listOf(error to context), committed)
}

operator fun <A> Success<A>.plus(f: Failure): Success<A> {
    return copy(attachedError = attachedError.plus(f))
}

operator fun Failure.plus(o: Failure?): Failure {
    return if (o != null) {
        Failure(errors + o.errors, committed || o.committed)
    } else {
        this
    }
}

@JvmName("plusN")
fun Failure?.plus(o: Failure?): Failure? {
    return this?.plus(o) ?: o
}
