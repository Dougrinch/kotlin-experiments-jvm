package com.dou.parser

data class Context(val name: String?, val parent: Context?, val location: Location) {
    fun addFrame(name: String?): Context {
        return if (name != null) {
            Context(name, this, location)
        } else {
            this
        }
    }

    fun advancedBy(n: Int): Context {
        return copy(location = location.advancedBy(n))
    }

    fun limitBy(n: Int): Context {
        return copy(location = location.limitBy(n))
    }
}

val Context.path: List<String>
    get() = generateSequence(this) { it.parent }.mapNotNull { it.name }
        .toList().asReversed()

val Context.pathString: String
    get() = path.joinToString(".") { name ->
        when {
            " or " in name -> "($name)"
            " - " in name -> "($name)"
            else -> name
        }
    }

val Context.shortPathString: String
    get() {
        val p = path
        return if (p.size <= 3) {
            p.joinToString(".")
        } else {
            "*." + p.takeLast(3).joinToString(".")
        }
    }
