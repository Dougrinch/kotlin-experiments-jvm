package com.dou.parser

fun Parser<*>.slice(): Parser<String> {
    return ParserImpl(name) { ctx ->
        when (val r = parse(ctx)) {
            is Success -> Success(r.consumed, r.attachedError) { ctx.location.substring(0, r.consumed) }
            is Failure -> r
        }
    }
}

infix fun <A> Parser<*>.parseAs(a: A): Parser<A> {
    return ParserImpl(name) { ctx ->
        when (val r = parse(ctx)) {
            is Success -> Success(r.consumed, r.attachedError) { a }
            is Failure -> r
        }
    }
}

infix fun <A, B> Parser<A>.map(f: (A) -> B): Parser<B> {
    return ParserImpl(name) { ctx ->
        when (val r = parse(ctx)) {
            is Success -> Success(r.consumed, r.attachedError) { f(r.value()) }
            is Failure -> r
        }
    }
}


infix fun <A> Parser<A>.or(other: Parser<A>): Parser<A> {
    return ParserImpl("$name or ${other.name}") { ctx ->
        when (val r1 = parse(ctx)) {
            is Success -> r1
            is Failure -> if (r1.committed) {
                r1
            } else {
                when (val r2 = other.parse(ctx)) {
                    is Success -> r2 + r1
                    is Failure -> r1 + r2
                }
            }
        }
    }
}

fun <A, B, C> Parser<A>.then(pb: (A) -> Parser<B>, f: (() -> A, () -> B) -> C): Parser<C> {
    return ParserImpl("$name - dynamic") { ctx ->
        when (val ar = parse(ctx)) {
            is Success -> when (val br = pb(ar.value()).parse(ctx.advancedBy(ar.consumed))) {
                is Success -> Success(ar.consumed + br.consumed, ar.attachedError.plus(br.attachedError)) {
                    f(ar.value, br.value)
                }
                is Failure -> br.copy(committed = ar.consumed != 0 || br.committed) + ar.attachedError
            }
            is Failure -> ar
        }
    }
}

fun <A, B, C> Parser<A>.then(pb: Parser<B>, f: (() -> A, () -> B) -> C): Parser<C> {
    return ParserImpl("$name - ${pb.name}") { ctx ->
        when (val ar = parse(ctx)) {
            is Success -> when (val br = pb.parse(ctx.advancedBy(ar.consumed))) {
                is Success -> Success(ar.consumed + br.consumed, ar.attachedError.plus(br.attachedError)) {
                    f(ar.value, br.value)
                }
                is Failure -> br.copy(committed = ar.consumed != 0 || br.committed) + ar.attachedError
            }
            is Failure -> ar
        }
    }
}

fun <A> attempt(p: Parser<A>): Parser<A> {
    return ParserImpl(p.name) { ctx ->
        when (val r = p.parse(ctx)) {
            is Success -> r
            is Failure -> r.copy(committed = false)
        }
    }
}

fun <A> Parser<A>.limitConsumed(n: Int): Parser<A> {
    return ParserImpl(name) { ctx ->
        parse(ctx.limitBy(n))
    }
}
