package com.dou.parser

val int: Parser<Int> by regex("[-+]?([1-9][0-9]*|0)") map { it.toInt() }
val long: Parser<Long> by regex("-?([1-9][0-9]*|0)") map { it.toLong() }
val digit: Parser<Int> by regex("[0-9]") map { it.toInt() }
val word: Parser<String> by regex("\\w+")
val ws: Parser<Unit> by regex("\\s+").ignoreValue()
val char: Parser<Char> by regex(".") map { it.single() }

val newLine: Parser<Unit> by const("\n")

fun char(a: Char): Parser<Char> = string(a.toString()).map { it[0] }
fun char(vararg chars: Char): Parser<Char> = chars.map { char(it) }.reduce { p1, p2 -> p1 or p2 }
fun char(chars: CharRange): Parser<Char> = regex("[${chars.first}-${chars.last}]") map { it.single() }

infix fun <A> Parser<A>.separatedBy(separator: String): Parser<List<A>> = many(separator, min = 1)
infix fun <A> Parser<A>.separatedBy(separator: Parser<Unit>): Parser<List<A>> = many(separator, min = 1)
fun <A> Parser<A>.separatedByLines(lines: Int = 1): Parser<List<A>> =
    many(newLine.many(exact = lines).ignoreValue(), min = 1)

fun <A : Any> Parser<A>.optional(): Parser<A?> = many(min = 0, max = 1) map { it.firstOrNull() }
