package com.dou.parser

infix fun <A, B, C> Parser<Pair<A, B>>.map(f: (A, B) -> C): Parser<C> = map { (a, b) -> f(a, b) }

infix fun <A, B, C, D> Parser<Pair<Pair<A, B>, C>>.map(f: (A, B, C) -> D): Parser<D> =
    map { (ab, c) -> f(ab.first, ab.second, c) }

infix fun <A, B, C, D, E> Parser<Pair<Pair<Pair<A, B>, C>, D>>.map(f: (A, B, C, D) -> E): Parser<E> =
    map { (abc, d) -> f(abc.first.first, abc.first.second, abc.second, d) }

infix fun <A, B, C, D, E, F> Parser<Pair<Pair<Pair<Pair<A, B>, C>, D>, E>>.map(f: (A, B, C, D, E) -> F): Parser<F> =
    map { (abcd, e) -> f(abcd.first.first.first, abcd.first.first.second, abcd.first.second, abcd.second, e) }

infix fun <A, B, C, D, E, F, G> Parser<Pair<Pair<Pair<Pair<Pair<A, B>, C>, D>, E>, F>>.map(f: (A, B, C, D, E, F) -> G): Parser<G> =
    map { (abcde, f) ->
        f(
            abcde.first.first.first.first,
            abcde.first.first.first.second,
            abcde.first.first.second,
            abcde.first.second,
            abcde.second,
            f
        )
    }

infix fun <A, B, C, D, E, F, G, H> Parser<Pair<Pair<Pair<Pair<Pair<Pair<A, B>, C>, D>, E>, F>, G>>.map(f: (A, B, C, D, E, F, G) -> H): Parser<H> =
    map { (abcdef, g) ->
        f(
            abcdef.first.first.first.first.first,
            abcdef.first.first.first.first.second,
            abcdef.first.first.first.second,
            abcdef.first.first.second,
            abcdef.first.second,
            abcdef.second,
            g
        )
    }


@JvmName("minusAB")
operator fun <A, B> Parser<A>.minus(pb: Parser<B>): Parser<Pair<A, B>> = then(pb) { a, b -> Pair(a(), b()) }

@JvmName("minusAB")
operator fun <A, B> Parser<A>.minus(pb: (A) -> Parser<B>): Parser<Pair<A, B>> = then(pb) { a, b -> Pair(a(), b()) }

@JvmName("minusAU")
operator fun <A> Parser<A>.minus(pb: Parser<Unit>): Parser<A> = then(pb) { a, _ -> a() }

@JvmName("minusUB")
operator fun <B> Parser<Unit>.minus(pb: Parser<B>): Parser<B> = then(pb) { _, b -> b() }

operator fun <B> String.minus(pb: Parser<B>): Parser<B> = string(this).ignoreValue() - pb
operator fun <A> Parser<A>.minus(pb: String): Parser<A> = minus(string(pb).ignoreValue())


fun <A> oneOf(vararg ps: Parser<A>): Parser<A> = ps.reduce { p1, p2 -> p1 or p2 }
fun oneOf(vararg ps: String): Parser<String> = ps.map { string(it) }.reduce { p1, p2 -> p1 or p2 }

infix fun <A> String.parseAs(a: A): Parser<A> = string(this).parseAs(a)
fun const(s: String): Parser<Unit> = string(s).ignoreValue()
fun const(vararg ss: String): Parser<Unit> = ss.map { string(it) }.reduce { p1, p2 -> p1 or p2 }.ignoreValue()
fun Parser<*>.ignoreValue(): Parser<Unit> = this parseAs Unit
