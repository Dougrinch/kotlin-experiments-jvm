package com.dou.parser

import kotlin.reflect.KProperty

sealed interface Parser<out A> {
    val name: String?
    fun parse(ctx: Context): ParseResult<A>

    operator fun getValue(thisRef: Any?, property: KProperty<*>): Parser<A>
}

class ParserImpl<out A> private constructor(
    override val name: String?,
    private val parse: () -> (Context) -> ParseResult<A>
) : Parser<A> {
    companion object {
        operator fun <A> invoke(name: String?, parse: (Context) -> ParseResult<A>): ParserImpl<A> {
            return ParserImpl(name) { parse }
        }
    }

    override fun parse(ctx: Context): ParseResult<A> {
        return parse().invoke(ctx.addFrame(name))
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): Parser<A> {
        return ParserImpl(property.name) { ::parse }
    }
}


fun <A> parser(name: String? = null, f: () -> Parser<A>): Parser<A> {
    return ParserImpl(name) { ctx -> f().parse(ctx) }
}
