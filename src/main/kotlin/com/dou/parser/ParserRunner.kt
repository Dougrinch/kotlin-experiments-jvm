package com.dou.parser

fun <A> Parser<A>.parse(input: String): A {
    val location = Location(input, 0, input.length)
    return when (val pr = parse(Context(null, null, location))) {
        is Success -> {
            val r = pr.value()
            if (pr.consumed != input.length) {
                if (pr.attachedError != null) {
                    throw pr.attachedError.toException()
                }
                throw ParsingException("Unexpected EOF", location.advancedBy(pr.consumed))
            }
            r
        }
        is Failure -> throw pr.toException()
    }
}

private fun Failure.toException(): ParsingException {
    val errors = errors
        .sortedByDescending { it.second.path.size }
        .groupBy { it.second.location }
        .toSortedMap(compareBy<Location> { it.offset }.reversed())
        .mapValues { (_, exp) ->
            if (exp.size == 1) {
                val (err, _) = exp.single()
                "${(err as Expected).e} expected"
            } else {
                exp.joinToString(
                    prefix = "One of ",
                    separator = " or ",
                    postfix = " expected"
                ) { (err, _) ->
                    (err as Expected).e
                }
            }
        }

    val firstError = errors.iterator().next()
    return ParsingException(firstError.value, firstError.key)
}

class ParsingException(val error: String, val location: Location) : RuntimeException(
    "$error at ${location.line}:${location.column}\n$location"
)
