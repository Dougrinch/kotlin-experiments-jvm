package com.dou.parser

sealed interface ParseError

data class Expected(val e: String) : ParseError
