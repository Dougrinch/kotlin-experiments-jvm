package com.dou.parser

fun <A> Parser<A>.many(
    separator: String,
    exact: Int? = null, min: Int? = exact, max: Int? = exact
): Parser<List<A>> =
    many(const(separator), min, max)

fun <A> Parser<A>.many(
    separator: Parser<Unit> = succeed(Unit),
    exact: Int? = null, min: Int? = exact, max: Int? = exact
): Parser<List<A>> =
    many(separator, min, max)

fun <A> Parser<A>.many(
    separator: Parser<Unit>, min: Int?, max: Int?
): Parser<List<A>> {
    return ParserImpl("many($name)") { ctx ->
        val values = ArrayList<() -> A>()
        var consumed = 0

        when (val r = parser("0") { this }.parse(ctx)) {
            is Success -> {
                consumed += r.consumed
                values += r.value
            }

            is Failure -> {
                return@ParserImpl if (min == null || min == 0) {
                    Success(0, r) { emptyList() }
                } else {
                    r
                }
            }
        }

        while (max == null || values.size < max) {
            val next = parser("${values.size}") { separator - this }
            when (val r = next.parse(ctx.advancedBy(consumed))) {
                is Success -> {
                    consumed += r.consumed
                    values += r.value
                }

                is Failure -> {
                    return@ParserImpl if (min == null || values.size >= min) {
                        Success(consumed, r) { values.map { it() } }
                    } else {
                        r
                    }
                }
            }
        }

        return@ParserImpl Success(consumed) { values.map { it() } }
    }
}
