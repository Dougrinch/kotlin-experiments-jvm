package com.dou.parser

import org.intellij.lang.annotations.Language
import kotlin.text.RegexOption.MULTILINE

fun <A> succeed(a: A): Parser<A> {
    return ParserImpl("$a") { Success(0) { a } }
}

fun string(s: String): Parser<String> {
    return ParserImpl(s) { ctx ->
        if (ctx.location.startsWith(s)) {
            Success(s.length) { s }
        } else {
            Failure(Expected(s), ctx, false)
        }
    }
}

fun regex(@Language("RegExp") pattern: String, multiline: Boolean = false): Parser<String> {
    return ParserImpl("regex") { ctx ->
        val regex = if (multiline) {
            pattern.toRegex(MULTILINE)
        } else {
            pattern.toRegex()
        }
        when (val mr = regex.matchAt(ctx.location, 0)) {
            null -> Failure(Expected(pattern), ctx, false)
            else -> mr.value.let { v -> Success(v.length) { v } }
        }
    }
}

fun codePoint(f: (Int) -> Boolean): Parser<String> {
    return ParserImpl("codePoint") { ctx ->
        val cpo = ctx.location.codePoints().findFirst()
        if (cpo.isEmpty) {
            Failure(Expected("Unexpected EOF"), ctx, false)
        } else {
            val cp = cpo.asInt
            if (f(cp)) {
                val v = String(codePoints = intArrayOf(cp), 0, 1)
                Success(v.length) { v }
            } else {
                Failure(Expected("Unmatched codePoint '$cp'"), ctx, false)
            }
        }
    }
}

val line: Parser<Int>
    get() {
        return ParserImpl("line") { ctx ->
            Success(0) { ctx.location.line }
        }
    }

val column: Parser<Int>
    get() {
        return ParserImpl("column") { ctx ->
            Success(0) { ctx.location.column }
        }
    }
