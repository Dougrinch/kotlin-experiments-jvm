package com.dou.parser

import com.dou.fp.e9.StringView

class Location(private val input: CharSequence, val offset: Int, private val limit: Int) : CharSequence {
    init {
        if (offset < 0 || offset > input.length) {
            throw IllegalArgumentException("offset: $offset")
        }
        if (limit < 0 || limit > input.length) {
            throw StringIndexOutOfBoundsException("offset: $offset, limit: $limit")
        }
    }

    override val length: Int
        get() = minOf(input.length, limit) - offset

    override fun get(index: Int): Char {
        checkIndex(index)
        return input[index + offset]
    }

    private fun checkIndex(index: Int, allowLast: Boolean = false) {
        when {
            index < 0 -> throw StringIndexOutOfBoundsException(index)
            allowLast && index > length -> throw StringIndexOutOfBoundsException(index)
            !allowLast && index >= length -> throw StringIndexOutOfBoundsException(index)
        }
    }

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        checkIndex(startIndex)
        checkIndex(endIndex, allowLast = true)
        return StringView(input, startIndex + offset, endIndex + offset)
    }

    fun advancedBy(n: Int): Location {
        checkIndex(n, allowLast = true)
        return Location(input, offset + n, limit)
    }

    fun limitBy(n: Int): Location {
        checkIndex(n, allowLast = true)
        return Location(input, offset, offset + n)
    }

    val lineStr: String by lazy { input.substring(lineStartIndex, lineEndIndex) }

    val line: Int by lazy { StringView(input, 0, offset).count { it == '\n' } + 1 }
    val column: Int by lazy { offset - lineStartIndex }

    private val lineStartIndex: Int by lazy {
        StringView(input, 0, offset).lastIndexOf('\n') + 1
    }
    private val lineEndIndex: Int by lazy {
        input.indexOf('\n', offset).let { if (it == -1) input.length else it }
    }

    override fun toString(): String {
        return "$lineStr\n${" ".repeat(column) + "^"}"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Location

        if (input != other.input) return false
        if (offset != other.offset) return false

        return true
    }

    override fun hashCode(): Int {
        var result = input.hashCode()
        result = 31 * result + offset
        return result
    }
}
