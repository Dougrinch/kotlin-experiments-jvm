package com.dou.util

@Suppress("FunctionName")
fun <T> UNDEFINED(): T {
    throw NotImplementedError()
}

@Suppress("FunctionName", "unused")
fun Any?.CONSUME() {
}
