package com.dou.mars

fun main() {
    val rules = listOf(
        1.pow to 2.comm,
        2.data to 2.comm and 2.nav,
        1.data and 1.nav to 4.comm,

        1.pow to 3.data,
        2.nav to 3.data,
        3.comm to 3.data and 2.nav,

        1.pow to 1.nav,
        1.data to 2.nav,
        1.comm to 1.nav and 1.data,

    )

    val game = Game(8.comm and 6.data and 6.nav, 6, rules, 4, 2)

    val solution = solve(game)
    solution!!.forEach(::println)

    val result = play(game, solution)
    println(result)
}
