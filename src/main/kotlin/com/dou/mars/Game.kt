package com.dou.mars

enum class Resource {
    Communication, Data, Navigation, Power
}

data class Resources(val data: Map<Resource, Int>)

val Int.comm: Resources
    get() = Resources(mapOf(Resource.Communication to this))
val Int.data: Resources
    get() = Resources(mapOf(Resource.Data to this))
val Int.nav: Resources
    get() = Resources(mapOf(Resource.Navigation to this))
val Int.pow: Resources
    get() = Resources(mapOf(Resource.Power to this))

infix fun Resources.and(o: Resources): Resources = Resources(data + o.data)

data class Conversion(val from: Resources, val to: Resources)
typealias Solution = List<Conversion>

infix fun Resources.to(o: Resources): Conversion = Conversion(this, o)
infix fun Conversion.and(o: Resources): Conversion = Conversion(from, to and o)

operator fun Resources.contains(o: Resources): Boolean = o.data.all { (r, v) -> data[r]?.let { it >= v } == true }
operator fun Resources.plus(o: Resources): Resources =
    Resources((data.toList() + o.data.toList()).groupBy { it.first }.mapValues { it.value.sumOf { it.second } })

operator fun Resources.unaryMinus(): Resources = Resources(data.mapValues { -it.value })

operator fun Resources.minus(o: Resources): Resources = this + -o

data class Game(
    val goal: Resources,
    val initialPower: Int,
    val rules: List<Conversion>,
    val turns: Int, val commands: Int
)

fun Conversion.tryApplyTo(state: Resources): Resources? {
    return if (from in state) {
        state - from + to
    } else {
        null
    }
}

fun solve(game: Game): Solution? {
    return solve(listOf(), game.initialPower.pow, game)
}

private fun solve(prevOrders: Solution, state: Resources, game: Game): Solution? {
    if (game.goal in state) {
        return prevOrders
    }
    if (prevOrders.size >= game.turns * game.commands) {
        return null
    }

    for (rule in game.rules) {
        val newState = rule.tryApplyTo(state)
        if (newState != null) {
            val solution = solve(prevOrders + rule, newState, game)
            if (solution != null) {
                return solution
            }
        }
    }

    return null
}

fun play(game: Game, orders: List<Conversion>): Resources {
    var state = game.initialPower.pow

    for (order in orders) {
        state = order.tryApplyTo(state)!!
    }

    return state
}
