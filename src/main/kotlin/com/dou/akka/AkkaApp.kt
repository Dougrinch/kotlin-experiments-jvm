package com.dou.akka

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import org.intellij.lang.annotations.Language

fun main() {
    @Language("HOCON")
    val config = """
        akka.extensions = ["akka.management.cluster.bootstrap.ClusterBootstrap"]
        akka.actor.provider = cluster
        akka.discovery.method = akka-dns
        """.trimIndent()

    val system = ActorSystem.create("cluster", ConfigFactory.parseString(config))
    println(system)
}
