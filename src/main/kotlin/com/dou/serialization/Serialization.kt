package com.dou.serialization

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Serializable
data class Data(val a: Int, val b: String)

fun main() {
    val json = Json.encodeToString(Data(42, "str"))
    Data.serializer()
    println(json)
}
