package com.dou.quantum

import com.dou.math.*
import com.dou.math.matrix.column

context(IntegerOps<T>)
fun <T> b0(): QuantumState<T, N2> = QuantumState(column(one, zero))

context(IntegerOps<T>)
fun <T> b1(): QuantumState<T, N2> = QuantumState(column(zero, one))


context(IntegerOps<T>)
fun <T> b00(): QuantumState<T, N4> = QuantumState(column(one, zero, zero, zero))

context(IntegerOps<T>)
fun <T> b01(): QuantumState<T, N4> = QuantumState(column(zero, one, zero, zero))

context(IntegerOps<T>)
fun <T> b10(): QuantumState<T, N4> = QuantumState(column(zero, zero, one, zero))

context(IntegerOps<T>)
fun <T> b11(): QuantumState<T, N4> = QuantumState(column(zero, zero, zero, one))
