package com.dou.quantum

import com.dou.math.IntOps
import com.dou.math.N4
import com.dou.math.matrix.times
import com.dou.math.matrix.x

fun main() = with(IntOps) {
    println(X() x X() x X())
    println(I() x CNOT())
    println(CNOT() x I())
    println(X() x I() x X())


    return@with
    val xi = X() x I()
    val cnot = CNOT()

    val cs = listOf(b00(), b01(), b10(), b11())

    val op = xi * cnot * xi
    println(op)

    for (c in cs) {
        println("$c -> ${op * c}")
    }



    fun U(f: (Int) -> Int, xy: QuantumState<Int, N4>): QuantumState<Int, N4> {
        TODO()
    }
}
