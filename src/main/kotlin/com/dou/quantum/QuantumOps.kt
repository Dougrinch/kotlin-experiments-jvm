@file:Suppress("FunctionName")

package com.dou.quantum

import com.dou.math.IntegerOps
import com.dou.math.matrix.*

context(IntegerOps<T>)
fun <T> I() = Matrix(
    row(one, zero),
    row(zero, one)
)

context(IntegerOps<T>)
fun <T> X() = Matrix(
    row(zero, one),
    row(one, zero)
)

context(IntegerOps<T>)
fun <T> CNOT() = Matrix(
    row(one, zero, zero, zero),
    row(zero, one, zero, zero),
    row(zero, zero, zero, one),
    row(zero, zero, one, zero)
)
