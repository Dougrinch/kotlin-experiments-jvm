package com.dou.quantum

import com.dou.math.IntegerOps
import com.dou.math.TypedNumber
import com.dou.math.matrix.*

context(IntegerOps<T>)
class QuantumState<T, N : TypedNumber>(val state: Column<T, N>) {

    override fun toString(): String {
        val maxBits = (state.m - 1).toString(2).length
        return state.asIterable().withIndex()
            .filter { it.value != zero }
            .joinToString(separator = " + ") { (b, v) ->
                val basis = b.toString(2).padStart(maxBits, padChar = '0')
                if (v == one) {
                    "|$basis⟩"
                } else {
                    "$v|$basis⟩"
                }
            }
    }
}

context(IntegerOps<T>)
operator fun <T, N : TypedNumber> Matrix<T, N, N>.times(state: QuantumState<T, N>): QuantumState<T, N> {
    return QuantumState(this * state.state)
}
