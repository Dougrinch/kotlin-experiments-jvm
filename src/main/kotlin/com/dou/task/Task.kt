package com.dou.task

interface Task {
    fun execute()
}

interface BaseTask : Task, TaskState {
    fun tryRun() {
        if (!enabled) {
            println("--task disabled, sorry--")
        } else {
            println("--before task body--")
            execute()
            println("--after task body--")
        }
    }
}

interface TaskState {
    var enabled: Boolean
}

data class State(override var enabled: Boolean = false) : TaskState

class MyTask : BaseTask, TaskState by State() {
    override fun execute() {
        println("Hello from task body!")
    }
}

fun main() {
    val task = MyTask()
    task.tryRun()

    task.enabled = true
    task.tryRun()
}
