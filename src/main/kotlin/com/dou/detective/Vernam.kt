package com.dou.detective

val alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
val value = alphabet.withIndex().associateBy { it }
    .mapKeys { e -> e.key.value }
    .mapValues { e -> e.value.index }

fun main() {
    println(decode("мдикбо", "фдппшк"))
}

private fun decode(key: String, encoded: String): String {
    val res = encoded.mapIndexed { idx, ch -> alphabet[(value[ch]!! - value[key[idx]]!! + 33) % 33] }
    return res.joinToString(separator = "")
}
