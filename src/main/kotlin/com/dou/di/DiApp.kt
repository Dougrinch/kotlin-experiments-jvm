@file:Suppress("unused", "UNUSED_PARAMETER")

package com.dou.di

import kotlin.reflect.KCallable

fun main() {
    val ctx = Context()

    ctx.register<Service1<String>>(::Service1)
    ctx.register<Service1<Int>>(::Service1)
    ctx.register(::Service2)
    ctx.register(::Service3)
}

class Context {
    fun <S : Service> register(f: KCallable<S>) {
        f.parameters.map { it.type }.forEach { println(it) }
        println("")
    }
}

interface Service

class Service1<T> : Service
class Service2(val s1: Service1<String>) : Service
class Service3(val s1: Service1<Int>, s2: Service2) : Service
