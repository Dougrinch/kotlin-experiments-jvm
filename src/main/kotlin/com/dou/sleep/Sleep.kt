package com.dou.sleep

import com.dou.sleep.Rarity.Rare
import com.dou.sleep.Rarity.Uncommon
import com.dou.sleep.Type.*

fun main() {
    val room = Room(
        allItems["Meditation", 2],
        allItems["Flower pot", 1],
        allItems["Yoga Mat", 2],
        allItems["Lotus Flower", 2],
        allItems["Spiritual Posters", 2]
    )

    println(room.totalPower())
    println(room.copy(storage = allItems["Cubby", 2]).totalPower())
    println(room.copy(bed = allItems["Leather sofa", 3]).totalPower())
    println(
        room.copy(
            setting = allItems["Meditation", 2],
            storage = allItems["Flower pot", 2]
        ).totalPower()
    )
//    println(room.copy(bed = allItems["Truck Bed", 1]).totalPower())
//    println(room.copy(bed = allItems["Leather sofa", 1]).totalPower())
//    println(room.copy(bed = allItems["Camper", 1]).totalPower())

    println(
        room.copy(
            storage = allItems["Flight cases", 1],
            rug = allItems["Green Rug", 1]
        ).totalPower()
    )
    println(
        room.copy(
            storage = allItems["Flight cases", 1],
            rug = allItems["Tiger", 1]
        ).totalPower()
    )

    allItems
        .asSequence()
        .filter { it.rarity == Rare && it.level == 3 }
        .filter { it.type != Bed || it.name == "Leather sofa" }
        .allVariations()
        .map { it.totalPower() to it }
        .sortedBy { it.first }
//        .forEach { println(it) }
        .last().let { (power, room) ->
            println(power)
            println(room.setting)
            println(room.storage)
            println(room.bed)
            println(room.rug)
            println(room.poster)
        }
}

val allItems = items {
    group(Setting, Uncommon) {
        item("Meditation", 1.1, 0.7, 0.95, 1.2, 1.45, 1.7)
        item("Vanity", 1.05, 0.9, 1.25, 1.6, 1.95, 2.3)
        item("Hideout", 1.0 to 1.1, 1.02 to 1.35, 1.04 to 1.6, 1.06 to 1.85, 1.08 to 2.1)
        item("Ancient Rome", 1.02, 1.2, 1.5, 1.8, 2.1, 2.4)
    }
    group(Setting, Rare) {
        item("Wilderness", 1.05, 1.5, 1.8, 2.1, 2.4, 2.7)
        item("Warm October", 1.0 to 1.55, 1.02 to 1.85, 1.04 to 2.15, 1.06 to 2.45, 1.08 to 2.75)
        item("Studio", 1.1, 1.1, 1.4, 1.7, 2.0, 2.3)
        item("Tribal", 1.02, 1.7, 2.1, 2.5, 2.9, 3.3)
    }

    group(Storage, Uncommon) {
        item("Flower pot", 1.1, 0.7, 0.95, 1.2, 1.45, 1.7)
        item("Mirror", 1.05, 0.9, 1.25, 1.6, 1.95, 2.3)
        item("Training Dummy", 1.0 to 1.1, 1.02 to 1.35, 1.04 to 1.6, 1.06 to 1.85, 1.08 to 2.1)
        item("Trophy Stand", 1.02, 1.2, 1.5, 1.8, 2.1, 2.4)
    }
    group(Storage, Rare) {
        item("Cubby", 1.05, 1.5, 1.8, 2.1, 2.4, 2.7)
        item("Pumpkin Crates", 1.0 to 1.55, 1.02 to 1.85, 1.04 to 2.15, 1.06 to 2.45, 1.08 to 2.75)
        item("Flight cases", 1.1, 1.1, 1.4, 1.7, 2.0, 2.3)
        item("Shelf", 1.02, 1.7, 2.1, 2.5, 2.9, 3.3)
    }

    group(Bed, Uncommon) {
        item("Yoga Mat", 1.1, 1.6, 2.1, 2.6, 3.1, 3.6)
        item("Ninja Bed", 1.0 to 2.1, 1.02 to 2.6, 1.04 to 3.1, 1.06 to 3.6, 1.08 to 4.1)
        item("Day Bed", 1.05, 1.9, 2.5, 3.1, 3.7, 4.3)
        item("Lectus", 1.02, 2.2, 2.8, 3.4, 4.0, 4.6)
    }
    group(Bed, Rare) {
        item("Truck Bed", 1.0 to 3.0, 1.02 to 3.6, 1.04 to 4.2, 1.06 to 4.8, 1.08 to 5.4)
        item("Leather sofa", 1.1, 2.5, 3.0, 3.5, 4.0, 4.5)
        item("Camper", 1.05, 3.0, 3.65, 4.3, 4.95, 5.6)
        item("Gold Bed", 1.02, 3.3, 3.95, 4.6, 5.25, 5.9)
    }

    group(Rug, Uncommon) {
        item("Pink Rug", 1.05, 0.9, 1.3, 1.7, 2.1, 2.5)
        item("Lotus Flower", 1.1, 0.7, 1.0, 1.3, 1.6, 1.9)
        item("Flower Pattern Rug", 1.0 to 1.1, 1.02 to 1.4, 1.04 to 1.7, 1.06 to 2.0, 1.08 to 2.3)
        item("Tiles", 1.02, 1.2, 1.6, 2.0, 2.4, 2.8)
    }
    group(Rug, Rare) {
        item("Green Rug", 1.05, 1.6, 2.0, 2.4, 2.8, 3.2)
        item("Tiger", 1.1, 1.2, 1.5, 1.8, 2.1, 2.4)
        item("Fall leaves", 1.0 to 1.6, 1.02 to 1.95, 1.04 to 2.3, 1.06 to 2.65, 1.08 to 3.0)
        item("Quilted Rug", 1.02, 1.9, 2.4, 2.9, 3.4, 3.9)
    }

    group(Poster, Uncommon) {
        item("Spiritual Posters", 1.1, 0.7, 1.0, 1.3, 1.6, 1.9)
        item("Painting", 1.05, 0.9, 1.3, 1.7, 2.1, 2.5)
        item("Scroll", 1.0 to 1.1, 1.02 to 1.4, 1.04 to 1.7, 1.06 to 2.0, 1.08 to 2.3)
        item("Spartan Shield", 1.02, 1.2, 1.6, 2.0, 2.4, 2.8)
    }
    group(Poster, Rare) {
        item("Legendary guitar", 1.1, 1.2, 1.5, 1.8, 2.1, 2.4)
        item("Merit Badges", 1.05, 1.6, 2.0, 2.4, 2.8, 3.2)
        item("Wheel of flowers", 1.0 to 1.6, 1.02 to 1.95, 1.04 to 2.3, 1.06 to 2.65, 1.08 to 3.0)
        item("Sun", 1.02, 1.9, 2.4, 2.9, 3.4, 3.9)
    }
}

fun items(body: GroupBuilder.() -> Unit): List<Item> {
    val result = mutableListOf<Item>()
    GroupBuilder(result).body()
    return result
}

class GroupBuilder(val result: MutableList<Item>) {
    fun group(type: Type, rarity: Rarity, body: ItemsBuilder.() -> Unit) {
        ItemsBuilder(type, rarity, result).body()
    }
}

class ItemsBuilder(private val type: Type, private val rarity: Rarity, private val result: MutableList<Item>) {
    fun item(name: String, combo: Double, vararg powers: Double) {
        for ((idx, power) in powers.withIndex()) {
            result += Item(name, type, rarity, idx + 1, combo, power)
        }
    }

    fun item(name: String, vararg comboAndPowers: Pair<Double, Double>) {
        for ((idx, pair) in comboAndPowers.withIndex()) {
            val (combo, power) = pair
            result += Item(name, type, rarity, idx + 1, combo, power)
        }
    }
}

data class Item(
    val name: String,
    val type: Type,
    val rarity: Rarity,
    val level: Int,
    val combo: Double,
    val rawPower: Double
)

data class Room(
    val setting: Item,
    val storage: Item,
    val bed: Item,
    val rug: Item,
    val poster: Item
)

enum class Type {
    Setting, Storage, Bed, Rug, Poster
}

enum class Rarity {
    Common, Uncommon, Rare, Super, Myth
}

fun Room.totalPower(): Double {
    val items = listOf(setting, storage, bed, rug, poster)
    val rawPower = items.sumOf { it.rawPower }
    val combo = items.fold(1.0) { acc, item -> acc * item.combo }
    val leagueCombo = 1.05
    return rawPower * combo * leagueCombo
}

fun Sequence<Item>.allVariations(): Sequence<Room> {
    val itemsByTypes = groupBy { it.type }
    return sequence {
        for (setting in itemsByTypes[Setting]!!) {
            for (storage in itemsByTypes[Storage]!!) {
                for (bed in itemsByTypes[Bed]!!) {
                    for (rug in itemsByTypes[Rug]!!) {
                        for (poster in itemsByTypes[Poster]!!) {
                            yield(Room(setting, storage, bed, rug, poster))
                        }
                    }
                }
            }
        }
    }
}

operator fun List<Item>.get(name: String, level: Int): Item {
    return single { it.name == name && it.level == level }
}
