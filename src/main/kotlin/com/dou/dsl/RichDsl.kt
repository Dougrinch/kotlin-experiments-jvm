package com.dou.dsl

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

interface WithA {
    val a: String
}

interface WithB {
    val b: String
}

interface WithC {
    val c: String
}

interface DslBuilder

class DslBuilderImpl : DslBuilder, WithA, WithB, WithC {
    override lateinit var a: String
    override lateinit var b: String
    override lateinit var c: String
}

@OptIn(ExperimentalContracts::class)
fun DslBuilder.setA(value: String) {
    contract {
        returns() implies (this@setA is WithA)
    }
    (this as DslBuilderImpl).a = value
}

@OptIn(ExperimentalContracts::class)
fun DslBuilder.setB(value: String) {
    contract {
        returns() implies (this@setB is WithB)
    }
    (this as DslBuilderImpl).b = value
}

@OptIn(ExperimentalContracts::class)
fun <B> B.setC() where B : WithA, B : WithB {
    contract {
        returns() implies (this@setC is WithC)
    }
    (this as DslBuilderImpl).c = a + b
}

fun foo(block: DslBuilder.() -> Foo): Foo {
    return DslBuilderImpl().block()
}

data class Foo(val a: String, val b: String, val c: String)

fun <B> B.build(): Foo where B : WithA, B : WithB, B : WithC = Foo(a, b, c)

fun main() {
    val f = foo {
        setA("Hello, ")
        setB("World!")
        setC()
        build()
    }
    println(f.c)
}
