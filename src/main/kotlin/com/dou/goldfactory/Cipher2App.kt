package com.dou.goldfactory

fun main() {
    val code = "Lzwjw ak s ljwskmjw zavvwf kgewozwjw, al ak dguslwv sl s kwujwl hdsuw af lzw Hsuaxau Guwsf"
    for (i in 0 until 26) {
        println("$i: " + code.map {
            if (it in 'a'..'z' || it in 'A'..'Z') {
                circularShift(it, i)
            } else {
                it
            }
        }.joinToString(separator = ""))
    }
}

fun circularShift(char: Char, shift: Int): Char {
    val alphabetSize = 26
    val startChar = 'a'
    // Ensure the shift is positive and within the range of 0 to 25
    val normalizedShift = ((shift % alphabetSize) + alphabetSize) % alphabetSize
    // Calculate the new position, wrapping around if necessary
    val newPosition = ((char - startChar + normalizedShift) % alphabetSize) + startChar.code
    return newPosition.toChar()
}
