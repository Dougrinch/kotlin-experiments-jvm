package com.dou.fib

fun f(n: Int): Long {
    if (n <= 1)
        return n.toLong()
    return f(n - 1) + f(n - 2) + 1
}

fun main() {
    for (i in 0..20) {
        println(f(i))
    }
}