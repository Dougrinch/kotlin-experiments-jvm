package com.dou.extractIds

import com.dou.parser.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class Row(
    val rowId: Int,
    val itemId: String,
    val semantics: Int
)

val appId by int
val itemType by oneOf(string("structure:generator"), string("jira:issue"))
val longId by long

val itemId by appId - "/" - itemType - "/" - longId

fun main() {
    val rows = Json.decodeFromString<List<Row>>(rawRows)
    val itemIds = rows.map { itemId.parse(it.itemId) }
        .filter { it.first.second == "jira:issue" }
        .map { it.second }

    println(itemIds.count())
    println(itemIds)

    val jqls = itemIds.chunked(100).map { "\"Parent Link\" in (${it.joinToString(separator = ",")}) order by issue asc" }

    jqls.forEach {
        println(it)
        println()
    }
}
