package com.dou.initiative

import java.io.File
import java.nio.charset.Charset
import java.util.*

fun main() {
    val alphabet = ('а'..'я').toList() + 'ё'

    val file = File("/Users/dougrinch/projects/kotlin-experiments-jvm/russian.txt")
    val words = file.readLines(Charset.forName("windows-1251"))
        .map { it.lowercase(Locale.getDefault()) }
        .distinct()
        .filter { it.all { it in alphabet } }

    val words1 = words.filter { it.length == 3 }.filter { it.toList().let { it.distinct().size == it.size } }
    val words2 = words.filter { it.length == 6 }.filter { it.toList().let { it.distinct().size == it.size } }
    val words3 = words.filter { it.length == 2 }.filter { it.toList().let { it.distinct().size == it.size } }
    val words4 = words.filter { it.length == 4 }.filter { it.toList().let { it.distinct().size == it.size } }

    var candidatesCount = 0L

    for (c1 in alphabet) {
        for (c2 in alphabet) {
            for (c3 in alphabet) {
                if (c1 == c2 || c2 == c3 || c3 == c1) {
                    continue
                }

                val c123 = listOf(c1, c2, c3)

                val ws1 = words1
                    .filter { it[1] == c1 && it[2] == c2 && it[0] !in c123 }
                val ws2 = words2
                    .filter { it.all { it !in c123 } }
                val ws3 = words3
                    .filter { it[1] == c3 && it[0] !in c123 }
                val ws4 = words4
                    .filter { it[0] == c1 && it[1] == c3 && it[3] == c2 && it[2] !in c123 }

                if (ws1.isEmpty() || ws2.isEmpty() || ws3.isEmpty() || ws4.isEmpty()) {
                    continue
                }

                candidatesCount += ws1.size * words2.size * ws3.size * ws4.size
            }
        }
    }

    println(candidatesCount)
}
