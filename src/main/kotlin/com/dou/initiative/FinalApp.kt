package com.dou.initiative

fun main() {
    val alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

    fun decode(msg: String, key: String): String {
        return msg.withIndex().map { (i, c) ->
            if (c == '-') {
                '-'
            } else {
                val nc = (alphabet.indexOf(c) + 1 + alphabet.indexOf(key[i]) + 1).let { nc ->
                    if (nc > 33)
                        nc - 33
                    else
                        nc
                }

                alphabet[nc - 1]
            }
        }.joinToString(separator = "")
    }

    println(decode("щсывркялч-ъчпсф", "чтонужносделать"))
}

//unexpectedgames.com
//ivrb2kf3aahokstraksn@unexpectedgames.com
//

//ivrb2kf3aahokstra1sn@unexpectedgames.com
//ivrbbkfcaahokstraasn@unexpectedgames.com

//ivrb3kl6[whovs;yw1sn@unexpectedgames.com

//ivrb2kf3aahokstra1sn


