package com.dou.initiative

import java.io.File
import java.nio.charset.Charset
import java.util.*

fun main() {
    val file = File("/Users/dougrinch/projects/kotlin-experiments-jvm/russian.txt")
    val words = file.readLines(Charset.forName("windows-1251"))

    words.asSequence()
        .map { it.lowercase(Locale.getDefault()) }
        .filter { word ->
            word.length == 9
                && listOf('а', 'у', 'с', 'л', 'о').all { word.containsOne(it) }
                && word.groupBy { it }.mapValues { (_, v) -> v.size }.let { charCounts ->
                charCounts.values.filter { it == 2 }.size == 1 &&
                    charCounts.values.filter { it == 1 }.size == 7
            }
        }
        .sorted()
        .forEach { println(it) }
//        .let { println(it.count()) }
}

fun String.containsOne(c: Char): Boolean {
    return this.count { it == c } == 1
}
