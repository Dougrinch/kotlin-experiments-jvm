package com.dou.advent.year2015

fun main() {
    Day25().run()
}

class Day25 {
    fun run() {
        val firstCode = 20151125

        val code = generateSequence(firstCode) { nextCode(it) }
            .drop(codeCount(3010, 3019) - 1)
            .first()

        println(code)
    }

    private fun nextCode(n: Int): Int {
        return (n.toLong() * 252533 % 33554393).toInt()
    }

    private fun codeCount(row: Int, column: Int): Int {
        return (1..(row + column - 2)).sum() + column
    }
}
