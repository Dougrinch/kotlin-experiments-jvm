package com.dou.advent.year2015

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day24().run()
}

class Day24 : TypedDay<List<Int>>() {
    override val test1: Any
        get() = 99
    override val test2: Any
        get() = 44

    override val parser by parser { int.separatedByLines() }

    override fun part1(input: List<Int>): Any {
        val groupSum = input.sum() / 3
        val result = ArrayList<List<Int>>()
        select(input, 0, groupSum, ArrayDeque(), result)
        return result
            .map { it to it.map { it.toLong() }.reduce(Long::times) }
            .sortedWith(compareBy({ it.first.size }, { it.second }))
            .first().second
    }

    override fun part2(input: List<Int>): Any {
        val groupSum = input.sum() / 4
        val result = ArrayList<List<Int>>()
        select(input, 0, groupSum, ArrayDeque(), result)
        return result
            .map { it to it.map { it.toLong() }.reduce(Long::times) }
            .sortedWith(compareBy({ it.first.size }, { it.second }))
            .first().second
    }

    private fun select(list: List<Int>, from: Int, sum: Int, stack: ArrayDeque<Int>, result: MutableList<List<Int>>) {
        for (i in from until list.size) {
            val a = list[i]
            val rest = sum - a
            if (rest == 0) {
                stack.addLast(a)
                result += stack.toList()
                stack.removeLast()
            } else if (rest > 0) {
                stack.addLast(a)
                select(list, i + 1, rest, stack, result)
                stack.removeLast()
            }
        }
    }
}
