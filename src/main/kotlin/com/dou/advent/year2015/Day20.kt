package com.dou.advent.year2015

import com.dou.advent.TypedDay
import com.dou.parser.int

fun main() {
    Day20().run()
}

class Day20 : TypedDay<Int>() {
    override val test1: Any
        get() = 4

    override val parser by int

    override fun part1(input: Int): Any {
        fun totalPresents(house: Int): Int {
            var r = 0
            for (i in 1..house) {
                if (house % i == 0) {
                    r += i * 10
                }
            }
            return r
        }

        var i = 1
        while (true) {
            if (totalPresents(i) >= input) {
                return i
            }
            i += 1
        }
    }

    override fun part2(input: Int): Any {
        fun totalPresents(house: Int): Int {
            var r = 0
            for (i in 1..house) {
                if (house % i == 0 && house / i <= 50) {
                    r += i * 11
                }
            }
            return r
        }

        var i = 1
        while (true) {
            if (totalPresents(i) >= input) {
                return i
            }
            i += 1
        }
    }
}
