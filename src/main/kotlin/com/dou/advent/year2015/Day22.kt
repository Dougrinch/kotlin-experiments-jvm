package com.dou.advent.year2015

import arrow.core.Some
import com.dou.advent.util.search
import com.dou.advent.year2015.Day22.Spell.*
import com.dou.fp.e6.State
import com.dou.fp.e6.state

fun main() {
    Day22().run()
}

class Day22 {
    fun run() {
        val player = Player(50, 500, 0, emptyMap())
        val boss = Boss(51, 9, emptyMap())

        println(simulate(player, boss, false))
        println(simulate(player, boss, true))
    }

    private fun simulate(player: Player, boss: Boss, hardMode: Boolean): Int {
        val r = search(
            Game(player, boss),
            { g ->
                Spell.values()
                    .filter { isPossibleSpell(g, it) }
                    .map {
                        if (hardMode) {
                            hardModeTurn(it).run(g).second to it.cost
                        } else {
                            turn(it).run(g).second to it.cost
                        }
                    }
            },
            { 0 },
            { (p, b) -> p.hp > 0 && b.hp <= 0 }
        )
        return (r as Some).value.second
    }

    private fun isPossibleSpell(game: Game, spell: Spell): Boolean {
        val player = game.player
        val boss = game.boss
        if (player.hp <= 0) {
            return false
        }
        if (player.mana < spell.cost) {
            return false
        }
        if ((spell == Shield || spell == Recharge) && spell in player.effects.filterValues { it > 1 }.keys) {
            return false
        }
        if (spell == Poison && spell in boss.effects.filterValues { it > 1 }.keys) {
            return false
        }
        return true
    }

    private operator fun State<Game, Unit>.minus(o: State<Game, Unit>): State<Game, Unit> = state {
        this@minus.bind()
        val (p, b) = get()
        if (p.hp > 0 && b.hp > 0) {
            o.bind()
        }
    }

    private fun turn(spell: Spell) = playerTurn(spell) - bossTurn()
    private fun hardModeTurn(spell: Spell) = hardMode() - playerTurn(spell) - bossTurn()

    private fun hardMode(): State<Game, Unit> = State { g ->
        val p = g.player
        Unit to g.copy(player = p.copy(hp = p.hp - 1))
    }

    private fun playerTurn(spell: Spell) = applyEffects() - castSpell(spell)

    private fun castSpell(spell: Spell): State<Game, Unit> = State { g ->
        var p = g.player
        var b = g.boss
        when (spell) {
            MagicMissile -> {
                p = p.copy(mana = p.mana - spell.cost)
                b = b.copy(hp = b.hp - 4)
            }
            Drain -> {
                p = p.copy(mana = p.mana - spell.cost, hp = p.hp + 2)
                b = b.copy(hp = b.hp - 2)
            }
            Shield -> {
                p = p.copy(mana = p.mana - spell.cost, effects = p.effects + (spell to 6))
            }
            Poison -> {
                p = p.copy(mana = p.mana - spell.cost)
                b = b.copy(effects = b.effects + (spell to 6))
            }
            Recharge -> {
                p = p.copy(mana = p.mana - spell.cost, effects = p.effects + (spell to 5))
            }
        }
        Unit to Game(p, b)
    }

    private fun bossTurn() = applyEffects() - attackPlayer()

    private fun attackPlayer(): State<Game, Unit> = State { g ->
        val p = g.player
        val b = g.boss
        Unit to g.copy(player = p.copy(hp = p.hp - maxOf(b.damage - p.armor, 1)))
    }

    private fun applyEffects(): State<Game, Unit> = State { g ->
        Unit to Game(g.player.applyEffects(), g.boss.applyEffects())
    }

    private fun Player.applyEffects(): Player {
        var armor = 0
        var mana = mana
        if (Shield in effects.keys) {
            armor = 7
        }
        if (Recharge in effects.keys) {
            mana += 101
        }
        return copy(
            armor = armor,
            mana = mana,
            effects = effects.mapValues { (_, t) -> t - 1 }.filterValues { it > 0 }
        )
    }

    private fun Boss.applyEffects(): Boss {
        var hp = hp
        if (Poison in effects.keys) {
            hp -= 3
        }
        return copy(
            hp = hp,
            effects = effects.mapValues { (_, t) -> t - 1 }.filterValues { it > 0 }
        )
    }

    data class Player(val hp: Int, val mana: Int, val armor: Int, val effects: Map<Spell, Int>)
    data class Boss(val hp: Int, val damage: Int, val effects: Map<Spell, Int>)
    data class Game(val player: Player, val boss: Boss)

    enum class Spell(val cost: Int) {
        MagicMissile(53),
        Drain(73),
        Shield(113),
        Poison(173),
        Recharge(229)
    }
}
