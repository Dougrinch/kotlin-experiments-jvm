package com.dou.advent.year2015

import com.dou.advent.TypedDay
import com.dou.advent.year2015.Day23.Program
import com.dou.parser.*

fun main() {
    Day23().run()
}

class Day23 : TypedDay<Program>() {
    override val test1: Any
        get() = mapOf('a' to 2, 'b' to 0)

    override val parser by parser {
        val r = char('a', 'b')
        val offset = int

        val hlf = "hlf " - r map ::Half
        val tpl = "tpl " - r map ::Triple
        val inc = "inc " - r map ::Increment
        val jmp = "jmp " - offset map ::Jump
        val jie = "jie " - r - ", " - offset map ::JumpIfEven
        val jio = "jio " - r - ", " - offset map ::JumpIfOne

        val instruction = oneOf(hlf, tpl, inc, jmp, jie, jio)

        val instructions = instruction.separatedByLines()

        instructions map { Program(mapOf('a' to 0, 'b' to 0), 0, it) }
    }

    override fun part1(input: Program): Any {
        var p = input
        while (p.pointer in p.instructions.indices) {
            p = p.makeStep()
        }
        return p.registers
    }

    override fun part2(input: Program): Any {
        var p = input.copy(registers = input.registers + ('a' to 1))
        while (p.pointer in p.instructions.indices) {
            p = p.makeStep()
        }
        return p.registers
    }

    private fun Program.makeStep(): Program {
        return when (val i = instructions[pointer]) {
            is Half -> {
                copy(registers = registers + (i.r to registers[i.r]!! / 2), pointer = pointer + 1)
            }
            is Triple -> {
                copy(registers = registers + (i.r to registers[i.r]!! * 3), pointer = pointer + 1)
            }
            is Increment -> {
                copy(registers = registers + (i.r to registers[i.r]!! + 1), pointer = pointer + 1)
            }
            is Jump -> {
                copy(pointer = pointer + i.offset)
            }
            is JumpIfEven -> {
                if (registers[i.r]!! % 2 == 0) {
                    copy(pointer = pointer + i.offset)
                } else {
                    copy(pointer = pointer + 1)
                }
            }
            is JumpIfOne -> {
                if (registers[i.r]!! == 1) {
                    copy(pointer = pointer + i.offset)
                } else {
                    copy(pointer = pointer + 1)
                }
            }
        }
    }

    data class Program(val registers: Map<Char, Int>, val pointer: Int, val instructions: List<Instruction>)

    sealed interface Instruction
    data class Half(val r: Char) : Instruction
    data class Triple(val r: Char) : Instruction
    data class Increment(val r: Char) : Instruction
    data class Jump(val offset: Int) : Instruction
    data class JumpIfEven(val r: Char, val offset: Int) : Instruction
    data class JumpIfOne(val r: Char, val offset: Int) : Instruction
}
