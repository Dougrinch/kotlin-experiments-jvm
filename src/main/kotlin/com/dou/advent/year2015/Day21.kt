package com.dou.advent.year2015

import com.dou.advent.TypedDay
import com.dou.advent.year2015.Day21.Store
import com.dou.advent.year2015.Day21.Unit
import com.dou.parser.*

fun main() {
    Day21().run()
}

class Day21 : TypedDay<Pair<Unit, Store>>() {
    override val parser by parser {
        val boss by "Hit Points: " - int - "\nDamage: " - int - "\nArmor: " - int map ::Unit

        val header by const("Weapons:    ", "Armor:      ", "Rings:      ") - "Cost  Damage  Armor"
        val item by regex("\\w+( \\+\\d+)?") - ws - int - ws - int - ws - int map ::Item

        val items by header - "\n" - item.many("\n")
        val store by items - "\n\n" - items - "\n\n" - items map ::Store

        boss - "\n\n" - store
    }

    override fun part1(input: Pair<Unit, Store>): Any {
        val (boss, store) = input
        return store.players()
            .filter { (player, _) -> simulate(player, boss).first.hp > 0 }
            .minOf { (_, cost) -> cost }
    }

    override fun part2(input: Pair<Unit, Store>): Any {
        val (boss, store) = input
        return store.players()
            .filter { (player, _) -> simulate(player, boss).first.hp <= 0 }
            .maxOf { (_, cost) -> cost }
    }

    private fun Store.players(): Sequence<Pair<Unit, Int>> = sequence {
        for (weapon in weapons()) {
            for (armor in armors()) {
                for (ring in rings()) {
                    val player = Unit(
                        100,
                        weapon.damage + (armor?.damage ?: 0) + (ring?.damage ?: 0),
                        weapon.armor + (armor?.armor ?: 0) + (ring?.armor ?: 0)
                    )
                    val cost = weapon.cost + (armor?.cost ?: 0) + (ring?.cost ?: 0)
                    yield(player to cost)
                }
            }
        }
    }

    private fun Store.weapons(): Sequence<Item> = sequence {
        yieldAll(weapons)
    }

    private fun Store.armors(): Sequence<Item?> = sequence {
        yield(null)
        yieldAll(armors)
    }

    private fun Store.rings(): Sequence<Item?> = sequence {
        yield(null)
        yieldAll(rings)
        for (r1 in rings) {
            for (r2 in rings) {
                if (r1 != r2) {
                    yield(
                        Item(
                            "${r1.name}, ${r2.name}",
                            r1.cost + r2.cost,
                            r1.damage + r2.damage,
                            r1.armor + r2.armor
                        )
                    )
                }
            }
        }
    }

    private fun simulate(player: Unit, boss: Unit): Pair<Unit, Unit> {
        var p = player
        var b = boss
        while (true) {
            b = p.attack(b)
            if (b.hp <= 0) {
                return p to b
            }
            p = b.attack(p)
            if (p.hp <= 0) {
                return p to b
            }
        }
    }

    private fun Unit.attack(enemy: Unit): Unit {
        val damage = maxOf(damage - enemy.armor, 1)
        return enemy.copy(hp = enemy.hp - damage)
    }

    data class Unit(val hp: Int, val damage: Int, val armor: Int)
    data class Item(val name: String, val cost: Int, val damage: Int, val armor: Int)
    data class Store(val weapons: List<Item>, val armors: List<Item>, val rings: List<Item>)
}
