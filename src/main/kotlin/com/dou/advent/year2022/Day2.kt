package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.year2022.Action.*
import com.dou.parser.*

fun main() {
    Day2().run()
}

class Day2 : TypedDay<List<Pair<Action, Char>>>() {
    override val test1: Any
        get() = 15

    override val test2: Any
        get() = 12

    override val parser = parser {
        val l = oneOf(
            char('A') parseAs Rock,
            char('B') parseAs Paper,
            char('C') parseAs Scissors
        )
        val r = oneOf(char('X'), char('Y'), char('Z'))
        (l - " " - r).separatedByLines()
    }

    override fun part1(input: List<Pair<Action, Char>>): Any {
        return input.sumOf { (ea, s) ->
            val ma = when (s) {
                'X' -> Rock
                'Y' -> Paper
                'Z' -> Scissors
                else -> throw IllegalStateException()
            }

            score(ma, ea)
        }
    }

    override fun part2(input: List<Pair<Action, Char>>): Any {
        return input.sumOf { (ea, r) ->
            val ma: Action = when (r) {
                'X' -> Action.values().single { ea.isWin(it) }
                'Y' -> ea
                'Z' -> Action.values().single { it.isWin(ea) }
                else -> throw IllegalStateException()
            }

            score(ma, ea)
        }
    }

    private fun score(ma: Action, ea: Action): Int {
        val ss = when (ma) {
            Rock -> 1
            Paper -> 2
            Scissors -> 3
        }

        val ws = when {
            ma == ea -> 3
            ma.isWin(ea) -> 6
            else -> 0
        }

        return ss + ws
    }

    private fun Action.isWin(ea: Action): Boolean {
        return when {
            this == Rock && ea == Scissors -> true
            this == Scissors && ea == Paper -> true
            this == Paper && ea == Rock -> true
            else -> false
        }
    }
}

enum class Action {
    Rock, Paper, Scissors
}
