package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day1().run()
}

class Day1 : TypedDay<List<List<Int>>>() {
    override val test1: Any
        get() = 24000

    override val test2: Any
        get() = 45000

    override val parser = parser {
        int.separatedByLines().separatedBy("\n\n")
    }

    override fun part1(input: List<List<Int>>): Any {
        return input.maxOf { it.sum() }
    }

    override fun part2(input: List<List<Int>>): Any {
        return input.map { it.sum() }.sortedDescending().take(3).sum()
    }
}
