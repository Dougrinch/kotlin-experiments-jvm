package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*
import java.util.stream.IntStream
import kotlin.math.abs

fun main() {
    Day15().run()
}

class Day15 : TypedDay<List<Pair<Pair<Int, Int>, Pair<Int, Int>>>>() {
    override val parser by parser {
        val coords by "x=" - int - ", y=" - int
        ("Sensor at " - coords - ": closest beacon is at " - coords).separatedByLines()
    }

//    override val test1: Any
//        get() = 26

//    override val test2: Any
//        get() = 56000011

    override fun part1(input: List<Pair<Pair<Int, Int>, Pair<Int, Int>>>): Any {
        val distances = input.map { (s, b) -> s to abs(s.first - b.first) + abs(s.second - b.second) }

        val y = 2000000
        var minX = Int.MAX_VALUE
        var maxX = Int.MIN_VALUE
        for ((sensor, distance) in distances) {
            val maxDx = distance - abs(sensor.second - y)
            if (maxDx >= 0) {
                minX = minOf(minX, sensor.first - maxDx)
                maxX = maxOf(maxX, sensor.first + maxDx)
            }
        }

        var r = 0
        for (x in minX until maxX) {
            val covered = distances.any { (sensor, distance) ->
                abs(sensor.first - x) + abs(sensor.second - y) <= distance
            }
            if (covered) {
                r += 1
            }
        }

        return r
    }

    override fun part2(input: List<Pair<Pair<Int, Int>, Pair<Int, Int>>>): Any {
        val distances = input.map { (s, b) -> s to abs(s.first - b.first) + abs(s.second - b.second) }

        val minX = 0
        val minY = 0
        val maxX = 4000000
        val maxY = 4000000

        return IntStream.range(minX, maxX)
            .parallel()
            .mapToObj { x ->
                var r: Int? = null
                for (y in minY until maxY) {
                    val covered = distances.all { (sensor, distance) ->
                        abs(sensor.first - x) + abs(sensor.second - y) > distance
                    }
                    if (covered) {
                        r = y
                        break
                    }
                }
                if (r == null) {
                    null
                } else {
                    x to r
                }
            }
            .filter { it != null }
            .findFirst()
            .get()
            .let { (x, y) -> x * 4000000L + y }
    }
}
