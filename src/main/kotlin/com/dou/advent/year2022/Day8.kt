package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction.*
import com.dou.parser.*

fun main() {
    Day8().run()
}

class Day8 : TypedDay<Grid<Int>>() {
    override val test1: Any
        get() = 21

    override val test2: Any
        get() = 8

    override val parser = parser {
        grid(digit)
    }

    override fun part1(input: Grid<Int>): Int {
        val visible = input.map { false }.toMutableGrid()
        fun look(starts: List<Position>, direction: Direction) {
            for (start in starts) {
                var pos = start
                visible[pos] = true
                var height = input[pos]
                pos += direction
                while (pos in input) {
                    if (input[pos] > height) {
                        visible[pos] = true
                        height = input[pos]
                    }
                    pos += direction
                }
            }
        }
        look(input.xs.toList().map { Position(it, 0) }, Down)
        look(input.xs.toList().map { Position(it, input.sizeY - 1) }, Up)
        look(input.ys.toList().map { Position(0, it) }, Right)
        look(input.ys.toList().map { Position(input.sizeX - 1, it) }, Left)
        return visible.values().count { it }
    }

    override fun part2(input: Grid<Int>): Any {
        return input.positions().maxOf { start ->
            fun count(direction: Direction): Int {
                val maxHeight = input[start]
                var count = 0
                var pos = start + direction
                while (pos in input) {
                    count += 1
                    if (input[pos] >= maxHeight) {
                        break
                    }
                    pos += direction
                }
                return count
            }

            var result = 1
            result *= count(Up)
            result *= count(Left)
            result *= count(Right)
            result *= count(Down)
            result
        }
    }
}
