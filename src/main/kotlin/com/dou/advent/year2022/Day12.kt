package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.*

fun main() {
    Day12().run()
}

class Day12 : TypedDay<Grid<Day12.Cell>>() {

    override val parser by parser {
        oneOf(
            "S" parseAs Cell.Start,
            "E" parseAs Cell.Exit,
            char map Cell::Simple
        ).let { grid(it) }
    }

    override val test1: Any
        get() = 31

    override val test2: Any
        get() = 29

    override fun part1(input: Grid<Cell>): Any {
        val start = input.positions().single { input[it] == Cell.Start }
        val exit = input.positions().single { input[it] == Cell.Exit }

        return minPath(start, input, exit)
    }

    override fun part2(input: Grid<Cell>): Any {
        val exit = input.positions().single { input[it] == Cell.Exit }

        return input.positions().filter { input[it].height == 'a' }
            .minOf { minPath(it, input, exit) }
    }

    private fun minPath(
        start: Position,
        input: Grid<Cell>,
        exit: Position
    ): Int {
        return search(
            start,
            { p ->
                p.neighbours(false)
                    .filter { it in input && input[it].height <= input[p].height + 1 }
                    .map { it to 1 }
            },
            { 0 },
            { it == exit }
        ).getOrNull()?.second ?: Int.MAX_VALUE
    }

    sealed interface Cell {
        val height: Char

        data object Start : Cell {
            override val height: Char
                get() = 'a'
        }

        data object Exit : Cell {
            override val height: Char
                get() = 'z'
        }

        data class Simple(override val height: Char) : Cell
    }
}
