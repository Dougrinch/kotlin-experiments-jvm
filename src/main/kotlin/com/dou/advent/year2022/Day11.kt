package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day11().run()
}

class Day11 : TypedDay<List<Day11.Monkey>>() {
    override val test1: Any
        get() = 10605

    override val test2: Any
        get() = 2713310158

    override val parser = parser {
        val id = "Monkey " - int - ":"
        val starting = "  Starting items: " - long.separatedBy(", ")
        val operation = "  Operation: new = old " - oneOf(
            const("+").parseAs<(Long, Long) -> Long>(Long::plus),
            const("*") parseAs Long::times
        ) - " " - { op ->
            oneOf(
                long map { n -> { old: Long -> op(old, n) } },
                const("old") map { { old: Long -> op(old, old) } }
            )
        } map { _, op -> op }
        val divisibleTest = "  Test: divisible by " - long
        val ifTrue = "    If true: throw to monkey " - int
        val ifFalse = "    If false: throw to monkey " - int

        val monkey = id - newLine -
            starting - newLine -
            operation - newLine -
            divisibleTest - newLine -
            ifTrue - newLine -
            ifFalse map ::Monkey

        monkey.separatedByLines(2)
    }

    override fun part1(input: List<Monkey>): Any {
        val monkeys = input.sortedBy { it.id }.toTypedArray<Monkey>()
        val inspections = Array(monkeys.size) { 0 }
        val items = Array(monkeys.size) { id -> ArrayDeque(input[id].starting) }
        for (round in 0 until 20) {
            for (id in items.indices) {
                while (items[id].isNotEmpty()) {
                    inspections[id] += 1
                    val item = items[id].removeFirst()
                    val newItem = monkeys[id].operation(item) / 3
                    val throwTo = if (newItem % monkeys[id].divisibleTest == 0L) {
                        monkeys[id].ifTrue
                    } else {
                        monkeys[id].ifFalse
                    }
                    items[throwTo].addLast(newItem)
                }
            }
        }
        return inspections.sortedDescending().take(2).reduce(Int::times)
    }

    override fun part2(input: List<Monkey>): Any {
        val monkeys = input.sortedBy { it.id }.toTypedArray<Monkey>()
        val inspections = Array(monkeys.size) { 0L }
        val items = Array(monkeys.size) { id -> ArrayDeque(input[id].starting) }
        val limit = monkeys.map { it.divisibleTest }.reduce(Long::times)
        for (round in 0 until 10000) {
            for (id in items.indices) {
                while (items[id].isNotEmpty()) {
                    inspections[id] += 1L
                    val item = items[id].removeFirst()
                    val newItem = monkeys[id].operation(item) % limit
                    val throwTo = if (newItem % monkeys[id].divisibleTest == 0L) {
                        monkeys[id].ifTrue
                    } else {
                        monkeys[id].ifFalse
                    }
                    items[throwTo].addLast(newItem)
                }
            }
        }
        return inspections.sortedDescending().take(2).reduce(Long::times)
    }

    data class Monkey(
        val id: Int,
        val starting: List<Long>,
        val operation: (Long) -> Long,
        val divisibleTest: Long,
        val ifTrue: Int,
        val ifFalse: Int
    )
}
