package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day6().run()
}

class Day6 : TypedDay<List<Char>>() {
    override val test1: Any
        get() = 7

    override val test2: Any
        get() = 19

    override val parser = parser {
        char.many()
    }

    override fun part1(input: List<Char>): Int {
        return solve(input, 4)
    }

    private fun solve(input: List<Char>, count: Int): Int {
        var result = 0
        val chars = ArrayDeque<Char>()
        for (c in input) {
            result += 1
            chars += c
            if (chars.size == count + 1) {
                chars.removeFirst()
            }
            if (chars.distinct().size == count) {
                break
            }
        }
        return result
    }

    override fun part2(input: List<Char>): Any {
        return solve(input, 14)
    }
}
