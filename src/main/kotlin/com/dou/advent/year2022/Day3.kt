package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day3().run()
}

class Day3 : TypedDay<List<String>>() {
    override val test1: Any
        get() = 157

    override val test2: Any
        get() = 70

    override val parser = parser {
        word.separatedByLines()
    }

    override fun part1(input: List<String>): Any {
        return input.sumOf { r ->
            val a = r.substring(0, r.length / 2)
            val b = r.substring(r.length / 2, r.length)

            val code = a.toSet().intersect(b.toSet()).single()
            score(code)
        }
    }

    override fun part2(input: List<String>): Any {
        return input.windowed(3, 3).sumOf { rs ->
            val code = rs[0].toSet().intersect(rs[1].toSet()).intersect(rs[2].toSet()).single()
            score(code)
        }
    }

    private fun score(code: Char): Int {
        return if (code in 'a'..'z') {
            code - 'a' + 1
        } else {
            code - 'A' + 27
        }
    }
}
