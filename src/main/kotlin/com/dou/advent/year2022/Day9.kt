package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.util.Direction
import com.dou.advent.util.Position
import com.dou.parser.*
import kotlin.math.*

fun main() {
    Day9().run()
}

class Day9 : TypedDay<List<Pair<Direction, Int>>>() {
    override val test1: Any
        get() = 13

    override val test2: Any
        get() = 1

    override val parser = parser {
        val dir = char.map { c -> Direction.entries.single { it.name.startsWith(c) } }
        dir - ws - int separatedBy newLine
    }

    override fun part1(input: List<Pair<Direction, Int>>): Int {
        var head = Position(0, 0)
        var tail = Position(0, 0)
        val visited = hashSetOf(tail)
        for ((direction, count) in input) {
            for (ignored in 0 until count) {
                head += direction
                tail = tail.moveTo(head)
                visited += tail
            }
        }
        return visited.size
    }

    private fun Position.moveTo(head: Position): Position {
        val (dx, dy) = head - this
        return if (abs(dx) > 1 || abs(dy) > 1) {
            this + (dx.sign * min(abs(dx), 1) to dy.sign * min(abs(dy), 1))
        } else {
            this
        }
    }

    override fun part2(input: List<Pair<Direction, Int>>): Int {
        val rope = Array(10) { Position(0, 0) }
        val visitedByTail = hashSetOf(rope.last())
        for ((direction, count) in input) {
            for (ignored in 0 until count) {
                rope[0] = rope[0] + direction
                for (i in 1 until rope.size) {
                    rope[i] = rope[i].moveTo(rope[i - 1])
                }
                visitedByTail += rope.last()
            }
        }
        return visitedByTail.size
    }
}
