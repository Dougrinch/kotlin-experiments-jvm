package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day13().run()
}

class Day13 : TypedDay<List<Pair<Day13.Data, Day13.Data>>>() {

    private val data: Parser<Data> by parser {
        oneOf(
            "[" - data.many(",") - "]" map Data::DataList,
            int map Data::DataInt
        )
    }

    override val parser by parser {
        (data - "\n" - data).separatedBy("\n\n")
    }

    sealed interface Data {
        data class DataList(val data: List<Data>) : Data
        data class DataInt(val value: Int) : Data
    }

    override val test1: Any
        get() = 13

    override val test2: Any
        get() = 140

    override fun part1(input: List<Pair<Data, Data>>): Any {
        return input.withIndex()
            .filter { (_, p) -> Comp.compare(p.first, p.second) <= 0 }
            .sumOf { it.index + 1 }
    }

    override fun part2(input: List<Pair<Data, Data>>): Any {
        val dp1 = Data.DataList(listOf(Data.DataList(listOf(Data.DataInt(2)))))
        val dp2 = Data.DataList(listOf(Data.DataList(listOf(Data.DataInt(6)))))

        return input
            .flatMap { listOf(it.first, it.second) }
            .let { it + listOf(dp1, dp2) }
            .sortedWith(Comp)
            .let { (it.indexOf(dp1) + 1) * (it.indexOf(dp2) + 1) }
    }

    data object Comp : Comparator<Data> {
        override fun compare(o1: Data, o2: Data): Int {
            return when (o1) {
                is Data.DataInt -> {
                    when (o2) {
                        is Data.DataInt -> {
                            o1.value.compareTo(o2.value)
                        }

                        is Data.DataList -> {
                            compare(Data.DataList(listOf(Data.DataInt(o1.value))), o2)
                        }
                    }
                }

                is Data.DataList -> {
                    when (o2) {
                        is Data.DataInt -> {
                            compare(o1, Data.DataList(listOf(Data.DataInt(o2.value))))
                        }

                        is Data.DataList -> {
                            run {
                                for (i in 0 until minOf(o1.data.size, o2.data.size)) {
                                    val r = compare(o1.data[i], o2.data[i])
                                    if (r != 0) {
                                        return@run r
                                    }
                                }
                                o1.data.size.compareTo(o2.data.size)
                            }
                        }
                    }
                }
            }
        }
    }
}
