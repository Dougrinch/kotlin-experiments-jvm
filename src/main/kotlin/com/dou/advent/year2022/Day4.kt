package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day4().run()
}

class Day4 : TypedDay<List<Pair<IntRange, IntRange>>>() {
    override val test1: Any
        get() = 2

    override val test2: Any
        get() = 4

    override val parser = parser {
        val rangeParser = int - "-" - int map { f, s -> f..s }
        (rangeParser - "," - rangeParser).separatedByLines()
    }

    override fun part1(input: List<Pair<IntRange, IntRange>>): Any {
        return input.count { (a, b) ->
            b in a || a in b
        }
    }

    override fun part2(input: List<Pair<IntRange, IntRange>>): Any {
        return input.count { (a, b) ->
            a.hasIntersection(b)
        }
    }

    private operator fun IntRange.contains(o: IntRange): Boolean {
        return this.first <= o.first && this.last >= o.last
    }

    private fun IntRange.hasIntersection(o: IntRange): Boolean {
        if (this.first > o.last) return false
        if (o.first > this.last) return false
        return true
    }
}
