package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.year2022.Day10.Cmd
import com.dou.advent.year2022.Day10.Cmd.AddX
import com.dou.advent.year2022.Day10.Cmd.Noop
import com.dou.parser.*

fun main() {
    Day10().run()
}

class Day10 : TypedDay<List<Cmd>>() {
    override val test1: Any
        get() = 13140

    override val test2: Any
        get() = """
            
            ##..##..##..##..##..##..##..##..##..##..
            ###...###...###...###...###...###...###.
            ####....####....####....####....####....
            #####.....#####.....#####.....#####.....
            ######......######......######......####
            #######.......#######.......#######.....
        """.trimIndent()

    override val parser = parser {
        oneOf(
            "addx " - int map ::AddX,
            const("noop") parseAs Noop
        ).separatedByLines()
    }

    override fun part1(input: List<Cmd>): Any {
        var cycle = 0
        var x = 1
        var ptr = 0
        var pending: AddX? = null

        var result = 0

        while (true) {
            cycle += 1
            if ((cycle - 20) % 40 == 0) {
                result += cycle * x
            }
            if (pending == null) {
                if (ptr !in input.indices) {
                    break
                }
                when (val cmd = input[ptr]) {
                    is Noop -> {}
                    is AddX -> {
                        pending = cmd
                    }
                }
                ptr += 1
            } else {
                x += pending.v
                pending = null
            }
        }

        return result
    }

    override fun part2(input: List<Cmd>): String {
        var cycle = 0
        var x = 1
        var ptr = 0
        var pending: AddX? = null

        var result = ""
        val row = arrayListOf<Char>()

        while (true) {
            cycle += 1

            if ((row.size + 1) in x..x + 2) {
                row += '#'
            } else {
                row += '.'
            }

            if (row.size == 40) {
                result += "\n${row.joinToString(separator = "")}"
                row.clear()
            }

            if (pending == null) {
                if (ptr !in input.indices) {
                    break
                }
                when (val cmd = input[ptr]) {
                    is Noop -> {}
                    is AddX -> {
                        pending = cmd
                    }
                }
                ptr += 1
            } else {
                x += pending.v
                pending = null
            }
        }

        return result
    }

    sealed interface Cmd {
        data class AddX(val v: Int) : Cmd
        data object Noop : Cmd
    }
}
