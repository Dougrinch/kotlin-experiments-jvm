package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction8.*
import com.dou.parser.*

fun main() {
    Day14().run()
}

class Day14 : TypedDay<List<List<Position>>>() {

    override val parser by parser {
        val point = int - "," - int map ::Position
        point separatedBy " -> " separatedBy "\n"
    }

    override val test1: Any
        get() = 24

    override val test2: Any
        get() = 93

    override fun part1(input: List<List<Position>>): Any {
        val (offset, grid) = createGrid(input)

        val dropFrom = Position(500 + offset.first, 0)

        while (dropNext(dropFrom, grid, null)) {
        }

        return grid.values().count { it == 'o' }
    }

    override fun part2(input: List<List<Position>>): Any {
        val (offset, grid) = createGrid(input)

        val dropFrom = Position(500, 0) + offset

        while (grid[dropFrom] == '.' && dropNext(dropFrom, grid, grid.sizeY - 1)) {
        }

        return grid.values().count { it == 'o' }
    }

    private fun createGrid(input: List<List<Position>>): Pair<Pair<Int, Int>, MutableGrid<Char>> {
        val minX = input.flatten().minOf { it.x } - 1000
        val maxX = input.flatten().maxOf { it.x } + 1000
        val minY = input.flatten().minOf { it.y } - 20
        val maxY = input.flatten().maxOf { it.y } + 2

        val offset = -minX to -minY

        val grid = Grid(maxX - minX + 1, maxY - minY + 1) { '.' }.toMutableGrid()
        for (line in input) {
            var current = line[0]
            grid[current + offset] = '#'
            for (i in 1 until line.size) {
                val last = line[i]
                val v = (last - current).norm()
                while (current != last) {
                    current += v
                    grid[current + offset] = '#'
                }
                grid[current + offset] = '#'
            }
        }
        return Pair(offset, grid)
    }

    private fun dropNext(from: Position, map: MutableGrid<Char>, floor: Int?): Boolean {
        var current = from
        map[current] = 'o'

        fun tryMoveTo(dir: Direction8): Boolean {
            val next = current + dir
            if (next in map && (map[next] != '.' || next.y == floor)) {
                return false
            }
            map[current] = '.'
            current = next
            if (current in map) {
                map[current] = 'o'
            }
            return true
        }

        while (current in map) {
            if (tryMoveTo(Down)) {
                continue
            }
            if (tryMoveTo(DownLeft)) {
                continue
            }
            if (tryMoveTo(DownRight)) {
                continue
            }
            return true
        }

        return false
    }
}
