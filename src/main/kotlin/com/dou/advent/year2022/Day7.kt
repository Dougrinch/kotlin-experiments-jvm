package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.advent.year2022.Day7.Cmd
import com.dou.advent.year2022.Day7.Cmd.Cd
import com.dou.advent.year2022.Day7.Cmd.Ls
import com.dou.advent.year2022.Day7.Node.Directory
import com.dou.advent.year2022.Day7.Node.File
import com.dou.parser.*

fun main() {
    Day7().run()
}

class Day7 : TypedDay<List<Cmd>>() {
    override val test1: Any
        get() = 95437

    override val test2: Any
        get() = 24933642

    override val parser = parser {
        val file = long - " " - regex("\\w+(\\.\\w+)?") map { (size, name) -> File(name, size) }
        val directory = "dir " - word map ::Directory

        val ls = "\$ ls\n" - oneOf(file, directory).separatedByLines() map ::Ls

        val cd = "\$ cd " - oneOf(string("/"), string(".."), word) map ::Cd

        oneOf(cd, ls).separatedByLines()
    }

    override fun part1(input: List<Cmd>): Long {
        val root = buildTree(input)
        return sizes(root).filter { it.second <= 100000 }.sumOf { it.second }
    }

    override fun part2(input: List<Cmd>): Long {
        val root = buildTree(input)
        val sizes = sizes(root)
        val free = 70000000 - sizes.maxOf { it.second }
        val required = 30000000 - free
        return sizes.filter { it.second >= required }.minOf { it.second }
    }

    private fun sizes(root: Tree): List<Pair<Tree, Long>> {
        val result = arrayListOf<Pair<Tree, Long>>()
        fun calcSize(dir: Tree): Long {
            var size = 0L
            for (file in dir.files) {
                size += file.size
            }
            for (subDir in dir.subDirs.values) {
                size += calcSize(subDir)
            }
            result += dir to size
            return size
        }
        calcSize(root)
        return result
    }

    private fun buildTree(input: List<Cmd>): Tree {
        val roots = hashMapOf<String, Tree>()
        val currentPath = arrayListOf<String>()
        for (cmd in input) {
            when (cmd) {
                is Cd -> {
                    when (cmd.dst) {
                        "/" -> {
                            currentPath.clear()
                            currentPath += "/"
                        }

                        ".." -> currentPath.removeLast()
                        else -> currentPath += cmd.dst
                    }
                }

                is Ls -> {
                    val root = roots.computeIfAbsent(currentPath[0]) { Tree(it, arrayListOf(), hashMapOf()) }
                    val node = currentPath.subList(1, currentPath.size).fold(root) { parent, name ->
                        (parent.subDirs as HashMap).computeIfAbsent(name) { Tree(it, arrayListOf(), hashMapOf()) }
                    }
                    (node.files as ArrayList).addAll(cmd.content.filterIsInstance<File>())
                }
            }
        }
        return roots.values.single()
    }

    data class Tree(val name: String, val files: List<File>, val subDirs: Map<String, Tree>)

    sealed interface Cmd {
        data class Cd(val dst: String) : Cmd

        data class Ls(val content: List<Node>) : Cmd
    }

    sealed interface Node {
        data class File(val name: String, val size: Long) : Node
        data class Directory(val name: String) : Node
    }
}
