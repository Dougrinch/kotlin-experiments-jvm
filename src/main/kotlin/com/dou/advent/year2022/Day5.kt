package com.dou.advent.year2022

import com.dou.advent.TypedDay
import com.dou.parser.*
import java.util.*

fun main() {
    Day5().run()
}

class Day5 : TypedDay<Pair<List<Deque<Char>>, List<Command>>>() {
    override val test1: Any
        get() = "CMZ"

    override val test2: Any
        get() = "MCD"

    override val parser by parser {
        val crate by oneOf(
            "   " parseAs null,
            "[" - char - "]"
        )
        val crates by crate.many(" ", min = 1).separatedByLines()
            .map { lines ->
                val result = lines[lines.size - 2].map { ArrayDeque<Char>() }

                for (line in lines) {
                    for ((i, c) in line.withIndex()) {
                        if (c != null) {
                            result[i].addFirst(c)
                        }
                    }
                }

                result
            }

        val skip by "\n " - digit.separatedBy("   ").ignoreValue() - " \n\n"

        val cmd by "move " - int - " from " - int - " to " - int map (::Command)

        crates - skip - cmd.separatedByLines()
    }

    override fun part1(input: Pair<List<Deque<Char>>, List<Command>>): Any {
        val (state, commands) = input
        for (command in commands) {
            repeat(command.amount) {
                val crate = state[command.from - 1].removeLast()
                state[command.to - 1].addLast(crate)
            }
        }
        return state.map { it.last }.joinToString(separator = "")
    }

    override fun part2(input: Pair<List<Deque<Char>>, List<Command>>): Any {
        val (state, commands) = input
        for (command in commands) {
            val rs = mutableListOf<Char>()
            repeat(command.amount) {
                rs += state[command.from - 1].removeLast()
            }
            for (r in rs.asReversed()) {
                state[command.to - 1].addLast(r)
            }
        }
        return state.map { it.last }.joinToString(separator = "")
    }
}

data class Command(val amount: Int, val from: Int, val to: Int)
