package com.dou.advent.year2017

import com.dou.advent.util.repeatPB
import com.dou.advent.util.ringListIterator
import java.util.*

fun main() {
    Day17().run()
}

class Day17 {
    fun run() {
        val step = 314

        val buffer = LinkedList<Int>()
        buffer += 0

        var value = 1
        val pos = buffer.ringListIterator()
        repeatPB(50000000) {
            repeat(step) {
                pos.next()
            }
            pos.add(value)
            value += 1
        }

        println(buffer[buffer.indexOf(0) + 1])
    }
}
