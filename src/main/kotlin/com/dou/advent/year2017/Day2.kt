package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day2().run()
}

class Day2 : TypedDay<List<List<Int>>>() {
    override val parser by parser {
        int separatedBy (const(" ") or const("\t")) separatedBy "\n"
    }

    override fun part1(input: List<List<Int>>): Any {
        return input.sumOf { it.maxOf { it } - it.minOf { it } }
    }

    override fun part2(input: List<List<Int>>): Any {
        return input.sumOf {
            it.flatMap { a -> it.filter { b -> a != b && a % b == 0 }.map { a to it } }
                .map { (a, b) -> a / b }
                .single()
        }
    }
}
