package com.dou.advent.year2017

import com.dou.advent.util.Position
import kotlin.math.abs

fun main() {
    Day3().run()
}

class Day3 {
    fun run() {
        val n = 361527
        part1(n)
        part2(n)
    }

    private fun part1(n: Int) {
        val p = spiral().take(n).last()
        val r = abs(p.x) + abs(p.y)
        println(r)
    }

    private fun part2(n: Int) {
        val data = mutableMapOf<Position, Int>().withDefault { 0 }
        data[Position(0, 0)] = 1
        for (pos in spiral().drop(1)) {
            val v = pos.neighbours(diagonal = true).sumOf { data.getValue(it) }
            if (v > n) {
                println(v)
                return
            }
            data[pos] = v
        }
    }

    private fun spiral(): Sequence<Position> {
        return sequence {
            var level = 1
            var current = Position(0, 0)
            yield(current)

            while (true) {
                level += 2
                current += 1 to 0
                yield(current)
                for (i in 1..(level - 2)) {
                    current += 0 to 1
                    yield(current)
                }
                for (i in 1 until level) {
                    current += -1 to 0
                    yield(current)
                }
                for (i in 1 until level) {
                    current += 0 to -1
                    yield(current)
                }
                for (i in 1 until level) {
                    current += 1 to 0
                    yield(current)
                }
            }
        }
    }
}
