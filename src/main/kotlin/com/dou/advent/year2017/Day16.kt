package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.year2017.Day16.Op
import com.dou.parser.*

fun main() {
    Day16().run()
}

class Day16 : TypedDay<Pair<String, List<Op>>>() {
    override val test1: Any
        get() = "baedc"

    override val parser by parser {
        val n by char('a'..'p')
        val p by int
        val op by oneOf(
            "s" - p map ::Spin,
            "x" - p - "/" - p map ::Exchange,
            "p" - n - "/" - n map ::Partner
        )
        word - "\n" - op.separatedBy(",")
    }

    override fun part1(input: Pair<String, List<Op>>): Any {
        return process(input.first.toMutableList(), input.second).joinToString("")
    }

    private fun process(src: List<Char>, ops: List<Op>): MutableList<Char> {
        var result = src.toMutableList()
        for (op in ops) {
            when (op) {
                is Spin -> {
                    result = (result.takeLast(op.n) + result.dropLast(op.n)).toMutableList()
                }
                is Exchange -> {
                    val v = result[op.a]
                    result[op.a] = result[op.b]
                    result[op.b] = v
                }
                is Partner -> {
                    val i = result.indexOf(op.b)
                    result[result.indexOf(op.a)] = op.b
                    result[i] = op.a
                }
            }
        }
        return result
    }


    override fun part2(input: Pair<String, List<Op>>): Any {
        val initial = input.first.toList()
        val period = generateSequence(initial) { process(it, input.second) }
            .withIndex()
            .drop(1)
            .first { (_, l) -> l == initial }
            .index

        var result = input.first.toMutableList()
        for (i in 0 until 1_000_000_000 % period) {
            result = process(result, input.second)
        }
        return result.joinToString("")
    }

    sealed interface Op
    data class Spin(val n: Int) : Op
    data class Exchange(val a: Int, val b: Int) : Op
    data class Partner(val a: Char, val b: Char) : Op
}
