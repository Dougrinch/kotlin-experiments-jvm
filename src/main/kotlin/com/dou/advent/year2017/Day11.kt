package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.util.HexDirection
import com.dou.advent.util.HexDirection.*
import com.dou.advent.util.HexPosition
import com.dou.parser.*

fun main() {
    Day11().run()
}

class Day11 : TypedDay<List<HexDirection>>() {
    override val parser by parser {
        val dir = oneOf(
            "ne" parseAs NorthEast,
            "se" parseAs SouthEast,
            "sw" parseAs SouthWest,
            "nw" parseAs NorthWest,
            "n" parseAs North,
            "s" parseAs South
        )
        dir.separatedBy(",")
    }

    override fun part1(input: List<HexDirection>): Any {
        val from = HexPosition(0, 0)
        val to = input.fold(from, HexPosition::plus)
        return from.distanceTo(to)
    }

    override fun part2(input: List<HexDirection>): Any {
        val from = HexPosition(0, 0)
        return input.fold(from to 0) { (pos, res), dir ->
            val t = pos + dir
            t to maxOf(res, from.distanceTo(t))
        }.second
    }
}
