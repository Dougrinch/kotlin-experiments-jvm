package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.year2017.Day9.Group
import com.dou.parser.*

fun main() {
    Day9().run()
}

class Day9 : TypedDay<Group>() {
    private val garbage by "<" - regex("(!.|[^>])*") - ">" map ::Garbage
    private val group: Parser<Group> by parser {
        "{" - oneOf(garbage, group).separatedBy(",") - "}" map ::Group
    }

    override val parser by group

    override fun part1(input: Group): Any {
        return totalScore(0, input)
    }

    private fun totalScore(p: Int, group: Group): Int {
        return p + 1 + group.groups.filterIsInstance<Group>().sumOf { totalScore(p + 1, it) }
    }

    override fun part2(input: Group): Any {
        return totalGarbageScore(input)
    }

    private fun totalGarbageScore(group: Group): Int {
        val g = group.groups.filterIsInstance<Garbage>().sumOf { (data) ->
            var i = 0
            var r = 0
            while (i < data.length) {
                if (data[i] != '!') {
                    i += 1
                    r += 1
                } else {
                    i += 2
                }
            }
            r
        }
        return g + group.groups.filterIsInstance<Group>().sumOf { totalGarbageScore(it) }
    }

    sealed interface Node
    data class Group(val groups: List<Node>) : Node
    data class Garbage(val data: String) : Node
}
