package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day7().run()
}

class Day7 : TypedDay<List<Program>>() {
    override val test1: Any
        get() = "tknk"
    override val test2: Any
        get() = 60

    override val parser by parser {
        val children by " -> " - word.separatedBy(", ") or succeed(listOf())
        word - " (" - int - ")" - children map ::Program separatedBy "\n"
    }

    override fun part1(input: List<Program>): Any {
        val parents = input.flatMap { (n, _, c) -> c.map { it to n } }.toMap()
        return input.single { it.name !in parents.keys }.name
    }

    override fun part2(input: List<Program>): Any {
        val parents = input.flatMap { (n, _, c) -> c.map { it to n } }.toMap()
        val root = input.single { it.name !in parents.keys }
        val programs = input.associateBy { it.name }
        val weights = buildMap { root.weights(programs, this) }
        val a = input
            .filter { it.children.isNotEmpty() }
            .map { it.children.map { it to weights[it]!! } }
            .filter { it.map { it.second }.distinct().size != 1 }
            .map {
                val target = it.groupBy({ it.second }, { it.first }).filterValues { it.size > 1 }.keys.single()
                val errored = it.groupBy({ it.second }, { it.first }).filterValues { it.size == 1 }.entries.single()
                val delta = target - errored.key
                programs[errored.value.single()]!!.weight + delta
            }
        return a.minOf { it }
    }

    private fun Program.weights(ps: Map<String, Program>, result: MutableMap<String, Int>): Int {
        result[name]?.let { return it }

        val r = weight + children.sumOf { ps[it]!!.weights(ps, result) }
        result[name] = r
        return r
    }
}

data class Program(val name: String, val weight: Int, val children: List<String>)
