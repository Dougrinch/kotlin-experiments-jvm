package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.year2017.Day21.Rule
import com.dou.parser.*

fun main() {
    Day21().run()
}

class Day21 : TypedDay<List<Rule>>() {
    override val parser by parser {
        val pixel by char('#', '.')

        val grid by grid(pixel, rowSep = "/")
        val rule by grid - " => " - grid map ::Rule

        rule.separatedByLines()
    }

    override fun part1(input: List<Rule>): Any {
        val rules = input.flatMap { rule ->
            generateSequence(rule.from) { it.rotate() }.take(4)
                .flatMap { listOf(it, it.flipVertical(), it.flipHorizontal()) }
                .toSet()
                .map { Rule(it, rule.to) }
        }.associateBy { it.from }

        var grid = Grid(
            listOf(
                listOf('.', '#', '.'),
                listOf('.', '.', '#'),
                listOf('#', '#', '#')
            )
        )

        repeat(5) {
            println(grid)
            println()
            grid = if (grid.sizeX % 2 == 0) {
                grid.splitBy(2).map { rules[it]!!.to }.merge()
            } else if (grid.sizeX % 3 == 0) {
                grid.splitBy(3).map { rules[it]!!.to }.merge()
            } else {
                throw IllegalStateException()
            }
        }

        println(grid)

        return grid.values().count { it == '#' }
    }

    data class Rule(val from: Grid<Char>, val to: Grid<Char>)

    private fun <T> Grid<T>.rotate(): Grid<T> {
        return Grid(ys.map { y -> xs.map { x -> get(Position(y, sizeX - x - 1)) } })
    }

    private fun <T> Grid<T>.flipHorizontal(): Grid<T> {
        return Grid(ys.map { y -> xs.map { x -> get(Position(x, sizeY - 1 - y)) } })
    }

    private fun <T> Grid<T>.flipVertical(): Grid<T> {
        return Grid(ys.map { y -> xs.map { x -> get(Position(sizeX - 1 - x, y)) } })
    }

    private fun <T> Grid<T>.splitBy(n: Int): Grid<Grid<T>> {
        return (0 until sizeX / n).map { dy ->
            (0 until sizeX / n).map { dx ->
                Grid((0 until n).map { y -> (0 until n).map { x -> get(Position(dx * 2 + x, dy * 2 + y)) } })
            }
        }.let { Grid(it) }
    }

    private fun <T> Grid<Grid<T>>.merge(): Grid<T> {
        val s = values().first().sizeX
        return (0 until sizeX * s).map { y ->
            (0 until sizeX * s).map { x ->
                get(Position(x / s, y / s))[Position(x % s, y % s)]
            }
        }.let { Grid(it) }
    }
}
