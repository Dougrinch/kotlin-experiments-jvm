package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day4().run()
}

class Day4 : TypedDay<List<List<String>>>() {
    override val parser by parser {
        word.separatedBy(" ").separatedByLines()
    }

    override fun part1(input: List<List<String>>): Any {
        return input.count { it.toSet().size == it.size }
    }

    override fun part2(input: List<List<String>>): Any {
        return input.count {
            val c = it.map { it.length to it.toSet() }
            c.toSet().size == c.size
        }
    }
}
