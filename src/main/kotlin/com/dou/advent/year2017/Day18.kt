package com.dou.advent.year2017

import com.dou.advent.Day

fun main() {
    Day18().run()
}

class Day18 : Day() {
    override val test1: Any
        get() = 4

    override fun part1(input: Sequence<String>): Any {
        val outbox = ArrayDeque<Long>()
        Program(0, input.toList(), 0, ArrayDeque(), outbox, true).run()
        return outbox.last()
    }

    override fun part2(input: Sequence<String>): Any {
        val ops = input.toList()
        val zeroToOne = ArrayDeque<Long>()
        val oneToZero = ArrayDeque<Long>()
        val p0 = Program(0, ops, 0, oneToZero, zeroToOne, false)
        val p1 = Program(1, ops, 0, zeroToOne, oneToZero, false)
        while (true) {
            val p0r = p0.run()
            val p1r = p1.run()

            if (p0r && p1r) {
                break
            }

            if (!p0r && !p1r && zeroToOne.isEmpty() && oneToZero.isEmpty()) {
                break
            }
        }

        return p1.sends
    }

    private class Program(
        private val pId: Int,
        private val ops: List<String>,
        private var pointer: Int,
        private val inbox: ArrayDeque<Long>,
        private val outbox: ArrayDeque<Long>,
        private val part1: Boolean
    ) {
        private val registers = hashMapOf<String, Long>().withDefault { 0 }.apply {
            put("p", pId.toLong())
        }

        var sends: Int = 0

        fun run(): Boolean {
            while (pointer < ops.size) {
                val op = ops[pointer]
                val cmd = op.take(3)
                val o = op.drop(4).split(" ")
                when (cmd) {
                    "snd" -> {
                        sends += 1
                        outbox.addLast(o[0](registers))
                    }
                    "set" -> registers[o[0]] = o[1](registers)
                    "add" -> registers[o[0]] = registers.getValue(o[0]) + o[1](registers)
                    "mul" -> registers[o[0]] = registers.getValue(o[0]) * o[1](registers)
                    "mod" -> registers[o[0]] = registers.getValue(o[0]) % o[1](registers)
                    "rcv" -> {
                        if (part1) {
                            if (o[0](registers) != 0L) return true
                        } else {
                            if (inbox.isNotEmpty()) {
                                registers[o[0]] = inbox.removeFirst()
                            } else {
                                return false
                            }
                        }
                    }
                    "jgz" -> if (o[0](registers) > 0) pointer += o[1](registers).toInt() - 1
                }
                pointer += 1
            }

            return true
        }

        private operator fun String.invoke(r: Map<String, Long>): Long {
            return if (length == 1 && first().isLetter()) {
                r.getValue(this)
            } else {
                toLong()
            }
        }
    }
}
