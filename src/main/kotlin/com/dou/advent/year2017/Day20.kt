package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.year2017.Day20.Particle
import com.dou.math.*
import com.dou.math.space.Point3D
import com.dou.math.space.Vector3D
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day20().run()
}

class Day20 : TypedDay<List<Particle>>(), IntOps {
    override val parser by parser {
        val pos by "<" - int - "," - int - "," - int - ">" map ::Point3D
        val vect by "<" - int - "," - int - "," - int - ">" map ::Vector3D

        val particle by "p=" - pos - ", v=" - vect - ", a=" - vect map ::Particle
        particle.separatedByLines()
    }

    override fun part1(input: List<Particle>): Any {
        var particles = input
        var result = -1
        var stable = 0

        while (stable < 2000) {
            particles = particles.map { p ->
                val nv = p.v + p.a
                val np = p.p + nv
                Particle(np, nv, p.a)
            }

            val min = particles.withIndex()
                .minByOrNull { (_, it) -> abs(it.p.x) + abs(it.p.y) + abs(it.p.z) }!!
                .index

            if (min == result) {
                stable += 1
            } else {
                result = min
                stable = 0
            }
        }

        return result
    }

    override fun part2(input: List<Particle>): Any {
        var particles = input
        var stable = 0

        while (stable < 2000) {
            particles = particles.map { p ->
                val nv = p.v + p.a
                val np = p.p + nv
                Particle(np, nv, p.a)
            }

            val withoutCollision = particles.groupBy { it.p }
                .flatMap { (_, ps) -> if (ps.size == 1) ps else emptyList() }

            if (withoutCollision.size == particles.size) {
                stable += 1
            } else {
                stable = 0
                particles = withoutCollision
            }
        }

        return particles.size
    }

    data class Particle(val p: IntPoint3D, val v: IntVector3D, val a: IntVector3D)
}
