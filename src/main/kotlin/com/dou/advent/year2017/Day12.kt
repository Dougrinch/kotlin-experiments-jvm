package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.util.removeAny
import com.dou.parser.*

fun main() {
    Day12().run()
}

class Day12 : TypedDay<List<Pair<Int, List<Int>>>>() {
    override val test1: Any
        get() = 6
    override val test2: Any
        get() = 2

    override val parser by parser {
        val p = int - " <-> " - int.separatedBy(", ")
        p.separatedByLines()
    }

    override fun part1(input: List<Pair<Int, List<Int>>>): Any {
        return input.toMap().groupWith(0).size
    }

    override fun part2(input: List<Pair<Int, List<Int>>>): Any {
        val groups = mutableListOf<Set<Int>>()
        val programs = input.toMap(mutableMapOf())
        while (programs.isNotEmpty()) {
            val group = programs.groupWith(programs.keys.first())
            groups += group
            programs -= group
        }
        return groups.size
    }

    private fun Map<Int, List<Int>>.groupWith(n: Int): Set<Int> {
        val result = mutableSetOf<Int>()
        val unprocessed = mutableSetOf<Int>()
        result += 0
        unprocessed += this[n]!!
        while (unprocessed.isNotEmpty()) {
            val v = unprocessed.removeAny()
            if (result.add(v)) {
                unprocessed += this[v]!!.filter { it !in result }
            }
        }
        return result
    }
}
