package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.util.Registers
import com.dou.advent.year2017.Day8.Instruction
import com.dou.parser.*

fun main() {
    Day8().run()
}

class Day8 : TypedDay<List<Instruction>>() {
    override val test1: Any
        get() = 1
    override val test2: Any
        get() = 10

    override val parser by parser {
        val r by word
        val v by int
        val cond by oneOf(
            attempt(r - " > ") - v map ::GreaterThan,
            attempt(r - " >= ") - v map ::GreaterThanOrEquals,
            attempt(r - " < ") - v map ::LessThan,
            attempt(r - " <= ") - v map ::LessThanOrEquals,
            attempt(r - " == ") - v map ::Equals,
            attempt(r - " != ") - v map ::NotEquals
        )
        val instr by oneOf(
            attempt(r - " inc ") - v - " if " - cond map ::Increment,
            attempt(r - " dec ") - v - " if " - cond map ::Decrement
        )
        instr.separatedByLines()
    }

    override fun part1(input: List<Instruction>): Any {
        val registers = Registers()
        input.forEach { it.run(registers) }
        return registers.maxOf { it.second }
    }

    override fun part2(input: List<Instruction>): Any {
        val registers = Registers()
        var max = 0
        input.forEach {
            it.run(registers)
            max = maxOf(max, registers.maxOfOrNull { it.second } ?: 0)
        }
        return max
    }

    sealed interface Condition {
        fun test(registers: Registers): Boolean
    }

    data class GreaterThan(val r: String, val v: Int) : Condition {
        override fun test(registers: Registers): Boolean {
            return registers[r] > v
        }
    }

    data class GreaterThanOrEquals(val r: String, val v: Int) : Condition {
        override fun test(registers: Registers): Boolean {
            return registers[r] >= v
        }
    }

    data class LessThan(val r: String, val v: Int) : Condition {
        override fun test(registers: Registers): Boolean {
            return registers[r] < v
        }
    }

    data class LessThanOrEquals(val r: String, val v: Int) : Condition {
        override fun test(registers: Registers): Boolean {
            return registers[r] <= v
        }
    }

    data class Equals(val r: String, val v: Int) : Condition {
        override fun test(registers: Registers): Boolean {
            return registers[r] == v
        }
    }

    data class NotEquals(val r: String, val v: Int) : Condition {
        override fun test(registers: Registers): Boolean {
            return registers[r] != v
        }
    }

    sealed interface Instruction {
        fun run(registers: Registers)
    }

    data class Increment(val r: String, val v: Int, val cond: Condition) : Instruction {
        override fun run(registers: Registers) {
            if (cond.test(registers)) {
                registers[r] += v
            }
        }
    }

    data class Decrement(val r: String, val v: Int, val cond: Condition) : Instruction {
        override fun run(registers: Registers) {
            if (cond.test(registers)) {
                registers[r] -= v
            }
        }
    }
}
