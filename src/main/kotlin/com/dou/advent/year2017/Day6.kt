package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day6().run()
}

class Day6 : TypedDay<List<Int>>() {
    override val test1: Any
        get() = 5
    override val test2: Any
        get() = 4

    override val parser by parser {
        int separatedBy ws
    }

    override fun part1(input: List<Int>): Any {
        var banks = input
        val seen = mutableSetOf(banks)
        var i = 0
        while (true) {
            i += 1
            banks = banks.next()
            if (banks in seen) {
                return i
            }
            seen += banks
        }
    }

    override fun part2(input: List<Int>): Any {
        var banks = input
        val seen = mutableMapOf(banks to 0)
        var i = 0
        while (true) {
            i += 1
            banks = banks.next()
            if (banks in seen) {
                return i - seen[banks]!!
            }
            seen += banks to i
        }
    }

    private fun List<Int>.next(): List<Int> {
        var r = this.toMutableList()
        var v = r.maxOf { it }
        var i = r.indexOfFirst { it == v }
        r[i] = 0
        while (v > 0) {
            v -= 1
            i += 1
            if (i == r.size) {
                i = 0
            }
            r[i] += 1
        }
        return r
    }
}
