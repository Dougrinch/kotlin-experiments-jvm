package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day13().run()
}

class Day13 : TypedDay<List<Pair<Int, Int>>>() {
    override val test1: Any
        get() = 24
    override val test2: Any
        get() = 10

    override val parser by parser {
        val scanner = int - ": " - int
        scanner.separatedByLines()
    }

    override fun part1(input: List<Pair<Int, Int>>): Any {
        return input.invalid().sumOf { (depth, range) -> depth * range }
    }

    override fun part2(input: List<Pair<Int, Int>>): Any {
        var delay = 0
        while (input.invalid(delay).isNotEmpty()) {
            delay += 1
        }
        return delay
    }

    private fun List<Pair<Int, Int>>.invalid(delay: Int = 0): List<Pair<Int, Int>> {
        return filter { (depth, range) ->
            val steps = range - 1
            val loop = steps * 2
            val pos = (depth + delay) % loop
            steps - abs(steps - pos) == 0
        }
    }
}
