package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day10().run()
}

class Day10 : TypedDay<Pair<Int, List<Int>>>() {
    override val test1: Any
        get() = 12

    override val parser by parser {
        int - "\n" - int.separatedBy(",")
    }

    override fun part1(input: Pair<Int, List<Int>>): Any {
        val ring = (0 until input.first).toMutableList()
        val lengths = input.second
        var i = 0
        var skip = 0
        for (length in lengths) {
            ring.reverse(i, length)
            i += length + skip
            i %= ring.size
            skip += 1
        }
        return ring[0] * ring[1]
    }

    override fun part2(input: Pair<Int, List<Int>>): Any {
        return knotHash(input.second.joinToString(",") { "$it" })
    }

    fun knotHash(str: String): String {
        val lengths = str.map { it.code } + listOf(17, 31, 73, 47, 23)
        val ring = (0 until 256).toMutableList()
        var i = 0
        var skip = 0
        repeat(64) {
            for (length in lengths) {
                ring.reverse(i, length)
                i += length + skip
                i %= ring.size
                skip += 1
            }
        }
        return ring.chunked(16).map { it.reduce { a, b -> a xor b }.toString(16).padStart(2, '0') }.joinToString("")
    }

    private fun MutableList<Int>.reverse(from: Int, length: Int) {
        if (length == 0) {
            return
        }
        if (length > size) {
            throw IllegalStateException()
        }
        val to = (from + length - 1) % size
        if (to >= from) {
            for (i in 0 until length / 2) {
                val v = this[from + i]
                this[from + i] = this[to - i]
                this[to - i] = v
            }
        } else {
            val vs = (subList(from, size) + subList(0, to + 1)).reversed()
            for (i in vs.indices) {
                this[(i + from) % size] = vs[i]
            }
        }
    }
}
