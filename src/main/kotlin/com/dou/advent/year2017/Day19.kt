package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction.Down
import com.dou.advent.year2017.Day19.Cell
import com.dou.parser.*

fun main() {
    Day19().run()
}

class Day19 : TypedDay<Grid<Cell>>() {
    override val test1: Any
        get() = "ABCDEF"

    override val parser by parser {
        val cell by oneOf(
            " " parseAs Empty,
            "|" parseAs UpDown,
            "-" parseAs LeftRight,
            "+" parseAs Turn,
            char map ::Letter
        )

        grid(cell, tailPadding = Empty)
    }

    override fun part1(input: Grid<Cell>): Any {
        return go(input).first
    }

    override fun part2(input: Grid<Cell>): Any {
        return go(input).second
    }

    private fun go(grid: Grid<Cell>): Pair<String, Int> {
        var dir = Down
        var pos = Position(grid.xs.single { grid[Position(it, 0)] == UpDown }, 0)
        val result = ArrayList<Char>()
        var steps = 0
        do {
            steps += 1
            if (grid[pos] is Letter) {
                result += (grid[pos] as Letter).value
            }

            dir = when (grid[pos]) {
                UpDown, LeftRight, is Letter -> dir
                Turn -> listOf(dir.rotateClockwise(), dir.rotateCounterclockwise())
                    .filter { pos + it in grid }
                    .single { grid[pos + it] != Empty }
                Empty -> break
            }

            pos += dir
        } while (pos in grid)

        return result.joinToString("") to steps - 1
    }

    sealed interface Cell
    object Empty : Cell
    object UpDown : Cell
    object LeftRight : Cell
    object Turn : Cell
    data class Letter(val value: Char) : Cell
}
