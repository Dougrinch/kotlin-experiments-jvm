package com.dou.advent.year2017

import com.dou.advent.util.*

fun main() {
    Day14().run()
}

class Day14 {
    fun run() {
        val key = "xlqgujun"

        val disk = (0 until 128).map { "$key-$it" }.map {
            hexToBinary(Day10().knotHash(it)).map { it == '1' }.toMutableList()
        }.let { MutableGrid(it) }

        println(disk.values().filter { it }.count())

        val groups = mutableSetOf<Set<Position>>()
        do {
            val group = disk.removeNextGroup()
            if (group.isNotEmpty()) {
                groups += group
            }
        } while (group.isNotEmpty())
        println(groups.size)
    }

    private fun MutableGrid<Boolean>.removeNextGroup(): Set<Position> {
        val first = positions().firstOrNull { this[it] } ?: return emptySet()

        val result = mutableSetOf<Position>()
        val unprocessed = mutableSetOf(first)

        while (unprocessed.isNotEmpty()) {
            val p = unprocessed.removeAny()

            result += p
            this[p] = false

            neighboursOf(p, diagonal = false).forEach { np ->
                if (np !in result && this[np]) {
                    unprocessed += np
                }
            }
        }

        return result
    }

    private fun hexToBinary(hex: String): String {
        return hex.map { it.digitToInt(16).toString(2).padStart(4, '0') }.joinToString("")
    }
}
