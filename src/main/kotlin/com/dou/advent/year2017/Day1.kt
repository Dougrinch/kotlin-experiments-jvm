package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day1().run()
}

class Day1 : TypedDay<List<Int>>() {
    override val parser by parser {
        digit.many()
    }

    override fun part1(input: List<Int>): Any {
        return input.indices
            .map { input[it] to input[(it + 1) % input.size] }
            .filter { it.first == it.second }
            .sumOf { it.first }
    }

    override fun part2(input: List<Int>): Any {
        return input.indices
            .map { input[it] to input[(it + input.size / 2) % input.size] }
            .filter { it.first == it.second }
            .sumOf { it.first }
    }
}
