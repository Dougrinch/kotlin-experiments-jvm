package com.dou.advent.year2017

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day5().run()
}

class Day5 : TypedDay<List<Int>>() {
    override val test1: Any
        get() = 5
    override val test2: Any
        get() = 10

    override val parser by parser {
        int.separatedByLines()
    }

    override fun part1(input: List<Int>): Any {
        val ops = input.toMutableList()
        var p = 0
        var i = 0
        while (p in ops.indices) {
            ops[p] += 1
            p += ops[p] - 1
            i += 1
        }
        return i
    }

    override fun part2(input: List<Int>): Any {
        val ops = input.toMutableList()
        var p = 0
        var i = 0
        while (p in ops.indices) {
            val d = ops[p]
            if (d >= 3) {
                ops[p] -= 1
            } else {
                ops[p] += 1
            }
            p += d
            i += 1
        }
        return i
    }
}
