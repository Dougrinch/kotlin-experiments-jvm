package com.dou.advent.year2017

fun main() {
    Day15().run()
}

class Day15 {
    fun run() {
        part1()
        part2()
    }

    private fun part1() {
        val ga = generator(591L, 16807, 1)
        val gb = generator(393L, 48271, 1)

        println(ga.zip(gb).take(40_000_000).count { (a, b) -> a and 0xFFFF == b and 0xFFFF })
    }

    private fun part2() {
        val ga = generator(591L, 16807, 4)
        val gb = generator(393L, 48271, 8)

        println(ga.zip(gb).take(5_000_000).count { (a, b) -> a and 0xFFFF == b and 0xFFFF })
    }

    private fun generator(base: Long, factor: Int, criteria: Int): Sequence<Long> {
        return generateSequence(base) { it * factor % 2147483647 }.filter { it % criteria == 0L }
    }
}
