package com.dou.advent.year2021

import arrow.core.Some
import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.digit
import com.dou.parser.parser

fun main() {
    Day15().run()
}

class Day15 : TypedDay<Grid<Int>>() {
    override val test1: Any
        get() = 40
    override val test2: Any
        get() = 315

    override val parser = parser { grid(digit) }

    override fun part1(input: Grid<Int>): Any {
        return bestPathCost(input)
    }

    override fun part2(input: Grid<Int>): Any {
        return bestPathCost(input.repeat(5))
    }

    private fun bestPathCost(grid: Grid<Int>): Int {
        val start = Position(0, 0)
        val target = Position(grid.sizeX - 1, grid.sizeY - 1)
        val result = search(
            start,
            { grid.neighboursOf(it, false).map { it to grid[it] } },
            { 1 },
            { it == target }
        ).map { it.first }
        return (result as Some).value.sumOf { grid[it] } - grid[start]
    }

    private fun Grid<Int>.repeat(n: Int): Grid<Int> {
        val lines = List(n * sizeY) { MutableList(n * sizeX) { 0 } }
        for (pos in positions()) {
            val (x, y) = pos
            for (dx in 0 until n) {
                for (dy in 0 until n) {
                    lines[y + dy * sizeY][x + dx * sizeX] = ((get(pos) + dx + dy - 1) % 9) + 1
                }
            }
        }
        return Grid(lines)
    }
}
