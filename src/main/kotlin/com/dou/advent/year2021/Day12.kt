package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.util.Move
import com.dou.advent.util.bruteForce
import com.dou.advent.year2021.Day12.Graph
import com.dou.fp.e3.*
import com.dou.parser.*
import kotlin.collections.List
import kotlin.collections.component1
import kotlin.collections.component2
import com.dou.fp.e3.List as FList

fun main() {
    Day12().run()
}

class Day12 : TypedDay<Graph>() {
    override val test1: Any
        get() = 10
    override val test2: Any
        get() = 36

    override val parser = parser {
        word - "-" - word map ::Edge separatedBy "\n" map ::Graph
    }

    override fun part1(input: Graph): Any {
        return input.allPaths { p, c -> c !in p }.count()
    }

    override fun part2(input: Graph): Any {
        return input.allPaths { p, c ->
            val counts = p.foldLeft(HashMap<String, Int>()) { acc, a -> acc.compute(a) { _, v -> (v ?: 0) + 1 }; acc }
            c != "start" && (counts.none { (k, v) -> k == k.lowercase() && v == 2 } || c !in p)
        }.count()
    }

    data class Edge(val from: String, val to: String)

    private fun Graph.allPaths(smallCaveFilter: (FList<String>, String) -> Boolean): Sequence<FList<String>> {
        return bruteForce(
            FList("start"),
            { p -> from(p.head()) },
            { path, next ->
                when {
                    next == "end" -> Move.PathCompleted(Cons(next, path))
                    next == next.uppercase() || smallCaveFilter(path, next) -> Move.Successful(Cons(next, path))
                    else -> Move.Unacceptable
                }
            }
        )
    }

    class Graph(edges: List<Edge>) {
        private val moves: Map<String, List<String>> = buildMap<String, MutableList<String>> {
            for (edge in edges) {
                computeIfAbsent(edge.from) { ArrayList() }.add(edge.to)
                computeIfAbsent(edge.to) { ArrayList() }.add(edge.from)
            }
        }

        fun from(from: String): List<String> = moves[from]!!
    }
}
