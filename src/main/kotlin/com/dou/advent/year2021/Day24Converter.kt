package com.dou.advent.year2021

import com.dou.advent.util.input
import java.util.concurrent.atomic.AtomicInteger

fun main() {
    val lines = input(2021, 24).lineSequence()
    val result = buildString {
        val w = AtomicInteger()

        appendLine()
        appendLine((1..14).joinToString(prefix = "fun validate(", postfix = "): Int {") { "val w$it: Int" })
        appendLine("var w = 0")
        appendLine("var x = 0")
        appendLine("var y = 0")
        appendLine("var z = 0")
        for (line in lines) {
            with(line) {
                when (cmd) {
                    "inp" -> appendLine("$p1 = w${w.incrementAndGet()}")
                    "add" -> appendLine("$p1 = $p1 + $p2")
                    "mul" -> appendLine("$p1 = $p1 * $p2")
                    "div" -> appendLine("$p1 = $p1 / $p2")
                    "mod" -> appendLine("$p1 = $p1 % $p2")
                    "eql" -> appendLine("$p1 = if ($p1 == $p2) 1 else 0")
                    else -> throw IllegalStateException()
                }
            }
        }
        appendLine("return z")
        appendLine("}")
        appendLine()
    }

    println(result)
}

private val String.cmd: String
    get() = substring(0..2)

private val String.p1: String
    get() = drop(4).split(" ")[0]

private val String.p2: String
    get() = drop(4).split(" ")[1]
