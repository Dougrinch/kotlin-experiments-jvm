package com.dou.advent.year2021

import com.dou.advent.Day
import com.dou.advent.year2021.Day3.Bit.One
import com.dou.advent.year2021.Day3.Bit.Zero

fun main() {
    Day3().run()
}

class Day3 : Day(2021, 3) {
    override val test1: Any
        get() = 198
    override val test2: Any
        get() = 230

    override fun part1(input: Sequence<String>): Any {
        val numbers = parse(input)
        val bits = countBits(numbers)
        val gamma = bits.map { it.mostCommon }.toInt()
        val epsilon = bits.map { it.leastCommon }.toInt()
        return gamma * epsilon
    }

    override fun part2(input: Sequence<String>): Any {
        val numbers = parse(input)
        val oxygenGeneratorRating = calcRating(numbers) { it.mostCommon }.toInt()
        val co2ScrubberRating = calcRating(numbers) { it.leastCommon }.toInt()
        return oxygenGeneratorRating * co2ScrubberRating
    }

    private fun calcRating(numbers: List<BitString>, f: (BitsCount) -> Bit): BitString {
        val length = numbers[0].length
        var candidates = numbers
        for (i in 0 until length) {
            val bits = countBits(candidates)
            val v = f(bits[i])
            candidates = candidates.filter { it[i] == v }
            if (candidates.size == 1) {
                return candidates.single()
            }
        }

        throw IllegalStateException()
    }

    private fun parse(input: Sequence<String>): List<BitString> {
        return input.map { it.toBitString() }.toList()
    }

    private fun countBits(numbers: List<BitString>): List<BitsCount> {
        val length = numbers[0].length
        return numbers
            .fold(Array(length) { BitsCount(0, 0) }) { r, l ->
                l.forEachIndexed { idx, b ->
                    r[idx] = r[idx] + BitsCount.of(b)
                }
                r
            }.toList()
    }

    private fun String.toBitString(): BitString {
        return map {
            when (it) {
                '1' -> One
                '0' -> Zero
                else -> throw IllegalStateException()
            }
        }.let { BitString(it) }
    }

    private fun List<Bit>.toInt(): Int {
        return BitString(this).toInt()
    }

    private enum class Bit {
        One, Zero
    }

    private data class BitString(private val data: List<Bit>) : List<Bit> by data {
        val length: Int
            get() = data.size

        fun toInt(): Int {
            return data.joinToString(separator = "") {
                when (it) {
                    One -> "1"
                    Zero -> "0"
                }
            }.toInt(2)
        }
    }

    private data class BitsCount(val zeros: Int, val ones: Int) {
        companion object {
            fun of(b: Bit): BitsCount = when (b) {
                One -> BitsCount(0, 1)
                Zero -> BitsCount(1, 0)
            }
        }

        operator fun plus(o: BitsCount): BitsCount {
            return BitsCount(zeros + o.zeros, ones + o.ones)
        }

        val mostCommon: Bit
            get() = if (ones >= zeros) One else Zero

        val leastCommon: Bit
            get() = if (zeros <= ones) Zero else One
    }
}
