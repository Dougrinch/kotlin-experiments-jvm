package com.dou.advent.year2021

fun main() {
    val results = ArrayList<String>()
    for (w1 in 1..9) {
        for (w2 in 1..9) {
            for (w3 in 1..9) {
                for (w4 in 1..9) {
                    for (w5 in 1..9) {
                        val x5 = ((w1 * 26 + w2 + 12) * 26 + w3 + 14) * 26 + w4
                        if (x5 % 26 - 2 != w5) continue
                        for (w6 in 1..9) {
                            for (w7 in 1..9) {
                                for (w8 in 1..9) {
                                    val x8 = (x5 / 26 * 26 + w6 + 15) * 26 + w7 + 11
                                    if (x8 % 26 - 15 != w8) continue
                                    for (w9 in 1..9) {
                                        for (w10 in 1..9) {
                                            val x10 = x8 / 26 * 26 + w9 + 1
                                            if (x10 % 26 - 9 != w10) continue
                                            for (w11 in 1..9) {
                                                val x11 = x10 / 26
                                                if (x11 % 26 - 9 != w11) continue
                                                for (w12 in 1..9) {
                                                    val x12 = x11 / 26
                                                    if (x12 % 26 - 7 != w12) continue
                                                    for (w13 in 1..9) {
                                                        val x13 = x12 / 26
                                                        if (x13 % 26 - 4 != w13) continue
                                                        for (w14 in 1..9) {
                                                            val x14 = x13 / 26
                                                            if (x14 % 26 - 6 != w14) continue

                                                            val z = validateFolded(
                                                                w1, w2, w3, w4, w5, w6, w7,
                                                                w8, w9, w10, w11, w12, w13, w14
                                                            )

                                                            val n = "$w1$w2$w3$w4$w5$w6$w7$w8$w9$w10$w11$w12$w13$w14"
                                                            if (z == 0) {
                                                                results += n
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    println(results.last())
    println(results.first())
}

private fun validateFolded(
    w1: Int, w2: Int, w3: Int, w4: Int, w5: Int, w6: Int, w7: Int,
    w8: Int, w9: Int, w10: Int, w11: Int, w12: Int, w13: Int, w14: Int
): Int {
    var z: Int

    val x5 = ((w1 * 26 + w2 + 12) * 26 + w3 + 14) * 26 + w4
    val p5 = if (x5 % 26 - 2 == w5) 0 else 1
    z = x5 / 26 * 25 * p5 + x5 / 26
    z = z + (w5 + 3) * p5 // z = (w1 * 26 + w2 + 12) * 26 + w3 + 14

    //(x5 / 26 * 26 + w6 + 15) * 26 + w7 + 11
    val x8 = (z * 26 + w6 + 15) * 26 + w7 + 11
    val p8 = if (x8 % 26 - 15 == w8) 0 else 1
    z = x8 / 26 * 25 * p8 + x8 / 26
    z = z + (w8 + 12) * p8 // z = ((w1 * 26 + w2 + 12) * 26 + w3 + 14) * 26 + w6 + 15

    //x8 / 26 * 26 + w9 + 1
    val x10 = z * 26 + w9 + 1
    val p10 = if (x10 % 26 - 9 == w10) 0 else 1
    z = x10 / 26 * 25 * p10 + x10 / 26
    z = z + (w10 + 12) * p10 // z = ((w1 * 26 + w2 + 12) * 26 + w3 + 14) * 26 + w6 + 15

    //x10 / 26
    val x11 = z
    val p11 = if (x11 % 26 - 9 == w11) 0 else 1
    z = x11 / 26 * 25 * p11 + x11 / 26
    z = z + (w11 + 3) * p11 // z = (w1 * 26 + w2 + 12) * 26 + w3 + 14

    //x11 / 26
    val x12 = z
    val p12 = if (x12 % 26 - 7 == w12) 0 else 1
    z = x12 / 26 * 25 * p12 + x12 / 26
    z = z + (w12 + 10) * p12 // z = w1 * 26 + w2 + 12

    //x12 / 26
    val x13 = z
    val p13 = if (x13 % 26 - 4 == w13) 0 else 1
    z = x13 / 26 * 25 * p13 + x13 / 26
    z = z + (w13 + 14) * p13 // z = w1

    //x13 / 26
    val x14 = z
    val p14 = if (x14 % 26 - 6 == w14) 0 else 1
    z = x14 / 26 * 25 * p14 + x14 / 26
    z = z + (w14 + 12) * p14 // z = 0

    return z
}
