package com.dou.advent.year2021

import arrow.core.Either
import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day13.Fold
import com.dou.advent.year2021.Day13.Paper
import com.dou.parser.*

fun main() {
    Day13().run()
}

class Day13 : TypedDay<Pair<Paper, List<Fold>>>() {
    override val test1: Any
        get() = 17

    override val parser = parser {
        val points = int - "," - int map ::Point separatedBy "\n" map ::Paper

        val folds = "fold along " - oneOf(
            "x=" - int map { Fold(Either.Left(it)) },
            "y=" - int map { Fold(Either.Right(it)) }
        ).separatedByLines()

        points - "\n\n" - folds
    }

    override fun part1(input: Pair<Paper, List<Fold>>): Any {
        return input.first.fold(input.second.first())
            .points.distinct().count()
    }

    override fun part2(input: Pair<Paper, List<Fold>>): Any {
        val result = input.second.fold(input.first) { p, f -> p.fold(f) }
        return result.toRichString()
    }

    private fun Paper.fold(f: Fold): Paper = when (val xy = f.xy) {
        is Either.Left -> {
            val fx = xy.value
            Paper(points.map { (x, y) ->
                if (x > fx) {
                    Point(x - 2 * (x - fx), y)
                } else {
                    Point(x, y)
                }
            })
        }
        is Either.Right -> {
            val fy = xy.value
            Paper(points.map { (x, y) ->
                if (y > fy) {
                    Point(x, y - 2 * (y - fy))
                } else {
                    Point(x, y)
                }
            })
        }
    }

    private fun Paper.toRichString(): String {
        val maxX = points.maxOf { it.x }
        val maxY = points.maxOf { it.y }
        val p = points.toSet()
        return sequence {
            yield('\n')
            (0..maxY).forEach { y ->
                (0..maxX).forEach { x ->
                    if (Point(x, y) in p) {
                        yield('#')
                    } else {
                        yield('.')
                    }
                }
                yield('\n')
            }
            yield('\n')
        }.joinToString(separator = "")
    }

    data class Point(val x: Int, val y: Int)
    data class Paper(val points: List<Point>)
    data class Fold(val xy: Either<Int, Int>)
}
