package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day17.TargetArea
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day17().run()
}

class Day17 : TypedDay<TargetArea>() {
    override val test1: Any
        get() = 45
    override val test2: Any
        get() = 112

    override val parser = parser {
        val range = int - ".." - int map { f, t -> f..t }
        "target area: x=" - range - ", y=" - range map ::TargetArea
    }

    override fun part1(input: TargetArea): Any {
        return acceptableVelocities(input)
            .first()
            .let { (_, y) -> (1 + y) * y / 2 }
    }

    override fun part2(input: TargetArea): Any {
        return acceptableVelocities(input).count()
    }

    private fun acceptableVelocities(area: TargetArea): Sequence<Pair<Int, Int>> = sequence {
        vy@ for (vy in abs(area.ys.first) downTo area.ys.first) {
            vx@ for (vx in 0..area.xs.last) {
                steps@ for ((x, y) in steps(vx to vy)) {
                    if (x > area.xs.last || y < area.ys.first) {
                        break@steps
                    }
                    if (x in area.xs && y in area.ys) {
                        yield(vx to vy)
                        continue@vx
                    }
                }
            }
        }
    }

    private fun steps(v: Pair<Int, Int>): Sequence<Pair<Int, Int>> = sequence {
        var vx = v.first
        var vy = v.second
        var x = 0
        var y = 0

        while (true) {
            x += vx
            y += vy
            if (vx > 0) {
                vx -= 1
            }
            vy -= 1
            yield(x to y)
        }
    }

    data class TargetArea(val xs: IntRange, val ys: IntRange)
}
