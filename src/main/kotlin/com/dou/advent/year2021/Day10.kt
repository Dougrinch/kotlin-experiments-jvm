package com.dou.advent.year2021

import com.dou.advent.Day

fun main() {
    Day10().run()
}

class Day10 : Day(2021, 10) {
    override val test1: Any
        get() = 26397
    override val test2: Any
        get() = 288957

    override fun part1(input: Sequence<String>): Any {
        return input
            .parse()
            .filterIsInstance<Line.Corrupted>()
            .sumOf { it.incorrect.errorPoints() }
    }

    override fun part2(input: Sequence<String>): Any {
        return input
            .parse()
            .filterIsInstance<Line.Incomplete>()
            .map { it.completion.map { it.completionPoints().toLong() }.reduce { acc, p -> acc * 5 + p } }
            .sorted()
            .let { l -> l[l.size / 2] }
    }

    private fun Sequence<String>.parse(): List<Line> {
        return map { line ->
            val stack = ArrayDeque<Char>()
            for (ch in line) {
                if (ch in listOf('(', '[', '{', '<')) {
                    stack.addLast(ch)
                } else if (ch in listOf(')', ']', '}', '>')) {
                    val expected = stack.removeLast().toClose()
                    if (ch != expected) {
                        return@map Line.Corrupted(line, ch)
                    }
                }
            }
            return@map Line.Incomplete(line, stack.map { it.toClose() }.reversed())
        }.toList()
    }

    sealed class Line {
        data class Incomplete(val line: String, val completion: List<Char>) : Line()
        data class Corrupted(val line: String, val incorrect: Char) : Line()
    }

    private fun Char.toClose(): Char = when (this) {
        '(' -> ')'
        '[' -> ']'
        '{' -> '}'
        '<' -> '>'
        else -> throw IllegalStateException()
    }

    private fun Char.errorPoints(): Int = when (this) {
        ')' -> 3
        ']' -> 57
        '}' -> 1197
        '>' -> 25137
        else -> throw IllegalStateException()
    }

    private fun Char.completionPoints(): Int = when (this) {
        ')' -> 1
        ']' -> 2
        '}' -> 3
        '>' -> 4
        else -> throw IllegalStateException()
    }
}
