package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day14().run()
}

class Day14 : TypedDay<Pair<String, Map<String, String>>>() {
    override val test1: Any
        get() = 1588
    override val test2: Any
        get() = 2188189693529

    override val parser = parser {
        val rule = regex("\\w\\w") - " -> " - regex("\\w")
        word - "\n\n" - rule.separatedByLines().map { it.associateBy({ it.first }, { it.second }) }
    }

    override fun part1(input: Pair<String, Map<String, String>>): Any {
        val counts = makeNStepsThenCount(10, input.first, input.second)
        return counts.maxOf { it.value } - counts.minOf { it.value }
    }

    override fun part2(input: Pair<String, Map<String, String>>): Any {
        val counts = makeNStepsThenCount(40, input.first, input.second)
        return counts.maxOf { it.value } - counts.minOf { it.value }
    }

    private fun makeNStepsThenCount(n: Int, polymer: String, rules: Map<String, String>): Map<Char, Long> {
        var pairs = polymer.windowed(2, 1).groupBy { it }.mapValues { it.value.size.toLong() }
        for (i in 0 until n) {
            val next = HashMap<String, Long>()
            for ((p, v) in pairs) {
                val c = rules[p]!!
                val a = p[0] + c
                val b = c + p[1]
                next[a] = next[a]?.plus(v) ?: v
                next[b] = next[b]?.plus(v) ?: v
            }
            pairs = next
        }
        return pairs
            .flatMap { (k, v) -> listOf(k[0] to v, k[1] to v) }
            .groupBy({ it.first }, { it.second })
            .mapValues { it.value.sum() }
            .mapValues { (_, v) -> if (v % 2 == 0L) v / 2 else (v + 1) / 2 }
    }
}
