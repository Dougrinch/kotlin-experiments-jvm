package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day21.Board
import com.dou.parser.*

fun main() {
    Day21().run()
}

class Day21 : TypedDay<Board>() {
    override val test1: Any
        get() = 739785
    override val test2: Any
        get() = 444356092776315

    override val parser = parser {
        val p = "Player 1 starting position: " - int - "\n" - "Player 2 starting position: " - int
        p map { p1, p2 -> Board(Player(p1, 0), Player(p2, 0)) }
    }

    override fun part1(input: Board): Any {
        val dice = DeterministicDice(1, 0)
        input.playWith(dice)
        return minOf(input.player1.score, input.player2.score) * dice.rollCount
    }

    override fun part2(input: Board): Any {
        val (turns1, wins1) = countTurnsToWinFrom(input.player1.position)
        val (turns2, wins2) = countTurnsToWinFrom(input.player2.position)

        val firstWins = wins1.map { (turns, c1) ->
            c1 * (turns2.getValue(turns - 1) - wins2.getValue(turns - 1))
        }.sum()

        val secondWin = wins2.map { (turns, c2) ->
            c2 * (turns1.getValue(turns) - wins1.getValue(turns))
        }.sum()

        return maxOf(firstWins, secondWin)
    }

    private fun countTurnsToWinFrom(n: Int): Pair<Map<Int, Long>, Map<Int, Long>> {
        val turns = HashMap<Int, Long>().withDefault { 0 }
        val wins = HashMap<Int, Long>().withDefault { 0 }
        count(1, n, 0, turns, wins, 1)
        return turns to wins
    }

    private fun count(
        step: Int, position: Int, score: Int, turns: MutableMap<Int, Long>, wins: MutableMap<Int, Long>, multiplier: Int
    ) {
        for ((roll, count) in rollsCount) {
            turns[step] = turns.getValue(step) + multiplier * count

            val nextPosition = position.nextPosition(roll)
            val nextScore = score + nextPosition
            if (nextScore >= 21) {
                wins[step] = wins.getValue(step) + multiplier * count
            } else {
                count(step + 1, nextPosition, nextScore, turns, wins, multiplier * count)
            }
        }
    }

    private val rollsCount: Map<Int, Int> = (1..3).flatMap { a ->
        (1..3).flatMap { b ->
            (1..3).map { c ->
                a + b + c
            }
        }
    }.groupBy { it }.mapValues { (_, v) -> v.size }

    private fun Int.nextPosition(roll: Int): Int {
        return (this + roll - 1).mod(10) + 1
    }

    private fun Board.playWith(dice: Dice) {
        while (true) {
            player1.makeTurn(dice)
            if (player1.score >= 1000) {
                return
            }
            player2.makeTurn(dice)
            if (player2.score >= 1000) {
                return
            }
        }
    }

    private fun Player.makeTurn(dice: Dice) {
        val moves = (1..3).sumOf { dice.roll() }
        val newPosition = position.nextPosition(moves)
        position = newPosition
        score += newPosition
    }

    interface Dice {
        fun roll(): Int
    }

    data class DeterministicDice(var next: Int, var rollCount: Int) : Dice {
        override fun roll(): Int {
            rollCount += 1
            val result = next
            next = (next + 1).takeIf { it <= 100 } ?: 1
            return result
        }
    }

    data class Player(var position: Int, var score: Int)
    data class Board(val player1: Player, val player2: Player)
}
