package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day18_2.SnailfishNumber
import com.dou.parser.*

fun main() {
    Day18_2().run()
}

class Day18_2 : TypedDay<List<SnailfishNumber>>(day = 18) {
    override val test1: Any
        get() = 4140
    override val test2: Any
        get() = 3993

    override val parser = NumberParsers.number().separatedByLines()

    override fun part1(input: List<SnailfishNumber>): Any {
        return input.reduce(::plus).magnitude()
    }

    override fun part2(input: List<SnailfishNumber>): Any {
        val numbers = input.toList()
        return numbers.flatMap { n1 -> numbers.map { n2 -> n1 to n2 } }
            .asSequence()
            .filter { (n1, n2) -> n1 != n2 }
            .map { (n1, n2) -> plus(n1, n2) }
            .maxOf { it.magnitude() }
    }

    private fun plus(n1: SnailfishNumber, n2: SnailfishNumber): SnailfishNumber {
        return PairNumber(n1, n2).reduce()
    }

    private fun SnailfishNumber.reduce(): SnailfishNumber {
        val r = copy()
        while (true) {
            if (r.tryExplode()) {
                continue
            }
            if (r.trySplit()) {
                continue
            }
            break
        }
        return r
    }

    private fun SnailfishNumber.tryExplode(): Boolean {
        for (number in leftToRight()) {
            if (number is PairNumber && number.depth() == 5) {
                number.prev()?.let { it.value += (number.left as JustNumber).value }
                number.next()?.let { it.value += (number.right as JustNumber).value }
                number.replaceWith(JustNumber(0))
                return true
            }
        }
        return false
    }

    private fun SnailfishNumber.trySplit(): Boolean {
        for (number in leftToRight()) {
            if (number is JustNumber && number.value >= 10) {
                val l = number.value / 2
                val r = number.value - l
                number.replaceWith(PairNumber(JustNumber(l), JustNumber(r)))
                return true
            }
        }
        return false
    }

    private fun SnailfishNumber.replaceWith(n: SnailfishNumber) {
        if (parent!!.left == this) {
            parent!!.left = n
        } else {
            parent!!.right = n
        }
    }

    private fun SnailfishNumber.depth(): Int {
        var depth = 1
        var number = this
        while (number.parent != null) {
            depth += 1
            number = number.parent!!
        }
        return depth
    }

    private fun SnailfishNumber.root(): SnailfishNumber {
        return if (parent != null) {
            parent!!.root()
        } else {
            this
        }
    }

    private fun SnailfishNumber.leftToRight(): Sequence<SnailfishNumber> {
        return sequence {
            yield(this@leftToRight)
            if (this@leftToRight is PairNumber) {
                yieldAll(left.leftToRight())
                yieldAll(right.leftToRight())
            }
        }
    }

    private fun SnailfishNumber.prev(): JustNumber? {
        return root().leftToRight()
            .takeWhile { it != this }
            .filterIsInstance<JustNumber>()
            .lastOrNull()
    }

    private fun SnailfishNumber.next(): JustNumber? {
        return root().leftToRight()
            .dropWhile { it != this }
            .dropWhile { generateSequence(it) { it.parent }.any { it == this } }
            .filterIsInstance<JustNumber>()
            .firstOrNull()
    }

    private fun SnailfishNumber.magnitude(): Int {
        return when (this) {
            is PairNumber -> 3 * left.magnitude() + 2 * right.magnitude()
            is JustNumber -> value
        }
    }

    object NumberParsers {
        fun number(): Parser<SnailfishNumber> = parser { oneOf(pair(), regular()) }
        fun pair() = "[" - number() - "," - number() - "]" map ::PairNumber
        fun regular() = digit map ::JustNumber
    }

    sealed class SnailfishNumber {
        var parent: PairNumber? = null
        abstract fun copy(): SnailfishNumber
    }

    class PairNumber(left: SnailfishNumber, right: SnailfishNumber) : SnailfishNumber() {
        var left: SnailfishNumber = left
            set(value) {
                value.parent = this
                field = value
            }

        var right: SnailfishNumber = right
            set(value) {
                value.parent = this
                field = value
            }

        init {
            this.left = left
            this.right = right
        }

        override fun copy(): SnailfishNumber {
            return PairNumber(left.copy(), right.copy())
        }

        override fun toString(): String {
            return "[$left,$right]"
        }
    }

    class JustNumber(var value: Int) : SnailfishNumber() {
        override fun copy(): SnailfishNumber {
            return JustNumber(value)
        }

        override fun toString(): String {
            return "$value"
        }
    }
}
