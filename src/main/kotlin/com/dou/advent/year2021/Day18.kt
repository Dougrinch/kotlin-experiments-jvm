package com.dou.advent.year2021

import arrow.core.*
import com.dou.advent.Day
import com.dou.fp.e3.*
import com.dou.parser.*

fun main() {
    Day18().run()
}

class Day18 : Day() {
    override val test1: Any
        get() = 4140
    override val test2: Any
        get() = 3993

    override fun part1(input: Sequence<String>): Any {
        val number = NumberParsers.number().parse(input.reduce(::plus))
        return number.magnitude()
    }

    override fun part2(input: Sequence<String>): Any {
        val numbers = input.toList()
        return numbers.flatMap { n1 -> numbers.map { n2 -> n1 to n2 } }
            .asSequence()
            .filter { (n1, n2) -> n1 != n2 }
            .map { (n1, n2) -> reduce(plus(n1, n2)) }
            .map { NumberParsers.number().parse(it) }
            .maxOf { it.magnitude() }
    }

    private fun Tree<Int>.magnitude(): Int {
        return when (this) {
            is Leaf -> value
            is Branch -> 3 * left.magnitude() + 2 * right.magnitude()
        }
    }

    private fun plus(n1: String, n2: String): String {
        return reduce("[$n1,$n2]")
    }

    private fun reduce(number: String): String {
        var r = number
        step@ while (true) {
            val er = tryExplode(r)
            if (er is Some) {
                r = er.value
                continue@step
            }

            val sr = trySplit(r)
            if (sr is Some) {
                r = sr.value
                continue@step
            }

            return r
        }
    }

    private fun tryExplode(number: String): Option<String> {
        var depth = 0
        var prevIdx = -1
        var idx = 0
        while (idx in number.indices) {
            when (val ch = number[idx]) {
                '[' -> {
                    depth += 1
                    if (depth == 5) {
                        var endIdx = idx
                        val a = number.readIntAt(endIdx + 1) { endIdx += it }
                        val b = number.readIntAt(endIdx + 3) { endIdx += it }

                        var nextIdx = ((endIdx + 5) until number.length)
                            .firstOrNull { number[it] in '0'..'9' }

                        return buildString {
                            if (prevIdx == -1) {
                                append(number.substring(0, idx))
                                append("0")
                            } else {
                                append(number.substring(0, prevIdx))
                                append(number.readIntAt(prevIdx) { prevIdx += it } + a)
                                append(number.substring(prevIdx + 1, idx))
                                append("0")
                            }
                            if (nextIdx == null) {
                                append(number.substring(endIdx + 5, number.length))
                            } else {
                                append(number.substring(endIdx + 5, nextIdx))
                                append(number.readIntAt(nextIdx) { nextIdx += it } + b)
                                append(number.substring(nextIdx + 1, number.length))
                            }
                        }.some()
                    }
                }
                ',' -> {}
                ']' -> depth -= 1
                in '0'..'9' -> {
                    prevIdx = idx
                    number.readIntAt(idx) { idx += it }
                }
                else -> throw IllegalStateException("$ch")
            }
            idx += 1
        }
        return none()
    }

    private fun trySplit(number: String): Option<String> {
        val (idx, value) = number.indices
            .windowed(2, 1)
            .map { it[0] to "${number[it[0]]}${number[it[1]]}".toIntOrNull() }
            .firstOrNull { it.second != null }
            ?.let { it.first to it.second!! } ?: return none()

        return buildString {
            append(number.substring(0, idx))
            append("[")
            append(value / 2)
            append(",")
            append(value - value / 2)
            append("]")
            append(number.substring(idx + 2, number.length))
        }.some()
    }

    private fun String.readIntAt(idx: Int, f: (Int) -> Unit): Int {
        val last = length - drop(idx).dropWhile { it in '0'..'9' }.length - 1
        f(last - idx)
        return substring(idx..last).toInt()
    }

    object NumberParsers {
        fun number(): Parser<Tree<Int>> = parser { oneOf(pair(), regular()) }
        fun pair() = "[" - number() - "," - number() - "]" map ::Branch
        fun regular() = digit map ::Leaf
    }
}
