package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day20.Algorithm
import com.dou.advent.year2021.Day20.Grid
import com.dou.parser.*

fun main() {
    Day20().run()
}

class Day20 : TypedDay<Pair<Algorithm, Grid>>() {
    override val test1: Any
        get() = 35

    override val parser = parser {
        val light = char('#') parseAs true
        val dark = char('.') parseAs false
        val algorithm = (light or dark).many(exact = 512) map ::Algorithm
        val grid = (light or dark).many().separatedByLines() map { Grid(false, it) }
        algorithm - "\n\n" - grid
    }

    override fun part1(input: Pair<Algorithm, Grid>): Any {
        val (algorithm, grid) = input
        return grid.makeStep(algorithm).makeStep(algorithm).data.values.count { it }
    }

    override fun part2(input: Pair<Algorithm, Grid>): Any {
        val (algorithm, grid) = input
        return (1..50).fold(grid) { g, _ -> g.makeStep(algorithm) }.data.values.count { it }
    }

    private fun Grid.makeStep(algorithm: Algorithm): Grid {
        val minX = data.minOf { it.key.first } - 1
        val maxX = data.maxOf { it.key.first } + 1
        val minY = data.minOf { it.key.second } - 1
        val maxY = data.maxOf { it.key.second } + 1

        val newData = HashMap<Pair<Int, Int>, Boolean>()

        for (x in minX..maxX) {
            for (y in minY..maxY) {
                val newValueIndex = sequence {
                    for (dy in -1..1) {
                        for (dx in -1..1) {
                            yield(dx to dy)
                        }
                    }
                }.map { (dx, dy) ->
                    data.getOrDefault(x + dx to y + dy, default)
                }.map { if (it) "1" else "0" }
                    .joinToString("")
                    .toInt(2)

                val newValue = algorithm.replaces[newValueIndex]
                newData += (x to y) to newValue
            }
        }

        val newDefault = if (default) algorithm.replaces.last() else algorithm.replaces.first()

        return Grid(newDefault, newData)
    }

    data class Algorithm(val replaces: List<Boolean>)
    data class Grid(val default: Boolean, val data: Map<Pair<Int, Int>, Boolean>) {
        constructor(default: Boolean, data: List<List<Boolean>>) : this(
            default,
            data.withIndex().flatMap { (y, xs) ->
                xs.withIndex().map { (x, v) -> (x to y) to v }
            }.toMap()
        )
    }
}
