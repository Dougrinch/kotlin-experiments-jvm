package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.util.MutableGrid
import com.dou.advent.util.grid
import com.dou.advent.year2021.Day25.Cucumber
import com.dou.parser.*

fun main() {
    Day25().run()
}

class Day25 : TypedDay<MutableGrid<Cucumber?>>() {
    override val test1: Any
        get() = 58

    override val parser = parser {
        val cucumber = oneOf(
            char('>') map { Cucumber(true) },
            char('v') map { Cucumber(false) },
            char('.') map { null }
        )
        grid(cucumber)
    }

    override fun part1(input: MutableGrid<Cucumber?>): Any {
        var i = 1
        while (makeStep(input)) {
            i += 1
        }
        return i
    }

    private fun MutableGrid<Cucumber?>.print() {
        println("")
        println(toString {
            when (it?.right) {
                true -> ">"
                false -> "v"
                null -> "."
            }
        })
        println("")
    }

    private fun makeStep(grid: MutableGrid<Cucumber?>): Boolean {
        var moveHappened = false

        val toRight = grid.positions().mapNotNull { pos ->
            val c = grid[pos]
            when (c?.right) {
                true -> {
                    val nextPos = (pos + (1 to 0)).let {
                        if (it.x == grid.sizeX) it.copy(x = 0) else it
                    }
                    when (grid[nextPos] == null) {
                        true -> c to pos to nextPos
                        false -> null
                    }
                }
                else -> null
            }
        }.toList()

        for ((cp, nextPos) in toRight) {
            grid[cp.second] = null
            grid[nextPos] = cp.first
            moveHappened = true
        }

        val toDown = grid.positions().mapNotNull { pos ->
            val c = grid[pos]
            when (c?.right) {
                false -> {
                    val nextPos = (pos + (0 to 1)).let {
                        if (it.y == grid.sizeY) it.copy(y = 0) else it
                    }
                    when (grid[nextPos] == null) {
                        true -> c to pos to nextPos
                        false -> null
                    }
                }
                else -> null
            }
        }.toList()

        for ((cp, nextPos) in toDown) {
            grid[cp.second] = null
            grid[nextPos] = cp.first
            moveHappened = true
        }

        return moveHappened
    }

    data class Cucumber(val right: Boolean)
}
