package com.dou.advent.year2021

import com.dou.advent.Day

fun main() {
    Day1().run()
}

class Day1 : Day(2021, 1) {
    override val test1: Any
        get() = 7
    override val test2: Any
        get() = 5

    override fun part1(input: Sequence<String>): Any {
        return input
            .map { it.toInt() }
            .zipWithNext()
            .count { it.second > it.first }
    }

    override fun part2(input: Sequence<String>): Any {
        return input
            .map { it.toInt() }
            .windowed(3, 1, false) { it.sum() }
            .zipWithNext()
            .count { it.second > it.first }
    }
}
