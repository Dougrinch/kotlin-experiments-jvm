package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day19.Scanner
import com.dou.fp.e2.head
import com.dou.fp.e2.tail
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day19().run()
}

class Day19 : TypedDay<List<Scanner>>() {
    override val test1: Any
        get() = 79
    override val test2: Any
        get() = 3621

    override val parser = parser {
        val beacon = int - "," - int - "," - int map ::Position
        val beacons = beacon.separatedByLines()
        val header = "--- scanner " - int - " ---"
        val scanner = header - "\n" - beacons map ::Scanner
        scanner separatedBy "\n\n"
    }

    override fun part1(input: List<Scanner>): Any {
        val scannersLocation = locateAll(input.head, input.tail)
        val scanners = input.map { s ->
            val (p, r) = scannersLocation.getValue(s.id)
            s.rotateThenMove(r, p)
        } + input.head

        val beacons = scanners.flatMapTo(HashSet()) { it.beacons.toHashSet() }
        return beacons.size
    }

    override fun part2(input: List<Scanner>): Any {
        val scanners = locateAll(input.head, input.tail).map { (_, v) -> v.first }
        return scanners.flatMap { p1 ->
            scanners.map { p2 ->
                abs(p1.x - p2.x) + abs(p1.y - p2.y) + abs(p1.z - p2.z)
            }
        }.maxOf { it }
    }

    private fun locateAll(origin: Scanner, scanners: List<Scanner>): Map<Int, Pair<Position, Rotation>> {
        val knownPositions = HashMap<Int, Pair<Position, Rotation>>()
        knownPositions += origin.id to (Position(0, 0, 0) to Rotation.empty)

        val knownScanners = ArrayList<Scanner>()
        knownScanners += origin

        val unknown = HashSet(scanners)

        next@ while (unknown.isNotEmpty()) {
            println("known: ${knownScanners.size}, unknown: ${unknown.size}")
            for (scanner in unknown) {
                for (base in knownScanners) {
                    val positionAndRotation = locate(base, scanner)
                    if (positionAndRotation != null) {
                        val (basePosition, baseRotation) = knownPositions.getValue(base.id)
                        val finalPosition = basePosition + baseRotation.rotate(positionAndRotation.first)
                        knownPositions += scanner.id to (finalPosition to positionAndRotation.second.then(baseRotation))
                        knownScanners += scanner
                        unknown -= scanner
                        continue@next
                    }
                }
            }
        }

        return knownPositions
    }

    private fun locate(origin: Scanner, second: Scanner): Pair<Position, Rotation>? {
        return second.rotations()
            .map { tryLocate(origin, it.second) to it.first }
            .filter { it.first != null }
            .map { (p, r) -> p!! to r }
            .firstOrNull()
    }

    private fun tryLocate(origin: Scanner, second: Scanner): Position? {
        for (b1 in origin.beacons) {
            val originBeacons = origin.beacons.toHashSet()
            for (b2 in second.beacons) {
                val delta = b1 - b2
                val secondBeacons = second.beacons.mapTo(HashSet()) { it + delta }
                if (originBeacons.intersect(secondBeacons).size >= 12) {
                    return delta
                }
            }
        }
        return null
    }

    class Scanner(val id: Int, val beacons: List<Position>) {
        override fun toString(): String {
            val b = beacons.joinToString(separator = "\n") { "${it.x},${it.y},${it.z}" }
            return "--- scanner $id ---\n$b"
        }

        fun rotations(): Sequence<Pair<Rotation, Scanner>> {
            return allRotations().map { r -> r to rotateThenMove(r, Position(0, 0, 0)) }
        }

        fun rotateThenMove(r: Rotation, p: Position): Scanner {
            return Scanner(id, beacons.map { r.rotate(it) + p })
        }
    }

    data class Position(val x: Int, val y: Int, val z: Int) {
        operator fun minus(o: Position): Position {
            return Position(x - o.x, y - o.y, z - o.z)
        }

        operator fun plus(d: Position): Position {
            return Position(x + d.x, y + d.y, z + d.z)
        }
    }

    class Rotation(val x: (Position) -> Int, val y: (Position) -> Int, val z: (Position) -> Int) {
        companion object {
            val empty = Rotation({ it.x }, { it.y }, { it.z })
        }

        fun rotate(p: Position): Position {
            return Position(x(p), y(p), z(p))
        }

        fun then(r: Rotation): Rotation {
            val r0 = this
            return Rotation({ r.x(r0.rotate(it)) }, { r.y(r0.rotate(it)) }, { r.z(r0.rotate(it)) })
        }
    }

    companion object {
        fun allRotations(): Sequence<Rotation> {
            val x = { p: Position -> p.x }
            val y = { p: Position -> p.y }
            val z = { p: Position -> p.z }

            return sequence {
                yieldAll(rotationsAroundX(x, y, z))
                yieldAll(rotationsAroundX(-x, y, -z))
                yieldAll(rotationsAroundX(y, -x, z))
                yieldAll(rotationsAroundX(-y, x, z))
                yieldAll(rotationsAroundX(z, y, -x))
                yieldAll(rotationsAroundX(-z, y, x))
            }
        }

        private fun rotationsAroundX(
            x: (Position) -> Int, y: (Position) -> Int, z: (Position) -> Int
        ): Sequence<Rotation> {
            return sequence {
                yield(x to y to z)
                yield(x to z to -y)
                yield(x to -y to -z)
                yield(x to -z to y)
            }.map { (xy, z) -> Rotation(xy.first, xy.second, z) }
        }

        private operator fun ((Position) -> Int).unaryMinus(): (Position) -> Int {
            return { -this(it) }
        }
    }
}
