package com.dou.advent.year2021

import com.dou.advent.Day

fun main() {
    Day6().run()
}

class Day6 : Day(2021, 6) {
    override val test1: Any
        get() = 5934
    override val test2: Any
        get() = 26984457539

    override fun part1(input: Sequence<String>): Any {
        var state = parse(input)
        for (i in 0 until 80) {
            state = state.nextDay()
        }
        return state.countDaysLeft.values.sum()
    }

    override fun part2(input: Sequence<String>): Any {
        var state = parse(input)
        for (i in 0 until 256) {
            state = state.nextDay()
        }
        return state.countDaysLeft.values.sum()
    }

    private fun parse(input: Sequence<String>): World {
        return input.single().split(',').map { it.toLong() }
            .groupBy { it }
            .mapValues { (_, v) -> v.size.toLong() }
            .let { World(it) }
    }

    private fun World.nextDay(): World {
        val result = HashMap<Long, Long>()
        for ((daysLeft, count) in countDaysLeft) {
            if (daysLeft > 0) {
                result.compute(daysLeft - 1) { _, v -> (v ?: 0) + count }
            } else {
                result.compute(6) { _, v -> (v ?: 0) + count }
                result.compute(8) { _, v -> (v ?: 0) + count }
            }
        }
        return World(result)
    }

    data class World(val countDaysLeft: Map<Long, Long>)
}
