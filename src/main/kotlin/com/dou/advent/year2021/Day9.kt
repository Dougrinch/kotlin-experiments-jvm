package com.dou.advent.year2021

import com.dou.advent.Day
import com.dou.advent.util.Grid
import com.dou.advent.util.Position

fun main() {
    Day9().run()
}

class Day9 : Day(2021, 9) {
    override val test1: Any
        get() = 15
    override val test2: Any
        get() = 1134

    override fun part1(input: Sequence<String>): Any {
        val grid = input.parse()
        return grid.lowPoints().map { c -> grid[c] }.sumOf { it + 1 }
    }

    override fun part2(input: Sequence<String>): Any {
        val grid = input.parse()
        val basins = grid.lowPoints().map { lp ->
            var basin = setOf(lp)
            do {
                val size = basin.size
                basin = basin.flatMapTo(HashSet()) { coord ->
                    grid.neighboursOf(coord, diagonal = false).filter { nc -> grid[nc] != 9 } + coord
                }
            } while (size != basin.size)
            basin.size
        }
        return basins.sortedDescending().take(3).reduce { a, b -> a * b }
    }

    private fun Sequence<String>.parse(): Grid<Int> =
        Grid(map { it.toCharArray().map { it.digitToInt() } }.toList())

    private fun Grid<Int>.lowPoints(): List<Position> {
        return positions().map { it to neighboursOf(it, diagonal = false) }
            .filter { (coord, ns) -> ns.all { ncoord -> get(ncoord) > get(coord) } }
            .map { it.first }
            .toList()
    }
}