package com.dou.advent.year2021

import com.dou.advent.Day
import com.dou.fp.e6.State

fun main() {
    Day2().run()
}

class Day2 : Day(2021, 2) {
    override val test1: Any
        get() = 150
    override val test2: Any
        get() = 900

    override fun part1(input: Sequence<String>): Any {
        return input
            .map { it.split(' ') }
            .map { it[0] to it[1].toInt() }
            .map { (cmd, n) ->
                when (cmd) {
                    "forward" -> update { it.copy(horizontal = it.horizontal + n) }
                    "down" -> update { it.copy(depth = it.depth + n) }
                    "up" -> update { it.copy(depth = it.depth - n) }
                    else -> throw IllegalStateException()
                }
            }
            .reduce { a, b -> a.flatMap { b } }
            .run(Position(0, 0, 0)).second
            .let { (h, d) -> h * d }
    }

    override fun part2(input: Sequence<String>): Any {
        return input
            .map { it.split(' ') }
            .map { it[0] to it[1].toInt() }
            .map { (cmd, n) ->
                when (cmd) {
                    "forward" -> update { it.copy(horizontal = it.horizontal + n, depth = it.depth + it.aim * n) }
                    "down" -> update { it.copy(aim = it.aim + n) }
                    "up" -> update { it.copy(aim = it.aim - n) }
                    else -> throw IllegalStateException()
                }
            }
            .reduce { a, b -> a.flatMap { b } }
            .run(Position(0, 0, 0)).second
            .let { (h, d) -> h * d }
    }

    data class Position(val horizontal: Int, val depth: Int, val aim: Int)

    fun update(body: (Position) -> Position): State<Position, Unit> {
        return State { Unit to body(it) }
    }
}
