package com.dou.advent.year2021

import arrow.core.*
import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day16.Packet
import com.dou.parser.*

fun main() {
    Day16().run()
}

class Day16 : TypedDay<Packet>() {
    override val test1: Any
        get() = 23

    override val parser = PacketParsers.transmission()

    override fun part1(input: Packet): Any {
        return versionSum(listOf(input))
    }

    override fun part2(input: Packet): Any {
        return eval(input)
    }

    private fun versionSum(ps: List<Packet>): Int {
        return ps.sumOf { p ->
            when (p) {
                is Packet.ValuePacket -> p.version
                is Packet.OperatorPacket -> p.version + versionSum(p.packets)
            }
        }
    }

    private fun eval(p: Packet): Long {
        return when (p) {
            is Packet.ValuePacket -> p.value
            is Packet.OperatorPacket -> when (p.typeId) {
                0 -> p.packets.sumOf { eval(it) }
                1 -> p.packets.fold(1) { acc, cp -> acc * eval(cp) }
                2 -> p.packets.minOf { eval(it) }
                3 -> p.packets.maxOf { eval(it) }
                5 -> if (eval(p.packets[0]) > eval(p.packets[1])) 1 else 0
                6 -> if (eval(p.packets[0]) < eval(p.packets[1])) 1 else 0
                7 -> if (eval(p.packets[0]) == eval(p.packets[1])) 1 else 0
                else -> throw IllegalStateException("${p.typeId}")
            }
        }
    }

    object PacketParsers {
        fun transmission() = packet() - trailingZeros()

        fun packet(): Parser<Packet> = version() - typeId() - { (_, t) -> packetValue(t) } map { v, t, c ->
            when (c) {
                is Either.Left -> Packet.ValuePacket(v, t, c.value)
                is Either.Right -> Packet.OperatorPacket(v, t, c.value)
            }
        }

        fun bits(n: Int) = regex("[01]{$n}")
        fun bitsNumber(n: Int) = regex("[01]{$n}") map { it.toInt(2) }

        fun version() = bitsNumber(3)
        fun typeId() = bitsNumber(3)

        fun nonTerminal() = "1" - bits(4)
        fun terminal() = "0" - bits(4)
        fun literal() = (nonTerminal().many() - terminal())
            .map { a, b -> (a + b).joinToString(separator = "") }
            .map { it.toLong(2) }

        fun packetValue(typeId: Int) = when (typeId) {
            4 -> literal().map { it.left() }
            else -> operatorValue().map { it.right() }
        }

        fun operatorValue(): Parser<List<Packet>> = oneOf(
            "0" - bitsNumber(15) - { n -> packet().many().limitConsumed(n) } map { _, ps -> ps },
            "1" - bitsNumber(11) - { n -> packet().many(exact = n) } map { _, ps -> ps }
        )

        fun trailingZeros() = regex("0*") map { }
    }

    override fun parseInput(input: String): Packet {
        val binaryInput = input
            .map { it.digitToInt(16).toString(2).padStart(4, '0') }
            .joinToString(separator = "")
        return super.parseInput(binaryInput)
    }

    sealed class Packet {
        abstract val version: Int
        abstract val typeId: Int

        data class ValuePacket(override val version: Int, override val typeId: Int, val value: Long) : Packet()
        data class OperatorPacket(
            override val version: Int, override val typeId: Int, val packets: List<Packet>
        ) : Packet()
    }
}
