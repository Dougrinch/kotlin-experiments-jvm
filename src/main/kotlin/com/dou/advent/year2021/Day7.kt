package com.dou.advent.year2021

import com.dou.advent.Day
import kotlin.math.abs

fun main() {
    Day7().run()
}

class Day7 : Day(2021, 7) {
    override val test1: Any
        get() = 37
    override val test2: Any
        get() = 168

    override fun part1(input: Sequence<String>): Any {
        val crabs = input.single().split(",").map { it.toInt() }
        return (crabs.minOf { it }..crabs.maxOf { it })
            .associateWith { target -> crabs.asSequence().map { abs(it - target) }.sum() }
            .minOf { it.value }
    }

    override fun part2(input: Sequence<String>): Any {
        val crabs = input.single().split(",").map { it.toInt() }
        return (crabs.minOf { it }..crabs.maxOf { it })
            .associateWith { target -> crabs.asSequence().map { abs(it - target).let { n -> n * (n + 1) / 2 } }.sum() }
            .minOf { it.value }
    }
}
