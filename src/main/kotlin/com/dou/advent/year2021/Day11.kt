package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.digit
import com.dou.parser.parser

fun main() {
    Day11().run()
}

class Day11 : TypedDay<MutableGrid<Int>>() {
    override val test1: Any
        get() = 1656
    override val test2: Any
        get() = 195

    override val parser = parser { grid(digit) }

    override fun part1(input: MutableGrid<Int>): Any {
        return generateSequence(input.nextStep()) { it.nextStep() }
            .map { it.values().count { it == 0 } }
            .take(100)
            .sum()
    }

    override fun part2(input: MutableGrid<Int>): Any {
        return generateSequence(input.nextStep()) { it.nextStep() }
            .map { it.values().any { it != 0 } }
            .takeWhile { it }
            .count() + 1
    }

    private fun MutableGrid<Int>.nextStep(): MutableGrid<Int> {
        val grid = this.copy()
        val toFlash = ArrayDeque<Position>()
        for (p in grid.positions()) {
            grid[p] += 1
            if (grid[p] > 9) {
                toFlash += p
            }
        }

        val flashed = mutableSetOf<Position>()
        while (toFlash.isNotEmpty()) {
            val c = toFlash.removeFirst()
            if (c !in flashed) {
                flashed += c
                for (nc in grid.neighboursOf(c, diagonal = true)) {
                    grid[nc] += 1
                    if (grid[nc] > 9) {
                        toFlash += nc
                    }
                }
            }
        }

        flashed.forEach { grid[it] = 0 }
        return grid
    }
}
