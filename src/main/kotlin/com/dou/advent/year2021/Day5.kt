package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day5.Line
import com.dou.parser.*
import kotlin.math.*

fun main() {
    Day5().run()
}

class Day5 : TypedDay<List<Line>>(2021, 5) {
    override val test1: Any
        get() = 5
    override val test2: Any
        get() = 12

    override val parser = parser {
        int - "," - int - " -> " - int - "," - int map ::Line separatedBy "\n"
    }

    override fun part1(input: List<Line>): Any {
        val grid = Grid(HashMap())
        for (line in input) {
            grid.markLine(line, false)
        }
        return grid.countDangerousAreas()
    }

    override fun part2(input: List<Line>): Any {
        val grid = Grid(HashMap())
        for (line in input) {
            grid.markLine(line, true)
        }
        return grid.countDangerousAreas()
    }

    data class Line(val x1: Int, val y1: Int, val x2: Int, val y2: Int)

    class Grid(val data: MutableMap<Pair<Int, Int>, Int>)

    private fun Grid.get(x: Int, y: Int): Int = data[x to y] ?: 0

    private fun Grid.set(x: Int, y: Int, value: Int) {
        data[x to y] = value
    }

    private fun Grid.inc(x: Int, y: Int) {
        val v = get(x, y)
        set(x, y, v + 1)
    }

    private fun Line.allPoints(includeDiagonal: Boolean): Sequence<Pair<Int, Int>> = sequence {
        if (x1 == x2) {
            for (y in min(y1, y2)..max(y1, y2)) {
                yield(x1 to y)
            }
        } else if (y1 == y2) {
            for (x in min(x1, x2)..max(x1, x2)) {
                yield(x to y1)
            }
        } else if (includeDiagonal && abs(x1 - x2) == abs(y1 - y2)) {
            val dx = (x2 - x1).sign
            val dy = (y2 - y1).sign
            val count = abs(x1 - x2)
            for (i in 0..count) {
                yield(x1 + dx * i to y1 + dy * i)
            }
        }
    }

    private fun Grid.markLine(line: Line, includeDiagonal: Boolean) {
        for ((x, y) in line.allPoints(includeDiagonal)) {
            inc(x, y)
        }
    }

    private fun Grid.countDangerousAreas(): Int = data.values.count { it >= 2 }
}
