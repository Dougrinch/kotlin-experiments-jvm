package com.dou.advent.year2021

import com.dou.advent.Day
import com.dou.advent.year2021.Day8.Digit.*

fun main() {
    Day8().run()
}

class Day8 : Day(2021, 8) {
    override val test1: Any
        get() = 26
    override val test2: Any
        get() = 61229

    override fun part1(input: Sequence<String>): Any {
        val uniqueSegments = listOf(One, Four, Seven, Eight).map { it.segments.size }

        return parse(input)
            .flatMap { (_, vs) -> vs }
            .count { it.size in uniqueSegments }
    }

    override fun part2(input: Sequence<String>): Any {
        return parse(input)
            .map { (segments, values) -> decode(segments) to values }
            .map { (segments, values) -> values.map { segments[it]!! } }
            .map { ds -> ds.map { it.value }.joinToString(separator = "") }
            .sumOf { it.toInt() }
    }

    private fun decode(segments: List<Set<Char>>): Map<Set<Char>, Digit> {
        val one = segments.single { it.size == 2 }
        val four = segments.single { it.size == 4 }
        val seven = segments.single { it.size == 3 }
        val eight = segments.single { it.size == 7 }
        val two = segments.filter { it.size == 5 }.single { (it + four).size == 7 }
        val five = segments.filter { it.size == 5 }.single { (it + two).size == 7 }
        val three = segments.filter { it.size == 5 }.single { it !in setOf(two, five) }
        val six = segments.filter { it.size == 6 }.single { (it + seven).size == 7 }
        val zero = segments.filter { it.size == 6 }.single { (it + five).size == 7 }
        val nine = segments.single { it !in setOf(zero, one, two, three, four, five, six, seven, eight) }

        val a = (seven - one).single()
        val b = (four - three).single()
        val c = (one - five).single()
        val d = (four - zero).single()
        val e = (two - five - c).single()
        val f = (one - c).single()
        val g = (nine - four - a).single()

        val conversions = mapOf(
            a to 'a',
            b to 'b',
            c to 'c',
            d to 'd',
            e to 'e',
            f to 'f',
            g to 'g'
        )

        return segments.associateWith { it.map { c -> conversions[c]!! }.toSet() }
            .mapValues { (_, v) -> Digit.values().single { d -> d.segments == v } }
    }

    private fun parse(input: Sequence<String>) = input.map { line ->
        val (rawSegments, rawValues) = line.split(" | ")
        val segments = rawSegments.split(" ").map { it.toCharArray().toSet() }
        val values = rawValues.split(" ").map { it.toCharArray().toSet() }
        segments to values
    }.toList()

    enum class Digit(val value: Int, segments: String) {
        One(1, "cf"),//2
        Seven(7, "acf"),//3
        Four(4, "bcdf"),//4
        Eight(8, "abcdefg"),//7

        Two(2, "acdeg"),//5
        Three(3, "acdfg"),//5
        Five(5, "abdfg"),//5

        Zero(0, "abcefg"),//6
        Six(6, "abdefg"),//6
        Nine(9, "abcdfg");//6

        val segments = segments.toCharArray().toSet()
    }
}
