package com.dou.advent.year2021

import com.dou.advent.TypedDay
import com.dou.advent.year2021.Day22.Cube
import com.dou.parser.*

fun main() {
    Day22().run()
}

class Day22 : TypedDay<List<Cube>>() {
    override val test1: Any
        get() = 474140
    override val test2: Any
        get() = 2758514936282235

    override val parser = parser {
        val action = string("on").parseAs(true) or string("off").parseAs(false)
        val range = int - ".." - int map { a, b -> a..b }
        val cube = action - " " - "x=" - range - ",y=" - range - ",z=" - range map ::Cube
        cube.separatedByLines()
    }

    override fun part1(input: List<Cube>): Any {
        return solve(input.map { it.limit(50) })
    }

    override fun part2(input: List<Cube>): Any {
        return solve(input)
    }

    private fun Cube.limit(n: Int): Cube {
        val xs = maxOf(xs.first, -n)..minOf(xs.last, n)
        val ys = maxOf(ys.first, -n)..minOf(ys.last, n)
        val zs = maxOf(zs.first, -n)..minOf(zs.last, n)
        return Cube(on, xs, ys, zs)
    }

    private fun solve(cubes: List<Cube>): Long {
        val volumes = mutableListOf<Cube>()

        for (cube in cubes) {
            volumes.addAll(volumes.mapNotNull { it.tryIntersect(cube) })
            if (cube.on) volumes.add(cube)
        }

        return volumes.sumOf { it.volume }
    }

    data class Cube(val on: Boolean, val xs: IntRange, val ys: IntRange, val zs: IntRange) {
        val volume: Long
            get() {
                val v = xs.size * ys.size * zs.size
                return if (on) v else -v
            }

        fun tryIntersect(other: Cube): Cube? {
            val xs = xs.tryIntersect(other.xs)
            val ys = ys.tryIntersect(other.ys)
            val zs = zs.tryIntersect(other.zs)
            return if (xs != null && ys != null && zs != null) Cube(!on, xs, ys, zs) else null
        }

        private fun IntRange.tryIntersect(other: IntRange): IntRange? {
            val r = maxOf(first, other.first)..minOf(last, other.last)
            return if (r.last >= r.first) r else null
        }

        private val IntRange.size: Long
            get() = maxOf(last.toLong() - first + 1, 0)
    }
}
