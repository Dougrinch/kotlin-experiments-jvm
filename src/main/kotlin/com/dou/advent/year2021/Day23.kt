package com.dou.advent.year2021

import arrow.core.Some
import com.dou.advent.TypedDay
import com.dou.advent.util.search
import com.dou.advent.year2021.Day23.Amphipod
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day23().run()
}

class Day23 : TypedDay<List<Amphipod>>() {
    override val test1: Any
        get() = 12521
    override val test2: Any
        get() = 44169

    override val parser = parser {
        val type = regex("[A-D]") map { AmphipodType.valueOf(it) }
        val amphipod = column.map { it - 1 } - line.map { it - 2 } - type map ::Amphipod
        val s = char('#', '.', '\n', ' ').many().ignoreValue()
        s - amphipod.separatedBy(s) - s
    }

    /**
     * 1.573352584s
     * 53.124282375s
     *
     * 686.011083ms
     * 31.175355541s
     */
//    @OptIn(ExperimentalTime::class)
//    override fun part1(input: List<Amphipod>): Any {
//        val r = measureTimedValue {
//            solve2(input)
//        }
//        println(r.duration)
//        return r.value
//    }

    override fun part2(input: List<Amphipod>): Any {
        val additional = parser.parse(
            """
            |  #D#C#B#A#
            |  #D#B#A#C#
            """.trimMargin()
        ).map { it.copy(depth = it.depth + 3) }
        val newInput = additional + input.map { if (it.depth == 2) it.copy(depth = 4) else it }
        return solve2(newInput)
    }

    private fun solve2(all: List<Amphipod>): Int {
        val result = search(
            all,
            { a -> generateSequence(0) { it + 1 }.take(a.size).flatMap { availableMoves(a, it) }.asIterable() },
            { it.estimatedCostToEnd() },
            { it.isDone() }
        )
//        println((result as Some).value.first.forEach { it.printBurrow() })
        return (result as Some).value.second
    }

    private fun List<Amphipod>.isDone(): Boolean {
        return all { a -> a.pos == a.type.target }
    }

    private fun List<Amphipod>.estimatedCostToEnd(): Int {
        return sumOf {
            val r = abs(it.pos - it.type.target)
            if (r == 0) {
                0
            } else if (it.depth != 0) {
                r * it.type.moveCost
            } else {
                (r + 1) * it.type.moveCost
            }
        }
    }

    private fun availableMoves(all: List<Amphipod>, idx: Int): Sequence<Pair<List<Amphipod>, Int>> {
        return sequence {
            val current = all[idx]

            if (current.depth == 0) {
                var otherInRoom = false

                for (other in all) {
                    if (other == current) continue

                    val min = minOf(current.type.target, current.pos)
                    val max = maxOf(current.type.target, current.pos)
                    if (other.depth == 0 && other.pos > min && other.pos < max) {
                        return@sequence
                    }

                    val inRoom = other.pos == current.type.target
                    if (other.type == current.type && inRoom) {
                        otherInRoom = true
                    } else if (inRoom) {
                        return@sequence
                    }
                }

                val depth = if (otherInRoom) 1 else 2
                yield(all.makeStep(idx, current, current.type.target, depth))
            } else if (current.pos == current.type.target && current.depth == 2) {
                return@sequence
            } else {
                if (current.pos == current.type.target) {
                    if (all.any { it != current && it.type == current.type && it.pos == it.type.target }) {
                        return@sequence
                    }
                }

                var left = -1
                var right = 11
                var otherInRoom = false
                var pathBlocked = false
                var roomAvailable = true

                val min = minOf(current.type.target, current.pos)
                val max = maxOf(current.type.target, current.pos)

                for (other in all) {
                    if (other == current) continue

                    if (!pathBlocked) {
                        if (other.depth == 0 && other.pos > min && other.pos < max) {
                            pathBlocked = true
                        }
                    }

                    if (other.depth == 0) {
                        if (other.pos < current.pos) {
                            left = maxOf(other.pos, left)
                        } else {
                            right = minOf(other.pos, right)
                        }
                    } else if (other.depth == 1 && other.pos == current.pos) {
                        return@sequence
                    } else if (other.pos == current.type.target) {
                        if (other.type == current.type) {
                            otherInRoom = true
                        } else {
                            roomAvailable = false
                        }
                    }
                }

                if (roomAvailable && !pathBlocked) {
                    val depth = if (otherInRoom) 1 else 2
                    yield(all.makeStep(idx, current, current.type.target, depth))
                }

                for (x in left + 1 until right) {
                    if (x !in rooms) {
                        yield(all.makeStep(idx, current, x, 0))
                    }
                }
            }
        }
    }

    private val rooms: List<Int> = AmphipodType.values().map { it.target }

    private fun List<Amphipod>.makeStep(idx: Int, current: Amphipod, toPos: Int, toDepth: Int): Pair<List<Amphipod>, Int> {
        val (newAmphipod, cost) = current.stepTo(toPos, toDepth)
        val newAll = ArrayList(this)
        newAll[idx] = newAmphipod
        return newAll to cost
    }

    private fun Amphipod.stepTo(toPos: Int, toDepth: Int): Pair<Amphipod, Int> {
        val steps = abs(pos - toPos) + abs(depth) + abs(toDepth)
        val cost = steps * type.moveCost
        return Amphipod(toPos, toDepth, type) to cost
    }

    private fun List<Amphipod>.printBurrow() {
        println("#############")
        print("#")
        for (x in 0..10) {
            print(find { it.pos == x && it.depth == 0 }?.type?.name ?: ".")
        }
        println("#")
        print("###")
        for (x in 0..6) {
            if (x % 2 == 0) {
                print(find { it.pos == x + 2 && it.depth == 1 }?.type?.name ?: ".")
            } else {
                print("#")
            }
        }
        println("###")
        print("  #")
        for (x in 0..6) {
            if (x % 2 == 0) {
                print(find { it.pos == x + 2 && it.depth == 2 }?.type?.name ?: ".")
            } else {
                print("#")
            }
        }
        println("#  ")
        println("  #########  ")
    }

    data class Amphipod(val pos: Int, val depth: Int, val type: AmphipodType)
    enum class AmphipodType(val target: Int, val moveCost: Int) {
        A(2, 1),
        B(4, 10),
        C(6, 100),
        D(8, 1000)
    }
}
