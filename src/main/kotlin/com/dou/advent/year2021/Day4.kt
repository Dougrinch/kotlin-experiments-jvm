package com.dou.advent.year2021

import com.dou.advent.Day
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.emptyString
import org.hamcrest.Matchers.equalTo

fun main() {
    Day4().run()
}

class Day4 : Day(2021, 4) {
    override val test1: Any
        get() = 4512
    override val test2: Any
        get() = 1924

    override fun part1(input: Sequence<String>): Any {
        val (numbers, boards) = parse(input)

        var remainingBoards = boards

        for (number in numbers) {
            remainingBoards = remainingBoards.map { board ->
                val newBoard = board.tryMark(number)
                if (newBoard.hasWon()) {
                    return score(newBoard, number)
                }
                newBoard
            }
        }

        throw IllegalStateException()
    }

    override fun part2(input: Sequence<String>): Any {
        val (numbers, boards) = parse(input)

        var remainingBoards = boards

        for (number in numbers) {
            remainingBoards = remainingBoards.map { board ->
                board.tryMark(number)
            }

            remainingBoards.singleOrNull()?.let { board ->
                if (board.hasWon()) {
                    return score(board, number)
                }
            }

            remainingBoards = remainingBoards.filterNot { it.hasWon() }
        }

        throw IllegalStateException()
    }

    private fun score(board: Board, lastNumber: Int): Int {
        return board.data.filterNot { it.marked }.map { it.value }.sum() * lastNumber
    }

    private fun parse(input: Sequence<String>): Pair<List<Int>, List<Board>> {
        val it = input.iterator()

        val numbers = it.next().split(',').map { it.toInt() }
        val boards = ArrayList<Board>()

        while (it.hasNext()) {
            assertThat(it.next(), emptyString())
            val data = ArrayList<BoardNumber>()
            for (i in 0 until 5) {
                val line = Regex("\\d+").findAll(it.next()).map { it.value }
                    .map { it.toInt() }.map { BoardNumber(it, false) }
                    .toList()
                assertThat(line.size, equalTo(5))
                data += line
            }
            boards += Board(5, 5, data)
        }

        return numbers to boards
    }

    data class BoardNumber(val value: Int, val marked: Boolean) {
        fun tryMark(n: Int): BoardNumber {
            return if (!marked && value == n) {
                BoardNumber(value, true)
            } else {
                this
            }
        }
    }

    data class Board(val width: Int, val height: Int, val data: List<BoardNumber>)

    private fun Board.tryMark(n: Int): Board {
        val newData = data.map { it.tryMark(n) }
        return copy(data = newData)
    }

    private fun Board.isMarked(x: Int, y: Int): Boolean {
        val idx = y * width + x
        return data[idx].marked
    }

    private fun Board.hasWon(): Boolean {
        val w = width
        val h = height

        for (x in 0 until w) {
            if ((0 until h).all { y -> isMarked(x, y) }) {
                return true
            }
        }

        for (y in 0 until h) {
            if ((0 until w).all { x -> isMarked(x, y) }) {
                return true
            }
        }

        return false
    }
}
