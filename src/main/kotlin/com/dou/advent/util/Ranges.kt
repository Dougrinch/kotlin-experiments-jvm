package com.dou.advent.util

import kotlin.math.max

val LongRange.length: Long
    get() = (endInclusive - start + 1).coerceAtLeast(0L)

val IntRange.length: Int
    get() = (endInclusive - start + 1).coerceAtLeast(0)

fun LongRange.shiftBy(offset: Long): LongRange {
    return (start + offset)..(endInclusive + offset)
}

fun IntRange.shiftBy(offset: Int): IntRange {
    return (start + offset)..(endInclusive + offset)
}

fun List<LongRange>.distinctRanges(): List<LongRange> {
    val result = arrayListOf<LongRange>()

    val unprocessed = toCollection(ArrayDeque())
        .apply {
            sortWith(compareBy { it.first })
        }

    while (unprocessed.isNotEmpty()) {
        val current = unprocessed.removeFirst()

        val first = current.first
        var last = current.last

        while (unprocessed.isNotEmpty() && unprocessed[0].first in first..last) {
            last = max(last, unprocessed.removeFirst().last)
        }

        result += first..last
    }

    return result
}

infix fun IntRange.intersectWith(right: IntRange): IntersectionResult<IntRange> {
    return intersectWith(right, Int::rangeTo, Int::until) { this + 1 }
}

infix fun LongRange.intersectWith(right: LongRange): IntersectionResult<LongRange> {
    return intersectWith(right, Long::rangeTo, Long::until) { this + 1 }
}

private fun <R : ClosedRange<T>, T : Comparable<T>> R.intersectWith(
    right: R,
    rangeTo: T.(T) -> R,
    until: T.(T) -> R,
    plusOne: T.() -> T
): IntersectionResult<R> {
    return when {
        start >= right.start && endInclusive <= right.endInclusive -> {
            IntersectionResult(
                listOf(this),
                listOf(),
                listOf(
                    right.start.until(start),
                    endInclusive.plusOne().rangeTo(right.endInclusive)
                ).filterNot { it.isEmpty() }
            )
        }

        right.start >= start && right.endInclusive <= endInclusive -> {
            IntersectionResult(
                listOf(right),
                listOf(
                    start.until(right.start),
                    right.endInclusive.plusOne().rangeTo(endInclusive)
                ).filterNot { it.isEmpty() },
                listOf()
            )
        }

        start > right.endInclusive || endInclusive < right.start -> {
            IntersectionResult(listOf(), listOf(this), listOf(right))
        }

        else -> {
            if (start < right.start) {
                val onlyLeft = start.until(right.start)
                val shared = right.start.rangeTo(endInclusive)
                val onlyRight = endInclusive.plusOne().rangeTo(right.endInclusive)
                IntersectionResult(listOf(shared), listOf(onlyLeft), listOf(onlyRight))
            } else {
                val onlyRight = right.start.until(start)
                val shared = start.rangeTo(right.endInclusive)
                val onlyLeft = right.endInclusive.plusOne().rangeTo(endInclusive)
                IntersectionResult(listOf(shared), listOf(onlyLeft), listOf(onlyRight))
            }
        }
    }
}

data class IntersectionResult<T>(
    val shared: List<T>, val onlyLeft: List<T>, val onlyRight: List<T>
)
