package com.dou.advent.util

import kotlinx.coroutines.*
import java.math.BigInteger
import java.security.MessageDigest
import java.time.Duration.between
import java.time.Duration.ofNanos
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.concurrent.atomic.AtomicInteger

fun input(year: Int, day: Int, test: Int? = null): String {
    if (test == null) {
        return tryGetInput(year, day)!!
    }

    return tryGetInput(year, day, test) ?: tryGetInput(year, day, 0)!!
}

private fun tryGetInput(year: Int, day: Int, test: Int? = null): String? {
    val testSuffix = if (test != null && test > 0) {
        "_test$test"
    } else if (test != null) {
        "_test"
    } else {
        ""
    }
    return Thread.currentThread().contextClassLoader
        .getResourceAsStream("com/dou/advent/year$year/Day$day$testSuffix.txt")
        ?.bufferedReader()?.readText()
}

fun md5(input: String): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
}

fun <T> MutableIterable<T>.removeAny(): T {
    return with(iterator()) {
        next().also { remove() }
    }
}

fun repeatPB(times: Int, action: (Int) -> Unit) {
    val counter = AtomicInteger()

    val start = LocalTime.now()

    @OptIn(DelicateCoroutinesApi::class)
    GlobalScope.launch {
        while (true) {
            delay(1000)
            val percents = counter.get().toDouble() / times * 100
            val now = LocalTime.now().withNano(0)
            val eta = ofNanos((between(start, now).toNanos() / percents * (100.0 - percents)).toLong())
            println("${String.format("%.2f%%", percents)} (ETA ${eta.withNanos(0)})")
        }
    }

    repeat(times) {
        action(it)
        counter.set(counter.get() + 1)
    }

    println("Total: ${between(start, LocalDateTime.now()).withNanos(0)}")
}

fun List<Int>.concat(): Int {
    return joinToString(separator = "").toInt()
}

fun List<Long>.concat(): Long {
    return joinToString(separator = "").toLong()
}

inline fun <T, R : Comparable<R>> Pair<T, T>.sortBy(crossinline selector: (T) -> R): Pair<T, T> {
    val f = selector(first)
    val s = selector(second)
    return if (f <= s) {
        this
    } else {
        second to first
    }
}
