package com.dou.advent.util

import arrow.core.*
import java.util.*
import kotlin.collections.ArrayDeque

fun <Node> search(
    start: Node,
    neighbours: (Node) -> Iterable<Pair<Node, Int>>,
    estimate: (Node) -> Int,
    found: (Node) -> Boolean,
): Option<Pair<List<Node>, Int>> {
    val costTo = HashMap<Node, Int>().withDefault { Int.MAX_VALUE }
    costTo[start] = 0

    val totalEstimated = HashMap<Node, Int>()
    totalEstimated[start] = estimate(start)

    val open = PriorityQueue(compareBy(totalEstimated::getValue))
    open += start

    val cameFrom = HashMap<Node, Node>()

    while (open.isNotEmpty()) {
        val current = open.poll()
        if (found(current)) {
            val result = ArrayDeque<Node>()
            result.addFirst(current)
            var node = current
            while (true) {
                val prev = cameFrom[node] ?: return (result to costTo.getValue(current)).some()
                result.addFirst(prev)
                node = prev
            }
        }

        for ((neighbour, nextStepCost) in neighbours(current)) {
            val currentCostTo = costTo.getValue(current) + nextStepCost
            if (currentCostTo < costTo.getValue(neighbour)) {
                cameFrom[neighbour] = current
                costTo[neighbour] = currentCostTo
                totalEstimated[neighbour] = currentCostTo + estimate(neighbour)
                if (neighbour !in open) {
                    open += neighbour
                }
            }
        }
    }

    return None
}
