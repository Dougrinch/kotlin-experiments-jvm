package com.dou.advent.util

import com.dou.advent.util.Direction.*

open class Grid<out T>(val lines: List<List<T>>) {
    constructor(width: Int, height: Int, value: (Position) -> T) :
        this((0 until height).map { y -> (0 until width).map { x -> value(Position(x, y)) } })

    init {
        check(lines.map { it.size }.distinct().size == 1)
    }

    val xs: IntRange = lines[0].indices
    val ys: IntRange = lines.indices

    val sizeX = xs.last + 1
    val sizeY = ys.last + 1

    open fun copy(): Grid<T> = MutableGrid(lines.map { ArrayList(it) })

    operator fun contains(pos: Position): Boolean {
        return pos.x in xs && pos.y in ys
    }

    operator fun get(pos: Position): T {
        return this[pos.x, pos.y]
    }

    operator fun get(x: Int, y: Int): T {
        return lines[y][x]
    }

    fun positions(): Sequence<Position> = sequence {
        for (x in xs) {
            for (y in ys) {
                yield(Position(x, y))
            }
        }
    }

    fun values(): Sequence<T> = positions().map(::get)

    fun neighboursOf(p: Position, diagonal: Boolean): List<Position> {
        return p.neighbours(diagonal).filter { nc -> getOrNull(nc) != null }
    }

    private fun getOrNull(pos: Position): T? = when {
        pos.x in xs && pos.y in ys -> get(pos)
        else -> null
    }

    fun toString(e: (T) -> Any?): String {
        return lines.joinToString(separator = "\n") { it.joinToString(separator = "") { e(it).toString() } }
    }

    override fun toString(): String {
        return toString { it }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Grid<*>) return false

        if (lines != other.lines) return false

        return true
    }

    override fun hashCode(): Int {
        return lines.hashCode()
    }

    fun <V> map(f: (T) -> V): Grid<V> {
        return Grid(lines.map { it.map(f) })
    }

    fun toMutableGrid(): MutableGrid<@UnsafeVariance T> {
        return MutableGrid(lines.map { it.toMutableList() })
    }
}

class MutableGrid<T>(lines: List<MutableList<T>>) : Grid<T>(lines) {
    override fun copy(): MutableGrid<T> = super.copy() as MutableGrid<T>

    operator fun set(pos: Position, value: T) {
        this[pos.x, pos.y] = value
    }

    operator fun set(x: Int, y: Int, value: T) {
        (lines[y] as MutableList<T>)[x] = value
    }

    override fun equals(other: Any?): Boolean {
        return (other as? Grid<*>)?.lines == lines
    }

    override fun hashCode(): Int {
        return lines.hashCode()
    }
}

data class Region(val minX: Int, val minY: Int, val maxX: Int, val maxY: Int)

operator fun Region.contains(p: Position): Boolean {
    return p.x in minX..maxX && p.y in minY..maxY
}

data class Position(val x: Int, val y: Int) {
    companion object {
        val zero = Position(0, 0)
        val horizontalNeighboursOffset = listOf(-1 to 0, 1 to 0, 0 to -1, 0 to 1)
        val diagonalNeighboursOffset = listOf(-1 to -1, -1 to 1, 1 to -1, 1 to 1)
    }

    fun neighbours(diagonal: Boolean): List<Position> = if (diagonal) {
        (horizontalNeighboursOffset + diagonalNeighboursOffset).map(::plus)
    } else {
        horizontalNeighboursOffset.map(::plus)
    }

    operator fun plus(p: Pair<Int, Int>): Position = Position(x + p.first, y + p.second)
    operator fun minus(p: Pair<Int, Int>): Position = Position(x - p.first, y - p.second)

    operator fun plus(d: Direction): Position = when (d) {
        Up -> Position(x, y - 1)
        Down -> Position(x, y + 1)
        Left -> Position(x - 1, y)
        Right -> Position(x + 1, y)
    }

    operator fun minus(p: Position): Pair<Int, Int> = Pair(x - p.x, y - p.y)
    operator fun minus(d: Direction): Position = plus(-d)

    operator fun plus(d: Direction8): Position = Position(x + d.dx, y + d.dy)
    operator fun minus(d: Direction8): Position = Position(x - d.dx, y - d.dy)
}

fun Pair<Int, Int>.norm(): Pair<Int, Int> {
    val nx = when {
        first > 0 -> 1
        first < 0 -> -1
        else -> 0
    }
    val ny = when {
        second > 0 -> 1
        second < 0 -> -1
        else -> 0
    }
    return nx to ny
}

enum class Direction {
    Up,
    Down,
    Left,
    Right;

    fun rotateClockwise(): Direction = when (this) {
        Up -> Right
        Right -> Down
        Down -> Left
        Left -> Up
    }

    fun rotateCounterclockwise(): Direction = when (this) {
        Up -> Left
        Left -> Down
        Down -> Right
        Right -> Up
    }

    operator fun unaryMinus(): Direction = when (this) {
        Up -> Down
        Down -> Up
        Left -> Right
        Right -> Left
    }
}

enum class Direction8(val dx: Int, val dy: Int) {
    Up(0, -1),
    Down(0, 1),
    Left(-1, 0),
    Right(1, 0),
    UpRight(1, -1),
    DownRight(1, 1),
    UpLeft(-1, -1),
    DownLeft(-1, 1)
}
