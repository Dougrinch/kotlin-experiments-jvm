package com.dou.advent.util

class Registers : Iterable<Pair<String, Int>> {
    private val data = HashMap<String, Int>().withDefault { 0 }

    operator fun get(r: String): Int {
        return data.getValue(r)
    }

    operator fun set(r: String, v: Int) {
        data[r] = v
    }

    override fun iterator(): Iterator<Pair<String, Int>> {
        val it = data.iterator()
        return iterator {
            for ((r, v) in it) {
                yield(r to v)
            }
        }
    }

    override fun toString(): String {
        return "Registers($data)"
    }
}
