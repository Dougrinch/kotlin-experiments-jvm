package com.dou.advent.util

fun <T> List<T>.ringListIterator(index: Int): ListIterator<T> {
    return RingIterator(this, index)
}

fun <T> List<T>.ringListIterator(): ListIterator<T> {
    return ringListIterator(0)
}

@JvmName("ringMutableListIterator")
fun <T> MutableList<T>.ringListIterator(index: Int): MutableListIterator<T> {
    return MutableRingIterator(this, index)
}

@JvmName("ringMutableListIterator")
fun <T> MutableList<T>.ringListIterator(): MutableListIterator<T> {
    return ringListIterator(0)
}

private class MutableRingIterator<T>(
    list: MutableList<T>, startIndex: Int
) : RingIterator<T>(list, startIndex), MutableListIterator<T> {

    override fun add(element: T) {
        (current as MutableListIterator<T>).add(element)
    }

    override fun remove() {
        (current as MutableListIterator<T>).remove()
    }

    override fun set(element: T) {
        (current as MutableListIterator<T>).set(element)
    }
}

private open class RingIterator<T>(private val list: List<T>, startIndex: Int) : ListIterator<T> {
    protected var current = list.listIterator(startIndex)

    override fun hasNext(): Boolean {
        return if (!current.hasNext()) {
            list.listIterator(0).hasNext()
        } else {
            current.hasNext()
        }
    }

    override fun next(): T {
        if (!current.hasNext()) {
            current = list.listIterator()
        }
        return current.next()
    }

    override fun nextIndex(): Int {
        return if (!current.hasNext()) {
            list.listIterator().nextIndex()
        } else {
            current.nextIndex()
        }
    }

    override fun hasPrevious(): Boolean {
        return if (!current.hasPrevious()) {
            list.listIterator(list.size).hasPrevious()
        } else {
            current.hasPrevious()
        }
    }

    override fun previous(): T {
        if (!current.hasPrevious()) {
            current = list.listIterator(list.size)
        }
        return current.previous()
    }

    override fun previousIndex(): Int {
        return if (!current.hasPrevious()) {
            list.listIterator(list.size).previousIndex()
        } else {
            current.previousIndex()
        }
    }
}
