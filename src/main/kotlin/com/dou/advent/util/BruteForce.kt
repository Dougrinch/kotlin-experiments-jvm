package com.dou.advent.util

fun <T> bruteForce(
    alphabet: List<T>,
    count: Int,
    copyLists: Boolean
): Sequence<List<T>> {
    return sequence {
        val current = (0 until count).mapTo(ArrayList()) { alphabet[0] }

        suspend fun SequenceScope<List<T>>.iterate(idx: Int) {
            if (idx == count) {
                if (copyLists) {
                    yield(ArrayList(current))
                } else {
                    yield(current)
                }
            } else {
                for (v in alphabet) {
                    current[idx] = v
                    iterate(idx + 1)
                }
            }
        }

        iterate(0)
    }
}

fun <Path, Point> bruteForce(
    initial: Path,
    potentialMoves: (Path) -> List<Point>,
    makeMove: (Path, Point) -> Move<Path>
): Sequence<Path> = sequence {
    val unprocessed = ArrayDeque<Path>()
    unprocessed += initial
    while (unprocessed.isNotEmpty()) {
        val path = unprocessed.removeFirst()
        val moves = potentialMoves(path)
        for (move in moves) {
            when (val moveResult = makeMove(path, move)) {
                is Move.Unacceptable -> {}
                is Move.PathCompleted -> yield(moveResult.path)
                is Move.Successful -> unprocessed += moveResult.path
            }
        }
    }
}

sealed class Move<out Path> {
    data object Unacceptable : Move<Nothing>()
    data class PathCompleted<Path>(val path: Path) : Move<Path>()
    data class Successful<Path>(val path: Path) : Move<Path>()
}
