package com.dou.advent.util

fun lcm(ns: List<Long>): Long {
    var result = ns[0]
    for (i in 1 until ns.size) {
        result *= (ns[i] / gcd(ns[i], result))
    }
    return result
}

fun gcd(a: Long, b: Long): Long {
    if (b == 0L) return a
    return gcd(b, a % b)
}
