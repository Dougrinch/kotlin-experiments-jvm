package com.dou.advent.util

import com.dou.parser.*

fun <A> grid(
    el: Parser<A>,
    columnSep: String = "",
    rowSep: String = "\n",
    tailPadding: A? = null
): Parser<MutableGrid<A>> {
    val cs = string(columnSep).ignoreValue()
    val rs = string(rowSep).ignoreValue()
    return grid(el, cs, rs, tailPadding)
}

fun <A> grid(
    el: Parser<A>,
    columnSep: Parser<Unit>,
    rowSep: Parser<Unit>,
    tailPadding: A? = null
): Parser<MutableGrid<A>> {
    return el.many(min = 1, separator = columnSep) map { it.toMutableList() } separatedBy rowSep map { cells ->
        val maxSize = cells.maxOf { it.size }
        cells.map { it.apply { addAll(generateSequence { tailPadding!! }.take(maxSize - it.size).toList()) } }
    } map ::MutableGrid
}
