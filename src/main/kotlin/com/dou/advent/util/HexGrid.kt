package com.dou.advent.util

import com.dou.advent.util.HexDirection.*
import kotlin.math.abs

data class HexPosition(val q: Int, val r: Int) {
    val s: Int = -q - r

    companion object {
        val neighboursOffset = mapOf(
            North to (0 to -1),
            NorthEast to (1 to -1),
            SouthEast to (1 to 0),
            South to (0 to 1),
            SouthWest to (-1 to 1),
            NorthWest to (-1 to 0),
        )
    }

    fun neighbours(): List<HexPosition> {
        return HexDirection.values().map(::plus)
    }

    operator fun plus(dir: HexDirection): HexPosition {
        val (dq, dr) = neighboursOffset[dir]!!
        return HexPosition(q + dq, r + dr)
    }

    fun distanceTo(o: HexPosition): Int {
        return maxOf(abs(q - o.q), abs(r - o.r), abs(s - o.s))
    }

    fun relativeTo(zero: HexPosition): HexPosition {
        return HexPosition(q - zero.q, r - zero.r)
    }
}

enum class HexDirection {
    North, NorthEast, SouthEast, South, SouthWest, NorthWest
}
