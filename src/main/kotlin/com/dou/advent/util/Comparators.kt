package com.dou.advent.util

import kotlin.math.min

fun <T> compareLists(
    elementComparator: Comparator<T>, shorterFirst: Boolean = true
): Comparator<List<T>> {
    return Comparator { l1, l2 ->
        for (i in 0 until min(l1.size, l2.size)) {
            val r = elementComparator.compare(l1[i], l2[i])
            if (r != 0) {
                return@Comparator r
            }
        }

        if (l1.size != l2.size) {
            return@Comparator if (l1.size < l2.size && shorterFirst) {
                -1
            } else {
                1
            }
        }

        0
    }
}
