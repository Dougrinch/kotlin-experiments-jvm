package com.dou.advent.util

fun <T> List<T>.permutations(): Sequence<List<T>> {
    return permutations(this, ArrayDeque())
}

private fun <T> permutations(rest: List<T>, stack: ArrayDeque<T>): Sequence<List<T>> {
    return sequence {
        if (rest.size == 1) {
            yield(stack + rest.single())
        } else {
            for (i in rest.indices) {
                stack.addLast(rest[i])
                yieldAll(permutations(rest.subList(0, i) + rest.subList(i + 1, rest.size), stack))
                stack.removeLast()
            }
        }
    }
}