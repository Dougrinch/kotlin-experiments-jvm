package com.dou.advent

import com.dou.advent.util.input
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import kotlin.time.measureTimedValue

abstract class AbstractDay<Input>(year: Int? = null, day: Int? = null) {
    private val year: Int by lazy {
        year ?: this::class.qualifiedName!!.split('.').let { it[it.size - 2] }
            .removePrefix("year")
            .toInt()
    }
    private val day: Int by lazy {
        day ?: this::class.simpleName!!.removePrefix("Day").toInt()
    }

    open val test1: Any get() = TODO()
    open val test2: Any get() = TODO()

    abstract fun parseInput(input: String): Input

    open fun part1(input: Input): Any = TODO()
    open fun part2(input: Input): Any = TODO()

    fun run() {
        test(::part1, ::test1, 1)
        run("Part One", ::part1)

        test(::part2, ::test2, 2)
        run("Part Two", ::part2)
    }

    private fun test(part: (Input) -> Any, test: () -> Any, idx: Int) {
        try {
            val expected = test()
            val actual = part(parseInput(input(year, day, test = idx)))
            assertThat(actual.toString(), equalTo(expected.toString()))
        } catch (_: NotImplementedError) {
        }
    }

    private fun run(name: String, part: (Input) -> Any) {
        try {
            val (answer, duration) = measureTimedValue {
                part(parseInput(input(year, day)))
            }
            println("$name: $answer ($duration)")
        } catch (_: NotImplementedError) {
        }
    }
}
