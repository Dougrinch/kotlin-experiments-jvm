package com.dou.advent

import com.dou.parser.Parser
import com.dou.parser.parse

fun (() -> TypedDay<*>).run() {
    invoke().run()
}

abstract class TypedDay<Input>(year: Int? = null, day: Int? = null) : AbstractDay<Input>(year, day) {
    abstract val parser: Parser<Input>

    override fun parseInput(input: String): Input {
        return parser.parse(input)
    }
}
