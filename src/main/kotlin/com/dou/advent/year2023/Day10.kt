package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction.*
import com.dou.advent.year2023.Day10.Cell
import com.dou.advent.year2023.Day10.Cell.*
import com.dou.parser.*

fun main() {
    Day10().run()
}

class Day10 : TypedDay<Grid<Cell>>() {
    override val test1: Any
        get() = 8

    override val test2: Any
        get() = 8

    override val parser = parser {
        val cell = oneOf(
            const(".") parseAs Empty,
            const("S") parseAs Animal,
            const("|") parseAs Pipe("|", listOf(Up, Down)),
            const("-") parseAs Pipe("-", listOf(Left, Right)),
            const("L") parseAs Pipe("L", listOf(Up, Right)),
            const("J") parseAs Pipe("J", listOf(Up, Left)),
            const("7") parseAs Pipe("7", listOf(Down, Left)),
            const("F") parseAs Pipe("F", listOf(Down, Right)),
        )
        grid(cell)
    }

    override fun part1(input: Grid<Cell>): Any {
        val pipe = findMainPipe(input)
        return pipe.size / 2
    }

    override fun part2(input: Grid<Cell>): Any {
        val pipe = findMainPipe(input).toSet()

        val within = input.positions()
            .filter { it !in pipe }
            .filter { countIntersections(it, input, pipe) % 2 == 1 }
            .toSet()

        val r = input.ys.joinToString(separator = "\n") { y ->
            input.xs.joinToString("") { x ->
                when (val p = Position(x, y)) {
                    in within -> "I"
                    !in pipe -> "O"
                    else -> when (val c = input[p]) {
                        Animal -> "S"
                        is Pipe -> c.name
                        else -> ""
                    }
                }
            }
        }

//        println(r)

        return within.count()
    }

    private fun countIntersections(from: Position, input: Grid<Cell>, mainPipe: Set<Position>): Int {
        var result = 0
        var pos = from
        var enterDir: Direction? = null
        while (pos in input) {
            pos += Right
            if (pos in mainPipe) {
                val dirs = when (val cell = input[pos]) {
                    is Pipe -> cell.dirs
                    is Animal -> findAnimal(input).second.dirs
                    else -> throw IllegalStateException()
                }
                when {
                    dirs.toSet() == setOf(Up, Down) -> {
                        result += 1
                    }

                    dirs.toSet() == setOf(Left, Right) -> {
                    }

                    Right in dirs -> {
                        enterDir = dirs.single { it != Right }
                    }

                    dirs.count { it == Left } == 1 -> {
                        if (dirs.single { it != Left } != enterDir!!) {
                            result += 1
                        }
                    }
                }
            }
        }
        return result
    }

    private fun findAnimal(input: Grid<Cell>): Pair<Position, Pipe> {
        val animal = input.positions().single { input[it] is Animal }
        val animalNeighbors = input.neighboursOf(animal, false)
            .filter { input[it] is Pipe }
            .map { n -> n to (input[n] as Pipe).dirs.singleOrNull { n + it == animal } }
            .filter { it.second != null }
        check(animalNeighbors.size == 2)
        return animal to Pipe("S", animalNeighbors.map { -it.second!! })
    }

    private fun findMainPipe(input: Grid<Cell>): List<Position> = buildList {
        val (start, animalPipe) = findAnimal(input)

        var pipeCount = 1
        var pos = start
        add(pos)
        var prev = pos
        pos += animalPipe.dirs[0]
        add(pos)
        while (pos != start) {
            pipeCount += 1
            val nextDir = (input[pos] as Pipe).dirs.single { pos + it != prev }
            prev = pos
            pos += nextDir
            add(pos)
        }
    }

    sealed interface Cell {
        data class Pipe(val name: String, val dirs: List<Direction>) : Cell
        data object Empty : Cell
        data object Animal : Cell
    }
}
