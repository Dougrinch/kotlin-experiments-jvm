package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.year2023.Day24.*
import com.dou.math.*
import com.dou.math.space.*
import com.dou.parser.*

fun main() {
    ::Day24.run()
}

class Day24 : TypedDay<List<Line<Long>>>(), DoubleOps {

    override val parser = parser {
        val pos = long - "," - ws - long - "," - ws - long map ::Point3D
        val vec = long - "," - ws - long - "," - ws - long map ::Vector3D

        pos - ws - "@" - ws - vec map ::Line separatedBy newLine
    }

    override fun part1(input: List<Line<Long>>): Any {
        val lines = input.map { it.run { copy(p = p.copy(z = 0), v = v.copy(z = 0)) }.map(Long::toDouble) }

        var result = 0
        val range = 200000000000000.0..400000000000000.0
        for (i1 in input.indices) {
            val l1 = lines[i1]
            for (i2 in (i1 + 1)..<input.size) {
                val l2 = lines[i2]
                val p = getNearestPoints(l1, l2)
                if (p != null) {
                    val (x, y) = p.first
                    if (x in range && y in range) {
                        result += 1
                    }
                }
            }
        }
        return result
    }

    private fun getNearestPoints(l1: Line<Double>, l2: Line<Double>): Pair<DoublePoint3D, DoublePoint3D>? {
        val n = l1.v x l2.v
        val n2 = l2.v x n
        val n1 = l1.v x n

        val c1 = l1.p + ((l2.p - l1.p) * n2) / (l1.v * n2) * l1.v
        val c2 = l2.p + ((l1.p - l2.p) * n1) / (l2.v * n1) * l2.v

        return if (c1.notInPastOn(l1) && c2.notInPastOn(l2)) {
            c1 to c2
        } else {
            null
        }
    }

    override fun part2(input: List<Line<Long>>): Any {
        val vs = (0..2).map { i ->
            input.groupBy { it.v[i] }.values
                .asSequence()
                .map { it.distinctBy { it.p[i] } }
                .filter { it.size > 1 }
                .map { ls ->
                    val l1 = ls[0]
                    val l2 = ls[1]
                    val v = l1.v[i]

                    (-1000L..1000L)
                        .filter { it != v }
                        .filterTo(hashSetOf()) { iv ->
                            (l1.p[i] - l2.p[i]) % (iv - v) == 0L
                        }
                }
                .reduce(Iterable<Long>::intersect)
                .single()
        }

        return vs.indices.sumOf { i -> input.filter { it.v[i] == vs[i] }.map { it.p[i] }.distinct().single() }
    }

    private fun DoublePoint3D.notInPastOn(l: Line<Double>): Boolean {
        return (this - l.p) * l.v >= 0
    }

    data class Line<T>(val p: Point3D<T>, val v: Vector3D<T>) {
        fun <G> map(f: (T) -> G): Line<G> {
            return Line(p.map(f), v.map(f))
        }
    }
}
