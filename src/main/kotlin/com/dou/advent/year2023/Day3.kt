package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction.Left
import com.dou.advent.util.Direction.Right
import com.dou.advent.year2023.Day3.Cell
import com.dou.parser.*

fun main() {
    Day3().run()
}

class Day3 : TypedDay<Grid<Cell>>() {

    override val test1: Any
        get() = 4361

    override val test2: Any
        get() = 467835

    override val parser = parser {
        val d by digit map ::Digit
        val n by "." parseAs None
        val s by char map ::Symbol
        grid(oneOf(d, n, s))
    }

    override fun part1(input: Grid<Cell>): Int {
        var result = 0

        for (y in input.ys) {
            var current = 0
            var isNumber = false

            for (x in input.xs) {
                val pos = Position(x, y)
                when (val cell = input[pos]) {
                    is Digit -> {
                        current = current * 10 + cell.d
                        if (!isNumber) {
                            isNumber = input.neighboursOf(pos, true).any { input[it] is Symbol }
                        }
                    }

                    else -> {
                        if (isNumber) {
                            result += current
                        }
                        isNumber = false
                        current = 0
                    }
                }
            }

            if (isNumber) {
                result += current
            }
        }

        return result
    }

    override fun part2(input: Grid<Cell>): Int {
        var finalResult = 0

        for (currentPos in input.positions()) {
            if (input[currentPos] != Symbol('*')) {
                continue
            }

            val digits = hashSetOf<Position>()
            for (p in input.neighboursOf(currentPos, true)) {
                if (input[p] is Digit) {
                    digits += p
                }
            }

            fun extractNumber(partDigits: HashSet<Position>): Int? {
                if (partDigits.isEmpty()) {
                    return null
                }
                val start = partDigits.first()
                partDigits.remove(start)

                var result = (input[start] as Digit).d

                var pos = start
                var scale = 10
                pos += Left
                while (pos in input && input[pos] is Digit) {
                    partDigits.remove(pos)
                    result += scale * (input[pos] as Digit).d
                    scale *= 10
                    pos += Left
                }

                pos = start
                pos += Right
                while (pos in input && input[pos] is Digit) {
                    partDigits.remove(pos)
                    result = result * 10 + (input[pos] as Digit).d
                    pos += Right
                }

                return result
            }

            val first = extractNumber(digits)
            val second = extractNumber(digits)

            if (digits.isEmpty() && first != null && second != null) {
                finalResult += first * second
            }
        }

        return finalResult
    }

    sealed interface Cell
    data class Digit(val d: Int) : Cell
    data class Symbol(val s: Char) : Cell
    data object None : Cell
}
