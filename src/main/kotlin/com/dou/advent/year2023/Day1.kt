package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day1().run()
}

class Day1 : TypedDay<List<String>>() {

    override val test1: Any
        get() = 142

    override val test2: Any
        get() = 281

    override val parser = parser { word.separatedByLines() }

    override fun part1(input: List<String>): Int {
        return input.sumOf { line ->
            val a = line.first { it.isDigit() }.digitToInt()
            val b = line.last { it.isDigit() }.digitToInt()
            a * 10 + b
        }
    }

    override fun part2(input: List<String>): Int {
        val numbers = arrayOf("one", "two", "three", "four", "five", "six", "seven", "eight", "nine")
            .withIndex().map { (idx, n) -> n to idx + 1 }
        return input.sumOf { line ->
            val sa = numbers.map { it to line.indexOf(it.first) }.filter { it.second >= 0 }.minByOrNull { it.second }
            val sb = numbers.map { it to line.lastIndexOf(it.first) }.filter { it.second >= 0 }.maxByOrNull { it.second }
            val ai = line.indexOfFirst { it.isDigit() }
            val bi = line.indexOfLast { it.isDigit() }

            val a = if (sa != null && (ai < 0 || sa.second < ai)) {
                sa.first.second
            } else {
                line[ai].digitToInt()
            }

            val b = if (sb != null && (bi < 0 || sb.second > bi)) {
                sb.first.second
            } else {
                line[bi].digitToInt()
            }

            a * 10 + b
        }
    }
}
