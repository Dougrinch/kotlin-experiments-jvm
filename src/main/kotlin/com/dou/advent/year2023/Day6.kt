package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.concat
import com.dou.parser.*

fun main() {
    Day6().run()
}

class Day6 : TypedDay<Pair<List<Long>, List<Long>>>() {
    override val test1: Any
        get() = 288
    override val test2: Any
        get() = 71503

    override val parser = parser {
        val time = "Time:" - ws - long.separatedBy(ws)
        val distance = "Distance:" - ws - long.separatedBy(ws)
        time - "\n" - distance
    }

    override fun part1(input: Pair<List<Long>, List<Long>>): Long {
        val time = input.first
        val distance = input.second
        return time.indices.map { i ->
            val t = time[i]
            (0..t).count { p ->
                (t - p) * p > distance[i]
            }.toLong()
        }.reduce(Long::times)
    }

    override fun part2(input: Pair<List<Long>, List<Long>>): Long {
        return part1(listOf(input.first.concat()) to listOf(input.second.concat()))
    }
}
