package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.advent.util.Direction.*
import com.dou.advent.util.Position.Companion.zero
import com.dou.advent.year2023.Day16.Cell
import com.dou.parser.*

fun main() {
    ::Day16.run()
}

class Day16 : TypedDay<Grid<Cell>>() {
    override val test1: Any
        get() = 46

    override val test2: Any
        get() = 51

    override val parser = parser {
        val cell = oneOf(
            const(".") parseAs Empty,
            oneOf("/", "\\") map ::Mirror,
            oneOf("|", "-") map ::Splitter
        )
        grid(cell)
    }

    override fun part1(input: Grid<Cell>): Any {
        return solve(input, Position(-1, 0) to Right)
    }

    override fun part2(input: Grid<Cell>): Any {
        return sequence {
            for (x in input.xs) {
                yield(Position(x, -1) to Down)
                yield(Position(x, input.sizeY) to Up)
            }
            for (y in input.ys) {
                yield(Position(-1, y) to Right)
                yield(Position(input.sizeX, y) to Left)
            }
        }.maxOf { solve(input, it) }
    }

    private fun solve(input: Grid<Cell>, start: Pair<Position, Direction>): Int {
        val visited = hashSetOf<Pair<Position, Direction>>()
        val energized = input.map { false }.toMutableGrid()
        val rays = ArrayDeque(listOf(start))
        while (rays.isNotEmpty()) {
            val ray = rays.removeFirst()
            if (ray in visited) {
                continue
            }
            visited += ray
            val (from, dir) = ray
            val next = from + dir
            if (next !in input) {
                continue
            }
            energized[next] = true

            when (val nextCell = input[next]) {
                is Empty -> {
                    rays += next to dir
                }

                is Mirror -> {
                    val nextDir = if (nextCell.v == "/") {
                        when (dir) {
                            Right -> Up
                            Up -> Right
                            Left -> Down
                            Down -> Left
                        }
                    } else {
                        when (dir) {
                            Right -> Down
                            Up -> Left
                            Left -> Up
                            Down -> Right
                        }
                    }

                    rays += next to nextDir
                }

                is Splitter -> {
                    if (nextCell.v == "-") {
                        rays += next to Left
                        rays += next to Right
                    } else {
                        rays += next to Up
                        rays += next to Down
                    }
                }
            }
        }
        return energized.values().count { it }
    }

    sealed interface Cell
    data object Empty : Cell
    data class Mirror(val v: String) : Cell
    data class Splitter(val v: String) : Cell
}
