package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.advent.year2023.Day22.Brick
import com.dou.parser.*
import kotlin.math.max
import kotlin.math.min

fun main() {
    ::Day22.run()
}

class Day22 : TypedDay<List<Brick>>() {
    override val test1: Any
        get() = 5

    override val test2: Any
        get() = 7

    override val parser = parser {
        val name = ("   <- " - word).optional()
        val brick =
            int - "," - int - "," - int - "~" - int - "," - int - "," - int - name map { x1, y1, z1, x2, y2, z2, n ->
                Brick(
                    min(x1, x2)..max(x1, x2),
                    min(y1, y2)..max(y1, y2),
                    min(z1, z2)..max(z1, z2),
                    n
                )
            }
        brick.separatedByLines()
    }

    override fun part1(input: List<Brick>): Any {
        val bricks = fall(input).first
        return bricks.count { brick ->
            (bricks - brick).let { fall(it).second == 0 }
        }
    }

    override fun part2(input: List<Brick>): Any {
        val bricks = fall(input).first
        return bricks.sumOf { brick ->
            (bricks - brick).let { fall(it).second }
        }
    }

    private fun fall(input: List<Brick>): Pair<List<Brick>, Int> {
        val grid = hashMapOf<Int, MutableMap<Int, Pair<Int, Brick>?>>()

        val result = ArrayList<Brick>()
        var moved = 0

        for (brick in input.sortedBy { it.zs.first }) {
            val targetZ = brick.xs.maxOf { x ->
                brick.ys.maxOf { y ->
                    grid[x]?.get(y)?.first ?: 0
                }
            } + brick.zs.length
            val offset = targetZ - brick.zs.last
            if (offset != 0) {
                moved += 1
            }
            val zs = brick.zs.shiftBy(offset)
            val movedBrick = brick.copy(zs = zs)
            result += movedBrick
            for (x in brick.xs) {
                for (y in brick.ys) {
                    grid.computeIfAbsent(x) { hashMapOf() }[y] = targetZ to movedBrick
                }
            }
        }

        return result to moved
    }

    data class Brick(val xs: IntRange, val ys: IntRange, val zs: IntRange, val name: String?)
}
