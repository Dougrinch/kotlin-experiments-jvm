package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.parser.*
import kotlinx.coroutines.*
import java.util.ArrayList
import java.util.concurrent.ConcurrentHashMap

fun main() {
    ::Day25.run()
}

class Day25 : TypedDay<List<Pair<String, List<String>>>>() {
    override val test1: Any
        get() = 54

    override val parser = parser {
        word - ": " - word.separatedBy(" ") separatedBy newLine
    }

    override fun part1(input: List<Pair<String, List<String>>>): Any {
        val rawEdges = input.flatMap { (a, b) -> b.map { Edge(a, it) } }.distinct()

        val edges = HashMap<String, List<Pair<String, Edge>>>()
        for (edge in rawEdges) {
            (edges.computeIfAbsent(edge.f) { ArrayList() } as MutableList) += edge.s to edge
            (edges.computeIfAbsent(edge.s) { ArrayList() } as MutableList) += edge.f to edge
        }

        val counts = ConcurrentHashMap<Edge, Int>()
        val nodes = edges.keys.toList()
        val jobs = mutableListOf<Job>()
        for (i in nodes.indices) {
            @OptIn(DelicateCoroutinesApi::class)
            jobs += GlobalScope.launch {
                println(i)
                val node = nodes[i]
                for (j in (i + 1)..<nodes.size) {
                    val other = nodes[j]

                    search<Pair<String, Edge?>>(
                        node to null,
                        { (current, _) -> edges[current]!!.map { it to 1 } },
                        { 0 },
                        { it.first == other }
                    ).getOrNull()!!.first.forEach { (_, edge) ->
                        if (edge != null) {
                            counts.compute(edge) { _, old -> (old ?: 0) + 1 }
                        }
                    }
                }
            }
        }

        runBlocking {
            for (job in jobs) {
                job.join()
            }
        }

        val connectors = counts.toList().sortedByDescending { it.second }.take(3).map { it.first }.toSet()

        val start = (nodes - connectors)[0]
        val graphSize = bruteForce(
            start,
            { p -> edges[p]!! },
            { f, (n, e) ->
                when {
                    e !in connectors -> Move.PathCompleted(n)
                    else -> Move.Unacceptable
                }
            }
        )

        var a = 0L
        for (e1 in rawEdges.indices) {
            for (e2 in e1 + 1..<rawEdges.size) {
                for (e3 in e2 + 1..<rawEdges.size) {
                    a += 1
                }
            }
        }

        return a
    }

    data class Edge(val f: String, val s: String)
}
