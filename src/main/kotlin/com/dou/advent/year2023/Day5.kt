package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.*

fun main() {
    Day5().run()
}

class Day5 : TypedDay<Pair<List<Long>, List<Pair<Pair<String, String>, List<Pair<LongRange, Long>>>>>>() {
    override val test1: Any
        get() = 35

    override val test2: Any
        get() = 46

    override val parser = parser {
        val ints by long separatedBy " "
        val seeds by "seeds: " - ints
        val mapping by word - "-to-" - word - " map:\n" - ints.separatedByLines().map { ms ->
            ms.map { (dst, src, length) -> src until (src + length) to dst - src }
        }
        seeds - "\n\n" - mapping.separatedBy("\n\n")
    }

    override fun part1(
        input: Pair<List<Long>, List<Pair<Pair<String, String>, List<Pair<LongRange, Long>>>>>
    ): Long {
        val numbers = input.first
        val mappings = input.second.associateBy({ it.first.first }, { it.first.second to it.second })

        var currentStep = "seed"
        var currentNumbers = numbers

        while (currentStep != "location") {
            val (nextStep, mapping) = mappings[currentStep]!!
            currentNumbers = currentNumbers.map { n ->
                val offset = mapping.find { n in it.first }?.second ?: 0L
                n + offset
            }
            currentStep = nextStep
        }

        return currentNumbers.min()
    }

    override fun part2(
        input: Pair<List<Long>, List<Pair<Pair<String, String>, List<Pair<LongRange, Long>>>>>
    ): Long {
        val numbers = input.first.chunked(2).map { (from, length) -> from until (from + length) }
        val mappings = input.second.associateBy({ it.first.first }, { it.first.second to it.second })

        var currentStep = "seed"
        var currentNumbers = numbers

        while (currentStep != "location") {
            val (nextStep, mapping) = mappings[currentStep]!!
            currentNumbers = currentNumbers.flatMap { ns ->
                val processed = arrayListOf<LongRange>()
                val unhandledRanges = arrayListOf(ns)
                uh@ while (unhandledRanges.isNotEmpty()) {
                    val unhandled = unhandledRanges.removeLast()
                    for ((range, offset) in mapping) {
                        val (inRange, notInRange, _) = unhandled.intersectWith(range)
                        if (inRange.isNotEmpty()) {
                            unhandledRanges += notInRange
                            processed += inRange.map { it.shiftBy(offset) }
                            continue@uh
                        }
                    }
                    processed += unhandled
                }
                processed
            }.distinctRanges()
            currentStep = nextStep
        }

        return currentNumbers.minOf { it.first }
    }
}
