package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.ringListIterator
import com.dou.parser.*

fun main() {
    Day8().run()
}

class Day8 : TypedDay<Pair<String, Map<String, Pair<String, String>>>>() {
    override val test2: Any
        get() = 6

    override val parser = parser {
        val path = regex("[LR]+")
        val step = word - " = (" - word - ", " - word - ")" map { a, b, c -> a to (b to c) }
        path - "\n\n" - step.separatedByLines().map { it.toMap() }
    }

    override fun part1(input: Pair<String, Map<String, Pair<String, String>>>): Int {
        val path = input.first.toList().ringListIterator()
        var current = "AAA"
        var steps = 0
        for (step in path) {
            if (current == "ZZZ") {
                break
            }
            steps += 1
            current = input.second[current]!!.let { if (step == 'R') it.second else it.first }
        }
        return steps
    }

    override fun part2(input: Pair<String, Map<String, Pair<String, String>>>): Long {
        val path = input.first.toList().ringListIterator()
        var current = input.second.keys.filter { it.endsWith('A') }

        var steps = 0L

        val offsets = Array(current.size) { -1L }
        val loopSizes = Array(current.size) { -1L }
        val loopMarker = Array(current.size) { "" }
        for (step in path) {
            for (i in current.indices) {
                if (current[i].endsWith('Z')) {
                    if (offsets[i] == -1L) {
                        offsets[i] = steps
                    } else if (loopMarker[i] == "") {
                        loopMarker[i] = current[i]
                        loopSizes[i] = steps - offsets[i]
                    } else {
                        check(current[i] == loopMarker[i])
                    }
                }
            }
            if (loopSizes.all { it != -1L }) {
                break
            }
            steps += 1
            current = current.map { input.second[it]!!.let { if (step == 'R') it.second else it.first } }
        }

        val maxLoopSize = loopSizes.maxOf { it }
        while (true) {
            if (offsets.indices.all { (steps - offsets[it]) % loopSizes[it] == 0L }) {
                break
            }
            steps += maxLoopSize
        }

        return steps
    }
}
