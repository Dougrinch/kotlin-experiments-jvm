package com.dou.advent.year2023

import com.dou.advent.*
import com.dou.advent.year2023.Day12.Spring
import com.dou.advent.year2023.Day12.Spring.*
import com.dou.parser.*
import kotlin.math.max

fun main() {
    Day12().run()
}

class Day12 : TypedDay<List<Pair<List<Spring>, List<Int>>>>() {
    override val test1: Any
        get() = 21

    override val test2: Any
        get() = 525152

    override val parser by parser {
        val spring = oneOf(
            const("?") parseAs Unknown,
            const(".") parseAs Operational,
            const("#") parseAs Damaged,
        )
        spring.many() - " " - int.separatedBy(",") separatedBy newLine
    }

    override fun part1(input: List<Pair<List<Spring>, List<Int>>>): Long {
        return input.sumOf { (springs, counts) -> solve(springs, counts) }
    }

    override fun part2(input: List<Pair<List<Spring>, List<Int>>>): Long {
        return input.sumOf { (springs, counts) ->
            val expandedSprings = (1..5).map { springs }.reduce { a, b -> a + Unknown + b }
            val expandedCounts = (1..5).flatMap { counts }
            solve(expandedSprings, expandedCounts)
        }
    }

    private fun solve(springs: List<Spring>, counts: List<Int>): Long {
        val available = IntArray(springs.size) { i ->
            val nextOperational = springs.drop(i).indexOf(Operational)
            if (nextOperational == -1) {
                springs.size - i
            } else {
                max(nextOperational, 0)
            }
        }

        val count = mutableMapOf<Pair<Int, Int>, Long>()

        fun countSolutions(si: Int, ci: Int): Long {
            return when {
                ci >= counts.size -> {
                    if (springs.drop(si).all { it != Damaged }) {
                        1
                    } else {
                        0
                    }
                }

                si >= springs.size -> {
                    0
                }

                si to ci in count -> {
                    count[si to ci]!!
                }

                else -> {
                    val canTake = counts[ci] <= available[si]
                        && (si + counts[ci] == springs.size || springs[si + counts[ci]] != Damaged)

                    val taken = if (canTake) {
                        countSolutions(si + counts[ci] + 1, ci + 1)
                    } else {
                        0
                    }

                    val notTaken = if (springs[si] != Damaged) {
                        countSolutions(si + 1, ci)
                    } else {
                        0
                    }

                    val result = taken + notTaken
                    count[si to ci] = result
                    result
                }
            }
        }

        return countSolutions(0, 0)
    }

    enum class Spring {
        Operational,
        Damaged,
        Unknown
    }
}
