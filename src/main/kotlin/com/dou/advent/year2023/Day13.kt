package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.Grid
import com.dou.advent.util.grid
import com.dou.advent.year2023.Day13.Cell
import com.dou.parser.*
import kotlin.math.max

fun main() {
    ::Day13.run()
}

class Day13 : TypedDay<List<Grid<Cell>>>() {

    override val test1: Any
        get() = 405

    override val test2: Any
        get() = 400

    override val parser = parser {
        grid(
            oneOf(
                const(".") parseAs Ash,
                const("#") parseAs Rock
            )
        ).separatedByLines(2)
    }

    override fun part1(input: List<Grid<Cell>>): Any {
        return solve(input, false)
    }

    override fun part2(input: List<Grid<Cell>>): Any {
        return solve(input, true)
    }

    private fun solve(input: List<Grid<Cell>>, withOneError: Boolean): Int {
        return input.sumOf { m ->
            m.findVerticalSymmetry(withOneError)?.let { it + 1 }
                ?: ((m.findHorizontalSymmetry(withOneError)!! + 1) * 100)
        }
    }

    private fun Grid<Cell>.findVerticalSymmetry(withOneError: Boolean): Int? {
        symmetry@ for (s in 0 until xs.last) {
            var errorFound = false
            for (x in max(0, s - (xs.last - s) + 1)..s) {
                for (y in ys) {
                    if (this[x, y] != this[s + (s - x) + 1, y]) {
                        if (!withOneError || errorFound) {
                            continue@symmetry
                        } else {
                            errorFound = true
                        }
                    }
                }
            }
            if (!withOneError || errorFound) {
                return s
            }
        }
        return null
    }

    private fun Grid<Cell>.findHorizontalSymmetry(withOneError: Boolean): Int? {
        symmetry@ for (s in 0 until ys.last) {
            var errorFound = false
            for (y in max(0, s - (ys.last - s) + 1)..s) {
                for (x in xs) {
                    if (this[x, y] != this[x, s + (s - y) + 1]) {
                        if (!withOneError || errorFound) {
                            continue@symmetry
                        } else {
                            errorFound = true
                        }
                    }
                }
            }
            if (!withOneError || errorFound) {
                return s
            }
        }
        return null
    }

    sealed interface Cell
    data object Ash : Cell
    data object Rock : Cell
}
