package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.parser.*

fun main() {
    ::Day21.run()
}

class Day21 : TypedDay<Grid<Char>>() {

    override val parser = parser {
        grid(char('.', 'S', '#'))
    }

    override fun part1(input: Grid<Char>): Any {
        var open = setOf(input.positions().single { input[it] == 'S' })
        repeat(64) {
            open = open.flatMapTo(hashSetOf()) { p ->
                p.neighbours(false).filter { it in input && input[it] != '#' }
            }
        }
        return open.size
    }

    override fun part2(input: Grid<Char>): Any {
        val gridSize = input.xs.length

        val grids = 26501365 / gridSize
        val rem = 26501365 % gridSize

        val sizes = ArrayList<Int>()

        var steps = 0
        var open = setOf(input.positions().single { input[it] == 'S' })
        repeat(3) { n ->
            while (steps < n * gridSize + rem) {
                open = open.flatMapTo(hashSetOf()) { p ->
                    p.neighbours(false).filter { (x, y) ->
                        input[Position(x.mod(gridSize), y.mod(gridSize))] != '#'
                    }
                }
                steps += 1
            }
            sizes += open.size
        }

        val c = sizes[0]
        val a = (sizes[2].toLong() - c - (2 * (sizes[1] - c))) / 2
        val b = sizes[1] - c - a

        val x = grids.toLong()

        return a * x * x + b * x + c
    }
}
