package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.lcm
import com.dou.advent.year2023.Day20.Module
import com.dou.advent.year2023.Day20.ModuleType.*
import com.dou.advent.year2023.Day20.PulseType.High
import com.dou.advent.year2023.Day20.PulseType.Low
import com.dou.parser.*
import kotlin.collections.ArrayDeque
import kotlin.collections.HashMap

fun main() {
    ::Day20.run()
}

class Day20 : TypedDay<List<Module>>() {
    override val test1: Any
        get() = 11687500

    override val parser = parser {
        val moduleType = oneOf(
            const("%") parseAs FlipFlop,
            const("&") parseAs Conjunction
        )

        val moduleDesc = oneOf(
            string("broadcaster") map { Broadcast to it },
            moduleType - word
        )

        val destination = word separatedBy ", "

        moduleDesc - " -> " - destination map ::Module separatedBy newLine
    }

    override fun part1(input: List<Module>): Any {
        val state = prepareState(input)
        var high = 0
        var low = 0
        repeat(1000) {
            state.pressButton("broadcaster") { (type, _, _) ->
                when (type) {
                    High -> high += 1
                    Low -> low += 1
                }
                true
            }
        }
        return high * low
    }

    override fun part2(input: List<Module>): Any {
        val state = prepareState(input)

        fun loopSizeFromTo(from: String): Int {
            var high = 0
            var low = 0
            var pressed = 0
            var stopped = false
            while (!stopped) {
                pressed += 1
                state.pressButton(from) { (type, _, to) ->
                    when (type) {
                        High -> high += 1
                        Low -> low += 1
                    }
                    if (to == "jq" && type == High) {
                        stopped = true
                    }
                    !stopped
                }
            }
            return pressed
        }

        return listOf("tf", "br", "zn", "nc")
            .map { loopSizeFromTo(it).toLong() }
            .let { lcm(it) }
    }

    private fun prepareState(input: List<Module>): State {
        val modules = input.associateBy { it.name }
        val broadcaster = input.single { it.type == Broadcast }
        val state = State(modules, broadcaster, hashMapOf(), hashMapOf())
        for ((type, name) in input) {
            when (type) {
                FlipFlop -> {
                    state.flipFlops[name] = false
                }

                Conjunction -> {
                    state.conjunctions[name] = input
                        .filter { name in it.destinations }
                        .associateByTo(HashMap(), { it.name }, { Low })
                }

                else -> {}
            }
        }
        return state
    }

    private fun State.pressButton(firstTarget: String, onPulse: (Pulse) -> Boolean) {
        val pulses = ArrayDeque<Pulse>()
        pulses += Pulse(Low, "button", firstTarget)
        while (pulses.isNotEmpty()) {
            val pulse = pulses.removeFirst()
            if (!onPulse(pulse)) {
                break
            }
            val (pulseType, from, to) = pulse
            val destination = modules[to]
            when (destination?.type) {
                FlipFlop -> {
                    if (pulseType == Low) {
                        val newState = !flipFlops[to]!!
                        flipFlops[to] = newState
                        pulses += if (newState) {
                            destination.destinations.map { Pulse(High, to, it) }
                        } else {
                            destination.destinations.map { Pulse(Low, to, it) }
                        }
                    }
                }

                Conjunction -> {
                    conjunctions[to]!![from] = pulseType
                    pulses += if (conjunctions[to]!!.all { it.value == High }) {
                        destination.destinations.map { Pulse(Low, to, it) }
                    } else {
                        destination.destinations.map { Pulse(High, to, it) }
                    }
                }

                Broadcast -> {
                    pulses += destination.destinations.map { Pulse(pulseType, to, it) }
                }

                else -> {}
            }
        }
    }

    data class State(
        val modules: Map<String, Module>,
        val broadcaster: Module,
        val flipFlops: MutableMap<String, Boolean>,
        val conjunctions: MutableMap<String, MutableMap<String, PulseType>>
    )

    data class Module(val type: ModuleType, val name: String, val destinations: List<String>)

    enum class ModuleType {
        FlipFlop,
        Conjunction,
        Broadcast
    }

    data class Pulse(val type: PulseType, val from: String, val to: String)

    enum class PulseType {
        High, Low
    }
}
