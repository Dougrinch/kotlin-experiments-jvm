package com.dou.advent.year2023

import arrow.core.Some
import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    ::Day17.run()
}

class Day17 : TypedDay<Grid<Int>>() {
    override val test1: Any
        get() = 102

    override val test2: Any
        get() = 94

    override val parser = parser { grid(digit) }

    override fun part1(input: Grid<Int>): Any {
        val target = Position(input.xs.last, input.ys.last)
        val path = search(
            Node(Position(0, 0), null, 0),
            { node ->
                Direction.entries
                    .asSequence()
                    .run {
                        if (node.prevDirection != null) {
                            filter { it != -node.prevDirection }
                        } else {
                            this
                        }
                    }
                    .run {
                        if (node.stepsInPrevDirection == 3) {
                            filter { it != node.prevDirection!! }
                        } else {
                            this
                        }
                    }
                    .mapNotNull { dir ->
                        val next = node.position + dir
                        if (next !in input) {
                            null
                        } else {
                            val steps = if (dir == node.prevDirection) {
                                node.stepsInPrevDirection + 1
                            } else {
                                1
                            }
                            Node(next, dir, steps) to input[next]
                        }
                    }
                    .toList()
            },
            { (target - it.position).run { abs(first) + abs(second) } },
            { it.position == target }
        )
        return (path as Some).value.second
    }

    override fun part2(input: Grid<Int>): Any {
        val target = Position(input.xs.last, input.ys.last)
        val path = search(
            Node(Position(0, 0), null, 0),
            { node ->
                Direction.entries
                    .asSequence()
                    .run {
                        if (node.prevDirection != null) {
                            filter { it != -node.prevDirection }
                        } else {
                            this
                        }
                    }
                    .run {
                        if (node.prevDirection != null && node.stepsInPrevDirection < 4) {
                            filter { it == node.prevDirection }
                        } else {
                            this
                        }
                    }
                    .run {
                        if (node.stepsInPrevDirection == 10) {
                            filter { it != node.prevDirection!! }
                        } else {
                            this
                        }
                    }
                    .mapNotNull { dir ->
                        val next = node.position + dir
                        if (next !in input) {
                            null
                        } else {
                            val steps = if (dir == node.prevDirection) {
                                node.stepsInPrevDirection + 1
                            } else {
                                1
                            }
                            Node(next, dir, steps) to input[next]
                        }
                    }
                    .toList()
            },
            { (target - it.position).run { abs(first) + abs(second) } },
            { it.position == target }
        )
        return (path as Some).value.second
    }

    data class Node(val position: Position, val prevDirection: Direction?, val stepsInPrevDirection: Int)
}
