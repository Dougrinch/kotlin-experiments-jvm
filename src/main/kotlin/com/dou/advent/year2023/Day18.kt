package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.advent.util.Direction.*
import com.dou.parser.*

fun main() {
    ::Day18.run()
}

class Day18 : TypedDay<List<Triple<Direction, Int, String>>>() {
    override val test1: Any
        get() = 62

    override val test2: Any
        get() = 952408144115L

    override val parser = parser {
        val dir = oneOf(
            "R" parseAs Right,
            "U" parseAs Up,
            "D" parseAs Down,
            "L" parseAs Left,
        )

        dir - ws - int - ws - "(" - regex("#\\w+") - ")" map ::Triple separatedBy newLine
    }

    override fun part1(input: List<Triple<Direction, Int, String>>): Any {
        val border = buildBorder(input)

        val area = border.indices.sumOf { i ->
            val pi = border[i]
            val pi1 = border[(i + 1) % border.size]
            pi.x.toLong() * pi1.y.toLong() - pi1.x.toLong() * pi.y.toLong()
        } / 2

        val innerPoints = area - border.size / 2 + 1

        return innerPoints + border.size
    }

    private fun buildBorder(input: List<Triple<Direction, Int, String>>): List<Position> {
        val result = ArrayList<Position>()
        var current = Position.zero
        for ((dir, distance, _) in input) {
            for (i in 0 until distance) {
                current += dir
                result += current
            }
        }
        return result
    }

    override fun part2(input: List<Triple<Direction, Int, String>>): Any {
        return part1(input.map { (_, _, color) ->
            val distance = color.drop(1).dropLast(1).toInt(16)
            val dir = when (color.takeLast(1)) {
                "0" -> Right
                "1" -> Down
                "2" -> Left
                "3" -> Up
                else -> throw IllegalStateException()
            }
            Triple(dir, distance, color)
        })
    }
}
