package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day2().run()
}

class Day2 : TypedDay<List<Pair<Int, List<Map<String, Int>>>>>() {

    override val test1: Any
        get() = 8

    override val test2: Any
        get() = 2286

    override val parser = parser {
        val cubes by int - " " - word separatedBy ", " map { it.associate { it.second to it.first } }
        val game = "Game " - int - ": " - cubes.separatedBy("; ")
        game.separatedByLines()
    }

    override fun part1(input: List<Pair<Int, List<Map<String, Int>>>>): Int {
        val limits = mapOf("red" to 12, "green" to 13, "blue" to 14)
        return input.filter { (_, g) -> g.all { cs -> cs.all { it.value <= limits[it.key]!! } } }.sumOf { it.first }
    }

    override fun part2(input: List<Pair<Int, List<Map<String, Int>>>>): Int {
        return input.sumOf { (_, game) ->
            val r = game.maxOf { it["red"] ?: 0 }
            val g = game.maxOf { it["green"] ?: 0 }
            val b = game.maxOf { it["blue"] ?: 0 }
            r * g * b
        }
    }
}
