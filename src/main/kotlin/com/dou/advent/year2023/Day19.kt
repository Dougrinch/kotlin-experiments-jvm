package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.year2023.Day19.Action.*
import com.dou.advent.year2023.Day19.ConditionOp.Greater
import com.dou.advent.year2023.Day19.ConditionOp.Less
import com.dou.advent.year2023.Day19.Part
import com.dou.advent.year2023.Day19.Rule.Conditional
import com.dou.advent.year2023.Day19.Workflow
import com.dou.fp.e10.par
import com.dou.parser.*

fun main() {
    ::Day19.run()
}

class Day19 : TypedDay<Pair<List<Workflow>, List<Part>>>() {
    override val test1: Any
        get() = 19114

    override val test2: Any
        get() = 167409079868000L

    override val parser = parser {
        val component = regex("[xmas]")

        val action = oneOf(
            const("A") parseAs Accept,
            const("R") parseAs Reject,
            word map ::Send
        )

        val conditionOp = oneOf(
            const(">") parseAs Greater,
            const("<") parseAs Less,
        )

        val condition = component - conditionOp - int map ::Condition

        val rule = oneOf(
            attempt(condition - ":" - action) map Rule::Conditional,
            action map Rule::Unconditional
        )

        val workflow = word - "{" - rule.separatedBy(",") - "}" map ::Workflow

        val components = component - "=" - int separatedBy "," map { it.associateBy({ it.first }, { it.second }) }
        val part = "{" - components - "}" map ::Part

        workflow.separatedByLines() - newLine - newLine - part.separatedByLines()
    }

    override fun part1(input: Pair<List<Workflow>, List<Part>>): Any {
        val workflows = input.first.associateBy({ it.name }, { it.rules })

        val acceptedParts = input.second.filter { part ->
            val accepted: Boolean
            var rules = workflows["in"]!!
            workflows@ while (true) {
                for (rule in rules) {
                    if (rule is Conditional && !rule.condition(part)) {
                        continue
                    }
                    when (val action = rule.action) {
                        Accept -> {
                            accepted = true
                            break@workflows
                        }

                        Reject -> {
                            accepted = false
                            break@workflows
                        }

                        is Send -> {
                            rules = workflows[action.rule]!!
                            continue@workflows
                        }
                    }
                }
            }
            accepted
        }

        return acceptedParts.sumOf { it.components.values.sum() }
    }

    override fun part2(input: Pair<List<Workflow>, List<Part>>): Any {
        val workflows = input.first.associateBy({ it.name }, { it.rules })
        return countAccepted(
            workflows, "in", 0, PartsRange(
                mapOf(
                    "x" to 1..4000,
                    "m" to 1..4000,
                    "a" to 1..4000,
                    "s" to 1..4000
                )
            )
        )
    }

    private fun countAccepted(
        workflows: Map<String, List<Rule>>,
        currentWorkflow: String,
        currentRule: Int,
        parts: PartsRange
    ): Long {
        var result = 0L

        val rule = workflows[currentWorkflow]!![currentRule]

        fun applyAction(p: PartsRange) {
            when (val action = rule.action) {
                Accept -> {
                    result += p.components.values.fold(1L) { acc, r -> acc * r.count() }
                }

                Reject -> {}

                is Send -> {
                    result += countAccepted(workflows, action.rule, 0, p)
                }
            }
        }

        if (rule is Conditional) {
            val condition = rule.condition
            val oldRange = parts.components[condition.component]!!

            val (passedRange, notPassedRange) = when (condition.op) {
                Greater -> {
                    Pair(
                        (condition.value + 1)..oldRange.last,
                        oldRange.first..condition.value,
                    )
                }

                Less -> {
                    Pair(
                        oldRange.first..<condition.value,
                        condition.value..oldRange.last
                    )
                }
            }

            fun partsWith(newRange: IntRange): PartsRange? {
                return if (newRange.first <= newRange.last) {
                    PartsRange(parts.components.mapValues { (c, r) ->
                        if (c == condition.component) newRange else r
                    })
                } else {
                    null
                }
            }

            partsWith(notPassedRange)?.let { p ->
                result += countAccepted(workflows, currentWorkflow, currentRule + 1, p)
            }

            partsWith(passedRange)?.let { p ->
                applyAction(p)
            }
        } else {
            applyAction(parts)
        }

        return result
    }

    data class PartsRange(val components: Map<String, IntRange>)

    data class Part(val components: Map<String, Int>)

    data class Workflow(val name: String, val rules: List<Rule>)

    sealed interface Action {
        data object Accept : Action
        data object Reject : Action
        data class Send(val rule: String) : Action
    }

    sealed interface Rule {
        val action: Action

        data class Unconditional(override val action: Action) : Rule
        data class Conditional(val condition: Condition, override val action: Action) : Rule
    }

    data class Condition(val component: String, val op: ConditionOp, val value: Int) {
        operator fun invoke(part: Part): Boolean {
            return when (op) {
                Greater -> part.components[component]!! > value
                Less -> part.components[component]!! < value
            }
        }
    }

    enum class ConditionOp {
        Greater, Less
    }
}
