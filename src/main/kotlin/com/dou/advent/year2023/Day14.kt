package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.advent.util.Direction.*
import com.dou.advent.year2023.Day14.Cell
import com.dou.parser.*

fun main() {
    ::Day14.run()
}

class Day14 : TypedDay<Grid<Cell>>() {

    override val test1: Any
        get() = 136

    override val test2: Any
        get() = 64

    override val parser = parser {
        grid(
            oneOf(
                const(".") parseAs Empty,
                const("O") parseAs Rounded,
                const("#") parseAs Cube
            )
        )
    }

    override fun part1(input: Grid<Cell>): Any {
        val map = input.toMutableGrid()
        map.tilt(Up)
        return map.positions().filter { map[it] == Rounded }.sumOf { (_, y) -> map.ys.length - y }
    }

    override fun part2(input: Grid<Cell>): Any {
        fun MutableGrid<Cell>.tiltAll() {
            tilt(Up)
            tilt(Left)
            tilt(Down)
            tilt(Right)
        }

        val visited = hashMapOf<Grid<Cell>, Int>()
        var current = input

        var tilts = 0

        while (current !in visited) {
            visited[current] = tilts
            current = current.toMutableGrid().apply { tiltAll() }
            tilts += 1
        }

        val total = 1000000000
        val loopSize = tilts - visited[current]!!
        val rest = (total - tilts) % loopSize
        repeat(rest) {
            current = current.toMutableGrid().apply { tiltAll() }
        }

        return current.positions().filter { current[it] == Rounded }.sumOf { (_, y) -> current.ys.length - y }
    }

    private fun MutableGrid<Cell>.tilt(direction: Direction) {
        val stop = when (direction) {
            Up -> Array(xs.length) { Position(it, 0) }
            Down -> Array(xs.length) { Position(it, ys.last) }
            Left -> Array(ys.length) { Position(0, it) }
            Right -> Array(ys.length) { Position(xs.last, it) }
        }
        val current = stop.copyOf()
        while (current[0] in this) {
            for (i in current.indices) {
                when (this[current[i]]) {
                    Cube -> {
                        stop[i] = current[i] - direction
                    }

                    Rounded -> {
                        this[current[i]] = Empty
                        this[stop[i]] = Rounded
                        stop[i] = stop[i] - direction
                    }

                    Empty -> {}
                }
                current[i] = current[i] - direction
            }
        }
    }

    sealed interface Cell
    data object Empty : Cell
    data object Rounded : Cell
    data object Cube : Cell
}
