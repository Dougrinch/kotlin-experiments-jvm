package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.bruteForce
import com.dou.advent.util.compareLists
import com.dou.advent.year2023.Day7.Hand
import com.dou.advent.year2023.Day7.HandType.*
import com.dou.parser.*
import kotlin.Pair

fun main() {
    Day7().run()
}

class Day7 : TypedDay<List<Pair<Hand, Int>>>() {
    override val test1: Any
        get() = 6440

    override val test2: Any
        get() = 5905

    override val parser = parser {
        val card = char
        val hand = card.many(exact = 5) map ::Hand
        val bid = int
        (hand - " " - bid).separatedByLines()
    }

    override fun part1(input: List<Pair<Hand, Int>>): Any {
        val alphabet = listOf('A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2').reversed()
        val a = input.sortedWith(
            compareBy<Pair<Hand, Int>> { it.first.cards.getType() }
                .then(compareBy(compareLists(compareCard(alphabet))) { it.first.cards })
        )
        return a.withIndex().sumOf { (i, p) -> (i + 1) * p.second }
    }

    override fun part2(input: List<Pair<Hand, Int>>): Any {
        val alphabet = listOf('A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J').reversed()
        val a = input.sortedWith(
            compareBy<Pair<Hand, Int>> { it.first.cards.bruteForceJokers(alphabet).map { it.getType() }.max() }
                .then(compareBy(compareLists(compareCard(alphabet))) { it.first.cards })
        )
        return a.withIndex().sumOf { (i, p) -> (i + 1) * p.second }
    }

    private fun compareCard(alphabet: List<Char>): Comparator<Char> {
        return compareBy { alphabet.indexOf(it) }
    }

    private fun List<Char>.bruteForceJokers(alphabet: List<Char>): Sequence<List<Char>> {
        val mutable = toMutableList()
        val jokers = withIndex().filter { it.value == 'J' }.map { it.index }
        return bruteForce(alphabet, jokers.size, false).map { js ->
            for (i in js.indices) {
                mutable[jokers[i]] = js[i]
            }
            mutable
        }
    }

    private fun List<Char>.getType(): HandType {
        check(size == 5)

        val distinctCards = distinct()
        return when {
            distinctCards.size == 1 -> Five

            distinctCards.size == 2
                && count { it == distinctCards[0] } in listOf(1, 4) -> Four

            distinctCards.size == 2
                && count { it == distinctCards[0] } in listOf(2, 3) -> FullHouse

            distinctCards.any { c -> count { it == c } == 3 } -> Three
            distinctCards.count { c -> count { it == c } == 2 } == 2 -> TwoPairs
            distinctCards.count { c -> count { it == c } == 2 } == 1 -> Pair
            else -> HighCard
        }
    }

    data class Hand(val cards: List<Char>)

    enum class HandType {
        HighCard, Pair, TwoPairs, Three, FullHouse, Four, Five
    }
}
