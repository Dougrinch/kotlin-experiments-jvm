package com.dou.advent.year2023

import com.dou.advent.*
import com.dou.parser.*

fun main() {
    ::Day15.run()
}

class Day15 : TypedDay<List<String>>() {
    override val test1: Any
        get() = 1320

    override val test2: Any
        get() = 145

    override val parser = parser {
        regex("[\\w-=]+").separatedBy(",")
    }

    override fun part1(input: List<String>): Any {
        return input.sumOf { hash(it) }
    }

    private fun hash(line: String): Int {
        var r = 0
        for (ch in line) {
            r += ch.code
            r *= 17
            r %= 256
        }
        return r
    }

    override fun part2(input: List<String>): Any {
        val cmd = parser { word - oneOf("-", "=") - digit.optional() map ::Triple }

        val boxes = Array<List<Pair<String, Int>>>(256) { emptyList() }

        for (s in input) {
            val (label, operation, focalLength) = cmd.parse(s)
            val box = hash(label)

            when (operation) {
                "-" -> {
                    boxes[box] = boxes[box].filter { it.first != label }
                }

                "=" -> {
                    boxes[box] = if (boxes[box].any { it.first == label }) {
                        boxes[box].map { if (it.first == label) label to focalLength!! else it }
                    } else {
                        boxes[box] + (label to focalLength!!)
                    }
                }
            }
        }

        return boxes.withIndex().sumOf { (boxIdx, ls) ->
            ls.withIndex().sumOf { (slotIdx, l) ->
                (boxIdx + 1) * (slotIdx + 1) * l.second
            }
        }
    }
}
