package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day9().run()
}

class Day9 : TypedDay<List<List<Long>>>() {
    override val test1: Any
        get() = 114

    override val test2: Any
        get() = 2

    override val parser = parser {
        long.separatedBy(" ").separatedByLines()
    }

    override fun part1(input: List<List<Long>>): Any {
        return input.sumOf { next(it) }
    }

    override fun part2(input: List<List<Long>>): Any {
        return input.sumOf { prev(it) }
    }

    private fun next(ls: List<Long>): Long {
        var current = ls
        val diffs = arrayListOf(ls)
        while (current.any { it != 0L }) {
            val next = current.zipWithNext().map { (p, n) -> n - p }
            diffs += next
            current = next
        }
        var result = 0L
        for (i in (diffs.size - 2).downTo(0)) {
            result += diffs[i].last()
        }
        return result
    }

    private fun prev(ls: List<Long>): Long {
        var current = ls
        val diffs = arrayListOf(ls)
        while (current.any { it != 0L }) {
            val next = current.zipWithNext().map { (p, n) -> n - p }
            diffs += next
            current = next
        }
        var result = 0L
        for (i in (diffs.size - 2).downTo(0)) {
            result = diffs[i].first() - result
        }
        return result
    }
}
