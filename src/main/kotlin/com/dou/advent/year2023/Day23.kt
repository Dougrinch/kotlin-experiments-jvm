package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.run
import com.dou.advent.util.*
import com.dou.advent.util.Direction.*
import com.dou.advent.year2023.Day23.Cell
import com.dou.parser.*
import java.util.concurrent.RecursiveTask
import kotlin.math.max

fun main() {
    ::Day23.run()
}

class Day23 : TypedDay<Grid<Cell>>() {

    override val test1: Any
        get() = 94

    override val test2: Any
        get() = 154

    override val parser = parser {
        val direction = oneOf(
            const(">") parseAs Right,
            const("<") parseAs Left,
            const("^") parseAs Up,
            const("v") parseAs Down
        )

        val cell = oneOf(
            const(".") parseAs Path,
            const("#") parseAs Forest,
            direction map ::Slope,
        )

        grid(cell)
    }

    override fun part1(input: Grid<Cell>): Any {
        return solve(input, true)
    }

    override fun part2(input: Grid<Cell>): Any {
        return solve(input, false)
    }

    private fun solve(input: Grid<Cell>, respectSlopes: Boolean): Int {
        val enter = input.xs.single { x -> input[x, 0] == Path }.let { Position(it, 0) }
        val exit = input.xs.single { x -> input[x, input.ys.last] == Path }.let { Position(it, input.ys.last) }
        return SolveTask(exit, input, mutableSetOf(enter), enter, respectSlopes).invoke()
    }

    class SolveTask(
        private val exit: Position,
        private val input: Grid<Cell>,
        private val visited: MutableSet<Position>,
        private val last: Position,
        private val respectSlopes: Boolean
    ) : RecursiveTask<Int>() {
        override fun compute(): Int {
            var current = last

            while (true) {
                val direction = if (respectSlopes) {
                    (input[current] as? Slope)?.direction
                } else {
                    null
                }

                val next = if (direction != null) {
                    val next = current + direction
                    if (next in input && next !in visited) {
                        listOf(next)
                    } else {
                        listOf()
                    }
                } else {
                    current.neighbours(false)
                        .filter { it in input && input[it] != Forest && it !in visited }
                }

                if (next.size == 1) {
                    if (next[0] == exit) {
                        return visited.size
                    } else {
                        visited += next[0]
                        current = next[0]
                        continue
                    }
                } else {
                    val tasks = next.map { nextLast ->
                        val nextVisited = mutableSetOf<Position>()
                        nextVisited += visited
                        nextVisited += nextLast
                        SolveTask(exit, input, nextVisited, nextLast, respectSlopes)
                    }
                    for (task in tasks) {
                        task.fork()
                    }
                    var result = 0
                    for (task in tasks) {
                        result = max(result, task.join())
                    }
                    return result
                }
            }
        }
    }

    sealed interface Cell
    data object Path : Cell
    data object Forest : Cell
    data class Slope(val direction: Direction) : Cell
}
