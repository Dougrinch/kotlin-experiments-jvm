package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.*
import kotlin.math.abs
import kotlin.math.sign

fun main() {
    Day11().run()
}

class Day11 : TypedDay<Grid<Boolean>>() {
    override val test1: Any
        get() = 374

    override val parser = parser {
        val cell = oneOf(
            const(".") parseAs false,
            const("#") parseAs true
        )

        grid(cell)
    }

    override fun part1(input: Grid<Boolean>): Any {
        val space = Grid(expandInlined(input.lines))
        val pairs = galaxyPairs(space)

        return pairs.sumOf { (from, to) ->
            (to - from).let { (dx, dy) -> abs(dx) + abs(dy) }
        }
    }

    private fun galaxyPairs(space: Grid<Boolean>): List<Pair<Position, Position>> {
        val galaxies = space.positions().filter { space[it] }.toList()
        return bruteForce(galaxies, 2, copyLists = false)
            .filter { (a, b) -> a != b }
            .map { (a, b) -> setOf(a, b) }
            .toSet()
            .map { it.toList().let { (from, to) -> from to to } }
    }

    private fun expandInlined(lines: List<List<Boolean>>): List<List<Boolean>> {
        val columnsToAdd = lines[0].indices.filter { c -> lines.all { !it[c] } }.reversed()
        val withColumns = lines.map { c ->
            columnsToAdd.fold(c.toMutableList()) { r, i -> r.add(i, false); r }
        }
        val rowsToAdd = withColumns.indices.filter { withColumns[it].all { !it } }.reversed()
        return rowsToAdd.fold(withColumns.toMutableList()) { r, i -> r.add(i, r[i]); r }
    }

    override fun part2(input: Grid<Boolean>): Any {
        val factor = 1000000
        val emptyColumns = input.xs.filter { x -> input.ys.all { y -> !input[Position(x, y)] } }.toSet()
        val emptyRows = input.ys.filter { y -> input.xs.all { x -> !input[Position(x, y)] } }.toSet()

        val pairs = galaxyPairs(input)

        return pairs.sumOf { (from, to) ->
            var distance = 0L
            val (dx, dy) = to - from
            val xStep = dx.sign
            val yStep = dy.sign

            var x = from.x
            while (x != to.x) {
                x += xStep
                distance += if (x in emptyColumns) {
                    factor
                } else {
                    1
                }
            }

            var y = from.y
            while (y != to.y) {
                y += yStep
                distance += if (y in emptyRows) {
                    factor
                } else {
                    1
                }
            }

            distance
        }
    }
}
