package com.dou.advent.year2023

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day4().run()
}

class Day4 : TypedDay<List<Pair<Int, Pair<List<Int>, List<Int>>>>>() {
    override val test1: Any
        get() = 13

    override val test2: Any
        get() = 30

    override val parser = parser {
        val numbers = int separatedBy ws
        val cardData = numbers - ws - "|" - ws - numbers
        val card = "Card" - ws - int - ":" - ws - cardData
        card.separatedByLines()
    }

    override fun part1(input: List<Pair<Int, Pair<List<Int>, List<Int>>>>): Int {
        var result = 0
        for (card in input) {
            val winning = card.second.first.toSet()
            val numbers = card.second.second
            var points = 0
            for (number in numbers) {
                if (number in winning) {
                    if (points == 0) {
                        points = 1
                    } else {
                        points *= 2
                    }
                }
            }
            result += points
        }
        return result
    }

    override fun part2(input: List<Pair<Int, Pair<List<Int>, List<Int>>>>): Int {
        val cardsCount = input.associateByTo(mutableMapOf(), { it.first }, { 1 })
        for (card in input) {
            val winning = card.second.first.toSet()
            val numbers = card.second.second
            val matching = numbers.count { it in winning }
            for (i in 1..matching) {
                cardsCount.compute(card.first + i) { _, old ->
                    old!! + cardsCount[card.first]!!
                }
            }
        }
        return cardsCount.values.sum()
    }
}
