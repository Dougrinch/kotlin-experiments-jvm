package com.dou.advent

abstract class Day(year: Int? = null, day: Int? = null) : AbstractDay<Sequence<String>>(year, day) {
    override fun parseInput(input: String): Sequence<String> {
        return input.lineSequence()
    }
}
