package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.digit
import com.dou.parser.parser

fun main() {
    Day10().run()
}

class Day10 : TypedDay<Grid<Int>>() {

    override val test1: Any
        get() = 36

    override val test2: Any
        get() = 81

    override val parser by parser {
        grid(digit)
    }

    override fun part1(input: Grid<Int>): Any {
        var r = 0

        for (trailhead in input.positions()) {
            if (input[trailhead] != 0) {
                continue
            }

            val score = bruteForce(
                listOf(trailhead),
                { path ->
                    val currentHeight = input[path.last()]
                    path.last()
                        .neighbours(false)
                        .filter { it in input && input[it] == currentHeight + 1 }
                },
                { path, next ->
                    if (input[next] == 9) {
                        Move.PathCompleted(path + next)
                    } else {
                        Move.Successful(path + next)
                    }
                }
            )
                .map { it.last() }
                .distinct()
                .count()

            r += score
        }

        return r
    }


    override fun part2(input: Grid<Int>): Any {
        var r = 0

        for (trailhead in input.positions()) {
            if (input[trailhead] != 0) {
                continue
            }

            val rating = bruteForce(
                listOf(trailhead),
                { path ->
                    val currentHeight = input[path.last()]
                    path.last()
                        .neighbours(false)
                        .filter { it in input && input[it] == currentHeight + 1 }
                },
                { path, next ->
                    if (input[next] == 9) {
                        Move.PathCompleted(path + next)
                    } else {
                        Move.Successful(path + next)
                    }
                }
            )
                .count()

            r += rating
        }

        return r
    }
}
