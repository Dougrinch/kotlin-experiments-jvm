package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day5().run()
}

class Day5 : TypedDay<Pair<List<Day5.Rule>, List<Day5.Update>>>() {

    override val parser by parser {
        val rule by int - "|" - int map ::Rule
        val update by int.separatedBy(",") map ::Update

        rule.separatedByLines() - newLine - newLine - update.separatedByLines()
    }

    override val test1: Any
        get() = 143

    override val test2: Any
        get() = 123

    override fun part1(input: Pair<List<Rule>, List<Update>>): Any {
        val rules = input.first.groupBy { it.page }.mapValues { it.value.map { it.before }.toSet() }

        fun isInRightOrder(update: Update): Boolean {
            for (i in update.pages.indices) {
                val mustBeAfter = rules[update.pages[i]] ?: emptySet()
                for (j in 0 until i) {
                    if (update.pages[j] in mustBeAfter) {
                        return false
                    }
                }
            }

            return true
        }

        return input.second.filter { isInRightOrder(it) }.sumOf { it.pages[it.pages.size / 2] }
    }

    override fun part2(input: Pair<List<Rule>, List<Update>>): Any {
        val rules = input.first.groupBy { it.page }.mapValues { it.value.map { it.before }.toSet() }

        val comp = Comparator<Int> { p1, p2 ->
            if (p2 in (rules[p1] ?: emptySet())) {
                -1
            } else if (p1 in (rules[p2] ?: emptySet())) {
                1
            } else {
                0
            }
        }

        fun isInRightOrder(update: Update): Boolean {
            for (i in update.pages.indices) {
                val mustBeAfter = rules[update.pages[i]] ?: emptySet()
                for (j in 0 until i) {
                    if (update.pages[j] in mustBeAfter) {
                        return false
                    }
                }
            }

            return true
        }

        return input.second
            .filterNot { isInRightOrder(it) }
            .map { it.pages.sortedWith(comp) }
            .sumOf { it[it.size / 2] }
    }

    data class Rule(val page: Int, val before: Int)
    data class Update(val pages: List<Int>)
}
