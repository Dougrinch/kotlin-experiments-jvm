package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.Grid
import com.dou.parser.*

fun main() {
    Day18().run()
}

class Day18 : TypedDay<List<Pair<Int, Int>>>() {

    override val parser by parser {
        (int - "," - int).separatedByLines()
    }

    override val test1: Any
        get() = 22

    override fun part1(input: List<Pair<Int, Int>>): Any {
        val width = 6
        val height = 6

//        val map = Grid<In>()

        return super.part1(input)
    }
}
