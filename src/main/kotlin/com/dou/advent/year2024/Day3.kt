package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day3().run()
}

class Day3 : TypedDay<List<Day3.Command>>() {

    override val parser = parser {
        val num = digit.many(min = 1, max = 3) map { it.joinToString(separator = "").toInt() }

        val cmd by oneOf(
            const("do()") parseAs Command.Do,
            const("don't()") parseAs Command.DoNot,
            attempt("mul(" - num - "," - num - ")" map Command::Mul),
            regex(".").ignoreValue()
        )

        cmd.many() map { it.filterIsInstance<Command>() }
    }

    override val test1: Any
        get() = 161

    override val test2: Any
        get() = 48

    override fun part1(input: List<Command>): Any {
        return input.filterIsInstance<Command.Mul>().sumOf { it.a * it.b }
    }

    override fun part2(input: List<Command>): Any {
        var enabled = true
        var result = 0
        for (command in input) {
            when (command) {
                is Command.Do -> enabled = true
                is Command.DoNot -> enabled = false
                is Command.Mul -> {
                    if (enabled) {
                        result += command.a * command.b
                    }
                }
            }
        }
        return result
    }

    sealed interface Command {
        data object Do : Command
        data object DoNot : Command
        data class Mul(val a: Int, val b: Int) : Command
    }
}
