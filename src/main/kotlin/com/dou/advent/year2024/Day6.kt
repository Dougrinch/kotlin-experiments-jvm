package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.year2024.Day6.Cell.*
import com.dou.parser.*

fun main() {
    Day6().run()
}

class Day6 : TypedDay<Grid<Day6.Cell>>() {

    override val test1: Any
        get() = 41

    override val test2: Any
        get() = 6

    override val parser by parser {
        val cell by oneOf(
            "." parseAs Empty,
            "^" parseAs Guard,
            "#" parseAs Obstruction
        )

        grid(cell)
    }

    override fun part1(input: Grid<Cell>): Any {
        return walk(input, null).first.distinctBy { it.first }.count()
    }

    private fun walk(input: Grid<Cell>, obstruction: Position?): Pair<Set<Pair<Position, Direction>>, Boolean> {
        var dir = Direction.Up
        val visited = mutableSetOf<Pair<Position, Direction>>()
        var current = input.positions().single { input[it] == Guard }
        visited += current to dir

        while (true) {
            if (current + dir !in input) {
                return visited to false
            }

            while (current + dir == obstruction || input[current + dir] == Obstruction) {
                dir = dir.rotateClockwise()
            }
            current += dir
            if (current to dir in visited) {
                return visited to true
            }
            visited += current to dir
        }
    }

    override fun part2(input: Grid<Cell>): Any {
        val initialGuardPos = input.positions().single { input[it] == Guard }
        val positionsToTest = walk(input, null).first
            .distinctBy { it.first }.map { it.first } - initialGuardPos
        return positionsToTest.count { walk(input, it).second }
    }

    enum class Cell {
        Obstruction,
        Guard,
        Empty
    }
}
