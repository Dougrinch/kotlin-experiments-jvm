package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.char
import com.dou.parser.parser

fun main() {
    Day12().run()
}

class Day12 : TypedDay<Grid<Char>>() {

    override val test1: Any
        get() = 1930

    override val test2: Any
        get() = 1206

    override val parser by parser {
        grid(char)
    }

    override fun part1(input: Grid<Char>): Any {
        var r = 0

        val processed = mutableSetOf<Position>()

        for (position in input.positions()) {
            if (position in processed) {
                continue
            }

            val currentRegion = input[position]

            val region = mutableSetOf<Position>()
            val open = ArrayDeque<Position>()
            open.add(position)

            while (open.isNotEmpty()) {
                val current = open.removeFirst()
                if (current in region || input[current] != currentRegion) {
                    continue
                }
                region.add(current)
                processed += current
                open.addAll(current.neighbours(false).filter { it in input })
            }


            val area = region.size
            val perimeter = region.sumOf {
                it.neighbours(false)
                    .count { n ->
                        n !in input || input[n] != currentRegion
                    }
            }

            r += area * perimeter
        }

        return r
    }

    override fun part2(input: Grid<Char>): Any {
        var r = 0

        val processed = mutableSetOf<Position>()

        for (position in input.positions()) {
            if (position in processed) {
                continue
            }

            val currentRegion = input[position]

            val region = mutableSetOf<Position>()
            val open = ArrayDeque<Position>()
            open.add(position)

            while (open.isNotEmpty()) {
                val current = open.removeFirst()
                if (current in region || input[current] != currentRegion) {
                    continue
                }
                region.add(current)
                processed += current
                open.addAll(current.neighbours(false).filter { it in input })
            }


            val area = region.size

            var fenceId = 0

            val fences = mutableMapOf<Position, MutableMap<Direction, Int>>()
            for (pos in region) {
                val rawFences = Direction.entries
                    .map { it to pos + it }
                    .filter { (_, n) -> n !in input || input[n] != currentRegion }
                    .associateTo(mutableMapOf()) { (d, _) -> d to fenceId++ }

                fun updateFenceIdTo(from: Position, testDirection: Direction, moveDirection: Direction) {
                    var current = from + moveDirection
                    while (testDirection in (fences[current] ?: emptyMap())) {
                        fences[current]!![testDirection] = rawFences[testDirection]!!
                        current += moveDirection
                    }
                }

                for (direction in Direction.entries) {
                    if (direction in rawFences) {
                        updateFenceIdTo(pos, direction, direction.rotateClockwise())
                        updateFenceIdTo(pos, direction, direction.rotateCounterclockwise())
                    }
                }

                fences[pos] = rawFences
            }

            val sidesCount = fences.values.map { it.values }.flatten().distinct().count()
            r += area * sidesCount
        }

        return r
    }
}
