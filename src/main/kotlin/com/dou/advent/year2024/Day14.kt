package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction.Right
import com.dou.parser.*

typealias VelocityDay14 = Pair<Int, Int>

fun main() {
    Day14().run()
}

class Day14 : TypedDay<List<Pair<Position, VelocityDay14>>>() {

//    override val test1: Any
//        get() = 12

    override val parser by parser {
        val xy by int - "," - int
        ("p=" - xy.map(::Position) - " v=" - xy.map(::VelocityDay14)).separatedByLines()
    }

    override fun part1(input: List<Pair<Position, VelocityDay14>>): Any {
        val maxX = 101
        val maxY = 103

        val state = input.toMutableList()
        for (s in 0 until 100) {
            for (i in state.indices) {
                val position = state[i].first + state[i].second
                val fixedPosition = Position((position.x + maxX) % maxX, (position.y + maxY) % maxY)
                state[i] = fixedPosition to state[i].second
            }
        }

        val r1 = Region(0, 0, maxX / 2 - 1, maxY / 2 - 1)
        val r2 = Region((maxX + 1) / 2, 0, maxX - 1, maxY / 2 - 1)
        val r3 = Region(0, (maxY + 1) / 2, maxX / 2 - 1, maxY - 1)
        val r4 = Region((maxX + 1) / 2, (maxY + 1) / 2, maxX - 1, maxY - 1)

        return state.count { (p, _) -> p in r1 } *
            state.count { (p, _) -> p in r2 } *
            state.count { (p, _) -> p in r3 } *
            state.count { (p, _) -> p in r4 }
    }

    override fun part2(input: List<Pair<Position, VelocityDay14>>): Any {
        val maxX = 101
        val maxY = 103

        val state = input.toMutableList()

        fun step() {
            for (i in state.indices) {
                val position = state[i].first + state[i].second
                val fixedPosition = Position((position.x + maxX) % maxX, (position.y + maxY) % maxY)
                state[i] = fixedPosition to state[i].second
            }
        }

        var r = 0

        fun printState() {
            for (y in 0 until maxY) {
                for (x in 0 until maxX) {
                    val hasRobots = state.any { (p, _) -> p == Position(x, y) }
                    if (hasRobots) {
                        print("#")
                    } else {
                        print(".")
                    }
                }
                print("\n")
            }
        }

        while (true) {
            r += 1
            step()

            val s = state.map { it.first }.toSet()
            if (s.any { p -> generateSequence(p) { it + Right }.take(10).all { it in s } }) {
                printState()
                return r
            }
        }
    }
}
