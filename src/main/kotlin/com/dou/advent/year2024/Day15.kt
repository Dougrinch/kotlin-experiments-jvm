package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.*

fun main() {
    Day15().run()
}

class Day15 : TypedDay<Pair<Grid<Day15.Cell>, List<Direction>>>() {

    override val test1: Any
        get() = 10092

    override val test2: Any
        get() = 9021

    override val parser by parser {
        val cell by oneOf(
            "." parseAs Cell.Empty,
            "#" parseAs Cell.Wall,
            "O" parseAs Cell.BoxLeft,
            "@" parseAs Cell.Robot
        )

        val cmd by oneOf(
            "<" parseAs Direction.Left,
            ">" parseAs Direction.Right,
            "v" parseAs Direction.Down,
            "^" parseAs Direction.Up,
        )

        val commands = cmd.many("").separatedByLines().map { it.flatten() }

        grid(cell) - "\n" - commands
    }

    override fun part1(input: Pair<Grid<Cell>, List<Direction>>): Any {
        val map = input.first.toMutableGrid()
        var robot = map.positions().single { input.first[it] == Cell.Robot }

        fun printMap() {
            println(map.map {
                when (it) {
                    Cell.Wall -> "#"
                    Cell.Empty -> "."
                    Cell.Robot -> "@"
                    Cell.BoxLeft -> "O"
                    Cell.BoxRight -> throw IllegalStateException()
                }
            })
            println()
        }

//        printMap()

        for (direction in input.second) {
            var next = robot + direction
            while (map[next] == Cell.BoxLeft) {
                next += direction
            }

            if (map[next] == Cell.Empty) {
                var prev = next - direction
                while (prev != robot) {
                    assert(map[prev] == Cell.BoxLeft)
                    map[next] = Cell.BoxLeft
                    map[prev] = Cell.Empty
                    next = prev
                    prev -= direction
                }
                map[next] = Cell.Robot
                map[robot] = Cell.Empty
                robot = next
            }

//            printMap()
        }

        return map.positions().filter { map[it] == Cell.BoxLeft }.sumOf { 100 * it.y + it.x }
    }

    override fun part2(input: Pair<Grid<Cell>, List<Direction>>): Any {
        val map = Grid(input.first.sizeX * 2, input.first.sizeY) { Cell.Empty }.toMutableGrid()
        for (position in input.first.positions()) {
            when (input.first[position]) {
                Cell.Empty, Cell.Wall -> {
                    map[2 * position.x, position.y] = input.first[position]
                    map[2 * position.x + 1, position.y] = input.first[position]
                }

                Cell.Robot -> {
                    map[2 * position.x, position.y] = Cell.Robot
                }

                Cell.BoxLeft -> {
                    map[2 * position.x, position.y] = Cell.BoxLeft
                    map[2 * position.x + 1, position.y] = Cell.BoxRight
                }

                Cell.BoxRight -> throw IllegalStateException()
            }
        }

        fun printMap() {
            println(map.map {
                when (it) {
                    Cell.Wall -> "#"
                    Cell.Empty -> "."
                    Cell.Robot -> "@"
                    Cell.BoxLeft -> "["
                    Cell.BoxRight -> "]"
                }
            })
            println()
        }


//        printMap()

        step@ for (direction in input.second) {
            val robot = map.positions().single { map[it] == Cell.Robot }

            val moves = mutableListOf<Triple<Cell, Position, Position>>()

            val nextPositionsToMove = ArrayDeque<Position>()
            nextPositionsToMove += robot

            val visited = mutableSetOf<Position>()

            while (nextPositionsToMove.isNotEmpty()) {
                val pos = nextPositionsToMove.removeFirst()
                if (pos in visited) continue

                val next = pos + direction
                when (map[next]) {
                    Cell.Empty -> {}
                    Cell.Wall -> {
                        continue@step
                    }

                    Cell.Robot -> throw IllegalStateException()
                    Cell.BoxLeft -> {
                        nextPositionsToMove += next
                        nextPositionsToMove += next + Direction.Right
                    }

                    Cell.BoxRight -> {
                        nextPositionsToMove += next + Direction.Left
                        nextPositionsToMove += next
                    }
                }

                moves += Triple(map[pos], pos, next)
                visited += pos
            }

            moves.forEach { (cell, pos, next) ->
                map[pos] = Cell.Empty
            }
            moves.forEach { (cell, pos, next) ->
                map[next] = cell
            }

//            printMap()
        }

        return map.positions().filter { map[it] == Cell.BoxLeft }.sumOf { 100 * it.y + it.x }
    }

    enum class Cell {
        Wall, Empty, Robot, BoxLeft, BoxRight
    }
}
