package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.util.Direction8.*
import com.dou.parser.char
import com.dou.parser.parser

fun main() {
    Day4().run()
}

class Day4 : TypedDay<Grid<Char>>() {

    override val parser by parser {
        grid(char)
    }

    override val test1: Any
        get() = 18

    override val test2: Any
        get() = 9

    override fun part1(input: Grid<Char>): Any {
        var result = 0
        for (pos in input.positions()) {
            for (d in Direction8.entries) {
                if (checkLineIs(input, pos, d, "XMAS")) {
                    result += 1
                }
            }
        }
        return result
    }

    override fun part2(input: Grid<Char>): Any {
        var result = 0
        for (topLeft in input.positions()) {
            if (checkLineIs(input, topLeft, DownRight, "MAS")
                || checkLineIs(input, topLeft, DownRight, "SAM")
            ) {
                val topRight = topLeft + Right + Right
                if (checkLineIs(input, topRight, DownLeft, "MAS")
                    || checkLineIs(input, topRight, DownLeft, "SAM")
                ) {
                    result += 1
                }
            }
        }
        return result
    }

    private fun checkLineIs(grid: Grid<Char>, from: Position, to: Direction8, expected: String): Boolean {
        return line(grid, from, to).take(expected.length).joinToString(separator = "") == expected
    }

    private fun line(grid: Grid<Char>, from: Position, to: Direction8): Sequence<Char> {
        return sequence {
            var current = from
            while (current in grid) {
                val v = grid[current]
                yield(v)
                current += to
            }
        }
    }
}
