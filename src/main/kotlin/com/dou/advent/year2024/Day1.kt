package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day1().run()
}

class Day1 : TypedDay<List<Pair<Int, Int>>>() {
    override val parser by parser {
        (int - ws - int).separatedByLines()
    }

    override val test1: Any
        get() = 11

    override val test2: Any
        get() = 31

    override fun part1(input: List<Pair<Int, Int>>): Any {
        val left = input.map { it.first }.sorted()
        val right = input.map { it.second }.sorted()
        return left.zip(right).sumOf { (a, b) -> abs(b - a) }
    }

    override fun part2(input: List<Pair<Int, Int>>): Any {
        val left = input.map { it.first }.sorted()
        val right = input.map { it.second }.groupBy { it }.mapValues { it.value.size }
        return left.sumOf { it * (right[it] ?: 0) }
    }
}
