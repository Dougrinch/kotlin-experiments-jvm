package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*
import java.math.BigDecimal
import kotlin.math.min

fun main() {
    Day13().run()
}

class Day13 : TypedDay<List<Day13.Machine>>() {

    override val test1: Any
        get() = 480

    override val parser by parser {
        val button by "X+" - long - ", Y+" - long map ::Button
        val prize by "X=" - long - ", Y=" - long
        val machine by "Button A: " - button - "\nButton B: " - button - "\nPrize: " - prize map ::Machine
        machine.separatedBy("\n\n")
    }

    override fun part1(input: List<Machine>): Any {
        return input.sumOf { m ->
            var minCost = Int.MAX_VALUE
            var found = false
            for (a in 0..100) {
                for (b in 0..100) {
                    val cost = 3 * a + b
                    val dx = a * m.buttonA.dx + b * m.buttonB.dx
                    val dy = a * m.buttonA.dy + b * m.buttonB.dy
                    val pos = dx to dy
                    if (pos == m.prize) {
                        minCost = min(minCost, cost)
                        found = true
                    }
                }
            }
            if (found) {
                minCost
            } else {
                0
            }
        }
    }

    override fun part2(input: List<Machine>): Any {
        val fixedInput = input.map { m ->
            m.copy(
                prize = Pair(
                    m.prize.first + 10000000000000,
                    m.prize.second + 10000000000000
                )
            )
        }

        var r = 0L

        for (m in fixedInput) {
            val x = BigDecimal(m.prize.first)
            val y = BigDecimal(m.prize.second)
            val xa = BigDecimal(m.buttonA.dx)
            val ya = BigDecimal(m.buttonA.dy)
            val xb = BigDecimal(m.buttonB.dx)
            val yb = BigDecimal(m.buttonB.dy)
            val a = (x * yb - y * xb) / (xa * yb - ya * xb)
            val b = (x - a * xa) / xb

            val fx = a.toLong() * m.buttonA.dx + b.toLong() * m.buttonB.dx
            val fy = a.toLong() * m.buttonA.dy + b.toLong() * m.buttonB.dy

            if (fx to fy == m.prize) {
                r += a.toLong() * 3 + b.toLong()
            }
        }

        return r
    }

    data class Button(val dx: Long, val dy: Long)
    data class Machine(val buttonA: Button, val buttonB: Button, val prize: Pair<Long, Long>)
}
