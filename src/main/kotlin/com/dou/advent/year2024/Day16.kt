package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day16().run()
}

class Day16 : TypedDay<Grid<Day16.Cell>>() {

    override val test1: Any
        get() = 11048

    override val parser by parser {
        val cell by oneOf(
            "#" parseAs Cell.Wall,
            "." parseAs Cell.Empty,
            "S" parseAs Cell.Start,
            "E" parseAs Cell.End,
        )

        grid(cell)
    }

    override fun part1(input: Grid<Cell>): Any {
        val end = input.positions().single { input[it] == Cell.End }
        val path = search(
            listOf(input.positions().single { input[it] == Cell.Start }) to Direction.Right,
            { (path, dir) ->
                val last = path.last()
                listOf(
                    dir to 1,
                    dir.rotateClockwise() to 1001,
                    dir.rotateCounterclockwise() to 1001
                )
                    .filter { (dir, _) -> path.none { it == last + dir } }
                    .map { (nextDir, cost) ->
                        path + (last + nextDir) to nextDir to cost
                    }.filter { (pathAndDirection, _) ->
                        input[pathAndDirection.first.last()] != Cell.Wall
                    }
            },
            { (end - it.first.last()).let { (dx, dy) -> abs(dx) + abs(dy) } },
            { it.first.last() == end }
        )
        return path.getOrNull()!!.second

        TODO()
//        val all = bruteForce(
//            listOf(
//                Triple(
//                    input.positions().single { input[it] == Cell.Start },
//                    Direction.Right,
//                    0
//                )
//            ),
//            { path ->
//                path.last().let { (p, d, _) ->
//                    listOf(
//                        Triple(p, d, 1),
//                        Triple(p, d.rotateClockwise(), 1001),
//                        Triple(p, d.rotateCounterclockwise(), 1001)
//                    )
//                }
//            },
//            { path, (from, direction, cost) ->
//                val next = from + direction
//                if (path.any { it.first == next }) {
//                    return@bruteForce Move.Unacceptable
//                }
//
//                when (input[next]) {
//                    Cell.Wall -> Move.Unacceptable
//                    Cell.End -> Move.PathCompleted(path + Triple(next, direction, cost))
//                    Cell.Start, Cell.Empty -> Move.Successful(path + Triple(next, direction, cost))
//                }
//            }
//        )
//
//        return all.minOf { it.sumOf { it.third } }//.forEach { println(it) }
    }

    enum class Cell {
        Start, End, Empty, Wall
    }
}
