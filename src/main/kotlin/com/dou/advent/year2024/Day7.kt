package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.bruteForce
import com.dou.parser.*

fun main() {
    Day7().run()
}

class Day7 : TypedDay<List<Pair<Long, List<Long>>>>() {

    override val parser by parser {
        (long - ": " - long.separatedBy(" ")).separatedByLines()
    }

    override val test1: Any
        get() = 3749

    override val test2: Any
        get() = 11387

    override fun part1(input: List<Pair<Long, List<Long>>>): Any {
        return input.filter { (expected, ns) ->
            bruteForce(listOf("+", "*"), ns.size - 1, false).any { ops ->
                var r = ns[0]
                for (i in ops.indices) {
                    when (ops[i]) {
                        "+" -> r += ns[i + 1]
                        "*" -> r *= ns[i + 1]
                    }
                }
                r == expected
            }
        }.sumOf { it.first }
    }

    override fun part2(input: List<Pair<Long, List<Long>>>): Any {
        return input.filter { (expected, ns) ->
            bruteForce(listOf("+", "*", "||"), ns.size - 1, false).any { ops ->
                var r = ns[0]
                for (i in ops.indices) {
                    when (ops[i]) {
                        "+" -> r += ns[i + 1]
                        "*" -> r *= ns[i + 1]
                        "||" -> r = "$r${ns[i + 1]}".toLong()
                    }
                }
                r == expected
            }
        }.sumOf { it.first }
    }
}
