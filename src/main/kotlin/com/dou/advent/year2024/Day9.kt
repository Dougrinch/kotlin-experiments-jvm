package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day9().run()
}

class Day9 : TypedDay<List<Int>>() {

    override val test1: Any
        get() = 1928

    override val test2: Any
        get() = 2858

    override val parser by parser {
        digit.many()
    }

    override fun part1(input: List<Int>): Any {
        val disk = decodeDisk(input)

        var firstEmpty = disk.indexOf(null)
        main@ while (true) {
            while (disk.last() == null) {
                disk.removeLast()
            }
            if (firstEmpty >= disk.size) {
                break
            }
            disk[firstEmpty] = disk.last()
            disk.removeLast()
            for (i in firstEmpty until disk.size) {
                if (disk[i] == null) {
                    firstEmpty = i
                    continue@main
                }
            }
            break
        }

        return disk.withIndex().sumOf { (idx, d) -> idx * d!!.toLong() }
    }

    private fun decodeDisk(input: List<Int>): MutableList<Int?> {
        val disk = mutableListOf<Int?>()

        var file = true
        var id = 0

        for (n in input) {
            if (file) {
                for (i in 0 until n) {
                    disk.add(id)
                }
                id += 1
            } else {
                for (i in 0 until n) {
                    disk.add(null)
                }
            }
            file = !file
        }
        return disk
    }

    override fun part2(input: List<Int>): Any {
        val disk = decodeDisk(input)

        val emptySpaces = findEmptySpaces(disk).toMutableList()
        val files = findFiles(disk)

        for (wid in files.indices.reversed()) {
            val sid = emptySpaces.indexOfFirst { it.size >= files[wid].size }

            if (sid < 0) {
                continue
            }

            if (emptySpaces[sid].from > files[wid].from) {
                continue
            }

            for (i in emptySpaces[sid].from until (emptySpaces[sid].from + files[wid].size)) {
                disk[i] = files[wid].id
            }

            for (i in files[wid].from..files[wid].to) {
                disk[i] = null
            }

            val newSpace = Space(emptySpaces[sid].from + files[wid].size, emptySpaces[sid].to)
            if (newSpace.size > 0) {
                emptySpaces[sid] = newSpace
            } else {
                emptySpaces.removeAt(sid)
            }
        }

        return disk.withIndex().sumOf { (idx, d) -> idx * (d ?: 0L).toLong() }
    }

    private fun findEmptySpaces(disk: MutableList<Int?>): List<Space> {
        val emptySpaces = mutableListOf<Space>()

        var start = 0
        while (start < disk.size) {
            if (disk[start] != null) {
                start += 1
            } else {
                var end = start
                for (j in start until disk.size) {
                    if (disk[j] == null) {
                        end = j
                    } else {
                        break
                    }
                }
                emptySpaces += Space(start, end)
                start = end + 1
            }
        }

        return emptySpaces
    }

    private fun findFiles(disk: MutableList<Int?>): List<File> {
        val files = mutableListOf<File>()

        var start = 0
        while (start < disk.size) {
            if (disk[start] == null) {
                start += 1
            } else {
                var end = start
                for (j in start until disk.size) {
                    if (disk[j] == disk[start]) {
                        end = j
                    } else {
                        break
                    }
                }
                files += File(disk[start]!!, start, end)
                start = end + 1
            }
        }

        return files
    }

    data class Space(val from: Int, val to: Int) {
        val size = to - from + 1
    }

    data class File(val id: Int, val from: Int, val to: Int) {
        val size = to - from + 1
    }
}
