package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*
import kotlin.math.abs

fun main() {
    Day2().run()
}

class Day2 : TypedDay<List<List<Int>>>() {

    override val parser by parser {
        (int separatedBy " ").separatedByLines()
    }

    override val test1: Any
        get() = 2

    override val test2: Any
        get() = 4

    override fun part1(input: List<List<Int>>): Any {
        return input.count {
            it.isSafe()
        }
    }

    override fun part2(input: List<List<Int>>): Any {
        return input.count { levels ->
            if (levels.isSafe()) {
                return@count true
            }

            for (i in 0 until levels.count()) {
                if (levels.toMutableList().also { it.removeAt(i) }.isSafe()) {
                    return@count true
                }
            }

            false
        }
    }

    private fun List<Int>.isSafe(): Boolean {
        val dsts = zipWithNext().map { it.second - it.first }
        return (dsts.all { it > 0 } || dsts.all { it < 0 }) && dsts.all { abs(it) <= 3 }
    }
}
