package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.parser.char
import com.dou.parser.parser

fun main() {
    Day8().run()
}

class Day8 : TypedDay<Grid<Char>>() {

    override val parser by parser {
        grid(char)
    }

    override val test1: Any
        get() = 14

    override val test2: Any
        get() = 34

    override fun part1(input: Grid<Char>): Any {
        val antinodes = mutableSetOf<Position>()

        val antennas = input.positions()
            .filter { input[it] != '.' }
            .groupBy { input[it] }

        for (ants in antennas.values) {
            for (i in 0 until ants.size - 1) {
                for (j in i + 1 until ants.size) {
                    val d = ants[j] - ants[i]
                    val p1 = ants[j] + d
                    val p2 = ants[i] - d
                    if (p1 in input) {
                        antinodes += p1
                    }
                    if (p2 in input) {
                        antinodes += p2
                    }
                }
            }
        }

        return antinodes.size
    }


    override fun part2(input: Grid<Char>): Any {
        val antinodes = mutableSetOf<Position>()

        val antennas = input.positions()
            .filter { input[it] != '.' }
            .groupBy { input[it] }

        for (ants in antennas.values) {
            for (i in 0 until ants.size - 1) {
                for (j in i + 1 until ants.size) {
                    val d = ants[j] - ants[i]
                    var p = ants[j]
                    while (p in input) {
                        antinodes += p
                        p += d
                    }
                    p = ants[i]
                    while (p in input) {
                        antinodes += p
                        p -= d
                    }
                }
            }
        }

        return antinodes.size
    }
}
