package com.dou.advent.year2024

import com.dou.advent.TypedDay
import com.dou.parser.*

fun main() {
    Day11().run()
}

class Day11 : TypedDay<List<Long>>() {

    override val test1: Any
        get() = 55312

    override val parser by parser {
        long.separatedBy(" ")
    }

    override fun part1(input: List<Long>): Any {
        var r = input.groupBy { it }.mapValues { it.value.size.toLong() }
        for (i in 0 until 25) {
            r = blink(r)
        }
        return r.values.sum()
    }

    override fun part2(input: List<Long>): Any {
        var r = input.groupBy { it }.mapValues { it.value.size.toLong() }
        for (i in 0 until 75) {
            r = blink(r)
        }
        return r.values.sum()
    }

    private fun blink(input: Map<Long, Long>): Map<Long, Long> {
        val r = mutableMapOf<Long, Long>()
        fun add(n: Long, count: Long) {
            r.compute(n) { _, v -> if (v == null) count else v + count }
        }
        for ((n, count) in input) {
            when {
                n == 0L -> add(1L, count)
                n.toString().length % 2 == 0 -> {
                    val src = n.toString()
                    val left = src.substring(0, src.length / 2)
                    val right = src.substring(src.length / 2, src.length)
                    add(left.toLong(), count)
                    add(right.toLong(), count)
                }

                else -> add(n * 2024, count)
            }
        }
        return r
    }
}
