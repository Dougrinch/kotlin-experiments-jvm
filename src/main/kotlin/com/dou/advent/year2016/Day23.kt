package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.advent.year2016.Day23.Op
import com.dou.parser.*

fun main() {
    Day23().run()
}

class Day23 : TypedDay<List<Op>>() {
    override val parser by parser {
        val const by int map ::Const
        val register by char map ::Register
        val value by const or register
        val op by oneOf(
            "cpy " - value - " " - register map ::Copy,
            "inc " - register map ::Inc,
            "dec " - register map ::Dec,
            "jnz " - value - " " - value map ::Jnz,
            "tgl " - value map ::Toggle
        )
        op.separatedByLines()
    }

    override fun part1(input: List<Op>): Any {
        return execute(input, 7)
    }

    override fun part2(input: List<Op>): Any {
        return execute(input, 12)
    }

    private fun execute(input: List<Op>, a: Int): Int {
        val ops = input.toMutableList()
        var p = 0
        val registers = mutableMapOf(Register('a') to a).withDefault { 0 }
        while (p <= ops.lastIndex) {
            when (val op = ops[p]) {
                is Copy -> {
                    registers[op.y] = op.x.read(registers)
                    p += 1
                }
                is Inc -> {
                    registers[op.x] = registers.getValue(op.x) + 1
                    p += 1
                }
                is Dec -> {
                    registers[op.x] = registers.getValue(op.x) - 1
                    p += 1
                }
                is Jnz -> {
                    p += if (op.x.read(registers) != 0) {
                        op.y.read(registers)
                    } else {
                        1
                    }
                }
                is Toggle -> {
                    val idx = p + op.x.read(registers)
                    if (idx in ops.indices) {
                        val newOp = when (val oldOp = ops[idx]) {
                            is Inc -> Dec(oldOp.x)
                            is Op1<*> -> {
                                if (oldOp.x is Register) {
                                    Inc(oldOp.x as Register)
                                } else {
                                    oldOp
                                }
                            }
                            is Jnz -> {
                                if (oldOp.y is Register) {
                                    Copy(oldOp.x, oldOp.y)
                                } else {
                                    oldOp
                                }
                            }
                            is Op2<*, *> -> Jnz(oldOp.x, oldOp.y)
                        }
                        ops[idx] = newOp
                    }
                    p += 1
                }
            }
        }
        return registers.getValue(Register('a'))
    }


    sealed interface Value {
        fun read(registers: Map<Register, Int>): Int
    }

    data class Const(val v: Int) : Value {
        override fun read(registers: Map<Register, Int>): Int {
            return v
        }
    }

    data class Register(val name: Char) : Value {
        override fun read(registers: Map<Register, Int>): Int {
            return registers.getValue(this)
        }
    }

    sealed interface Op

    sealed interface Op1<X : Value> : Op {
        val x: X
    }

    sealed interface Op2<X : Value, Y : Value> : Op {
        val x: X
        val y: Y
    }

    data class Copy(override val x: Value, override val y: Register) : Op2<Value, Register>
    data class Inc(override val x: Register) : Op1<Register>
    data class Dec(override val x: Register) : Op1<Register>
    data class Jnz(override val x: Value, override val y: Value) : Op2<Value, Value>
    data class Toggle(override val x: Value) : Op1<Value>
}
