package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.advent.year2016.Day18.Tile
import com.dou.advent.year2016.Day18.Tile.Safe
import com.dou.advent.year2016.Day18.Tile.Trap
import com.dou.parser.*

fun main() {
    Day18().run()
}

class Day18 : TypedDay<List<Tile>>() {
    override val parser by parser {
        val tile by oneOf(
            "." parseAs Safe,
            "^" parseAs Trap
        )
        tile.many()
    }

    override fun part1(input: List<Tile>): Any {
        return generateSequence(input) { line -> line.indices.map { nextLineTile(line, it) } }
            .take(40)
            .sumOf { it.count { it == Safe } }
    }

    override fun part2(input: List<Tile>): Any {
        return generateSequence(input) { line -> line.indices.map { nextLineTile(line, it) } }
            .take(400000)
            .sumOf { it.count { it == Safe } }
    }

    private fun nextLineTile(prev: List<Tile>, i: Int): Tile {
        val left = if (i == 0) Safe else prev[i - 1]
        val center = prev[i]
        val right = if (i == prev.lastIndex) Safe else prev[i + 1]

        return when {
            left == Trap && center == Trap && right == Safe -> Trap
            left == Safe && center == Trap && right == Trap -> Trap
            left == Trap && center == Safe && right == Safe -> Trap
            left == Safe && center == Safe && right == Trap -> Trap
            else -> Safe
        }
    }

    enum class Tile {
        Trap, Safe
    }
}
