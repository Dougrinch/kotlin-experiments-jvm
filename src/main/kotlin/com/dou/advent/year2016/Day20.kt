package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.parser.*
import java.util.*

fun main() {
    Day20().run()
}

class Day20 : TypedDay<Pair<LongRange, List<LongRange>>>() {
    override val test1: Any
        get() = 3
    override val test2: Any
        get() = 2

    override val parser by parser {
        val range by long - ".." - long map Long::rangeTo
        val block by long - "-" - long map Long::rangeTo
        range - "\n" - block.separatedByLines()
    }

    override fun part1(input: Pair<LongRange, List<LongRange>>): Any {
        val (range, blocks) = input
        return blocks
            .asSequence()
            .map {
                val left = (it.first - 1).let { c -> if (c in range && blocks.none { c in it }) c else Long.MAX_VALUE }
                val right = (it.last + 1).let { c -> if (c in range && blocks.none { c in it }) c else Long.MAX_VALUE }
                minOf(left, right)
            }
            .filterNot { it == Long.MAX_VALUE }
            .minOf { it }
    }

    override fun part2(input: Pair<LongRange, List<LongRange>>): Any {
        val (range, blocks) = input
        val borders = blocks
            .flatMapTo(TreeSet()) { listOf(it.first - 1, it.last + 1) }
            .filter { it in range }
        var count = 0L
        for (i in borders.indices) {
            if (borders[i].allowed(blocks)) {
                if (i != borders.lastIndex && borders[i + 1].allowed(blocks)) {
                    count += borders[i + 1] - borders[i] + 1
                } else if (i != 0 && !borders[i - 1].allowed(blocks)) {
                    count += 1
                } else if (i == 0) {
                    count += 1
                }
            }
        }
        return count
    }

    private fun Long.allowed(blocks: List<LongRange>): Boolean {
        return blocks.none { this in it }
    }
}
