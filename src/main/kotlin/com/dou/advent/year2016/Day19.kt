package com.dou.advent.year2016

import com.dou.advent.util.ringListIterator
import java.util.*

fun main() {
    Day19().run()
}

class Day19 {
    fun run() {
        part1(3018458)
        part2(3018458)
    }

    private fun part1(elvesCount: Int) {
        val elves = (1..elvesCount).toCollection(LinkedList())

        val target = elves.ringListIterator(0)
        while (elves.size > 1) {
            target.next()
            target.next()
            target.remove()
        }

        println(elves.single())
    }

    private fun part2(elvesCount: Int) {
        val elves = (1..elvesCount).toCollection(LinkedList())

        val target = elves.ringListIterator(elves.size / 2)
        var skip = elves.size % 2 == 1
        target.next()
        target.remove()

        while (elves.size > 1) {
            if (skip) {
                target.next()
            }
            skip = !skip
            target.next()
            target.remove()
        }

        println(elves.single())
    }
}
