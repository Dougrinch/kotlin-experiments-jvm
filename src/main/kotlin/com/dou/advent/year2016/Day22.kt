package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.advent.util.Position
import com.dou.advent.year2016.Day22.Node
import com.dou.parser.*

fun main() {
    Day22().run()
}

class Day22 : TypedDay<List<Node>>() {
    override val parser by parser {
        val name by "/dev/grid/node-x" - int - "-y" - int map ::Position
        val node by name - ws - int - "T" - ws - int - "T" - ws - int - "T" - ws - int - "%" map ::Node
        val header by "Filesystem" - ws - "Size  Used  Avail  Use%\n"
        header - node.separatedByLines()
    }

    override fun part1(input: List<Node>): Any {
        return input.flatMap { a -> input.map { b -> a to b } }
            .count { (a, b) -> a.used != 0 && a != b && a.useP < b.avail }
    }

    override fun part2(input: List<Node>): Any {
        val maxX = input.maxOf { it.pos.x }
        val maxY = input.maxOf { it.pos.y }

        val grid = input.associateBy { it.pos }

        for (y in 0..maxY) {
            for (x in 0..maxX) {
                val node = grid[Position(x, y)]!!
                if (node.used == 0) {
                    print(" _ ")
                } else if (node.used > 100) {
                    print(" # ")
                } else {
                    print(" . ")
                }
            }
            println()
        }

        return "-"
    }

    data class Node(val pos: Position, val size: Int, val used: Int, val avail: Int, val useP: Int)
}
