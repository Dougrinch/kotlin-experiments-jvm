package com.dou.advent.year2016

import arrow.core.Some
import com.dou.advent.TypedDay
import com.dou.advent.util.*
import com.dou.advent.year2016.Day24.Cell
import com.dou.parser.*

fun main() {
    Day24().run()
}

class Day24 : TypedDay<Grid<Cell>>() {
    override val test1: Any
        get() = 14

    override val parser by parser {
        val start by "0" parseAs Start
        val wall by "#" parseAs Wall
        val empty by "." parseAs Empty
        val target by char('1'..'9') map ::Target
        val cell by oneOf(start, wall, empty, target)
        grid(cell)
    }

    override fun part1(input: Grid<Cell>): Any {
        val lengths = lengths(input)
        return paths(input, false)
            .map { it.zipWithNext().sumOf { lengths[it]!! } }
            .minOf { it }
    }

    override fun part2(input: Grid<Cell>): Any {
        val lengths = lengths(input)
        return paths(input, true)
            .map { it.zipWithNext().sumOf { lengths[it]!! } }
            .minOf { it }
    }

    private fun lengths(grid: Grid<Cell>): Map<Pair<Position, Position>, Int> {
        val locations = grid.positions().filter { grid[it] == Start || grid[it] is Target }.toList()
        val lengths = locations.flatMap { from ->
            locations.mapNotNull { to ->
                if (from != to) {
                    from to to to path(grid, from, to)
                } else {
                    null
                }
            }
        }.toMap()
        return lengths
    }

    private fun paths(grid: Grid<Cell>, finishAtStart: Boolean): Sequence<List<Position>> {
        val start = grid.positions().single { grid[it] == Start }
        val targets = grid.positions().filter { grid[it] is Target }.toList()
        return if (finishAtStart) {
            targets.permutations().map { listOf(start) + it + start }
        } else {
            targets.permutations().map { listOf(start) + it }
        }
    }

    private fun path(grid: Grid<Cell>, from: Position, to: Position): Int {
        val r = search(
            from,
            { grid.neighboursOf(it, diagonal = false).filter { grid[it] != Wall }.map { it to 1 } },
            { 0 },
            { it == to }
        ).map { it.second } as Some
        return r.value
    }

    sealed interface Cell
    object Wall : Cell
    object Empty : Cell
    object Start : Cell
    data class Target(val id: Char) : Cell
}
