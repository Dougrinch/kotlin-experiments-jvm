package com.dou.advent.year2016

import com.dou.advent.util.*

fun main() {
    Day17().run()
}

class Day17 {
    fun run() {
        search(
            0 to 0 to "",
            { (xy, p) -> nextSteps(xy, p, "qtetzkpl").map { it to 1 } },
            { 0 },
            { (xy, _) -> xy == 3 to 3 }
        ).map { it.first.last().second }
            .let { println(it) }

        bruteForce(
            0 to 0 to "",
            { (xy, p) -> nextSteps(xy, p, "qtetzkpl") },
            { _, xyp ->
                when (xyp.first) {
                    3 to 3 -> Move.PathCompleted(xyp)
                    else -> Move.Successful(xyp)
                }
            }
        ).maxOf { it.second.length }
            .let { println(it) }
    }

    private fun nextSteps(
        xy: Pair<Int, Int>, path: String, passcode: String
    ): List<Pair<Pair<Int, Int>, String>> {
        val x = xy.first
        val y = xy.second
        val hash = md5("$passcode$path")
        val result = ArrayList<Pair<Pair<Int, Int>, String>>()
        if (hash[0] in 'b'..'f' && y > 0) {
            result += x to y - 1 to "${path}U"
        }
        if (hash[1] in 'b'..'f' && y < 3) {
            result += x to y + 1 to "${path}D"
        }
        if (hash[2] in 'b'..'f' && x > 0) {
            result += x - 1 to y to "${path}L"
        }
        if (hash[3] in 'b'..'f' && x < 3) {
            result += x + 1 to y to "${path}R"
        }
        return result
    }
}
