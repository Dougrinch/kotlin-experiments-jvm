package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.advent.year2016.Day21.Op
import com.dou.parser.*

fun main() {
    Day21().run()
}

class Day21 : TypedDay<Pair<String, List<Op>>>() {
    override val test1: Any
        get() = "decab"

    override val parser by parser {
        word - "\n" - oneOf(
            "swap position " - int - " with position " - int map ::SwapPosition,
            "swap letter " - char - " with letter " - char map ::SwapLetter,
            "rotate left " - int - regex(" steps?").ignoreValue() map ::RotateLeft,
            "rotate right " - int - regex(" steps?").ignoreValue() map ::RotateRight,
            "rotate based on position of letter " - char map ::RotatePosition,
            "reverse positions " - int - " through " - int map ::ReversePositions,
            "move position " - int - " to position " - int map ::MovePosition
        ).separatedByLines()
    }

    override fun part1(input: Pair<String, List<Op>>): Any {
        val (password, ops) = input
        var result = password.toList()
        for (op in ops) {
            result = apply(result, op)
        }
        return result.joinToString("")
    }

    override fun part2(input: Pair<String, List<Op>>): Any {
        var password = "fbgdceah".toList()
        for (op in input.second.asReversed()) {
            password = when (op) {
                is SwapPosition -> apply(password, op)
                is SwapLetter -> apply(password, op)
                is RotateRight -> apply(password, RotateLeft(op.x))
                is RotateLeft -> apply(password, RotateRight(op.x))
                is RotatePosition -> {
                    (1..password.size)
                        .map { apply(password, RotateRight(it)) }
                        .first { apply(it, op) == password }
                }
                is ReversePositions -> apply(password, op)
                is MovePosition -> apply(password, MovePosition(op.y, op.x))
            }
        }
        return password.joinToString("")
    }

    private fun apply(password: List<Char>, op: Op): List<Char> {
        return when (op) {
            is SwapPosition -> {
                password.indices.map {
                    when (it) {
                        op.x -> password[op.y]
                        op.y -> password[op.x]
                        else -> password[it]
                    }
                }
            }
            is SwapLetter -> {
                password.map {
                    when (it) {
                        op.x -> op.y
                        op.y -> op.x
                        else -> it
                    }
                }
            }
            is RotateRight -> {
                val d = op.x % password.size
                password.subList(password.size - d, password.size) + password.subList(0, password.size - d)
            }
            is RotateLeft -> {
                val d = op.x % password.size
                password.subList(d, password.size) + password.subList(0, d)
            }
            is RotatePosition -> {
                val idx = password.indexOf(op.x)
                if (idx >= 4) {
                    apply(password, RotateRight(idx + 2))
                } else {
                    apply(password, RotateRight(idx + 1))
                }
            }
            is ReversePositions -> {
                password.subList(0, op.x) +
                    password.subList(op.x, op.y + 1).asReversed() +
                    password.subList(op.y + 1, password.size)
            }
            is MovePosition -> {
                val r = ArrayList(password)
                r.add(op.y, r.removeAt(op.x))
                r
            }
        }
    }

    sealed interface Op
    data class SwapPosition(val x: Int, val y: Int) : Op
    data class SwapLetter(val x: Char, val y: Char) : Op
    data class RotateLeft(val x: Int) : Op
    data class RotateRight(val x: Int) : Op
    data class RotatePosition(val x: Char) : Op
    data class ReversePositions(val x: Int, val y: Int) : Op
    data class MovePosition(val x: Int, val y: Int) : Op
}
