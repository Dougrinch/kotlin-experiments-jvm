package com.dou.advent.year2016

import com.dou.advent.util.md5
import java.util.*

fun main() {
    Day14().run()
}

class Day14 {
    fun run() {
        println(keys("zpqevtbw", ::hash).take(64).map { it.first }.last())
        println(keys("zpqevtbw", ::stretchedHash).take(64).map { it.first }.last())
    }

    private fun keys(salt: String, hashF: (Int, String) -> String): Sequence<Pair<Int, String>> = sequence {
        val keys = PriorityQueue<Pair<Int, String>>(compareBy { it.first })
        val candidates = ArrayList<Triple<Int, String, Char>>()
        var i = 0
        while (true) {
            val hash = hashF(i, salt)

            val it = candidates.iterator()
            while (it.hasNext()) {
                val (idx, key, c) = it.next()
                if ("$c$c$c$c$c" in hash) {
                    keys += idx - 1000 to key
                    it.remove()
                } else if (idx == i) {
                    it.remove()
                }
            }

            for (ic in 0 until (hash.length - 2)) {
                if (hash[ic + 1] == hash[ic] && hash[ic + 2] == hash[ic]) {
                    candidates += Triple(i + 1000, hash, hash[ic])
                    break
                }
            }

            while (keys.isNotEmpty() && keys.peek().first + 1000 < i) {
                yield(keys.poll())
            }

            i += 1
        }
    }

    private fun hash(n: Int, salt: String): String {
        return md5("$salt$n")
    }

    private fun stretchedHash(n: Int, salt: String): String {
        var hash = md5("$salt$n")
        for (i in 1..2016) {
            hash = md5(hash)
        }
        return hash
    }
}
