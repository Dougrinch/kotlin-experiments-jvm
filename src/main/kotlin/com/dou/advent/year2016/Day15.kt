package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.advent.year2016.Day15.Disc
import com.dou.parser.*

fun main() {
    Day15().run()
}

class Day15 : TypedDay<List<Disc>>() {
    override val test1: Any
        get() = 5

    override val parser by parser {
        val disc by "Disc #" - int - " has " - int - " positions; at time=0, it is at position " - int - "." map ::Disc
        disc.separatedByLines()
    }

    override fun part1(input: List<Disc>): Any {
        return generateSequence(0) { it + 1 }
            .filter { check(it, input) }
            .first()
    }

    override fun part2(input: List<Disc>): Any {
        return generateSequence(0) { it + 1 }
            .filter { check(it, input + Disc(input.size + 1, 11, 0)) }
            .first()
    }

    private fun check(t: Int, discs: List<Disc>): Boolean {
        return discs.all { (it.startPosition + t + it.idx) % it.positions == 0 }
    }

    data class Disc(val idx: Int, val positions: Int, val startPosition: Int)
}
