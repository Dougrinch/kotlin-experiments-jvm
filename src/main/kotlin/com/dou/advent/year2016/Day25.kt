package com.dou.advent.year2016

import com.dou.advent.TypedDay
import com.dou.advent.year2016.Day25.Op
import com.dou.parser.*

fun main() {
    Day25().run()
}

class Day25 : TypedDay<List<Op>>() {
    override val parser by parser {
        val const by int map ::Const
        val register by char map ::Register
        val value by const or register
        val op by oneOf(
            "cpy " - value - " " - register map ::Copy,
            "inc " - register map ::Inc,
            "dec " - register map ::Dec,
            "jnz " - value - " " - value map ::Jnz,
            "out " - value map ::Out
        )
        op.separatedByLines()
    }

    override fun part1(input: List<Op>): Any {
        return generateSequence(1) { it + 1 }
            .map { it to execute(input, it) }
            .first { (_, s) -> s.take(10).toList() == (1..5).flatMap { listOf(0, 1) } }
            .first
    }

    private fun execute(ops: List<Op>, a: Int): Sequence<Int> {
        return sequence {
            var p = 0
            val registers = mutableMapOf(Register('a') to a).withDefault { 0 }
            while (p <= ops.lastIndex) {
                when (val op = ops[p]) {
                    is Copy -> {
                        registers[op.y] = op.x.read(registers)
                        p += 1
                    }
                    is Inc -> {
                        registers[op.x] = registers.getValue(op.x) + 1
                        p += 1
                    }
                    is Dec -> {
                        registers[op.x] = registers.getValue(op.x) - 1
                        p += 1
                    }
                    is Jnz -> {
                        p += if (op.x.read(registers) != 0) {
                            op.y.read(registers)
                        } else {
                            1
                        }
                    }
                    is Out -> {
                        yield(op.x.read(registers))
                        p += 1
                    }
                }
            }
        }
    }

    sealed interface Value {
        fun read(registers: Map<Register, Int>): Int
    }

    data class Const(val v: Int) : Value {
        override fun read(registers: Map<Register, Int>): Int {
            return v
        }
    }

    data class Register(val name: Char) : Value {
        override fun read(registers: Map<Register, Int>): Int {
            return registers.getValue(this)
        }
    }

    sealed interface Op
    data class Copy(val x: Value, val y: Register) : Op
    data class Inc(val x: Register) : Op
    data class Dec(val x: Register) : Op
    data class Jnz(val x: Value, val y: Value) : Op
    data class Out(val x: Value) : Op
}
