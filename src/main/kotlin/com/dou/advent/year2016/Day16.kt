package com.dou.advent.year2016

fun main() {
    Day16().run()
}

class Day16 {
    fun run() {
        println(checksum("10000", 20))
        println(checksum("00111101111101000", 272))
        println(checksum("00111101111101000", 35651584))
    }

    private fun checksum(initial: String, discLength: Int): String {
        val discData = data(initial)
            .dropWhile { it.length < discLength }
            .first()
            .substring(0 until discLength)
        return checksum(discData)
    }

    private fun checksum(data: String): String {
        if (data.length % 2 == 1) {
            return data
        }

        return checksum(data.chunked(2).map { if (it[0] == it[1]) '1' else '0' }.joinToString(""))
    }

    private fun data(initial: String): Sequence<String> {
        return generateSequence(initial) { step(it) }
    }

    private fun step(initial: String): String {
        val b = initial.reversed().map {
            when (it) {
                '0' -> '1'
                '1' -> '0'
                else -> throw IllegalStateException()
            }
        }.joinToString("")

        return initial + "0" + b
    }
}
