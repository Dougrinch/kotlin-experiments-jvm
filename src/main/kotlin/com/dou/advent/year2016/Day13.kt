package com.dou.advent.year2016

import arrow.core.Some
import com.dou.advent.util.search

fun main() {
    Day13().run()
}

class Day13 {
    fun run() {
        val favouriteNumber = 1362
        println(path(1 to 1, 31 to 39, favouriteNumber).size - 1)
        println(allInNSteps(1 to 1, 50, favouriteNumber).size)
    }

    private fun path(from: Pair<Int, Int>, to: Pair<Int, Int>, favouriteNumber: Int): List<Pair<Int, Int>> {
        val r = search(
            from,
            { (x, y) ->
                listOf(x to y - 1, x to y + 1, x - 1 to y, x + 1 to y)
                    .filter { it.first >= 0 && it.second >= 0 }
                    .filter { isOpen(it.first, it.second, favouriteNumber) }
                    .map { it to 1 }
            },
            { 0 },
            { it == to }
        )
        return (r as Some).value.first
    }

    private fun allInNSteps(from: Pair<Int, Int>, n: Int, favouriteNumber: Int): Set<Pair<Int, Int>> {
        val visited = HashSet<Pair<Int, Int>>()
        var current = setOf(from)
        for (i in 0 until n) {
            current = current.nextStep(visited, favouriteNumber)
            visited += current
        }
        return visited
    }

    private fun Set<Pair<Int, Int>>.nextStep(
        allVisited: Set<Pair<Int, Int>>,
        favouriteNumber: Int
    ): Set<Pair<Int, Int>> {
        return flatMapTo(HashSet()) { (x, y) ->
            listOf(x to y - 1, x to y + 1, x - 1 to y, x + 1 to y).filter { it.first >= 0 && it.second >= 0 }
        }
            .filter { it !in allVisited }
            .filter { isOpen(it.first, it.second, favouriteNumber) }
            .toSet()
    }

    private fun isOpen(x: Int, y: Int, favouriteNumber: Int): Boolean {
        val a = x * x + 3 * x + 2 * x * y + y + y * y
        val b = a + favouriteNumber
        val c = b.toString(2)
        return c.count { it == '1' } % 2 == 0
    }
}
