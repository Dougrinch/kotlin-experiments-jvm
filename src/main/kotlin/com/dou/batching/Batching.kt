package com.dou.batching

import kotlinx.coroutines.delay
import java.util.concurrent.atomic.AtomicReference
import kotlin.coroutines.*
import kotlin.experimental.ExperimentalTypeInference
import kotlin.time.Duration.Companion.seconds
import kotlin.time.ExperimentalTime

data class Key<T>(val name: String)

val NAME = Key<String>("name")
val AGE = Key<Int>("age")

interface RootQuery<out T>

sealed interface Query<T : Any>
data class Get<T : Any>(val key: Key<T>) : Query<T>

@OptIn(ExperimentalTime::class)
@Suppress("UNCHECKED_CAST")
suspend fun <T : Any> realRunAll(queries: List<Query<T>>): List<T> {
    delay(1.seconds)
    val result = queries.map { query ->
        when (query) {
            is Get<*> -> when (query.key) {
                NAME -> "Ivan" as T
                AGE -> 18 as T
                else -> TODO()
            }
        }
    }
    println("$queries -> $result")
    return result
}

@OptIn(ExperimentalTypeInference::class)
fun <T> query(@BuilderInference body: suspend QueryScope.() -> T): RootQuery<T> {
    val query = QueryScopeImpl<T>()
    query.firstStep = body.createCoroutine(query, query)
    return query
}

@RestrictsSuspension
interface QueryScope {
    suspend fun <T : Any> Query<T>.await(): T
}

class QueryScopeImpl<T> : RootQuery<T>, QueryScope, Continuation<T> {

    lateinit var firstStep: Continuation<Unit>

    var nextQueryToRun: AtomicReference<Pair<Query<*>, Continuation<*>>?> = AtomicReference()
    var result: Result<T>? = null

    override val context: CoroutineContext = EmptyCoroutineContext

    @Suppress("UNCHECKED_CAST")
    override suspend fun <T : Any> Query<T>.await(): T {
        return suspendCoroutine {
            if (!nextQueryToRun.compareAndSet(null, this to it)) {
                throw IllegalStateException()
            }
        }
    }

    override fun resumeWith(result: Result<T>) {
        this.result = result
    }
}

suspend fun <T> run(query: RootQuery<T>): T = runAll(listOf(query)).single()

@Suppress("UNCHECKED_CAST")
suspend fun <T> runAll(queries: List<RootQuery<T>>): List<T> {
    val qs = queries.map { it as QueryScopeImpl<*> }

    qs.forEach { it.firstStep.resume(Unit) }

    while (true) {
        val queriesToRun = qs.map { it.nextQueryToRun }.mapNotNull { it.getAndSet(null) }
        if (queriesToRun.isEmpty()) {
            break
        }

        val uniqueQueries = queriesToRun.groupBy({ it.first }, { it.second }).toList()
        val queryResults = realRunAll(uniqueQueries.map { it.first })
        queryResults.zip(uniqueQueries.map { it.second }).forEach { (v, cs) ->
            cs.forEach { (it as Continuation<Any>).resume(v) }
        }
    }

    return qs.map { it.result!!.getOrThrow() } as List<T>
}

suspend fun main() {
    val name = query { Get(NAME).await() }
    val age = query { Get(AGE).await() }
    val desc = query {
        val n = Get(NAME).await()
        val a = Get(AGE).await()
        "$n ($a)"
    }

    println(runAll(listOf(name, age, desc)))
}
