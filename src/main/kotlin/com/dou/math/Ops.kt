package com.dou.math

interface IntegerOps<T> {
    val zero: T
    val one: T
    operator fun T.unaryMinus(): T
    operator fun T.plus(o: T): T
    operator fun T.minus(o: T): T
    operator fun T.times(o: T): T
}

interface RealOps<T> : IntegerOps<T> {
    operator fun T.div(o: T): T
    fun sqrt(x: T): T
}

interface TrigonometryOps<T> : IntegerOps<T> {
    fun sin(a: T): T
    fun cos(a: T): T
    fun atan2(y: T, x: T): T
}

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
interface IntOps : IntegerOps<Int> {
    companion object : IntOps

    override val zero: Int
        get() = 0

    override val one: Int
        get() = 1

    override fun Int.unaryMinus(): Int {
        return -this
    }

    override fun Int.plus(o: Int): Int {
        return this + o
    }

    override fun Int.minus(o: Int): Int {
        return this - o
    }

    override fun Int.times(o: Int): Int {
        return this * o
    }
}

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
interface DoubleOps : RealOps<Double>, TrigonometryOps<Double> {
    companion object : DoubleOps

    override val zero: Double
        get() = 0.0

    override val one: Double
        get() = 1.0

    override fun Double.unaryMinus(): Double {
        return -this
    }

    override fun Double.plus(o: Double): Double {
        return this + o
    }

    override fun Double.minus(o: Double): Double {
        return this - o
    }

    override fun Double.times(o: Double): Double {
        return this * o
    }

    override fun Double.div(o: Double): Double {
        return this / o
    }

    override fun sqrt(x: Double): Double {
        return kotlin.math.sqrt(x)
    }

    override fun sin(a: Double): Double {
        return kotlin.math.sin(a)
    }

    override fun cos(a: Double): Double {
        return kotlin.math.cos(a)
    }

    override fun atan2(y: Double, x: Double): Double {
        return kotlin.math.atan2(y, x)
    }
}
