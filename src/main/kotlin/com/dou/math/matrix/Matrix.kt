package com.dou.math.matrix

import com.dou.math.N1
import com.dou.math.TypedNumber

interface Matrix<T, @Suppress("unused") M : TypedNumber, @Suppress("unused") N : TypedNumber> {
    val m: Int
    val n: Int
    operator fun get(i: Int, j: Int): T

    val columns: List<Column<T, M>>
    val rows: List<Row<T, N>>

    companion object
}

typealias Column<T, M> = Matrix<T, M, N1>
typealias Row<T, N> = Matrix<T, N1, N>

@JvmName("columnGet")
operator fun <T, M : TypedNumber> Column<T, M>.get(i: Int): T = this[i, 0]

@JvmName("rowGet")
operator fun <T, M : TypedNumber> Row<T, M>.get(j: Int): T = this[0, j]

fun <T, M : TypedNumber, N : TypedNumber> Matrix(m: Int, n: Int, element: (i: Int, j: Int) -> T): Matrix<T, M, N> {
    val values = ArrayList<T>(m * n)
    for (i in 0..<m) {
        for (j in 0..<n) {
            values += element(i, j)
        }
    }
    return SingleListMatrix(m, n, values)
}

abstract class AbstractMatrix<T, M : TypedNumber, N : TypedNumber>(
    override val m: Int,
    override val n: Int
) : Matrix<T, M, N> {
    override fun get(i: Int, j: Int): T {
        if (i !in 0..<m) {
            throw IndexOutOfBoundsException("Index 'i' out of range: $i")
        }
        if (j !in 0..<n) {
            throw IndexOutOfBoundsException("Index 'j' out of range: $j")
        }
        return unsafeGet(i, j)
    }

    protected abstract fun unsafeGet(i: Int, j: Int): T

    override val columns: List<Column<T, M>>
        get() {
            return (0..<n).map { mj ->
                object : AbstractMatrix<T, M, N1>(m, 1) {
                    override fun unsafeGet(i: Int, j: Int): T {
                        return this@AbstractMatrix[i, mj]
                    }
                }
            }
        }

    override val rows: List<Row<T, N>>
        get() {
            return (0..<m).map { mi ->
                object : AbstractMatrix<T, N1, N>(1, n) {
                    override fun unsafeGet(i: Int, j: Int): T {
                        return this@AbstractMatrix[mi, j]
                    }
                }
            }
        }

    override fun hashCode(): Int {
        return hashCode
    }

    private val hashCode: Int by lazy {
        var hash = 0
        for (i in 0..<m) {
            for (j in 0..<n) {
                hash = 31 * hash + this[i, j].hashCode()
            }
        }
        hash
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Matrix<*, *, *> || m != other.m || n != other.n) {
            return false
        }

        if (this is SingleListMatrix && other is SingleListMatrix<*, *, *>) {
            return this.values == other.values
        } else {
            for (i in 0..<m) {
                for (j in 0..<n) {
                    if (this[i, j] != other[i, j]) {
                        return false
                    }
                }
            }
            return true
        }
    }

    override fun toString(): String {
        return buildString {
            val cs = columns.map { c -> c.asIterable().map { it.toString() } }
            val csLength = cs.map { c -> c.maxOf { it.length } }

            fun printCell(i: Int, j: Int) {
                val m = csLength[j]
                val v = cs[j][i]
                for (ignored in 0..<(m - v.length)) {
                    append(" ")
                }
                append(v)
            }

            fun printRow(i: Int) {
                append("| ")
                printCell(i, 0)
                for (j in 1..<n) {
                    append(" ")
                    printCell(i, j)
                }
                append(" |")
            }

            printRow(0)
            for (i in 1..<m) {
                appendLine()
                printRow(i)
            }
            appendLine()
        }
    }
}

private class SingleListMatrix<T, M : TypedNumber, N : TypedNumber>(
    m: Int, n: Int,
    val values: List<T>
) : AbstractMatrix<T, M, N>(m, n) {
    init {
        check(values.size == m * n)
    }

    override fun unsafeGet(i: Int, j: Int): T {
        return values[n * i + j]
    }
}
