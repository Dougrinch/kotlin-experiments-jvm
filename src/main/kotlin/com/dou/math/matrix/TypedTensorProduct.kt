@file:Suppress("UNCHECKED_CAST")

package com.dou.math.matrix

import com.dou.math.*

context(IntegerOps<T>)
@JvmName("x2222")
infix fun <T> Matrix<T, N2, N2>.x(
    b: Matrix<T, N2, N2>
): Matrix<T, N4, N4> = untypedTensorProduct(b) as Matrix<T, N4, N4>

context(IntegerOps<T>)
@JvmName("x4422")
infix fun <T> Matrix<T, N4, N4>.x(
    b: Matrix<T, N2, N2>
): Matrix<T, N8, N8> = untypedTensorProduct(b) as Matrix<T, N8, N8>

context(IntegerOps<T>)
@JvmName("x2244")
infix fun <T> Matrix<T, N2, N2>.x(
    b: Matrix<T, N4, N4>
): Matrix<T, N8, N8> = untypedTensorProduct(b) as Matrix<T, N8, N8>

context(IntegerOps<T>)
@JvmName("x2121")
infix fun <T> Column<T, N2>.x(
    b: Column<T, N2>
): Column<T, N4> = untypedTensorProduct(b) as Column<T, N4>

context(IntegerOps<T>)
@JvmName("x1212")
infix fun <T> Row<T, N2>.x(
    b: Row<T, N2>
): Row<T, N4> = untypedTensorProduct(b) as Row<T, N4>
