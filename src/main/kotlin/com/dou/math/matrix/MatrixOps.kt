package com.dou.math.matrix

import com.dou.math.*

fun <T> Matrix<T, N1, N1>.single(): T = this[0, 0]

@JvmName("columnAsIterable")
fun <T> Column<T, *>.asIterable(): Iterable<T> {
    return object : Iterable<T> {
        override fun iterator(): Iterator<T> {
            return iterator {
                for (i in 0..<m) {
                    yield(get(i))
                }
            }
        }
    }
}

@JvmName("rowAsIterable")
fun <T> Row<T, *>.asIterable(): Iterable<T> {
    return object : Iterable<T> {
        override fun iterator(): Iterator<T> {
            return iterator {
                for (i in 0..<n) {
                    yield(get(i))
                }
            }
        }
    }
}

context(IntegerOps<T>)
operator fun <T, M : TypedNumber, N : TypedNumber> Matrix<T, M, N>.plus(
    b: Matrix<T, M, N>
): Matrix<T, M, N> {
    return Matrix(m, n) { i, j ->
        this[i, j] + b[i, j]
    }
}

context(IntegerOps<T>)
operator fun <T, L : TypedNumber, M : TypedNumber, N : TypedNumber> Matrix<T, L, M>.times(
    b: Matrix<T, M, N>
): Matrix<T, L, N> {
    return Matrix(m, b.n) { i, j ->
        var v = this[i, 0] * b[0, j]
        for (r in 1..<n) {
            v += this[i, r] * b[r, j]
        }
        v
    }
}

context(IntegerOps<T>)
operator fun <T, M : TypedNumber, N : TypedNumber> Matrix<T, M, N>.times(
    t: T
): Matrix<T, M, N> {
    return Matrix(m, n) { i, j ->
        this[i, j] * t
    }
}

context(IntegerOps<T>)
operator fun <T, M : TypedNumber, N : TypedNumber> T.times(
    m: Matrix<T, M, N>
): Matrix<T, M, N> {
    return m * this
}

context(IntegerOps<T>)
fun <T> Matrix<T, *, *>.untypedTensorProduct(
    b: Matrix<T, *, *>
): Matrix<T, *, *> {
    return Matrix<T, Undefined, Undefined>(m * b.m, n * b.n) { i, j ->
        this[i / b.m, j / b.n] * b[i % b.m, j % b.n]
    }
}
