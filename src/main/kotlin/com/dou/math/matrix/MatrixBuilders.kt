@file:Suppress("unused")

package com.dou.math.matrix

import com.dou.math.*

operator fun <T, N : TypedNumber> Matrix.Companion.invoke(
    r1: Row<T, N>
): Matrix<T, N1, N> = unsafeMatrix(r1)

operator fun <T, N : TypedNumber> Matrix.Companion.invoke(
    r1: Row<T, N>, r2: Row<T, N>
): Matrix<T, N2, N> = unsafeMatrix(r1, r2)

operator fun <T, N : TypedNumber> Matrix.Companion.invoke(
    r1: Row<T, N>, r2: Row<T, N>, r3: Row<T, N>
): Matrix<T, N3, N> = unsafeMatrix(r1, r2, r3)

operator fun <T, N : TypedNumber> Matrix.Companion.invoke(
    r1: Row<T, N>, r2: Row<T, N>, r3: Row<T, N>, r4: Row<T, N>
): Matrix<T, N4, N> = unsafeMatrix(r1, r2, r3, r4)

fun <T> row(v1: T): Row<T, N1> = unsafeRow(v1)

fun <T> row(v1: T, v2: T): Row<T, N2> = unsafeRow(v1, v2)

fun <T> row(v1: T, v2: T, v3: T): Row<T, N3> = unsafeRow(v1, v2, v3)

fun <T> row(v1: T, v2: T, v3: T, v4: T): Row<T, N4> = unsafeRow(v1, v2, v3, v4)

fun <T> column(v1: T): Column<T, N1> = unsafeColumn(v1)

fun <T> column(v1: T, v2: T): Column<T, N2> = unsafeColumn(v1, v2)

fun <T> column(v1: T, v2: T, v3: T): Column<T, N3> = unsafeColumn(v1, v2, v3)

fun <T> column(v1: T, v2: T, v3: T, v4: T): Column<T, N4> = unsafeColumn(v1, v2, v3, v4)

fun <T, M : TypedNumber, N : TypedNumber> unsafeMatrix(vararg rs: Row<T, N>): Matrix<T, M, N> {
    return object : AbstractMatrix<T, M, N>(rs.size, rs[0].n) {
        override fun unsafeGet(i: Int, j: Int): T {
            return rs[i][j]
        }

        override val rows: List<Row<T, N>>
            get() = rs.asList()
    }
}

fun <T, N : TypedNumber> unsafeRow(vararg vs: T): Row<T, N> {
    return object : AbstractMatrix<T, N1, N>(1, vs.size) {
        override fun unsafeGet(i: Int, j: Int): T {
            return vs[j]
        }
    }
}

fun <T, M : TypedNumber> unsafeColumn(vararg vs: T): Column<T, M> {
    return object : AbstractMatrix<T, M, N1>(vs.size, 1) {
        override fun unsafeGet(i: Int, j: Int): T {
            return vs[i]
        }
    }
}
