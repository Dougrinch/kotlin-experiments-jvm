@file:Suppress("unused")

package com.dou.math

interface TypedNumber
interface Undefined : TypedNumber
interface Zero : TypedNumber
interface Next<T : TypedNumber> : TypedNumber

typealias N0 = Zero
typealias N1 = Next<Zero>
typealias N2 = Next<N1>
typealias N3 = Next<N2>
typealias N4 = Next<N3>
typealias N5 = Next<N4>
typealias N6 = Next<N5>
typealias N7 = Next<N6>
typealias N8 = Next<N7>
