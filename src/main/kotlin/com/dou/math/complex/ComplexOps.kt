package com.dou.math.complex

import com.dou.math.RealOps

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
interface ComplexOps : RealOps<Complex> {
    companion object : ComplexOps

    override val zero: Complex
        get() = Complex.ZERO

    override val one: Complex
        get() = Complex.ONE

    override fun Complex.unaryMinus(): Complex {
        return -this
    }

    override fun Complex.plus(o: Complex): Complex {
        return this + o
    }

    override fun Complex.minus(o: Complex): Complex {
        return this - o
    }

    override fun Complex.times(o: Complex): Complex {
        return this * o
    }

    override fun Complex.div(o: Complex): Complex {
        return this / o
    }

    override fun sqrt(x: Complex): Complex {
        TODO()
    }
}
