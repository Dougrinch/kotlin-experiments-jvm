package com.dou.math.space

import com.dou.math.*

interface Vector<T, N : TypedNumber, V : Vector<T, N, V>> {
    val n: Int

    operator fun get(i: Int): T

    context(IntegerOps<T>)
    operator fun unaryMinus(): V

    context(IntegerOps<T>)
    operator fun plus(v: V): V

    context(IntegerOps<T>)
    operator fun times(v: V): T

    context(IntegerOps<T>)
    operator fun times(t: T): V

    context(RealOps<T>)
    operator fun div(t: T): V
}

abstract class AbstractVector<T, N : TypedNumber, V : Vector<T, N, V>> : Vector<T, N, V> {
    protected abstract fun vector(element: (Int) -> T): V

    context(IntegerOps<T>)
    override fun unaryMinus(): V {
        return vector { -this[it] }
    }

    context(IntegerOps<T>)
    override fun plus(v: V): V {
        return vector { this[it] + v[it] }
    }

    context(IntegerOps<T>)
    override fun times(v: V): T {
        var result = zero
        for (i in 0 until n) {
            result += this[i] * v[i]
        }
        return result
    }

    context(IntegerOps<T>)
    override fun times(t: T): V {
        return vector { this[it] * t }
    }

    context(RealOps<T>)
    override fun div(t: T): V {
        return vector { this[it] / t }
    }
}

data class Vector2D<T>(val x: T, val y: T) : AbstractVector<T, N2, Vector2D<T>>() {
    override val n: Int
        get() = 2

    override fun vector(element: (Int) -> T): Vector2D<T> {
        return Vector2D(element(0), element(1))
    }

    override fun get(i: Int): T {
        return when (i) {
            0 -> x
            1 -> y
            else -> throw IndexOutOfBoundsException(i)
        }
    }

    override fun toString(): String {
        return "($x, $y)"
    }
}

data class Vector3D<T>(val x: T, val y: T, val z: T) : AbstractVector<T, N3, Vector3D<T>>() {
    override val n: Int
        get() = 3

    override fun vector(element: (Int) -> T): Vector3D<T> {
        return Vector3D(element(0), element(1), element(2))
    }

    override fun get(i: Int): T {
        return when (i) {
            0 -> x
            1 -> y
            2 -> z
            else -> throw IndexOutOfBoundsException(i)
        }
    }

    context(IntegerOps<T>)
    infix fun x(v: Vector3D<T>): Vector3D<T> {
        val rx = y * v.z - z * v.y
        val ry = z * v.x - x * v.z
        val rz = x * v.y - y * v.x
        return Vector3D(rx, ry, rz)
    }

    override fun toString(): String {
        return "($x, $y, $z)"
    }
}

context(IntegerOps<T>)
operator fun <T, N : TypedNumber, V : Vector<T, N, V>> T.times(v: V): V {
    return v * this
}

fun <T, G> Vector3D<T>.map(f: (T) -> G): Vector3D<G> {
    return Vector3D(f(x), f(y), f(z))
}
