package com.dou.math.space

import com.dou.math.*
import com.dou.math.matrix.invoke
import com.dou.math.matrix.row

context(Space2D<T>, IntegerOps<T>)
fun <T> scaleBy(s: T): Transformation<T, N2> {
    return Transformation(
        row(s, zero, zero),
        row(zero, s, zero),
        row(zero, zero, one)
    )
}

context(Space2D<T>, IntegerOps<T>)
fun <T> shiftBy(v: Vector2D<T>): Transformation<T, N2> {
    return Transformation(
        row(one, zero, zero),
        row(zero, one, zero),
        row(v.x, v.y, one)
    )
}

context(Space2D<T>, TrigonometryOps<T>)
fun <T> rotateBy(a: T): Transformation<T, N2> {
    return Transformation(
        row(cos(a), sin(a), zero),
        row(-sin(a), cos(a), zero),
        row(zero, zero, one)
    )
}


context(Space3D<T>, IntegerOps<T>)
fun <T> scaleBy(s: T): Transformation<T, N3> {
    return Transformation(
        row(s, zero, zero, zero),
        row(zero, s, zero, zero),
        row(zero, zero, s, zero),
        row(zero, zero, zero, one)
    )
}

context(Space3D<T>, IntegerOps<T>)
fun <T> shiftBy(v: Vector3D<T>): Transformation<T, N3> {
    return Transformation(
        row(one, zero, zero, zero),
        row(zero, one, zero, zero),
        row(zero, zero, one, zero),
        row(v.x, v.y, v.z, one)
    )
}

context(Space3D<T>, TrigonometryOps<T>)
fun <T> rotateByAround(a: T, around: Vector3D<T>): Transformation<T, N3> {
    return with(around) {
        val cos = cos(a)
        val sin = sin(a)
        val oneMinusCos = one - cos
        val xx = x * x * oneMinusCos
        val xy = x * y * oneMinusCos
        val yx = xy
        val xz = x * z * oneMinusCos
        val zx = xz
        val yy = y * y * oneMinusCos
        val yz = y * z * oneMinusCos
        val zy = yz
        val zz = z * z * oneMinusCos
        val zsin = z * sin
        val ysin = y * sin
        val xsin = x * sin

        Transformation(
            row(xx + cos, xy + zsin, xz - ysin, zero),
            row(yx - zsin, yy + cos, yz + xsin, zero),
            row(zx + ysin, zy - xsin, zz + cos, zero),
            row(zero, zero, zero, one)
        )
    }
}

context(RealOps<T>)
val <T> Vector<T, *, *>.length: T
    get() {
        var length2 = zero
        for (i in 0 until n) {
            length2 += this[i] * this[i]
        }
        return sqrt(length2)
    }

context(Space2D<T>, TrigonometryOps<T>)
val <T> Vector2D<T>.angle: T
    get() {
        return atan2(y, x)
    }
