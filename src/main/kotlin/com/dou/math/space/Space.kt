package com.dou.math.space

import com.dou.math.*
import com.dou.math.matrix.*

typealias Transformation<T, N> = Matrix<T, Next<N>, Next<N>>

interface Space<T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> {

    fun point(element: (Int) -> T): P

    fun vector(element: (Int) -> T): V
}

interface Space2D<T> : Space<T, N2, Point2D<T>, Vector2D<T>> {

    override fun point(element: (Int) -> T): Point2D<T> {
        return Point2D(element(0), element(1))
    }

    override fun vector(element: (Int) -> T): Vector2D<T> {
        return Vector2D(element(0), element(1))
    }

    fun point(x: T, y: T): Point2D<T> {
        return Point2D(x, y)
    }

    fun vector(x: T, y: T): Vector2D<T> {
        return Vector2D(x, y)
    }
}

interface Space3D<T> : Space<T, N3, Point3D<T>, Vector3D<T>> {

    override fun point(element: (Int) -> T): Point3D<T> {
        return Point3D(element(0), element(1), element(2))
    }

    override fun vector(element: (Int) -> T): Vector3D<T> {
        return Vector3D(element(0), element(1), element(2))
    }

    fun point(x: T, y: T, z: T): Point3D<T> {
        return Point3D(x, y, z)
    }

    fun vector(x: T, y: T, z: T): Vector3D<T> {
        return Vector3D(x, y, z)
    }
}


context(Space<T, N, P, V>, IntegerOps<T>)
@Suppress("DuplicatedCode")
fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> P.homogenous(): Row<T, Next<N>> {
    return object : AbstractMatrix<T, N1, Next<N>>(1, n + 1) {
        override fun unsafeGet(i: Int, j: Int): T {
            return when (j) {
                this@homogenous.n -> one
                else -> this@homogenous[j]
            }
        }
    }
}

context(Space<T, N, P, V>, IntegerOps<T>)
@Suppress("DuplicatedCode")
fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> V.homogenous(): Row<T, Next<N>> {
    return object : AbstractMatrix<T, N1, Next<N>>(1, n + 1) {
        override fun unsafeGet(i: Int, j: Int): T {
            return when (j) {
                this@homogenous.n -> zero
                else -> this@homogenous[j]
            }
        }
    }
}

context(Space<T, N, P, V>, IntegerOps<T>)
fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> Row<T, Next<N>>.vector(): V {
    check(this[n - 1] == zero)
    return vector { this[it] }
}

context(Space<T, N, P, V>, IntegerOps<T>)
fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> Row<T, Next<N>>.point(): P {
    check(this[n - 1] == one)
    return point { this[it] }
}

context(Space<T, N, P, V>, RealOps<T>)
fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> Row<T, Next<N>>.point(): P {
    return point { this[it] / this[n - 1] }
}

context(Space<T, N, P, V>, IntegerOps<T>)
operator fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> V.times(
    t: Transformation<T, N>
): V {
    return (homogenous() * t).vector()
}

context(Space<T, N, P, V>, IntegerOps<T>)
operator fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> P.times(
    t: Transformation<T, N>
): P {
    return (homogenous() * t).point()
}

context(Space<T, N, P, V>, RealOps<T>)
operator fun <T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> P.times(
    t: Transformation<T, N>
): P {
    return (homogenous() * t).point()
}
