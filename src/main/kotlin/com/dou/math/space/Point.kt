package com.dou.math.space

import com.dou.math.*

interface Point<T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> {
    val n: Int

    operator fun get(i: Int): T

    context(IntegerOps<T>)
    operator fun plus(v: V): P

    context(IntegerOps<T>)
    operator fun minus(v: V): P

    context(IntegerOps<T>)
    operator fun minus(p: P): V
}


abstract class AbstractPoint<T, N : TypedNumber, P : Point<T, N, P, V>, V : Vector<T, N, V>> : Point<T, N, P, V> {
    protected abstract fun point(element: (Int) -> T): P
    protected abstract fun vector(element: (Int) -> T): V

    context(IntegerOps<T>)
    override fun plus(v: V): P {
        return point { this[it] + v[it] }
    }

    context(IntegerOps<T>)
    override fun minus(v: V): P {
        return point { this[it] - v[it] }
    }

    context(IntegerOps<T>)
    override fun minus(p: P): V {
        return vector { this[it] - p[it] }
    }
}

data class Point2D<T>(val x: T, val y: T) : AbstractPoint<T, N2, Point2D<T>, Vector2D<T>>() {

    override val n: Int
        get() = 2

    override fun point(element: (Int) -> T): Point2D<T> {
        return Point2D(element(0), element(1))
    }

    override fun vector(element: (Int) -> T): Vector2D<T> {
        return Vector2D(element(0), element(1))
    }

    override fun get(i: Int): T {
        return when (i) {
            0 -> x
            1 -> y
            else -> throw IndexOutOfBoundsException(i)
        }
    }

    override fun toString(): String {
        return "($x, $y)"
    }
}

data class Point3D<T>(val x: T, val y: T, val z: T) : AbstractPoint<T, N3, Point3D<T>, Vector3D<T>>() {

    override val n: Int
        get() = 3

    override fun point(element: (Int) -> T): Point3D<T> {
        return Point3D(element(0), element(1), element(2))
    }

    override fun vector(element: (Int) -> T): Vector3D<T> {
        return Vector3D(element(0), element(1), element(2))
    }

    override fun get(i: Int): T {
        return when (i) {
            0 -> x
            1 -> y
            2 -> z
            else -> throw IndexOutOfBoundsException(i)
        }
    }

    override fun toString(): String {
        return "($x, $y, $z)"
    }
}

fun <T, G> Point3D<T>.map(f: (T) -> G): Point3D<G> {
    return Point3D(f(x), f(y), f(z))
}
