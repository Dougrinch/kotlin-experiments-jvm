package com.dou.math.space

import com.dou.math.matrix.times
import com.dou.math.scalar.Scalar
import com.dou.math.scalar.ScalarOps

fun main() = with(object : Space2D<Scalar>, ScalarOps {}) {
    val p1 = Point2D(Scalar(2.0), Scalar(1.0))
    val p2 = Point2D(Scalar(1.0), Scalar(2.0))

    val p3 = Point2D(Scalar(3.0), Scalar(1.0))
    val p4 = Point2D(Scalar(5.0), Scalar(3.0))

    val p12 = p1 + (p2 - p1) / Scalar(2.0)
    val p34 = p3 + (p4 - p3) / Scalar(2.0)

    val t1 = shiftBy(point { zero } - p12)
    val t2 = shiftBy(p34 - point { zero })
    val s = scaleBy((p4 - p3).length / (p2 - p1).length)
    val r = rotateBy((p4 - p3).angle - (p2 - p1).angle)

    val f = t1 * r * s * t2

    println(p1 * f)
    println(p2 * f)
}
