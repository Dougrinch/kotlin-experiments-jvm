@file:Suppress("unused")

package com.dou.math

import com.dou.math.space.*

typealias IntPoint2D = Point2D<Int>
typealias IntPoint3D = Point3D<Int>

typealias IntVector2D = Vector2D<Int>
typealias IntVector3D = Vector3D<Int>


typealias LongPoint2D = Point2D<Long>
typealias LongPoint3D = Point3D<Long>

typealias LongVector2D = Vector2D<Long>
typealias LongVector3D = Vector3D<Long>


typealias DoublePoint2D = Point2D<Double>
typealias DoublePoint3D = Point3D<Double>

typealias DoubleVector2D = Vector2D<Double>
typealias DoubleVector3D = Vector3D<Double>
