package com.dou.math.scalar

operator fun Scalar.div(n: Double): Scalar = div(Scalar(n))

sealed class Scalar(protected val composite: Boolean, protected val priority: Int) {
    companion object {
        val Zero: Scalar = Number(0.0)
        val One: Scalar = Number(1.0)

        operator fun invoke(number: Double): Scalar = Number(number)
        operator fun invoke(symbol: String): Scalar = Symbol(symbol)

        fun sin(a: Scalar): Scalar {
            if (a is Number) {
                return Number(kotlin.math.sin(a.v))
            }
            return Function("sin", listOf(a))
        }

        fun cos(a: Scalar): Scalar {
            if (a is Number) {
                return Number(kotlin.math.cos(a.v))
            }
            return Function("cos", listOf(a))
        }

        fun sqrt(a: Scalar): Scalar {
            if (a is Number) {
                return Number(kotlin.math.sqrt(a.v))
            }
            return Function("sqrt", listOf(a))
        }

        fun atan2(y: Scalar, x: Scalar): Scalar {
            return if (y is Number && x is Number) {
                Number(kotlin.math.atan2(y.v, x.v))
            } else {
                Function("atan2", listOf(y, x))
            }
        }
    }

    open operator fun plus(o: Scalar): Scalar {
        if (o == Zero) {
            return this
        }
        return Plus(this, o)
    }

    open operator fun minus(o: Scalar): Scalar {
        if (o == Zero) {
            return this
        }
        return Minus(this, o)
    }

    open operator fun times(o: Scalar): Scalar {
        if (o == Zero) {
            return Zero
        }
        if (o == One) {
            return this
        }
        if (o == -One) {
            return -this
        }
        return Times(this, o)
    }

    open operator fun div(o: Scalar): Scalar {
        if (o == One) {
            return this
        }
        return Div(this, o)
    }

    open operator fun unaryMinus(): Scalar {
        return UnaryMinus(this)
    }

    private data class Number(val v: Double) : Scalar(false, 0) {
        override fun plus(o: Scalar): Scalar {
            if (o is Number) {
                return Number(v + o.v)
            }
            return super.plus(o)
        }

        override fun minus(o: Scalar): Scalar {
            if (o is Number) {
                return Number(v - o.v)
            }
            return super.plus(o)
        }

        override fun times(o: Scalar): Scalar {
            if (o is Number) {
                return Number(v * o.v)
            }
            return super.times(o)
        }

        override fun div(o: Scalar): Scalar {
            if (o is Number) {
                return Number(v / o.v)
            }
            return super.times(o)
        }

        override fun unaryMinus(): Scalar {
            return Number(-v)
        }

        override fun toString(): String {
            return "$v"
        }
    }

    private data class Symbol(val s: String) : Scalar(false, 0) {
        override fun toString(): String {
            return s
        }
    }

    private abstract class InfixBinaryOperation(
        open val a: Scalar, open val b: Scalar, val name: String, priority: Int
    ) : Scalar(true, priority) {

        override fun toString(): String {
            val a = if (a.composite && a.priority < priority) "($a)" else "$a"
            val b = if (b.composite && b.priority < priority) "($b)" else "$b"
            return "$a $name $b"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as InfixBinaryOperation

            if (a != other.a) return false
            if (b != other.b) return false
            if (name != other.name) return false

            return true
        }

        override fun hashCode(): Int {
            var result = a.hashCode()
            result = 31 * result + b.hashCode()
            result = 31 * result + name.hashCode()
            return result
        }
    }

    private class Plus(a: Scalar, b: Scalar) : InfixBinaryOperation(a, b, "+", 1)
    private class Minus(a: Scalar, b: Scalar) : InfixBinaryOperation(a, b, "-", 1)

    private class Times(a: Scalar, b: Scalar) : InfixBinaryOperation(a, b, "*", 2)
    private class Div(a: Scalar, b: Scalar) : InfixBinaryOperation(a, b, "/", 2)

    private data class Function(
        val name: String, val params: List<Scalar>
    ) : Scalar(false, 3) {
        override fun toString(): String {
            return params.joinToString(prefix = "$name(", postfix = ")", separator = ", ") { "$it" }
        }
    }

    private data class UnaryMinus(val a: Scalar) : Scalar(false, 4) {
        override fun toString(): String {
            val a = if (a.composite && a.priority < priority) "($a)" else "$a"
            return "-$a"
        }
    }
}
