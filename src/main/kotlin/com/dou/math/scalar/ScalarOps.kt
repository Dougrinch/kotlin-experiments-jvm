package com.dou.math.scalar

import com.dou.math.RealOps
import com.dou.math.TrigonometryOps

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
interface ScalarOps : RealOps<Scalar>, TrigonometryOps<Scalar> {
    companion object : ScalarOps

    override val zero: Scalar
        get() = Scalar.Zero

    override val one: Scalar
        get() = Scalar.One

    override fun Scalar.unaryMinus(): Scalar {
        return -this
    }

    override fun Scalar.plus(o: Scalar): Scalar {
        return this + o
    }

    override fun Scalar.minus(o: Scalar): Scalar {
        return this - o
    }

    override fun Scalar.times(o: Scalar): Scalar {
        return this * o
    }

    override fun Scalar.div(o: Scalar): Scalar {
        return this / o
    }

    override fun sqrt(x: Scalar): Scalar {
        return Scalar.sqrt(x)
    }

    override fun sin(a: Scalar): Scalar {
        return Scalar.sin(a)
    }

    override fun cos(a: Scalar): Scalar {
        return Scalar.cos(a)
    }

    override fun atan2(y: Scalar, x: Scalar): Scalar {
        return Scalar.atan2(y, x)
    }
}
