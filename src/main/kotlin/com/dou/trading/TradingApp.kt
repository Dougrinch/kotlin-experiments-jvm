package com.dou.trading

import kotlin.math.abs

fun main() {
    printQtyPerEP(
        listOf(0.89, 0.88, 0.87),
        0.83,
        50.0
    )
}

fun printQtyPerEP(eps: List<Double>, sl: Double, risk: Double) {
    println(getQty(eps, sl, risk) / eps.count())
}

fun getQty(eps: List<Double>, sl: Double, risk: Double): Double {
    val averageEp = eps.average()
    return risk / abs(averageEp - sl)
}
