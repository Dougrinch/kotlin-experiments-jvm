package com.dou.fp.e10

import arrow.core.Either
import com.dou.fp.e3.List
import com.dou.fp.e3.Tree
import com.dou.fp.e4.Option
import com.dou.fp.e5.Stream
import com.dou.fp.e6.State

interface Kind<out F, out A>


class ForList private constructor()
typealias ListOf<A> = Kind<ForList, A>

fun <A> ListOf<A>.fix() = this as List<A>


class ForTree private constructor()
typealias TreeOf<A> = Kind<ForTree, A>

fun <A> TreeOf<A>.fix() = this as Tree<A>


class ForOption private constructor()
typealias OptionOf<A> = Kind<ForOption, A>

fun <A> OptionOf<A>.fix() = this as Option<A>


class ForStream private constructor()
typealias StreamOf<A> = Kind<ForStream, A>

fun <A> StreamOf<A>.fix() = this as Stream<A>


class ForState<S> private constructor()
typealias StateOf<S, A> = Kind<ForState<S>, A>

fun <S, A> StateOf<S, A>.fix() = this as State<S, A>


class ForEither<E> private constructor()
data class EitherOf<E, A>(val e: Either<E, A>) : Kind<ForEither<E>, A>

fun <E, A> Kind<ForEither<E>, A>.fix(): Either<E, A> = (this as EitherOf<E, A>).e
fun <E, A> Either<E, A>.kind(): EitherOf<E, A> = EitherOf(this)
