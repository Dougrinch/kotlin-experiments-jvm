package com.dou.fp.e10

import com.dou.fp.e3.*
import com.dou.fp.e3.List
import com.dou.fp.e4.None
import com.dou.fp.e4.Some

interface Foldable<F> {
    fun <A, B> foldRight(fa: Kind<F, A>, z: B, f: (A, B) -> B): B =
        foldMap(fa, endoMonoid<B>()) { a -> { b -> f(a, b) } }.invoke(z)

    fun <A, B> foldLeft(fa: Kind<F, A>, z: B, f: (B, A) -> B): B =
        foldMap(fa, dual(endoMonoid<B>())) { a -> { b -> f(b, a) } }.invoke(z)

    fun <A, B> foldMap(fa: Kind<F, A>, m: Monoid<B>, f: (A) -> B): B =
        foldLeft(fa, m.nil) { b, a -> m.combine(b, f(a)) }
}

fun <F, A> Foldable<F>.concatenate(fa: Kind<F, A>, m: Monoid<A>): A = foldMap(fa, m) { it }
fun <F, A> Foldable<F>.toList(fa: Kind<F, A>): List<A> = foldLeft(fa, List()) { t, h -> Cons(h, t) }

object ListFoldable : Foldable<ForList> {
    override fun <A, B> foldRight(fa: ListOf<A>, z: B, f: (A, B) -> B): B {
        return fa.fix().foldRight(z, f)
    }

    override fun <A, B> foldLeft(fa: ListOf<A>, z: B, f: (B, A) -> B): B {
        return fa.fix().foldLeft(z, f)
    }
}

object TreeFoldable : Foldable<ForTree> {
    override fun <A, B> foldMap(fa: TreeOf<A>, m: Monoid<B>, f: (A) -> B): B {
        return fa.fix().fold(f, m::combine)
    }
}

object OptionFoldable : Foldable<ForOption> {
    override fun <A, B> foldMap(fa: OptionOf<A>, m: Monoid<B>, f: (A) -> B): B {
        return when (val o = fa.fix()) {
            is None -> m.nil
            is Some -> f(o.value)
        }
    }
}

fun main() {
    ListFoldable.concatenate(List("1", "2", "3"), stringMonoid).let { println(it) }
    TreeFoldable.concatenate(Branch(Leaf("1"), Branch(Leaf("2"), Leaf("3"))), stringMonoid).let { println(it) }
    TreeFoldable.toList(Branch(Leaf("1"), Branch(Leaf("2"), Leaf("3")))).let { println(it) }
}
