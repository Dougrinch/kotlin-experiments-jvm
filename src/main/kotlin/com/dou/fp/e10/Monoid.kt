@file:Suppress("unused")

package com.dou.fp.e10

import arrow.core.*
import com.dou.fp.e3.foldLeft
import com.dou.fp.e3.foldRight
import com.dou.fp.e7.*
import com.dou.fp.e8.*
import java.util.concurrent.Executors
import kotlin.collections.List
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.component3
import com.dou.fp.e3.List as FList

interface Semigroup<A> {
    fun combine(a1: A, a2: A): A
}

interface Monoid<A> : Semigroup<A> {
    val nil: A
}

val stringMonoid: Monoid<String> = object : Monoid<String> {
    override fun combine(a1: String, a2: String): String = a1 + a2
    override val nil: String = ""
}

fun <T> listMonoid(): Monoid<List<T>> = object : Monoid<List<T>> {
    override fun combine(a1: List<T>, a2: List<T>): List<T> = a1 + a2
    override val nil: List<T> = emptyList()
}

fun intAddition(): Monoid<Int> = object : Monoid<Int> {
    override fun combine(a1: Int, a2: Int): Int = a1 + a2
    override val nil: Int = 0
}

fun intMultiplication(): Monoid<Int> = object : Monoid<Int> {
    override fun combine(a1: Int, a2: Int): Int = a1 * a2
    override val nil: Int = 1
}

fun booleanOr(): Monoid<Boolean> = object : Monoid<Boolean> {
    override fun combine(a1: Boolean, a2: Boolean): Boolean = a1 || a2
    override val nil: Boolean = false
}

fun booleanAnd(): Monoid<Boolean> = object : Monoid<Boolean> {
    override fun combine(a1: Boolean, a2: Boolean): Boolean = a1 && a2
    override val nil: Boolean = true
}

fun <A> optionMonoid(): Monoid<Option<A>> = object : Monoid<Option<A>> {
    override fun combine(a1: Option<A>, a2: Option<A>): Option<A> = a1.recover { a2.bind() }
    override val nil: Option<A> = None
}

fun <A> dual(m: Monoid<A>): Monoid<A> = object : Monoid<A> {
    override fun combine(a1: A, a2: A): A = m.combine(a2, a1)
    override val nil: A = m.nil
}

fun <A> endoMonoid(): Monoid<(A) -> A> = object : Monoid<(A) -> A> {
    override fun combine(a1: (A) -> A, a2: (A) -> A): (A) -> A = { a2(a1(it)) }
    override val nil: (A) -> A = { it }
}

fun main() {
    val plusFive = { x: Int -> x + 5 }
    val mulTwo = { x: Int -> x * 2 }

    println(endoMonoid<Int>().combine(plusFive, mulTwo)(10))
    println(endoMonoid<Int>().combine(mulTwo, plusFive)(10))

    run(monoidLaws(intAddition(), Gen.int()))
    run(monoidLaws(dual(stringMonoid), Gen.word()))

    println(listOf("1", "2", "3").flattenWith(stringMonoid))

    println(foldMapLeft(FList(1, 2, 3, 4), stringMonoid) { "$it" })
    println(foldRight(FList(1, 2, 3, 4), "0") { i, s -> "$i$s" })
    println(foldLeft(FList(1, 2, 3, 4), "0") { s, i -> "$s$i" })

    println(foldMap(listOf(1, 2, 3, 4), stringMonoid) { "$it" })

    val es = Executors.newSingleThreadExecutor()
    try {
        val p = parFoldMap(listOf(1, 2, 3, 4), stringMonoid) { "$it" }
        println(run(es, p))
    } finally {
        es.shutdown()
    }

    println(wordCount("lorem ipsum dolor sit amet, "))

    val fm = functionMonoid<String, Int>(intMultiplication())
    println(fm.combine({ it.length - 2 }, { it.length + 2 })("ivan"))
}

fun <A> monoidLaws(m: Monoid<A>, gen: Gen<A>): Prop =
    forAll(Gen.listOfN(3, gen)) { (a, b, c) ->
        m.combine(m.combine(a, b), c) == m.combine(a, m.combine(b, c))
    }.merge(forAll(gen) { a ->
        m.combine(a, m.nil) == a && m.combine(m.nil, a) == a
    })


fun <A, M : Monoid<A>> List<A>.flattenWith(m: M): A = fold(m.nil, m::combine)


fun <A, B> foldMapLeft(la: FList<A>, m: Monoid<B>, f: (A) -> B): B = la.foldLeft(m.nil) { b, a -> m.combine(b, f(a)) }
fun <A, B> foldMapRight(la: FList<A>, m: Monoid<B>, f: (A) -> B): B = la.foldRight(m.nil) { a, b -> m.combine(f(a), b) }

fun <A, B> foldRight(la: FList<A>, z: B, f: (A, B) -> B): B {
    return foldMapLeft(
        la,
        endoMonoid<B>()
    ) { a -> { b -> f(a, b) } }
        .invoke(z)
}

fun <A, B> foldLeft(la: FList<A>, z: B, f: (B, A) -> B): B {
    return foldMapLeft(
        la,
        endoMonoid<B>()
    ) { a -> { b -> f(b, a) } }
        .invoke(z)
}

fun <A, B> foldMap(la: List<A>, m: Monoid<B>, f: (A) -> B): B = when (la.size) {
    0 -> m.nil
    1 -> f(la[0])
    2 -> m.combine(f(la[0]), f(la[1]))
    else -> {
        val (l, r) = la.splitAt(la.size / 2)
        m.combine(foldMap(l, m, f), foldMap(r, m, f))
    }
}

fun <A, B> productMonoid(ma: Monoid<A>, mb: Monoid<B>): Monoid<Pair<A, B>> = object : Monoid<Pair<A, B>> {
    override val nil: Pair<A, B> = ma.nil to mb.nil
    override fun combine(a1: Pair<A, B>, a2: Pair<A, B>): Pair<A, B> =
        ma.combine(a1.first, a2.first) to mb.combine(a1.second, a2.second)
}

fun <K, V> mapMergeMonoid(mv: Monoid<V>): Monoid<Map<K, V>> = object : Monoid<Map<K, V>> {
    override val nil: Map<K, V> = emptyMap()

    override fun combine(a1: Map<K, V>, a2: Map<K, V>): Map<K, V> {
        val result = HashMap<K, V>(a1)
        for ((k, v2) in a2) {
            result.compute(k) { _, v1 ->
                if (v1 != null) {
                    mv.combine(v1, v2)
                } else {
                    v2
                }
            }
        }
        return result
    }
}

val m: Monoid<Map<String, Map<String, Int>>> = mapMergeMonoid(mapMergeMonoid(intAddition()))

fun <A, B> functionMonoid(mb: Monoid<B>): Monoid<(A) -> B> = object : Monoid<(A) -> B> {
    override val nil: (A) -> B = { mb.nil }

    override fun combine(a1: (A) -> B, a2: (A) -> B): (A) -> B {
        return { a -> mb.combine(a1(a), a2(a)) }
    }
}

fun <A> par(m: Monoid<A>): Monoid<Par<A>> = object : Monoid<Par<A>> {
    override val nil: Par<A> = unit(m.nil)
    override fun combine(a1: Par<A>, a2: Par<A>): Par<A> = flatMap(a1) { pa1 -> map(a2) { pa2 -> m.combine(pa1, pa2) } }
}

fun <A, B> parFoldMap(la: List<A>, m: Monoid<B>, f: (A) -> B): Par<B> {
    return when (la.size) {
        0 -> unit(m.nil)
        1 -> unit(f(la.single()))
        2 -> par(m).combine(unit(f(la[0])), unit(f(la[1])))
        else -> {
            val (l, r) = la.splitAt(la.size / 2)
            par(m).combine(fork { parFoldMap(l, m, f) }, fork { parFoldMap(r, m, f) })
        }
    }
}

fun <A> bag(la: List<A>): Map<A, Int> = la.map { mapOf(it to 1) }.flattenWith(mapMergeMonoid(intAddition()))


fun wordCount(str: String): Int = when (val wc = str.chunked(3).map { Stub(it) }.flattenWith(wcMonoid())) {
    is Stub -> throw IllegalStateException()
    is Part -> wc.words
}

sealed class WC

data class Stub(val chars: String) : WC()
data class Part(val ls: String, val words: Int, val rs: String) : WC()

fun wcMonoid(): Monoid<WC> = object : Monoid<WC> {
    override fun combine(a1: WC, a2: WC): WC {
        return when (a1) {
            is Stub -> when (a2) {
                is Stub -> {
                    val (l, w, r) = allWords(a1.chars + a2.chars)
                    Part(l, w, r)
                }
                is Part -> {
                    val (l, lw) = leftWords(a1.chars + a2.ls)
                    val (rw, r) = rightWords(a2.rs)
                    val w = a2.words + lw + rw
                    Part(l, w, r)
                }
            }
            is Part -> when (a2) {
                is Stub -> {
                    val (l, lw) = leftWords(a1.ls)
                    val (rw, r) = rightWords(a1.rs + a2.chars)
                    val w = a1.words + lw + rw
                    Part(l, w, r)
                }
                is Part -> {
                    val (l, lw) = leftWords(a1.ls)
                    val mw = words(a1.rs + a2.ls)
                    val (rw, r) = rightWords(a2.rs)
                    val w = a1.words + a2.words + lw + mw + rw
                    Part(l, w, r)
                }
            }
        }
    }

    private fun allWords(str: String): Triple<String, Int, String> {
        val first = str.indexOf(" ")
        val last = str.lastIndexOf(" ")
        if (first == -1) {
            return Triple("", 0, str)
        }
        return Triple(str.substring(0, first), words(str.substring(first, last)), str.substring(last, str.length))
    }

    private fun leftWords(str: String): Pair<String, Int> {
        val first = str.indexOf(" ")
        if (first == -1) {
            return str to 0
        }
        return str.substring(0, first) to words(str.substring(first, str.length))
    }

    private fun rightWords(str: String): Pair<Int, String> {
        val last = str.lastIndexOf(" ")
        if (last == -1) {
            return 0 to str
        }
        return words(str.substring(0, last)) to str.substring(last, str.length)
    }

    private fun words(str: String): Int {
        return str.split(" ").count { it.isNotBlank() }
    }

    override val nil: WC = Stub("")
}














