package com.dou.fp.e10

import arrow.core.Either
import arrow.core.left
import arrow.core.right

interface Functor<F> {
    fun <A, B> Kind<F, A>.map(f: (A) -> B): Kind<F, B>


    fun <A, B> unzip(f: Kind<F, Pair<A, B>>): Pair<Kind<F, A>, Kind<F, B>> =
        f.map { it.first } to f.map { it.second }

    fun <A, B> join(f: Either<Kind<F, A>, Kind<F, B>>): Kind<F, Either<A, B>> =
        when (f) {
            is Either.Left -> f.value.map { it.left() }
            is Either.Right -> f.value.map { it.right() }
        }
}
