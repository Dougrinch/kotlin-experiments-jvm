package com.dou.fp.e12

import com.dou.fp.e10.Functor
import com.dou.fp.e10.Kind

interface Traversable<F> : Functor<F> {
    fun <A, G, B> traverse(
        fa: Kind<F, A>,
        ag: Applicative<G>,
        f: (A) -> Kind<G, B>
    ): Kind<G, Kind<F, B>> {
        return sequence(fa.map(f), ag)
    }

    fun <A, G> sequence(
        fga: Kind<F, Kind<G, A>>,
        ag: Applicative<G>
    ): Kind<G, Kind<F, A>> {
        return traverse(fga, ag) { it }
    }
}
