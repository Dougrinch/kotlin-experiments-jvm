@file:Suppress("UNUSED_ANONYMOUS_PARAMETER", "UNREACHABLE_CODE")

package com.dou.fp.e12

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.dou.fp.e10.*
import com.dou.fp.e5.Stream
import com.dou.fp.e5.forEach
import com.dou.fp.e5.zipWith

object StreamApplicative : Applicative<ForStream> {
    override fun <A> unit(a: A): Kind<ForStream, A> {
        return Stream.continually(a)
    }

    override fun <A, B> apply(fab: Kind<ForStream, (A) -> B>, fa: Kind<ForStream, A>): Kind<ForStream, B> {
        return fab.fix().zipWith(fa.fix()) { ab, a -> ab(a) }
    }
}


fun <E> eitherApplicative(errorMerge: (E, E) -> E): Applicative<ForEither<E>> = object : Applicative<ForEither<E>> {
    override fun <A> unit(a: A): EitherOf<E, A> {
        return a.right().kind()
    }

    override fun <A, B, C> map2(
        fa: Kind<ForEither<E>, A>, fb: Kind<ForEither<E>, B>, f: (A, B) -> C
    ): Kind<ForEither<E>, C> {
        return when (val ea = fa.fix()) {
            is Either.Left -> {
                val e1 = ea.value
                when (val eb = fb.fix()) {
                    is Either.Left -> {
                        val e2 = eb.value
                        errorMerge(e1, e2).left().kind()
                    }
                    is Either.Right -> {
                        e1.left().kind()
                    }
                }
            }
            is Either.Right -> {
                val a = ea.value
                when (val eb = fb.fix()) {
                    is Either.Left -> {
                        eb.value.left().kind()
                    }
                    is Either.Right -> {
                        val b = eb.value
                        unit(f(a, b))
                    }
                }
            }
        }
    }
}


fun main() {
    with(StreamApplicative) {
        Stream.of(1, 2, 3).map { it + 1 }.fix().forEach { println(it) }
    }

    with(eitherApplicative<String> { e1, e2 -> "$e1;$e2" }) {
        val e1 = "error1".left().kind()
        val e2 = "error2".left().kind()
        val e3 = "error3".left().kind()
        val a = map3(e1, e2, e3) { s1, s2, s3 -> "$s1$s2$s3" }.fix()
        println(a)
    }
}
