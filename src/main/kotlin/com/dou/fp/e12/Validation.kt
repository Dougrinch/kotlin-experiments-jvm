package com.dou.fp.e12

import com.dou.fp.e10.Kind
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import com.dou.fp.e3.List.Companion as FList

class ForValidation<out E> private constructor()
typealias ValidationOf<E, A> = Kind<ForValidation<E>, A>

fun <E, A> ValidationOf<E, A>.fix() = this as Validation<E, A>

sealed class Validation<out E, out A> : ValidationOf<E, A>

data class Failure<E>(val errors: List<E>) : Validation<E, Nothing>() {
    constructor(error: E) : this(listOf(error))
}

data class Success<A>(val value: A) : Validation<Nothing, A>()

fun <E> validationApplicative(): Applicative<ForValidation<E>> = object : Applicative<ForValidation<E>> {
    override fun <A> unit(a: A): Kind<ForValidation<E>, A> {
        return Success(a)
    }

    override fun <A, B, C> map2(
        fa: Kind<ForValidation<E>, A>, fb: Kind<ForValidation<E>, B>, f: (A, B) -> C
    ): Kind<ForValidation<E>, C> {
        return when (val ea = fa.fix()) {
            is Failure -> {
                when (val eb = fb.fix()) {
                    is Failure -> Failure(ea.errors + eb.errors)
                    is Success -> ea
                }
            }
            is Success -> {
                when (val eb = fb.fix()) {
                    is Failure -> eb
                    is Success -> unit(f(ea.value, eb.value))
                }
            }
        }
    }
}

fun main() {
    println(validatedWebForm("Ivan", "2014-02-17", "9111234567"))
    println(validatedWebForm("", "2014-02-17", "9111234567"))
    println(validatedWebForm("Ivan", "2014-17", "9111234567"))
    println(validatedWebForm("", "2014-17", "911123457"))

    with(validationApplicative<String>()) {
        val a = FList(Failure("e1"), Success(18), Failure("e2"))
        println(a)
        println(a.flatten())

        val b = mapOf("1" to Failure("e1"), "2" to Success(18), "3" to Success(10))
        println(b)
        println(b.flatten())
    }
}

fun validatedWebForm(
    name: String,
    dob: String,
    phone: String
): Validation<String, WebForm> = with(validationApplicative<String>()) {
    val result = map3(
        validName(name),
        validDateOfBirth(dob),
        validPhone(phone)
    ) { n, d, p -> WebForm(n, d, p) }
    return result.fix()
}

data class WebForm(val name: String, val dateOfBirthday: LocalDate, val phone: String)

fun validName(name: String): Validation<String, String> =
    if (name != "") Success(name)
    else Failure("Name cannot be empty")

fun validDateOfBirth(dob: String): Validation<String, LocalDate> =
    try {
        Success(LocalDate.parse(dob, DateTimeFormatter.ofPattern("yyyy-MM-dd")))
    } catch (e: Exception) {
        Failure("Date of birth must be in format yyyy-MM-dd")
    }

fun validPhone(phone: String): Validation<String, String> =
    if (phone.matches("[0-9]{10}".toRegex())) Success(phone)
    else Failure("Phone number must be 10 digits")
