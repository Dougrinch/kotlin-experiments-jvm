package com.dou.fp.e12

import arrow.core.*
import com.dou.fp.e10.Functor
import com.dou.fp.e10.Kind
import com.dou.fp.e3.Cons
import com.dou.fp.e3.List
import com.dou.fp.e3.foldRightL
import com.dou.fp.e5.Stream
import com.dou.fp.e5.constant
import com.dou.fp.e5.take
import com.dou.fp.e5.toList

interface Applicative<F> : Functor<F> {
    fun <A> unit(a: A): Kind<F, A>

    fun <A, B> apply(fab: Kind<F, (A) -> B>, fa: Kind<F, A>): Kind<F, B> {
        return map2(fab, fa) { f, a -> f(a) }
    }

    fun <A, B, C> map2(fa: Kind<F, A>, fb: Kind<F, B>, f: (A, B) -> C): Kind<F, C> {
        return apply(apply(unit(f.curried()), fa), fb)
    }

    fun <A, B, C, D> map3(fa: Kind<F, A>, fb: Kind<F, B>, fc: Kind<F, C>, f: (A, B, C) -> D): Kind<F, D> {
        return apply(apply(apply(unit(f.curried()), fa), fb), fc)
    }

    override fun <A, B> Kind<F, A>.map(f: (A) -> B): Kind<F, B> = apply(unit(f), this)

    fun <A, B> List<A>.traverse(f: (A) -> Kind<F, B>): Kind<F, List<B>> {
        return foldRightL(unit(List.empty())) { a, b -> map2(f(a), b, ::Cons) }
    }

    fun <A> List<Kind<F, A>>.flatten(): Kind<F, List<A>> = traverse { it }

    fun <K, A> Map<K, Kind<F, A>>.flatten(): Kind<F, Map<K, A>> {
        return fold(unit(emptyMap())) { facc, (k, fa) ->
            map2(facc, fa) { acc, a ->
                acc + (k to a)
            }
        }
    }

    fun <A> Kind<F, A>.repeat(n: Int): Kind<F, List<A>> = Stream.constant(this).take(n).toList().flatten()

    fun <A, B> join(fa: Kind<F, A>, fb: Kind<F, B>): Kind<F, Pair<A, B>> = map2(fa, fb) { a, b -> a to b }
}
