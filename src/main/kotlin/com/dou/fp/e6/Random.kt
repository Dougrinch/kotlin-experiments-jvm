package com.dou.fp.e6

import com.dou.fp.e3.*
import com.dou.fp.e3.Cons
import com.dou.fp.e3.List
import com.dou.fp.e3.List.Companion.empty
import com.dou.fp.e4.Option.Companion.some
import com.dou.fp.e5.*
import com.dou.fp.e6.State.Companion.map2

interface RNG {
    fun nextInt(): Pair<Int, RNG>
}

data class SimpleRNG(val seed: Long) : RNG {
    override fun nextInt(): Pair<Int, RNG> {
        val newSeed = (seed * 0x5DEECE66DL + 0xBL) and 0xFFFFFFFFFFFFL
        val nextRNG = SimpleRNG(newSeed)
        val n = (newSeed ushr 16).toInt()
        return Pair(n, nextRNG)
    }
}

tailrec fun RNG.nextNonNegativeInt(): Pair<Int, RNG> {
    val (r, rng) = nextInt()
    return when {
        r > 0 -> Pair(r, rng)
        r == Int.MIN_VALUE -> rng.nextNonNegativeInt()
        else -> Pair(-r, rng)
    }
}

fun RNG.nextDouble(): Pair<Double, RNG> {
    val (i, rng) = nextNonNegativeInt()
    return i.toDouble() / (Int.MAX_VALUE.toDouble() + 1) to rng
}

fun RNG.nextIntDouble(): Pair<Pair<Int, Double>, RNG> {
    val (i, rng1) = this.nextInt()
    val (d, rng2) = rng1.nextDouble()
    return (i to d) to rng2
}

fun RNG.nextDoubleInt(): Pair<Pair<Double, Int>, RNG> {
    val (d, rng1) = this.nextDouble()
    val (i, rng2) = rng1.nextInt()
    return (d to i) to rng2
}

fun RNG.nextDouble3(): Pair<Triple<Double, Double, Double>, RNG> {
    val (d1, rng1) = this.nextDouble()
    val (d2, rng2) = rng1.nextDouble()
    val (d3, rng3) = rng2.nextDouble()
    return Triple(d1, d2, d3) to rng3
}

fun RNG.nextInts(count: Int): Pair<List<Int>, RNG> {
    val (reversed, rng) = Stream.unfold(this) { rng ->
        val (v, nextRng) = rng.nextInt()
        some((v to nextRng) to nextRng)
    }.take(count).toList().foldLeft(Pair(empty<Int>(), this)) { (tail, _), (head, rng) ->
        Pair(Cons(head, tail), rng)
    }
    return reversed.reverse() to rng
}

typealias Rand<A> = State<RNG, A>

fun <A, B> both(ra: Rand<A>, rb: Rand<B>): Rand<Pair<A, B>> = map2(ra, rb) { a, b -> a to b }

fun <A> unit(a: A): Rand<A> = Rand { a to it }
val intR: Rand<Int> = Rand(RNG::nextInt)
val nonNegativeInt: Rand<Int> = Rand(RNG::nextNonNegativeInt)
val nonNegativeEven: Rand<Int> = nonNegativeInt.map { it - (it % 2) }
val doubleR: Rand<Double> = nonNegativeInt.map { it / (Int.MAX_VALUE.toDouble() + 1) }
val intDoubleR: Rand<Pair<Int, Double>> = both(intR, doubleR)
val doubleIntR: Rand<Pair<Double, Int>> = both(doubleR, intR)

fun <A> sequence(fs: List<Rand<A>>): Rand<List<A>> {
    return fs.foldRight(unit(empty())) { headR, tailR -> map2(headR, tailR) { head, tail -> Cons(head, tail) } }
}

fun intsR(count: Int): Rand<List<Int>> = sequence(Stream.constant(intR).take(count).toList())

fun nonNegativeIntLessThan(n: Int): Rand<Int> = nonNegativeInt.flatMap { i ->
    val mod = i % n
    if (i + (n - 1) - mod >= 0) unit(mod)
    else nonNegativeIntLessThan(n)
}

fun rollDice(): Rand<Int> = nonNegativeIntLessThan(6).map { it + 1 }
