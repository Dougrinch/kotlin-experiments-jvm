package com.dou.fp.e6

import com.dou.fp.e10.StateOf
import com.dou.fp.e3.Cons
import com.dou.fp.e3.List
import com.dou.fp.e3.foldRight
import kotlin.coroutines.RestrictsSuspension
import kotlin.experimental.ExperimentalTypeInference
import kotlin.collections.List as KotlinList

@OptIn(ExperimentalTypeInference::class)
fun <S, A> state(@BuilderInference block: StateGeneratorScope<S>.() -> A): State<S, A> {
    return State { s ->
        val scope = StateGeneratorScopeImpl(s)
        val a = scope.block()
        a to scope.s
    }
}

@RestrictsSuspension
interface StateGeneratorScope<S> {
    fun <A> State<S, A>.bind(): A
    fun get(): S
    fun set(s: S)
}

class StateGeneratorScopeImpl<S>(var s: S) : StateGeneratorScope<S> {
    override fun <A> State<S, A>.bind(): A {
        val p = run(s)
        s = p.second
        return p.first
    }

    override fun get(): S {
        return s
    }

    override fun set(s: S) {
        this.s = s
    }
}

data class State<S, out A>(val run: (S) -> Pair<A, S>) : StateOf<S, A> {
    companion object {
        fun <S, A> unit(a: A): State<S, A> = State { a to it }

        fun <S, A, B, C> map2(sa: State<S, A>, sb: State<S, B>, f: (A, B) -> C): State<S, C> =
            sa.flatMap { a -> sb.map { b -> f(a, b) } }
    }

    fun <B> map(f: (A) -> B): State<S, B> = State { s0 ->
        val (a, s1) = run(s0)
        f(a) to s1
    }

    fun <B> flatMap(f: (A) -> State<S, B>): State<S, B> = State { s0 ->
        val (a, s1) = run(s0)
        f(a).run(s1)
    }
}

fun <S> get(): State<S, S> = State { it to it }
fun <S> set(s: S): State<S, Unit> = State { Unit to s }
fun <S> modify(f: (S) -> S): State<S, Unit> = get<S>().flatMap { set(f(it)) }

fun <S, A> List<State<S, A>>.sequence(): State<S, List<A>> {
    return foldRight(State.unit(List.empty())) { headR, tailR ->
        State.map2(headR, tailR) { head, tail -> Cons(head, tail) }
    }
}

fun <S, A> KotlinList<State<S, A>>.flatten(): State<S, KotlinList<A>> = State { s0 ->
    val result = ArrayList<A>(size)
    var s = s0
    for (state in this) {
        val (a, ns) = state.run(s)
        result += a
        s = ns
    }
    result to s
}
