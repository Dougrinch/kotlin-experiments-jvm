package com.dou.fp.e6

import com.dou.fp.e3.List
import com.dou.fp.e3.map

sealed class Input
object Coin : Input()
object Turn : Input()

data class Machine(
    val locked: Boolean,
    val candies: Int,
    val coins: Int
)

fun simulateMachine(inputs: List<Input>): State<Machine, Pair<Int, Int>> =
    inputs.map { makeTurn(it) }.sequence().flatMap { get() }.map { it.coins to it.candies }

fun makeTurn(input: Input): State<Machine, Unit> = state {
    val machine = get()
    if (machine.candies <= 0)
        return@state

    val newMachine = when (input) {
        is Coin -> {
            if (machine.locked) {
                machine.copy(locked = false)
            } else {
                machine
            }
        }
        is Turn -> {
            if (machine.locked) {
                machine
            } else {
                machine.copy(locked = true, candies = machine.candies - 1, coins = machine.coins + 1)
            }
        }
    }
    set(newMachine)
}
