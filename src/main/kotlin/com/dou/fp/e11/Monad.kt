package com.dou.fp.e11

import com.dou.fp.e10.ForList
import com.dou.fp.e10.ForOption
import com.dou.fp.e10.Kind
import com.dou.fp.e10.fix
import com.dou.fp.e12.Applicative
import com.dou.fp.e3.Cons
import com.dou.fp.e3.List
import com.dou.fp.e3.Nil
import com.dou.fp.e4.None
import com.dou.fp.e4.Option
import com.dou.fp.e4.Some
import com.dou.fp.e3.flatMap as listFlatMap
import com.dou.fp.e3.map as listMap
import com.dou.fp.e4.flatMap as optionFlatMap

interface Monad<F> : Applicative<F> {
    fun <A, B> Kind<F, A>.flatMap(f: (A) -> Kind<F, B>): Kind<F, B>

    override fun <A, B> apply(fab: Kind<F, (A) -> B>, fa: Kind<F, A>): Kind<F, B> {
        return fab.flatMap { fa.map(it) }
    }

    override fun <A, B> Kind<F, A>.map(f: (A) -> B): Kind<F, B> = flatMap { unit(f(it)) }

    fun <A> List<A>.filterM(f: (A) -> Kind<F, Boolean>): Kind<F, List<A>> {
        return when (this) {
            is Nil -> unit(Nil)
            is Cons -> f(head).flatMap { succeed ->
                if (succeed) {
                    tail.filterM(f).map { tail -> Cons(head, tail) }
                } else {
                    tail.filterM(f)
                }
            }
        }
    }

    fun <A> Kind<F, Kind<F, A>>.flatten(): Kind<F, A> = flatMap { it }

    fun <A, B, C> compose(f: (A) -> Kind<F, B>, g: (B) -> Kind<F, C>): (A) -> Kind<F, C> = { a -> f(a).flatMap(g) }
}

object ListMonad : Monad<ForList> {
    override fun <A> unit(a: A): Kind<ForList, A> {
        return List(a)
    }

    override fun <A, B> Kind<ForList, A>.flatMap(f: (A) -> Kind<ForList, B>): Kind<ForList, B> {
        return fix().listFlatMap { f(it).fix() }
    }

    override fun <A, B> Kind<ForList, A>.map(f: (A) -> B): Kind<ForList, B> {
        return fix().listMap(f)
    }
}

object OptionMonad : Monad<ForOption> {
    override fun <A> unit(a: A): Kind<ForOption, A> {
        return Some(a)
    }

    override fun <A, B> Kind<ForOption, A>.flatMap(f: (A) -> Kind<ForOption, B>): Kind<ForOption, B> {
        return fix().optionFlatMap { f(it).fix() }
    }
}

fun main() {
    with(ListMonad) {
        println(map2(List(1, 2, 3), List("a", "b")) { i, s -> "$i$s" })
        println(List(1, 2, 3).repeat(3))
        println(List(1, 2, 3, 4).filterM { List(true, false) })
        println(List(1, 2, 3, 4).filterM { if (it % 2 == 1) List(true, false, true) else List(false) })
        println(List(1, 2, 3, 4).filterM { if (it % 2 == 1) List(true, false, true) else List() })
    }

    with(OptionMonad) {
        println(Option(1).repeat(3))
        println(List(1, 2, 3, 4).filterM { if (it % 2 == 1) Option(true) else Option(false) })
        println(List(1, 2, 3, 4).filterM { if (it % 2 == 1) Option(true) else None })
    }
}
