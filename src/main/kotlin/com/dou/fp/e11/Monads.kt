package com.dou.fp.e11

import arrow.core.Either
import arrow.core.right
import com.dou.fp.e10.*
import com.dou.fp.e3.Branch
import com.dou.fp.e3.Leaf
import com.dou.fp.e6.State
import com.dou.fp.e6.state

class ForId private constructor()
typealias IdOf<A> = Kind<ForId, A>

fun <A> IdOf<A>.fix() = this as Id<A>


data class Id<A>(val a: A) : IdOf<A>


object IdMonad : Monad<ForId> {
    override fun <A> unit(a: A): Kind<ForId, A> {
        return Id(a)
    }

    override fun <A, B> Kind<ForId, A>.flatMap(f: (A) -> Kind<ForId, B>): Kind<ForId, B> {
        return f(fix().a)
    }
}


fun <S> stateMonad(): Monad<ForState<S>> = object : Monad<ForState<S>> {
    override fun <A> unit(a: A): Kind<ForState<S>, A> {
        return State.unit(a)
    }

    override fun <A, B> Kind<ForState<S>, A>.flatMap(f: (A) -> Kind<ForState<S>, B>): Kind<ForState<S>, B> {
        return fix().flatMap { f(it).fix() }
    }
}


fun <E> eitherMonad(): Monad<ForEither<E>> = object : Monad<ForEither<E>> {
    override fun <A> unit(a: A): Kind<ForEither<E>, A> {
        return a.right().kind()
    }

    override fun <A, B> Kind<ForEither<E>, A>.flatMap(f: (A) -> Kind<ForEither<E>, B>): Kind<ForEither<E>, B> {
        return when (val e = fix()) {
            is Either.Left -> e.kind()
            is Either.Right -> f(e.value)
        }
    }
}


object TreeMonad : Monad<ForTree> {
    override fun <A> unit(a: A): Kind<ForTree, A> {
        return Leaf(a)
    }

    override fun <A, B> Kind<ForTree, A>.flatMap(f: (A) -> Kind<ForTree, B>): Kind<ForTree, B> {
        return when (val t = fix()) {
            is Leaf -> f(t.value)
            is Branch -> Branch(t.left.flatMap(f).fix(), t.right.flatMap(f).fix())
        }
    }
}


fun main() {
    with(IdMonad) {
        val id = Id("Hello, ").flatMap { a -> Id("monad").flatMap { b -> Id(a + b) } }
        println(id)
    }

    with(stateMonad<Int>()) {
        val getAndInc = state<Int, Int> {
            val r = get()
            set(r + 1)
            r
        }
        val m = getAndInc.repeat(5)
        println(m.fix().run(3))
    }
}
