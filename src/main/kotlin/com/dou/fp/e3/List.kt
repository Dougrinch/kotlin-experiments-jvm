@file:Suppress("unused")

package com.dou.fp.e3

import com.dou.fp.e10.ListOf

sealed class List<out A> : ListOf<A> {
    companion object {
        fun <A> empty(): List<A> = Nil

        operator fun <A> invoke(vararg elements: A): List<A> = of(*elements)

        private fun <A> of(vararg elements: A): List<A> {
            tailrec fun go(elements: Array<out A>, head: Int, tail: List<A>): List<A> {
                return when {
                    head < 0 -> tail
                    else -> go(elements, head - 1, Cons(elements[head], tail))
                }
            }
            return go(elements, elements.size - 1, Nil)
        }

        operator fun <A> invoke(n: Int, f: (Int) -> A): List<A> {
            tailrec fun go(i: Int, tail: List<A>): List<A> {
                return when {
                    i < n -> go(i + 1, Cons(f(i), tail))
                    else -> tail
                }
            }
            return go(0, Nil)
        }
    }
}

object Nil : List<Nothing>() {
    override fun toString(): String = "()"
}

data class Cons<out A>(val head: A, val tail: List<A>) : List<A>() {
    override fun toString(): String = "(${foldLeft("") { a, b -> if (a.isEmpty()) "$b" else "$a,$b" }})"
}

fun List<*>.isEmpty(): Boolean = this is Nil

fun <A> List<A>.head(): A = when (this) {
    is Nil -> throw NoSuchElementException()
    is Cons -> head
}

fun <A> List<A>.tail(): List<A> = when (this) {
    is Nil -> throw NoSuchElementException()
    is Cons -> tail
}

fun <A> List<A>.setHead(x: A): List<A> = when (this) {
    is Nil -> throw NoSuchElementException()
    is Cons -> Cons(x, tail)
}

tailrec fun <A> List<A>.drop(n: Int): List<A> = when {
    n <= 0 -> this
    else -> when (this) {
        is Nil -> throw NoSuchElementException()
        is Cons -> tail.drop(n - 1)
    }
}

tailrec fun <A> List<A>.dropWhile(f: (A) -> Boolean): List<A> = when (this) {
    is Nil -> Nil
    is Cons -> when (f(head)) {
        true -> tail.dropWhile(f)
        false -> this
    }
}

fun <A> List<A>.init(): List<A> = when (this) {
    is Nil -> throw NoSuchElementException()
    is Cons -> when (tail) {
        is Nil -> Nil
        is Cons -> Cons(head, tail.init())
    }
}

fun <A, B> List<A>.foldRight(z: B, f: (A, B) -> B): B = when (this) {
    is Nil -> z
    is Cons -> f(head, tail.foldRight(z, f))
}

tailrec fun <A, B> List<A>.foldLeft(z: B, f: (B, A) -> B): B = when (this) {
    is Nil -> z
    is Cons -> tail.foldLeft(f(z, head), f)
}

fun <A> List<A>.length(): Int {
    tailrec fun length(n: Int, l: List<A>): Int = when (l) {
        is Nil -> n
        is Cons -> length(n + 1, l.tail)
    }
    return length(0, this)
}

fun List<Int>.sum(): Int = foldLeft(0) { a, b -> a + b }
fun List<Double>.sum(): Double = foldLeft(0.0) { a, b -> a + b }
fun List<Double>.product(): Double = foldLeft(0.0) { a, b -> a * b }
fun List<*>.length2(): Int = foldLeft(0) { a, _ -> a + 1 }

fun <A> List<A>.reverse(): List<A> = foldLeft(List.empty()) { a, b -> Cons(b, a) }

fun <A, B> List<A>.foldRightLR(z: B, f: (A, B) -> B): B = reverse().foldLeft(z) { a, b -> f(b, a) }

fun <A, B> List<A>.foldLeftR(z: B, f: (B, A) -> B): B = foldRight(
    { b: B -> b },
    { a, g -> { b -> g(f(b, a)) } }
)(z)

fun <A, B> List<A>.foldRightL(z: B, f: (A, B) -> B): B = foldLeft(
    { b: B -> b },
    { g, a -> { b -> g(f(a, b)) } }
)(z)

fun <A> List<A>.append(a: A): List<A> = foldRight(List(a), ::Cons)
fun <A> List<A>.append(l: List<A>): List<A> = foldRight(l, ::Cons)

fun <A> List<List<A>>.flatten(): List<A> = foldRight(List.empty(), List<A>::append)

fun <A, B> List<A>.map(f: (A) -> B): List<B> = when (this) {
    is Nil -> Nil
    is Cons -> Cons(f(head), tail.map(f))
}

fun <A, B> List<A>.mapF(f: (A) -> B): List<B> = reverse().foldLeft(List.empty()) { t, a -> Cons(f(a), t) }

fun List<Int>.plusOne(): List<Int> = map { it + 1 }
fun <A> List<A>.mapToString(): List<String> = map { it.toString() }

fun <A> List<A>.filter(f: (A) -> Boolean): List<A> = when (this) {
    is Nil -> Nil
    is Cons -> when (f(head)) {
        true -> Cons(head, tail.filter(f))
        false -> tail.filter(f)
    }
}

tailrec fun <A> List<A>.any(f: (A) -> Boolean): Boolean = when (this) {
    is Nil -> false
    is Cons -> when (f(head)) {
        true -> true
        false -> tail.any(f)
    }
}

operator fun <A> List<A>.contains(a: A): Boolean = any { it == a }

tailrec fun <A> List<A>.all(f: (A) -> Boolean): Boolean = when (this) {
    is Nil -> true
    is Cons -> when (f(head)) {
        true -> tail.all(f)
        false -> false
    }
}

fun List<Int>.filterNotOdd(): List<Int> = filter { it % 2 == 0 }

fun <A, B> List<A>.flatMap(f: (A) -> List<B>): List<B> = map(f).flatten()

fun <A, B, C> List<A>.zipWith(l: List<B>, f: (A, B) -> C): List<C> = when (this) {
    is Nil -> Nil
    is Cons -> when (l) {
        is Nil -> Nil
        is Cons -> Cons(f(head, l.head), tail.zipWith(l.tail, f))
    }
}

fun List<Int>.plusElements(l: List<Int>): List<Int> = zipWith(l, Int::plus)

fun <A> List<A>.hasSubsequence(sub: List<A>): Boolean {
    tailrec fun List<A>.startsWith(sub: List<A>): Boolean = when (sub) {
        is Nil -> true
        is Cons -> when (this) {
            is Nil -> false
            is Cons -> if (head == sub.head) tail.startsWith(sub.tail) else false
        }
    }

    tailrec fun List<A>.containsTail(f: (List<A>) -> Boolean): Boolean = when (this) {
        is Nil -> false
        is Cons -> if (f(this)) true else tail.containsTail(f)
    }

    return containsTail { it.startsWith(sub) }
}

fun main() {
    println(List(1, 2, 3, 4).foldRight("0") { a, b -> "($a, $b)" })
    println(List(1, 2, 3, 4).foldLeft("0") { a, b -> "($a, $b)" })
    println(List(1, 2, 3, 4).foldRightLR("0") { a, b -> "($a, $b)" })
    println(List(1, 2, 3, 4).foldLeftR("0") { a, b -> "($a, $b)" })
    println(List(1, 2, 3, 4).foldRightL("0") { a, b -> "($a, $b)" })

    println(List(1, 2).append(3))
    println(List(List(1, 2), List(3, 4)))
    println(List(List(1, 2), List(3, 4)).flatten())

    println(List(1, 2, 3).flatMap { List(it, it) })

    println(List(1, 2, 3).plusElements(List(4, 5, 6)))

    println(List(1, 2, 3, 4).hasSubsequence(List(1, 2)))
    println(List(1, 2, 3, 4).hasSubsequence(List(2, 3)))
    println(List(1, 2, 3, 4).hasSubsequence(List(4)))
    println(List(1, 2, 3, 4).hasSubsequence(List(2, 4)))
}

tailrec fun <A> List<A>.forEach(f: (A) -> Unit): Unit = when (this) {
    is Cons -> {
        f(head)
        tail.forEach(f)
    }
    is Nil -> Unit
}
