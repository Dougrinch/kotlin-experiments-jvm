package com.dou.fp.e3

import com.dou.fp.e10.TreeOf

sealed class Tree<out A> : TreeOf<A> {
    companion object
}

data class Leaf<out A>(val value: A) : Tree<A>()
data class Branch<out A>(val left: Tree<A>, val right: Tree<A>) : Tree<A>()

fun <A> Tree<A>.size(): Int = when (this) {
    is Leaf -> 1
    is Branch -> left.size() + right.size()
}

fun Tree<Int>.maximum(): Int = when (this) {
    is Leaf -> value
    is Branch -> maxOf(left.maximum(), right.maximum())
}

fun <A> Tree<A>.depth(): Int = when (this) {
    is Leaf -> 1
    is Branch -> maxOf(left.depth(), right.depth()) + 1
}

fun <A, B> Tree<A>.map(f: (A) -> B): Tree<B> = when (this) {
    is Leaf -> Leaf(f(value))
    is Branch -> Branch(left.map(f), right.map(f))
}

fun <A, B> Tree<A>.fold(l: (A) -> B, b: (B, B) -> B): B = when (this) {
    is Leaf -> l(value)
    is Branch -> b(left.fold(l, b), right.fold(l, b))
}

fun <A> Tree<A>.sizeF(): Int = fold({ 1 }, Int::plus)
fun Tree<Int>.maximumF(): Int = fold({ it }, ::maxOf)
fun <A> Tree<A>.depthF(): Int = fold({ 1 }, { l, r -> maxOf(l, r) + 1 })
fun <A, B> Tree<A>.mapF(f: (A) -> B): Tree<B> = fold({ Leaf(f(it)) }, ::Branch)

fun main() {
    println(Branch(Leaf(1), Branch(Leaf(2), Leaf(3))).size())
    println(Branch(Leaf(1), Branch(Leaf(3), Leaf(2))).maximum())
    println(Branch(Leaf(1), Branch(Leaf(3), Leaf(2))).depth())
}
