package com.dou.fp.e7

import arrow.core.Either
import arrow.core.firstOrNone
import arrow.core.getOrElse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.launch
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.sqrt

class Actor<T>(es: ExecutorService, body: (T) -> Unit) {
    private val dispatcher = es.asCoroutineDispatcher()

    @OptIn(ObsoleteCoroutinesApi::class)
    private val sendChannel = CoroutineScope(dispatcher).actor<T> {
        for (msg in channel) {
            body(msg)
        }
    }

    fun send(msg: T) {
        CoroutineScope(dispatcher).launch {
            sendChannel.send(msg)
        }
    }
}

typealias Future<A> = ((A) -> Unit) -> Unit

class Par<A>(private val get: (ExecutorService) -> Future<A>) {
    operator fun invoke(es: ExecutorService): Future<A> = get(es)
}

fun <A> fork(a: () -> Par<A>): Par<A> = Par { es ->
    return@Par { ca -> es.execute { a()(es)(ca) } }
}

fun <A> delay(pa: () -> Par<A>): Par<A> = Par { es ->
    pa()(es)
}

fun <A> unit(a: A): Par<A> = Par { { it(a) } }
fun <A> lazyUnit(a: () -> A): Par<A> = fork { unit(a()) }

fun <A> run(es: ExecutorService, a: Par<A>): A {
    val ref = CompletableFuture<A>()
    a(es)(ref::complete)
    return ref.get()
}

fun <A, B, C> map2(pa: Par<A>, pb: Par<B>, f: (A, B) -> C): Par<C> = Par { es ->
    return@Par { cb ->
        var ar: A? = null
        var br: B? = null
        val combiner = Actor<Either<A, B>>(es) { eab ->
            eab.fold({ ar = it }, { br = it })
            if (ar != null && br != null) {
                cb(f(ar!!, br!!))
            }
        }
        pa(es).invoke { a -> combiner.send(Either.Left(a)) }
        pb(es).invoke { b -> combiner.send(Either.Right(b)) }
    }
}

fun <A, B> asyncF(f: (A) -> B): (A) -> Par<B> = { a -> lazyUnit { f(a) } }

fun <A, B> map(pa: Par<A>, f: (A) -> B): Par<B> = map2(pa, unit(Unit)) { a, _ -> f(a) }

fun <A, B, C, D> map3(pa: Par<A>, pb: Par<B>, pc: Par<C>, f: (A, B, C) -> D): Par<D> =
    map2(pa, map2(pb, pc) { b, c -> b to c }) { a, (b, c) -> f(a, b, c) }

fun <A, B, C, D, E> map4(pa: Par<A>, pb: Par<B>, pc: Par<C>, pd: Par<D>, f: (A, B, C, D) -> E): Par<E> =
    map2(map2(pa, pb) { a, b -> a to b }, map2(pc, pd) { c, d -> c to d }) { (a, b), (c, d) -> f(a, b, c, d) }

fun main() {
    val es = Executors.newSingleThreadExecutor()
    val par = parMap((1..100000).toList()) { sqrt(it.toDouble()) }
    println(run(es, par))
    es.shutdown()
}

fun sortPar(parList: Par<List<Int>>): Par<List<Int>> = map(parList) { it.sorted() }

fun <A, B> parMap(ps: List<A>, f: (A) -> B): Par<List<B>> = sequence(ps.map(asyncF(f)))

fun <A> sequence(ps: List<Par<A>>): Par<List<A>> = when {
    ps.isEmpty() -> unit(listOf())
    ps.size == 1 -> map(ps[0]) { listOf(it) }
    ps.size == 2 -> map2(ps[0], ps[1]) { f, s -> listOf(f, s) }
    else -> {
        val (l, r) = ps.splitAt(ps.size / 2)
        map2(sequence(l), sequence(r)) { lr, lb -> lr + lb }
    }
}

fun <A> parFilter(sa: List<A>, f: (A) -> Boolean): Par<List<A>> =
    map(sequence(sa.map(asyncF { it to f(it) }))) { it.filter { it.second }.map { it.first } }

fun <A> choice(cond: Par<Boolean>, t: Par<A>, f: Par<A>): Par<A> = flatMap(cond) { if (it) t else f }

fun <A> choiceN(n: Par<Int>, choices: List<Par<A>>): Par<A> = flatMap(n) { choices[it] }

fun <K, V> choiceMap(key: Par<K>, choices: Map<K, Par<V>>): Par<V> = flatMap(key) { choices.getValue(it) }

fun <A, B> flatMap(pa: Par<A>, f: (A) -> Par<B>): Par<B> = join(map(pa, f))

fun <A> join(ppa: Par<Par<A>>): Par<A> = Par { es ->
    { ca -> ppa(es).invoke { pa -> pa(es).invoke { a -> ca(a) } } }
}

fun sum(ints: List<Int>): Par<Int> {
    return if (ints.size <= 1) {
        unit(ints.firstOrNone().getOrElse { 0 })
    } else {
        val (l, r) = ints.splitAt(ints.size / 2)
        map2(
            fork { sum(l) },
            fork { sum(r) }
        ) { lx, rx -> lx + rx }
    }
}

fun <T> List<T>.splitAt(n: Int): Pair<List<T>, List<T>> {
    val first = subList(0, n)
    val second = subList(n, size)
    return first to second
}
