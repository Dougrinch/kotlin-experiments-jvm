package com.dou.fp.e8

import com.dou.fp.e6.RNG
import com.dou.fp.e6.SimpleRNG
import kotlin.math.max
import kotlin.math.min

data class SGen<A>(val forSize: (Int) -> Gen<A>) {
    operator fun invoke(size: Int): Gen<A> = forSize(size)

    fun <B> map(f: (A) -> B): SGen<B> = SGen { forSize(it).map(f) }

    fun <B> flatMap(f: (A) -> Gen<B>): SGen<B> = SGen { forSize(it).flatMap(f) }

    companion object {
        fun <A> Gen<A>.unsized(): SGen<A> = SGen { this }

        fun <A> listOf(g: Gen<A>): SGen<List<A>> = SGen { Gen.listOfN(it, g) }

        fun <A> nonEmptyListOf(g: Gen<A>): SGen<List<A>> = SGen { Gen.listOfN(max(it, 1), g) }
    }
}

fun <A> forAll(g: SGen<A>, f: (A) -> Boolean): Prop = Prop { max, n, rng ->
    val props = sequence {
        val casePerSize = n / max
        var remSize = n
        for (i in 0..max) {
            if (remSize <= 0) {
                break
            }

            val cases = min(casePerSize, remSize)
            remSize -= casePerSize

            yield(Prop { m, _, r ->
                val p = forAll(g(i), f)
                p.check(m, cases, r)
            })
        }
    }

    props.reduce(Prop::merge).check(max, n, rng)
}

fun run(
    prop: Prop,
    maxSize: MaxSize = 100,
    testCases: TestCases = 100,
    rng: RNG = SimpleRNG(System.currentTimeMillis())
) {
    when (val result = prop.check(maxSize, testCases, rng)) {
        is Result.Passed -> println("OK, passed ${result.successes.size} tests.")
        is Result.Proved -> println("OK, proved property.")
        is Result.Falsified -> println("Falsified after ${result.successes.size} passed tests: ${result.failures}")
    }
}

fun check(p: () -> Boolean): Prop = Prop { _, _, _ ->
    try {
        if (p()) {
            Result.Proved(listOf(TestCaseResult.Success("()")))
        } else {
            Result.Falsified(listOf(TestCaseResult.Fail("()", null)), emptyList())
        }
    } catch (ex: Throwable) {
        Result.Falsified(listOf(TestCaseResult.Fail("()", ex)), emptyList())
    }
}

fun main() {
    val smallInt = Gen.choose(-10, 10)

    val maxProp = forAll(SGen.nonEmptyListOf(smallInt)) { ns ->
        val max = ns.maxOf { it }
        ns.all { it <= max }
    }

    run(maxProp)
}
