package com.dou.fp.e8

import com.dou.fp.e7.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import com.dou.fp.e7.run as parRun

val ges: Gen<ExecutorService> = Gen.weighted(
    Gen.choose(1, 4).map { Executors.newFixedThreadPool(it) } to .75,
    Gen.unit(Executors.newCachedThreadPool()) to 0.25
)

fun <A> forAllPar(ga: Gen<A>, f: (A) -> Par<Boolean>): Prop {
    return forAll(combine(ges, ga)) { (es, a) -> parRun(es, f(a)) }
}

fun <A, B> combine(ga: Gen<A>, gb: Gen<B>): Gen<Pair<A, B>> = ga.flatMap { a -> gb.map { b -> a to b } }

fun <A> equal(ga: Par<A>, gb: Par<A>): Par<Boolean> = map2(ga, gb) { a, b -> a == b }

fun checkPar(f: Par<Boolean>): Prop = forAllPar(Gen.unit(Unit)) { f }

infix fun <A> Par<A>.equalTo(p: Par<A>): Par<Boolean> = equal(this, p)

fun main() {
    run(checkPar(
        map(unit(1)) { it + 1 } equalTo unit(2)
    ))

    run(forAllPar(Gen.int().map { map2(fork { fork { lazyUnit { 18 } } }, fork { unit(it) }, Int::plus) }) { x ->
        map(x) { it } equalTo x
    })

    run(forAllPar(Gen.int().map { unit(it) }) { x ->
        fork { x } equalTo x
    })
}
