package com.dou.fp.e8

fun <A> Gen.Companion.oneOf(v: List<A>): Gen<A> = choose(0, v.size).map { v[it] }

fun Gen.Companion.char(): Gen<Char> = oneOf(('a'..'z') + ('A'..'Z') + ('0'..'9'))

fun Gen.Companion.word(): Gen<String> = listOfN(choose(0, 10), char()).map { it.joinToString(separator = "") }

fun Gen.Companion.any(): Gen<Any> = TODO()
