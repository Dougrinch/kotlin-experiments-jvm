@file:Suppress("UNUSED_PARAMETER")

package com.dou.fp.e8

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import com.dou.fp.e6.*
import com.dou.fp.e6.unit as randUnit

data class Gen<A>(val sample: Rand<A>) {
    companion object {
        fun choose(start: Int, stopExclusive: Int): Gen<Int> =
            Gen(nonNegativeIntLessThan(stopExclusive - start).map { it + start })

        fun <A> unit(a: A): Gen<A> = Gen(randUnit(a))

        fun boolean(): Gen<Boolean> = Gen(intR.map { it % 2 == 0 })

        fun int(): Gen<Int> = Gen(intR)

        fun <A> listOfN(n: Int, ga: Gen<A>): Gen<List<A>> = Gen(List(n) { ga.sample }.flatten())

        fun <A> listOfN(gn: Gen<Int>, ga: Gen<A>): Gen<List<A>> = gn.flatMap { listOfN(it, ga) }

        fun <A, B> pair(ga: Gen<A>, gb: Gen<B>): Gen<Pair<A, B>> = ga.flatMap { a -> gb.map { b -> a to b } }

        fun <A> option(ga: Gen<A>): Gen<Option<A>> =
            boolean().flatMap { f -> if (f) unit(None) else ga.map { a -> Some(a) } }

        fun <A> union(ga: Gen<A>, gb: Gen<A>): Gen<A> = boolean().flatMap { if (it) ga else gb }

        fun <A> weighted(pga: Pair<Gen<A>, Double>, pgb: Pair<Gen<A>, Double>): Gen<A> {
            val (ga, wa) = pga
            val (gb, wb) = pga
            val prob = wa / (wa + wb)
            return Gen(doubleR).flatMap { if (it < prob) ga else gb }
        }
    }

    fun <B> map(f: (A) -> B): Gen<B> = Gen(sample.map(f))

    fun <B> flatMap(f: (A) -> Gen<B>): Gen<B> = Gen(sample.flatMap { f(it).sample })
}

typealias MaxSize = Int
typealias TestCases = Int

sealed class TestCaseResult {
    abstract val desc: String

    data class Success(override val desc: String) : TestCaseResult()
    data class Fail(override val desc: String, val ex: Throwable?) : TestCaseResult()
}

sealed class Result {
    abstract val successes: List<TestCaseResult.Success>
    abstract val failures: List<TestCaseResult.Fail>

    data class Proved(override val successes: List<TestCaseResult.Success>) : Result() {
        override val failures: List<TestCaseResult.Fail>
            get() = emptyList()

        override fun toString(): String = "Proved"
    }

    data class Passed(override val successes: List<TestCaseResult.Success>) : Result() {
        override val failures: List<TestCaseResult.Fail>
            get() = emptyList()
    }

    data class Falsified(
        override val failures: List<TestCaseResult.Fail>, override val successes: List<TestCaseResult.Success>
    ) : Result()

    companion object {
        fun of(testCases: Sequence<TestCaseResult>): Result {
            val successes = ArrayList<TestCaseResult.Success>()
            val failures = ArrayList<TestCaseResult.Fail>()
            for (testCase in testCases) {
                when (testCase) {
                    is TestCaseResult.Success -> successes += testCase
                    is TestCaseResult.Fail -> failures += testCase
                }
            }
            return if (failures.isEmpty()) {
                Passed(successes)
            } else {
                Falsified(failures, successes)
            }
        }
    }
}

fun Result.mergeWith(r: Result): Result {
    return when {
        this is Result.Proved && r is Result.Proved -> Result.Proved(successes + r.successes)
        else -> Result.of(successes.asSequence() + r.successes.asSequence())
    }
}

data class Prop(val check: (MaxSize, TestCases, RNG) -> Result) {
    fun merge(p: Prop): Prop = Prop { max, n, rng ->
        val r1 = check(max, n, rng)
        val r2 = p.check(max, n, rng)
        r1.mergeWith(r2)
    }
}

fun <A> forAll(g: Gen<A>, f: (A) -> Boolean): Prop = Prop { _, n, rng ->
    state<RNG, Result> {
        val testCases = sequence {
            for (i in 0 until n) {
                val a = g.sample.bind()
                val testCase = Either.catch { f(a) }
                    .fold({ th ->
                        TestCaseResult.Fail(a.toString(), th)
                    }) { passed ->
                        if (passed) {
                            TestCaseResult.Success(a.toString())
                        } else {
                            TestCaseResult.Fail(a.toString(), null)
                        }
                    }
                yield(testCase)

                if (testCase is TestCaseResult.Fail) {
                    break
                }
            }
        }
        Result.of(testCases)
    }.run(rng).first
}

fun main() {
    forAll(Gen.listOfN(Gen(nonNegativeIntLessThan(5)), Gen.boolean())) {
        println(it)
        true
    }.check(5, 10, SimpleRNG(4)).let { println(it) }
}
