package com.dou.fp.e4

import com.dou.fp.e10.OptionOf
import com.dou.fp.e3.*
import com.dou.fp.e3.List
import kotlin.math.pow

sealed class Option<out A> : OptionOf<A> {
    companion object {
        fun <A> empty(): Option<A> = None
        fun <A> some(v: A): Option<A> = Some(v)

        operator fun <A : Any> invoke(a: A?): Option<A> = when (a) {
            null -> empty()
            else -> some(a)
        }
    }
}

object None : Option<Nothing>() {
    override fun toString(): String = "None"
}

data class Some<out A>(val value: A) : Option<A>() {
    override fun toString(): String = "Some($value)"
}

fun <A> Option<A>.isEmpty(): Boolean = when (this) {
    is None -> true
    is Some -> false
}

fun <A> Option<A>.isNotEmpty(): Boolean = !isEmpty()

fun <A, B> Option<A>.map(f: (A) -> B): Option<B> = when (this) {
    is None -> None
    is Some -> Some(f(value))
}

fun <A, B> Option<A>.flatMap(f: (A) -> Option<B>): Option<B> = when (this) {
    is None -> None
    is Some -> f(value)
}

fun <A, B, C> Option<A>.zipWith(that: Option<B>, f: (A, B) -> C): Option<C> = flatMap { a -> that.map { b -> f(a, b) } }

fun <A> Option<A>.getOrElse(def: () -> A): A = when (this) {
    is None -> def()
    is Some -> value
}

fun <A> Option<A>.orElse(def: () -> Option<A>): Option<A> = when (this) {
    is None -> def()
    is Some -> this
}

fun <A> Option<A>.filter(f: (A) -> Boolean): Option<A> = when (this) {
    is None -> None
    is Some -> if (f(value)) this else None
}

fun List<Double>.mean(): Option<Double> = if (isEmpty()) None else Some(sum() / length())

fun List<Double>.variance(): Option<Double> {
    val xs = this
    return mean().flatMap { m -> xs.map { x -> (x - m).pow(2) }.mean() }
}

fun <A, B, C> map2(oa: Option<A>, ob: Option<B>, f: (A, B) -> C): Option<C> =
    oa.flatMap { a -> ob.map { b -> f(a, b) } }

fun <A> List<Option<A>>.sequence(): Option<List<A>> = traverse { it }

fun <A, B> List<A>.traverse(f: (A) -> Option<B>): Option<List<B>> {
    return when (this) {
        is Nil -> Some(Nil)
        is Cons -> when (val head = f(head)) {
            is None -> None
            is Some -> when (val tail = tail.traverse(f)) {
                is None -> None
                is Some -> Some(Cons(head.value, tail.value))
            }
        }
    }
}

fun <A, B> List<A>.traverseF(f: (A) -> Option<B>): Option<List<B>> {
    return foldRightL(Some(List.empty())) { a, b ->
        f(a).flatMap { h -> b.map { t -> Cons(h, t) } }
    }
}

fun <A> Option.Companion.catch(f: () -> A): Option<A> = try {
    Some(f())
} catch (e: Throwable) {
    None
}

fun String.parseToInt(): Option<Int> = Option.catch { toInt() }

fun main() {
    println(List("1", "2", "3").traverse { it.parseToInt() })
    println(List("1", "j2", "3").traverse { it.parseToInt() })
    println(List("1", "2", "3").traverseF { it.parseToInt() })
    println(List("1", "j2", "3").traverseF { it.parseToInt() })
}
