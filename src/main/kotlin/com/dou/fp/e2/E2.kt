package com.dou.fp.e2

import java.math.BigDecimal

fun fib(n: Int): BigDecimal {
    tailrec fun fib(f1: BigDecimal, f2: BigDecimal, i: Int): BigDecimal {
        return when (i) {
            0 -> BigDecimal.ZERO
            1 -> f2
            else -> fib(f2, f1 + f2, i - 1)
        }
    }
    return fib(BigDecimal.ZERO, BigDecimal.ONE, n)
}

val <T> List<T>.head: T
    get() = first()

val <T> List<T>.tail: List<T>
    get() = drop(1)

tailrec fun <A> isSorted(aa: List<A>, order: (A, A) -> Boolean): Boolean {
    if (aa.isEmpty()) {
        return true
    }

    val head = aa.head
    val tail = aa.tail

    return when {
        tail.isEmpty() -> true
        !order(head, tail.head) -> false
        else -> isSorted(tail, order)
    }
}

fun <A, B, C> curry(f: (A, B) -> C): (A) -> (B) -> C = { a -> { b -> f(a, b) } }

fun <A, B, C> uncurry(f: (A) -> (B) -> C): (A, B) -> C = { a, b -> f(a)(b) }

fun <A, B, C> compose(f: (B) -> C, g: (A) -> B): (A) -> C = { a -> f(g(a)) }

fun main() {
    println(isSorted(listOf<Int>()) { a, b -> a < b })
    println(isSorted(listOf(1)) { a, b -> a < b })
    println(isSorted(listOf(1, 4, 7)) { a, b -> a < b })
    println(isSorted(listOf(4, 1, 7)) { a, b -> a < b })
}
