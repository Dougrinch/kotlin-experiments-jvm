package com.dou.fp.e9

import com.dou.fp.e9.JSON.*

fun main() {
    val s = """
        {
          "Company name" : "Microsoft Corporation",
          "Ticker": "MSFT",
          "Active": true,
          "Price": 30.66,
          "Shares outstanding": 8.38e9,
          "Related companies": [ "HPQ", "IBM", "YHOO" "DELL", "GOOG" ]
        }
    """.trimIndent()

    run(JsonParsers.json, s).fold({ println(it) }, { println(it) })
}

sealed class JSON {
    data object JNull : JSON()

    data class JNumber(val get: Double) : JSON()
    data class JString(val get: String) : JSON()
    data class JBoolean(val get: Boolean) : JSON()
    data class JArray(val get: List<JSON>) : JSON()
    data class JObject(val get: Map<String, JSON>) : JSON()
}

object JsonParsers : ParsersDsl() {
    val json: Parser<JSON> by parser { element }

    private val value: Parser<JSON> by parser {
        oneOf(
            obj,
            arr,
            str map { JString(it) },
            number,
            bool,
            nul
        )
    }

    private val obj by parser {
        oneOf(
            attempt("{" - ws - "}" map { JObject(emptyMap()) }),
            "{" - members - "}" map { JObject(it.toMap()) },
        )
    }

    private val members by parser { member.many(separator = ",") }

    private val member by parser { ws - str - ws - ":" - element }

    private val arr by parser {
        oneOf(
            attempt("[" - ws - "]" map { JArray(emptyList()) }),
            "[" - elements - "]" map { JArray(it) },
        )
    }

    private val elements by parser { element.many(separator = ",") }

    private val element by parser { ws - value - ws }

    private val str by parser { "\"" - characters - "\"" }

    private val characters by parser { character.many() map { it.joinToString(separator = "") } }

    private val character by parser {
        oneOf(
            codePoint { c -> c != '"'.code && c != '\\'.code && c in 0x0020..0x10FFFF },
            "\\" - escape
        ).slice()
    }

    private val escape by parser {
        oneOf(
            oneOf('"', '\\', '/', 'b', 'f', 'n', 'r', 't'),
            "u" - hex.many(exact = 4) map { Char(it.joinToString(separator = "").toInt(16)) }
        )
    }

    private val hex by parser {
        oneOf(
            oneOf('0'..'9'),
            oneOf('A'..'F'),
            oneOf('a'..'f')
        )
    }

    private val number by parser {
        regex("-?(0|[1-9]\\d*)(\\.\\d+)?([Ee][+-]?\\d+)?").map { JNumber(it.toDouble()) }
    }

    private val bool by parser {
        oneOf(
            "true" parseAs JBoolean(true),
            "false" parseAs JBoolean(false)
        )
    }

    private val nul by parser { "null" parseAs JNull }

    private val ws: Parser<Unit> by parser {
        oneOf(
            "\u0020" - ws,
            "\u000A" - ws,
            "\u000D" - ws,
            "\u0009" - ws,
            "" parseAs Unit
        )
    }
}
