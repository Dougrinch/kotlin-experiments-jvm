package com.dou.fp.e9

class StringView(
    private val string: CharSequence, private val startIndex: Int, private val endIndex: Int
) : CharSequence {

    override val length: Int
        get() = endIndex - startIndex

    override fun get(index: Int): Char {
        return string[index + startIndex]
    }

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        return StringView(string, startIndex + this.startIndex, endIndex + this.startIndex)
    }

    override fun toString(): String {
        return string.substring(startIndex, endIndex)
    }
}
