package com.dou.fp.e9

open class ParsersDsl : ParsersImpl() {
    infix fun <A> String.parseAs(c: A): Parser<A> = string(this).map { c }
    infix fun <A> Parser<*>.parseAs(c: A): Parser<A> = this.map { c }
    fun const(s: String): Parser<Unit> = s parseAs Unit

    fun digit(): Parser<Int> = oneOf('0'..'9').map { it.digitToInt() }
    fun char(a: Char): Parser<Char> = string(a.toString()).map { it[0] }
    fun int(): Parser<Int> = regex("-?\\d+").map { it.toInt() }
    fun long(): Parser<Long> = regex("-?\\d+").map { it.toLong() }
    fun oneOf(chars: CharRange): Parser<Char> = chars.map { char(it) }.reduce { p1, p2 -> p1 or p2 }
    fun oneOf(vararg chars: Char): Parser<Char> = chars.map { char(it) }.reduce { p1, p2 -> p1 or p2 }
    fun <A> oneOf(vararg ps: Parser<A>): Parser<A> = ps.reduce { p1, p2 -> p1 or p2 }

    fun <A> Parser<A>.many(separator: String, exact: Int): Parser<List<A>> =
        many(const(separator), exact)

    fun <A> Parser<A>.many(separator: Parser<Unit>? = null, exact: Int): Parser<List<A>> =
        many(separator, min = exact, max = exact)

    fun <A> Parser<A>.many(separator: String, min: Int = 0, max: Int? = null): Parser<List<A>> =
        many(const(separator), min, max)

    fun <A> Parser<A>.many(separator: Parser<Unit>? = null, min: Int = 0, max: Int? = null): Parser<List<A>> =
        Parser { input ->
            val result = ArrayList<A>()

            var consumed = 0

            when (val ra = parse(input)) {
                is Failed -> {
                    return@Parser if (min == 0) {
                        Parsed(result, consumed)
                    } else {
                        ra
                    }
                }
                is Parsed -> {
                    result += ra.value
                    consumed += ra.consumed
                }
            }

            val next = if (separator != null) separator - this else this
            while (max == null || result.size < max) {
                when (val ra = next.parse(input.advancedBy(consumed))) {
                    is Failed -> {
                        return@Parser if (result.size >= min) {
                            Parsed(result, consumed)
                        } else {
                            ra
                        }
                    }
                    is Parsed -> {
                        result += ra.value
                        consumed += ra.consumed
                    }
                }
            }

            return@Parser Parsed(result, consumed)
        }

    infix fun <A, B> Parser<A>.map(f: (A) -> B): Parser<B> = flatMap { a -> succeed(f(a)) }

    infix fun <A, B, C> Parser<Pair<A, B>>.map(f: (A, B) -> C): Parser<C> = flatMap { (a, b) -> succeed(f(a, b)) }

    infix fun <A, B, C, D> Parser<Pair<Pair<A, B>, C>>.map(f: (A, B, C) -> D): Parser<D> =
        flatMap { (ab, c) -> succeed(f(ab.first, ab.second, c)) }

    infix fun <A, B, C, D, E> Parser<Pair<Pair<Pair<A, B>, C>, D>>.map(f: (A, B, C, D) -> E): Parser<E> =
        flatMap { (abc, d) -> succeed(f(abc.first.first, abc.first.second, abc.second, d)) }

    @JvmName("minusAB")
    operator fun <A, B> Parser<A>.minus(pb: Parser<B>): Parser<Pair<A, B>> = minus(this, { pb }) { a, b -> a to b }

    @JvmName("minusAFB")
    operator fun <A, B> Parser<A>.minus(pb: (A) -> Parser<B>): Parser<Pair<A, B>> = minus(this, pb) { a, b -> a to b }

    @JvmName("minusAFB")
    operator fun <A, B> Parser<A>.minus(pb: () -> Parser<B>): Parser<Pair<A, B>> =
        minus(this, { pb() }) { a, b -> a to b }

    @JvmName("minusAU")
    operator fun <A> Parser<A>.minus(p: Parser<Unit>): Parser<A> = minus(this, { p }) { a, _ -> a }

    @JvmName("minusAFU")
    operator fun <A> Parser<A>.minus(pb: () -> Parser<Unit>): Parser<A> = minus(this, { pb() }) { a, _ -> a }

    @JvmName("minusUB")
    operator fun <B> Parser<Unit>.minus(p: Parser<B>): Parser<B> = minus(this, { p }) { _, a -> a }

    @JvmName("minusUFB")
    operator fun <B> Parser<Unit>.minus(p: () -> Parser<B>): Parser<B> = minus(this, { p() }) { _, a -> a }

    @JvmName("minusUU")
    operator fun Parser<Unit>.minus(p: Parser<Unit>): Parser<Unit> = minus(this, { p }) { _, _ -> }

    @JvmName("minusUFU")
    operator fun Parser<Unit>.minus(p: () -> Parser<Unit>): Parser<Unit> = minus(this, { p() }) { _, _ -> }

    private fun <A, B, C> minus(pa: Parser<A>, pb: (A) -> Parser<B>, f: (A, B) -> C): Parser<C> =
        pa.flatMap { a -> pb(a).map { b -> f(a, b) } }

    operator fun <A> Parser<A>.minus(s: String): Parser<A> = minus(const(s))
    operator fun <B> String.minus(pb: Parser<B>): Parser<B> = const(this) - pb
    operator fun <B> String.minus(pb: () -> Parser<B>): Parser<B> = const(this) - pb
}
