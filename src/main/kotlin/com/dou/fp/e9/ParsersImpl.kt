package com.dou.fp.e9

import org.intellij.lang.annotations.Language

open class ParsersImpl : Parsers {
    override fun <A> succeed(a: A): Parser<A> = Parser { Parsed(a, 0) }

    override fun string(s: String): Parser<String> = Parser { input ->
        if (input.startsWith(s)) {
            Parsed(s, s.length)
        } else {
            Failed(ParseError("\"$s\" expected", input), false)
        }
    }

    @OptIn(ExperimentalStdlibApi::class)
    override fun regex(@Language("RegExp") pattern: String): Parser<String> = Parser { input ->
        when (val mr = pattern.toRegex().matchAt(input, 0)) {
            null -> Failed(ParseError("Pattern \"$pattern\" expected", input), false)
            else -> mr.value.let { v -> Parsed(v, v.length) }
        }
    }

    override fun codePoint(f: (Int) -> Boolean): Parser<String> = Parser { input ->
        val cpo = input.codePoints().findFirst()
        if (cpo.isEmpty) {
            Failed(ParseError("Unexpected EOF", input), false)
        } else {
            val cp = cpo.asInt
            if (f(cp)) {
                val v = String(codePoints = intArrayOf(cp), 0, 1)
                Parsed(v, v.length)
            } else {
                Failed(ParseError("Unmatched codePoint '$cp'", input), false)
            }
        }
    }

    override fun <A> (Parser<A>).slice(): Parser<String> = Parser { input ->
        when (val result = parse(input)) {
            is Failed -> result
            is Parsed -> Parsed(input.substring(0, result.consumed), result.consumed)
        }
    }

    override fun <A> (Parser<A>).or(p: Parser<A>): Parser<A> = Parser { input ->
        when (val ra = parse(input)) {
            is Parsed -> ra
            is Failed -> if (ra.committed) ra else p.parse(input)
        }
    }

    override fun <A, B> (Parser<A>).flatMap(f: (A) -> Parser<B>): Parser<B> = Parser { input ->
        when (val ra = parse(input)) {
            is Failed -> ra
            is Parsed -> when (val rb = f(ra.value).parse(input.advancedBy(ra.consumed))) {
                is Failed -> rb.copy(committed = ra.consumed != 0 || rb.committed)
                is Parsed -> Parsed(rb.value, ra.consumed + rb.consumed)
            }
        }
    }

    override fun <A> tag(msg: String, p: Parser<A>): Parser<A> = Parser { input ->
        when (val r = p.parse(input)) {
            is Parsed -> r
            is Failed -> r.copy(ParseError(msg, input))
        }
    }

    override fun <A> scope(msg: String, f: () -> Parser<A>): Parser<A> = Parser { input ->
        when (val r = f().parse(input)) {
            is Parsed -> r
            is Failed -> r.copy(ParseError(r.error.stack + (msg to input)))
        }
    }

    override fun <A> attempt(p: Parser<A>): Parser<A> = Parser { input ->
        when (val r = p.parse(input)) {
            is Parsed -> r
            is Failed -> r.copy(committed = false)
        }
    }

    override fun <A> Parser<A>.limitConsumed(n: Int): Parser<A> = Parser { input ->
        parse(input.limitBy(n))
    }
}
