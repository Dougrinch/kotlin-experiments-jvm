package com.dou.fp.e9

data class ParseError(val stack: List<Pair<String, Location>>) {
    constructor(message: String, location: Location) : this(listOf(message to location))

    override fun toString(): String {
        val errorsByLocation = stack.groupBy({ it.second }, { it.first })
            .toSortedMap(compareBy { it.offset })

        val messages = errorsByLocation
            .mapValues { (_, ms) -> ms.joinToString() }
            .mapKeys { (l, _) -> "${l.line}.${l.column}" }
            .map { (l, m) -> "$l $m" }
            .joinToString(separator = "\n")

        val location = errorsByLocation.lastKey()

        return messages + "\n" + location
    }
}
