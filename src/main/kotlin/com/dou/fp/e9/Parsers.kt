package com.dou.fp.e9

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import org.intellij.lang.annotations.Language
import kotlin.properties.ReadOnlyProperty

fun interface Parser<out A> {
    fun parse(input: Location): Result<A>
}

fun <A> Parser<A>.onError(f: (Failed) -> Unit): Parser<A> {
    return Parser { l ->
        val pr = parse(l)
        if (pr is Failed) {
            f(pr)
        }
        pr
    }
}

private val stack = ArrayDeque<String>()

fun <A> parser(f: () -> Parser<A>): ReadOnlyProperty<Any?, Parser<A>> {
    return ReadOnlyProperty { _, property ->
        Parser { l ->
            try {
                stack.addLast(property.name)
                val pr = f().parse(l)
                if (pr is Failed) {
                    println()
                    println(stack)
                    println(pr)
                    println()
                }
                pr
            } finally {
                stack.removeLast()
            }
        }
    }
}

sealed class Result<out A>
data class Parsed<A>(val value: A, val consumed: Int) : Result<A>()
data class Failed(val error: ParseError, val committed: Boolean) : Result<Nothing>()

interface Parsers {
    fun <A> succeed(a: A): Parser<A>
    fun string(s: String): Parser<String>
    fun regex(@Language("RegExp") pattern: String): Parser<String>
    fun codePoint(f: (Int) -> Boolean): Parser<String>

    fun <A> Parser<A>.slice(): Parser<String>
    infix fun <A> Parser<A>.or(p: Parser<A>): Parser<A>
    fun <A, B> Parser<A>.flatMap(f: (A) -> Parser<B>): Parser<B>

    fun <A> tag(msg: String, p: Parser<A>): Parser<A>
    fun <A> scope(msg: String, f: () -> Parser<A>): Parser<A>
    fun <A> attempt(p: Parser<A>): Parser<A>

    fun <A> Parser<A>.limitConsumed(n: Int): Parser<A>
}

fun <A> Parser<A>.parse(input: String): Either<ParseError, A> = run(this, input)

fun <A> run(p: Parser<A>, input: String): Either<ParseError, A> {
    return when (val res = p.parse(Location(input, 0))) {
        is Parsed -> when (res.consumed) {
            input.length -> Right(res.value)
            else -> Left(ParseError("Unexpected symbol '${input[res.consumed]}'", Location(input, res.consumed)))
        }
        is Failed -> Left(res.error)
    }
}
