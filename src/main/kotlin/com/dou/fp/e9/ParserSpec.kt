package com.dou.fp.e9

import arrow.core.Either
import arrow.core.Either.Right
import com.dou.fp.e8.Gen
import com.dou.fp.e8.char
import com.dou.fp.e8.word
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo

fun main() = parserSpec {
    forAll(Gen.char()) { ch ->
        char(ch) applying ch.toString() parsesTo Right(ch)
    }

    forAll(Gen.word()) { s ->
        string(s) applying s parsesTo s
        (string(s) - "," - string(s)) applying "$s,$s" parsesTo Right(s to s)
    }

    check {
        (char('a') or char('b')).many().slice() applying "aaba" parsesTo "aaba"

        string("abra") or string("cadabra") applying "abra" parsesTo "abra"
        string("abra") or string("cadabra") applying "cadabra" parsesTo "cadabra"

        (string("ab") or string("cde")).many(exact = 3) applying "ababab" parsesTo listOf("ab", "ab", "ab")
        (string("ab") or string("cde")).many(exact = 3) applying "cdecdecde" parsesTo listOf("cde", "cde", "cde")
        (string("ab") or string("cde")).many(exact = 3) applying "ababcde" parsesTo listOf("ab", "ab", "cde")
        (string("ab") or string("cde")).many(exact = 3) applying "cdecdeab" parsesTo listOf("cde", "cde", "ab")

        int() applying "18" parsesTo 18

        (int() - "," - int()) applying "18,42" parsesTo Pair(18, 42)

        (int() - "," - int()).map { (a, b) -> a + b }.many(separator = ",").let { p ->
            p applying "1,2,3,4,5,6" parsesTo listOf(3, 7, 11)
        }

        int().many(separator = ",").map { it.sum() }.many(separator = ";").let { p ->
            p applying "1,2;3;4,5,6" parsesTo listOf(3, 3, 15)
        }

        (int() - "," - { n -> int().many(separator = ",", exact = n) } - "," - int().many(separator = ","))
            .let { p ->
                p applying "3,1,2,3,4" parsesTo (3 to listOf(1, 2, 3) to listOf(4))
                p applying "2,1,2,3,4" parsesTo (2 to listOf(1, 2) to listOf(3, 4))
            }

        digit().many(separator = ",") - "," - regex("\\w+") applying "1,2,3,word" parsesTo (listOf(1, 2, 3) to "word")
    }
}

private fun parserSpec(body: ParserSpecContext.() -> Unit) {
    object : ParserSpecContext {
        override fun <A> forAll(ga: Gen<A>, body: TestContext.(A) -> Unit) {
            com.dou.fp.e8.run(com.dou.fp.e8.forAll(ga) { a -> TestContext().body(a); true })
        }

        override fun check(body: TestContext.() -> Unit) {
            com.dou.fp.e8.run(com.dou.fp.e8.check { TestContext().body(); true })
        }
    }.body()
}

interface ParserSpecContext {
    fun <A> forAll(ga: Gen<A>, body: TestContext.(A) -> Unit)
    fun check(body: TestContext.() -> Unit)
}

class TestContext : ParsersDsl() {
    infix fun <A> Parser<A>.applying(input: String): ApplyingContext<A> {
        return ApplyingContext(this, input)
    }

    infix fun <A> ApplyingContext<A>.parsesTo(result: Either<Unit, A>) {
        assertThat(run(p, input), equalTo(result))
    }

    infix fun <A> ApplyingContext<A>.parsesTo(result: A) {
        parsesTo(Right(result))
    }

    class ApplyingContext<A>(val p: Parser<A>, val input: String)
}
