package com.dou.fp.e9

import kotlin.math.min

class Location(private val input: CharSequence, val offset: Int) : CharSequence {
    override val length: Int
        get() = input.length - offset

    override fun get(index: Int): Char {
        return input[index + offset]
    }

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        return StringView(input, startIndex + offset, endIndex + offset)
    }

    fun advancedBy(n: Int): Location = Location(input, offset = offset + n)
    fun limitBy(n: Int): Location = Location(StringView(input, 0, min(input.length, offset + n)), offset)

    val lineStr: String by lazy { input.substring(lineStartIndex, lineEndIndex) }

    val line: Int by lazy { StringView(input, 0, offset).count { it == '\n' } + 1 }
    val column: Int by lazy { offset - lineStartIndex }

    private val lineStartIndex: Int by lazy {
        StringView(input, 0, offset).lastIndexOf('\n') + 1
    }
    private val lineEndIndex: Int by lazy {
        input.indexOf('\n', offset).let { if (it == -1) input.length else it }
    }

    override fun toString(): String {
        return "$lineStr\n${" ".repeat(column) + "^"}"
    }
}
