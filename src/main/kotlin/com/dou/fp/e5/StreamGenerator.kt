@file:Suppress("UNUSED_VARIABLE", "UNUSED_PARAMETER")

package com.dou.fp.e5

import kotlin.coroutines.RestrictsSuspension
import kotlin.experimental.ExperimentalTypeInference

fun test() {
    val str = stream {
        yield(34)
    }

    val seq = sequence {
        yield(34)
    }
}

@OptIn(ExperimentalTypeInference::class)
fun <T> stream(@BuilderInference block: suspend StreamGeneratorScope<T>.() -> Unit): Stream<T> = TODO()

@RestrictsSuspension
interface StreamGeneratorScope<in T> {
    suspend fun yield(v: T)
}
