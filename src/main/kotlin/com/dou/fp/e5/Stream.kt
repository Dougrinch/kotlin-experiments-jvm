@file:Suppress("unused")

package com.dou.fp.e5

import com.dou.fp.e10.StreamOf
import com.dou.fp.e3.List
import com.dou.fp.e3.reverse
import com.dou.fp.e4.*
import com.dou.fp.e4.Option.Companion.some
import com.dou.fp.e5.Stream.Companion.cons
import com.dou.fp.e5.Stream.Companion.empty
import com.dou.fp.e5.Stream.Companion.of
import com.dou.fp.e3.Cons as ConsL
import com.dou.fp.e3.Nil as NilL

sealed class Stream<out A> : StreamOf<A> {
    companion object {
        fun <A> cons(headP: () -> A, tailP: () -> Stream<A>): Stream<A> {
            val head: A by lazy { headP() }
            val tail: Stream<A> by lazy { tailP() }
            return Cons({ head }, { tail })
        }

        fun <A> empty(): Stream<A> = Empty

        fun <A> continually(a: A): Stream<A> = cons({ a }, { continually(a) })

        fun <A> of(vararg xs: A): Stream<A> = when {
            xs.isEmpty() -> empty()
            else -> cons({ xs[0] }, { of(*xs.sliceArray(1 until xs.size)) })
        }
    }
}

data class Cons<out A>(val head: () -> A, val tail: () -> Stream<A>) : Stream<A>()
object Empty : Stream<Nothing>()

fun <A> Stream<A>.head(): Option<A> = when (this) {
    is Empty -> None
    is Cons -> Some(head())
}

fun <A> Stream<A>.tail(): Option<Stream<A>> = when (this) {
    is Empty -> None
    is Cons -> Some(tail())
}

fun <A> Stream<A>.headF(): Option<A> = foldRight({ Option.empty() }, { a, _ -> Some(a()) })

fun <A> Stream<A>.toList(): List<A> {
    tailrec fun Stream<A>.toList(listTail: List<A>): List<A> = when (this) {
        is Empty -> listTail
        is Cons -> tail().toList(ConsL(head(), listTail))
    }
    return toList(NilL).reverse()
}

fun <A> Stream<A>.take(n: Int): Stream<A> = when {
    n <= 0 -> Empty
    else -> when (this) {
        is Empty -> Empty
        is Cons -> Cons(head) { tail().take(n - 1) }
    }
}

fun <A> Stream<A>.takeWhile(f: (A) -> Boolean): Stream<A> = when (this) {
    is Empty -> this
    is Cons -> when (f(head())) {
        true -> Cons(head) { tail().takeWhile(f) }
        false -> Empty
    }
}

tailrec fun <A> Stream<A>.drop(n: Int): Stream<A> = when {
    n <= 0 -> this
    else -> when (this) {
        is Empty -> Empty
        is Cons -> tail().drop(n - 1)
    }
}

tailrec fun <A> Stream<A>.dropWhile(f: (A) -> Boolean): Stream<A> = when (this) {
    is Empty -> this
    is Cons -> when (f(head())) {
        true -> tail().dropWhile(f)
        false -> this
    }
}

tailrec fun <A, B> Stream<A>.foldLeft(z: B, f: (B, () -> A) -> B): B = when (this) {
    is Empty -> z
    is Cons -> tail().foldLeft(f(z, head), f)
}

fun <A, B> Stream<A>.foldRight(z: () -> B, f: (() -> A, () -> B) -> B): B = when (this) {
    is Empty -> z()
    is Cons -> f(head) { tail().foldRight(z, f) }
}

fun <A, B> Stream<A>.scanRight(z: () -> B, f: (() -> A, () -> B) -> B): Stream<B> {
    val z1 by lazy { z() }
    return foldRight({ z1 to of(z1) }, { a, p0 ->
        val p1: Pair<B, Stream<B>> by lazy { p0() }
        val b2: B = f(a) { p1.first }
        b2 to cons({ b2 }, { p1.second })
    }).second
}

fun <A> Stream<A>.exists(p: (A) -> Boolean): Boolean = foldRight({ false }, { a, b -> p(a()) || b() })
fun <A> Stream<A>.allF(p: (A) -> Boolean): Boolean = foldRight({ true }, { a, b -> p(a()) && b() })

tailrec fun <A> Stream<A>.all(p: (A) -> Boolean): Boolean = when (this) {
    is Empty -> true
    is Cons -> if (!p(head())) false else tail().all(p)
}

fun <A> Stream<A>.takeWhileF(f: (A) -> Boolean): Stream<A> =
    foldRight({ empty() }, { a, b -> if (f(a())) Cons(a) { b() } else Empty })

fun <A, B> Stream<A>.map(f: (A) -> B): Stream<B> =
    foldRight({ empty() }, { a, b -> cons({ f(a()) }, b) })

fun <A> Stream<A>.filter(f: (A) -> Boolean): Stream<A> =
    foldRight({ empty() }, { a, b -> if (f(a())) cons(a, b) else b() })

fun <A> Stream<A>.append(a: () -> A): Stream<A> = foldRight({ cons(a) { Empty } }, ::cons)
fun <A> Stream<A>.appendAll(a: () -> Stream<A>): Stream<A> = foldRight(a, ::cons)
fun <A, B> Stream<A>.flatMap(f: (A) -> Stream<B>): Stream<B> = foldRight({ empty() }) { a, b -> f(a()).appendAll(b) }

tailrec fun <A> Stream<A>.forEach(f: (A) -> Unit): Unit = when (this) {
    is Empty -> Unit
    is Cons -> {
        f(head())
        tail().forEach(f)
    }
}

fun <A> Stream.Companion.constant(a: A): Stream<A> = cons({ a }, { constant(a) })

fun <A, S> Stream.Companion.unfold(initial: S, f: (S) -> Option<Pair<A, S>>): Stream<A> = when (val o = f(initial)) {
    is None -> empty()
    is Some -> {
        val (a, next) = o.value
        cons({ a }, { unfold(next, f) })
    }
}

fun Stream.Companion.from(n: Int): Stream<Int> = unfold(n) { Some(Pair(it, it + 1)) }
fun Stream.Companion.fibs(): Stream<Int> = unfold(Pair(-1, 1)) {
    val current = it.first + it.second
    Some(Pair(current, Pair(it.second, current)))
}

fun <A, B> Stream<A>.mapU(f: (A) -> B): Stream<B> = Stream.unfold(this) {
    when (it) {
        is Empty -> None
        is Cons -> Some(Pair(f(it.head()), it.tail()))
    }
}

fun <A, B, C> Stream<A>.zipWith(that: Stream<B>, f: (A, B) -> C): Stream<C> =
    Stream.unfold(this to that) { (s1, s2) ->
        val o1 = s1.head()
        val o2 = s2.head()
        o1.zipWith(o2, f).map {
            val t1 = s1.tail().getOrElse { empty() }
            val t2 = s2.tail().getOrElse { empty() }
            Pair(it, t1 to t2)
        }
    }

fun <A, B> Stream<A>.zipAll(that: Stream<B>): Stream<Pair<Option<A>, Option<B>>> =
    Stream.unfold(this to that) { (s1, s2) ->
        val o1 = s1.head()
        val o2 = s2.head()
        if (o1.isNotEmpty() || o2.isNotEmpty()) {
            val t1 = s1.tail().getOrElse { empty() }
            val t2 = s2.tail().getOrElse { empty() }
            Some(Pair(o1 to o2, t1 to t2))
        } else {
            None
        }
    }

fun <A> Stream<A>.startsWith(s: Stream<A>): Boolean =
    zipAll(s).dropWhile { (o1, o2) -> o1.flatMap { a1 -> o2.map { a2 -> a1 == a2 } }.getOrElse { false } }
        .take(1).all { (_, o2) -> o2.isEmpty() }

fun <A> Stream<A>.tails(): Stream<Stream<A>> = Stream.unfold(some(this)) {
    it.map { s -> s to s.tail() }
}

fun <A> Stream<A>.hasSubsequence(s: Stream<A>): Boolean = tails().exists { it.startsWith(s) }

fun main() {
    println(of(1, 2, 3, 4, 5, 1, 2, 3, 4, 5).takeWhile { it < 3 }.toList())
    println(of(1, 2, 3, 4, 5, 1, 2, 3, 4, 5).takeWhileF { it < 3 }.toList())

    println(of(1, 2, 3).appendAll { of(4, 5, 6) }.toList())
    println("+++++++")
    of(1, 2, 3).flatMap { i ->
        println("asdf")
        of("$i-4", "$i-5", "$i-6")
    }.forEach { println(it) }

    println("-----------------")

    val src = c(1) + c(2) + c(3) + c(4)
    println(src)
    src
        .map { println("map1 $it"); it + 10 }
        .filter { println("filter $it"); it % 2 == 0 }
        .map { println("map2 $it"); it * 3 }
        .forEach { println("<-- $it") }

    println(Stream.from(5).filter { it % 2 == 0 }.take(5).toList())
    println(Stream.fibs().take(10).toList())

    println(of(1, 2, 3, 4, 5).zipAll(Stream.constant("hell").take(10)).map { (a, b) -> "-$a-$b-" }.toList())

    println(Stream.constant(1).startsWith(of(1)))
    println(Stream.constant(1).startsWith(of(1, 1, 1, 1, 1)))
    println(Stream.constant(1).startsWith(of(1, 1, 1, 1, 1).appendAll { Stream.constant(2) }))

    println(of(1, 2, 3).tails().map { it.toList() }.toList())

    println("---")
    println(of(1, 2, 3).scanRight({ 0 }, { a, b -> a() + b() }).toList())
}

fun <A> c(a: A): Stream<A> = cons({ println("-> $a"); a }, { empty() })
operator fun <A> Stream<A>.plus(s: Stream<A>): Stream<A> = appendAll { s }
