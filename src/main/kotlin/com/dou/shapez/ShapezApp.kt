package com.dou.shapez

import com.dou.advent.util.Move
import com.dou.advent.util.bruteForce

data class Shape(val a: Int, val b: Int, val c: Int, val d: Int)

fun rotateCW(s: Shape): Shape {
    return Shape(s.d, s.a, s.b, s.c)
}

fun rotateCCW(s: Shape): Shape {
    return Shape(s.b, s.c, s.d, s.a)
}

fun rotate180(s: Shape): Shape {
    return Shape(s.c, s.d, s.a, s.b)
}

fun swap(s1: Shape, s2: Shape): Pair<Shape, Shape> {
    return Shape(s1.a, s1.b, s2.c, s2.d) to Shape(s2.a, s2.b, s1.c, s1.d)
}

enum class RotateOp {
    None, RotateCW, RotateCCW, Rotate180
}

data class State(val s1: Shape, val s2: Shape)

sealed interface Step//(val op1: RotateOp, val op2: RotateOp, val swapOp: SwapOp)
data class RotateStep(val op1: RotateOp, val op2: RotateOp) : Step
data class SwapStep(val swapOp: SwapOp) : Step

enum class SwapOp {
    Swap12//, Swap34, Swap1234, Swap23
}

fun rotate(s: Shape, op: RotateOp): Shape {
    return when (op) {
        RotateOp.None -> s
        RotateOp.RotateCW -> rotateCW(s)
        RotateOp.RotateCCW -> rotateCCW(s)
        RotateOp.Rotate180 -> rotate180(s)
    }
}

fun main() {
    val allMoves = RotateOp.entries.flatMap { op1 ->
        RotateOp.entries.mapNotNull { op2 ->
            if (op1 != RotateOp.None || op2 != RotateOp.None) {
                RotateStep(op1, op2)
            } else {
                null
            }
        }
    } + SwapStep(SwapOp.Swap12)

    println(allMoves.size)

    val initialS1 = Shape(2, 2, 2, 2)
    val initialS2 = Shape(3, 3, 3, 4)
//    val desiredS = Shape(3, 2, 3, 2)

    val target1 = Shape(1, 2, 3, 8)
    val target2 = Shape(5, 6, 7, 4)

    var max = 0

    bruteForce(
        initial = listOf<Pair<Step?, State>>(
            null to State(
//                Shape(1, 2, 3, 4),
//                Shape(5, 6, 7, 8),
                Shape(5,7,4,6),
                Shape(5,7,4,6),
//                Shape(7, 6, 4, 5),
//                Shape(7, 6, 4, 5),
            )
        ),
        potentialMoves = { _ -> allMoves },
        makeMove = { stages, step ->
            if (stages.size > max) {
                max = stages.size
                println(max)
            }
            val before = stages.last().second

            val next = when (step) {
                is RotateStep -> {
                    val s1 = rotate(before.s1, step.op1)
                    val s2 = rotate(before.s2, step.op2)
                    State(s1, s2)
                }

                is SwapStep -> {
                    when (step.swapOp) {
                        SwapOp.Swap12 -> {
                            val (s1, s2) = swap(before.s1, before.s2)
                            State(s1, s2)
                        }
                    }
                }
            }

            if (next.s1 == target2 && next.s2 == target2) {// && next.s3 == target && next.s4 == target) {
//            if (next.s1 == target1) {// && next.s3 == target && next.s4 == target) {
                Move.PathCompleted(stages + (step to next))
            } else {
                Move.Successful(stages + (step to next))
            }
        }
    ).first().let {
        for ((step, state) in it.asReversed()) {
            println(state)
            if (step != null) {
                println(step)
            }
        }
//        for (step in it) {
//            val s1 = step.first.first
//            val s2 = step.first.second
//            val op1 = step.second?.first
//            val op2 = step.second?.second
//            val stepDesc = """
//                $op1      $op2
//                ${s1.d} ${s1.a}         ${s2.d} ${s2.a}
//                ${s1.c} ${s1.b}         ${s2.c} ${s2.b}
//            """.trimIndent()
//            println(stepDesc)
//        }
    }
}
