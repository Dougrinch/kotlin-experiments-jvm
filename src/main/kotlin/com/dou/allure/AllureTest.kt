package com.dou.allure

import io.qameta.allure.Step
import io.qameta.allure.junitplatform.AllureJunitPlatform
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.junit.platform.engine.discovery.DiscoverySelectors.selectClass
import org.junit.platform.launcher.TestExecutionListener
import org.junit.platform.launcher.TestPlan
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder
import org.junit.platform.launcher.core.LauncherFactory
import org.junit.platform.launcher.listeners.SummaryGeneratingListener
import java.io.PrintWriter

fun main() {
    val request = LauncherDiscoveryRequestBuilder.request()
        .selectors(selectClass(AllureTest::class.java), selectClass(SecondTest::class.java))
        .build()

    val summaryListener = SummaryGeneratingListener()

    val launcher = LauncherFactory.create()
    launcher.registerTestExecutionListeners(AllureJunitPlatform())
    launcher.registerTestExecutionListeners(summaryListener)
    launcher.registerTestExecutionListeners(object : TestExecutionListener {
        override fun testPlanExecutionFinished(testPlan: TestPlan) {
            println("after all")
        }
    })
    launcher.execute(request)

    summaryListener.summary.printTo(PrintWriter(System.out))
    summaryListener.summary.printFailuresTo(PrintWriter(System.out))
}

class SystemNotStarted : Exception()

class AllureTest {
    @BeforeEach
    fun setup() {
        throw SystemNotStarted()
    }

    @Test
    fun test1() {
        login()
        logic()
        logout()
    }

    @Test
    fun test2() {
    }

    @Test
    fun test3() {
    }

    @Step
    private fun login() {
    }

    @Step
    private fun logic() {
        fail("logic hasn't implemented yet")
    }

    @Step
    private fun logout() {
    }
}

class SecondTest {
    @Test
    fun test1() {
        throw AssertionError()
    }

    @Test
    fun test2() {
    }

    @Test
    fun test3() {
    }
}
