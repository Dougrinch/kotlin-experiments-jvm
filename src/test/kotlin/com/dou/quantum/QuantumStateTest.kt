package com.dou.quantum

import com.dou.math.IntOps
import com.dou.math.matrix.column
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

class QuantumStateTest {

    @Test
    fun testToString() = with(IntOps) {
        val q = QuantumState(column(1, 0, 3, 0))
        assertThat(q.toString(), equalTo("|00⟩ + 3|10⟩"))
    }
}
