package com.dou.typed

import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

class TypedWhenTest {

    @Test
    fun oneLevelMatch() {
        val typedWhen = buildWhen<String, Int, Unit, String>({ s, i -> s.toIntOrNull() == i }) {
            match(1) { it }
            match(2, 3) { it }
            match(setOf(4, 5, 6)) { it.joinToString() }
        }

        typedWhen.assertRun(
            supportedKeys = setOf(1, 2, 3, 4, 5, 6),
            context = Unit,
            keys = setOf("1", "2", "3", "4", "5", "-"),
            matched = listOf("1", "2", "3", "4, 5"),
            unsupported = setOf("-")
        )
    }

    @Test
    fun twoLevelsMatch() {
        val typedWhen = buildWhen<Int, Int, Int, Int, Unit, String>(Int::equals, Int::equals) {
            match(1) {
                match(1) { i1, i2 -> "1$i1$i2" }
                match(2, 3) { i1, i2 -> "2$i1$i2" }
                match(setOf(4, 5, 6)) { i1, i2 -> "3$i1$i2" }
            }
            match(2, 3) {
                match(1) { i1, i2 -> "4$i1$i2" }
                match(2, 3) { i1, i2 -> "5$i1$i2" }
                match(setOf(4, 5, 6)) { i1, i2 -> "6$i1$i2" }
            }
            match(setOf(4, 5, 6)) {
                match(1) { i1, i2 -> "7$i1$i2" }
                match(2, 3) { i1, i2 -> "8$i1$i2" }
                match(setOf(4, 5, 6)) { i1, i2 -> "9$i1$i2" }
            }
        }

        typedWhen.assertRun(
            supportedKeys = (1..6).flatMapTo(HashSet()) { f -> (1..6).map { s -> f to s } },
            context = Unit,
            keys = (0..5).flatMapTo(HashSet()) { f -> (0..5).map { s -> f to s } },
            matched = listOf(
                "111", "212", "213", "31[4, 5]",
                "421", "522", "523", "62[4, 5]",
                "431", "532", "533", "63[4, 5]",
                "7[4, 5]1", "8[4, 5]2", "8[4, 5]3", "9[4, 5][4, 5]"
            ),
            unsupported = (0..5).mapTo(HashSet()) { 0 to it } + (0..5).mapTo(HashSet()) { it to 0 }
        )
    }

    @Test
    fun threeLevelsMatch() {
        val typedWhen = buildWhen<Int, Int, Int, Int, Int, Int, Unit, String>(Int::equals, Int::equals, Int::equals) {
            match(setOf(1, 2)) {
                match(3, 4) {
                    match(setOf(5, 6)) { a, b, c -> "$a$b$c" }
                }
            }
        }

        val keys = setOf(
            Triple(1, 3, 5),
            Triple(1, 3, 6),
            Triple(1, 4, 5),
            Triple(1, 4, 6),
            Triple(2, 3, 5),
            Triple(2, 3, 6),
            Triple(2, 4, 5),
            Triple(2, 4, 6),
        )

        typedWhen.assertRun(
            supportedKeys = keys,
            context = Unit,
            keys = keys,
            matched = listOf("[1, 2]3[5, 6]", "[1, 2]4[5, 6]"),
            unsupported = setOf()
        )
    }

    private fun <R : Any, M : Any, C : Any, V : Any> TypedWhen<R, M, C, V>.assertRun(
        supportedKeys: Set<M>, context: C, keys: Set<R>, matched: List<V>, unsupported: Set<R>
    ) {
        assertThat(this.supportedKeys, equalTo(supportedKeys))
        runBlocking {
            val (m, u) = run(keys, context)
            assertThat(m, containsInAnyOrder(matched.map { equalTo(it) }))
            assertThat(u, containsInAnyOrder(unsupported.map { equalTo(it) }))
        }
    }
}
