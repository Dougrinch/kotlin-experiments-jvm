package com.dou.fp.e6

import com.dou.fp.e3.Cons
import com.dou.fp.e3.List
import com.dou.fp.e3.Nil
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.lessThan
import org.junit.jupiter.api.Test

class RandomTest {

    @Test
    fun nextNonNegativeInt() {
        assertThat(SequenceRNG(List(0)).nextNonNegativeInt().first, equalTo(0))
        assertThat(SequenceRNG(List(100)).nextNonNegativeInt().first, equalTo(100))
        assertThat(SequenceRNG(List(Int.MAX_VALUE)).nextNonNegativeInt().first, equalTo(Int.MAX_VALUE))
        assertThat(SequenceRNG(List(-100)).nextNonNegativeInt().first, equalTo(100))
        assertThat(SequenceRNG(List(Int.MIN_VALUE, -100)).nextNonNegativeInt().first, equalTo(100))
    }

    @Test
    fun nextDouble() {
        assertThat(SequenceRNG(List(0)).nextDouble().first, equalTo(0.0))
        assertThat(SequenceRNG(List(Int.MAX_VALUE)).nextDouble().first, lessThan(1.0))
    }

    @Test
    fun nextInts() {
        val (ints, rng) = SequenceRNG(List(1, 2, 3, 4)).nextInts(3)
        assertThat(ints, equalTo(List(1, 2, 3)))
        assertThat(rng, equalTo(SequenceRNG(List(4))))
    }

    @Test
    fun intsR() {
        val (ints, rng) = intsR(3).run(SequenceRNG(List(1, 2, 3, 4)))
        assertThat(ints, equalTo(List(1, 2, 3)))
        assertThat(rng, equalTo(SequenceRNG(List(4))))
    }
}

private data class SequenceRNG(val vs: List<Int>) : RNG {
    override fun nextInt(): Pair<Int, RNG> {
        return when (vs) {
            is Nil -> throw AssertionError()
            is Cons -> vs.head to SequenceRNG(vs.tail)
        }
    }
}
