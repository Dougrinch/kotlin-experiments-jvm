package com.dou.fp.e6

import com.dou.fp.e3.List
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

class MachineTest {

    @Test
    fun `Inserting a coin into a locked machine will cause it to unlock if there's any candy left`() {
        val machine = Machine(true, 3, 0)
        val result = makeTurn(Coin).run(machine).second
        assertThat(result, equalTo(Machine(false, 3, 0)))
    }

    @Test
    fun `Turning the knob on an unlocked machine will cause it to dispense candy and become locked`() {
        val machine = Machine(false, 3, 0)
        val result = makeTurn(Turn).run(machine).second
        assertThat(result, equalTo(Machine(true, 2, 1)))
    }

    @Test
    fun `Turning the knob on a locked machine does nothing`() {
        val machine = Machine(true, 3, 0)
        val result = makeTurn(Turn).run(machine).second
        assertThat(result, equalTo(Machine(true, 3, 0)))
    }

    @Test
    fun `Inserting a coin into an unlocked machine does nothing`() {
        val machine = Machine(false, 3, 0)
        val result = makeTurn(Coin).run(machine).second
        assertThat(result, equalTo(Machine(false, 3, 0)))
    }

    @Test
    fun `A machine that's out of candy ignores all inputs`() {
        val machine = Machine(true, 0, 6)

        val coinResult = makeTurn(Coin).run(machine).second
        assertThat(coinResult, equalTo(Machine(true, 0, 6)))

        val turnResult = makeTurn(Turn).run(machine).second
        assertThat(turnResult, equalTo(Machine(true, 0, 6)))
    }

    @Test
    fun `Simple scenario`() {
        val machine = Machine(true, 5, 10)

        val inputs = List(Coin, Turn, Coin, Turn, Coin, Turn, Coin, Turn)
        val result = simulateMachine(inputs).run(machine).first
        assertThat(result, equalTo(14 to 1))
    }
}
