package com.dou

import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.number.IsCloseTo
import kotlin.math.abs

fun closeTo(operand: Double, percentError: Double = 0.000_000_1): Matcher<Double> {
    val error = abs(operand) / 100 * percentError
    return Matchers.closeTo(operand, error)
}
