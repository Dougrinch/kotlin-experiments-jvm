package com.dou.advent.util

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

class RingIteratorTest {

    @Test
    fun testNext() {
        val list = arrayListOf(1, 2, 3)

        val next = sequence {
            val it = list.ringListIterator(0)
            while (it.hasNext()) {
                yield(it.next())
            }
        }

        assertThat(next.take(10).toList(), equalTo(listOf(1, 2, 3, 1, 2, 3, 1, 2, 3, 1)))
    }

    @Test
    fun testPrev() {
        val list = arrayListOf(1, 2, 3)

        val next = sequence {
            val it = list.ringListIterator(0)
            while (it.hasPrevious()) {
                yield(it.previous())
            }
        }

        assertThat(next.take(10).toList(), equalTo(listOf(3, 2, 1, 3, 2, 1, 3, 2, 1, 3)))
    }

    @Test
    fun testNextPrevIndex() {
        val list = arrayListOf(1, 2, 3)
        val it = list.ringListIterator()

        assertThat(it.nextIndex(), equalTo(0))
        assertThat(it.nextIndex(), equalTo(0))
        assertThat(it.previousIndex(), equalTo(2))
        assertThat(it.hasNext(), equalTo(true))
        assertThat(it.hasPrevious(), equalTo(true))
        assertThat(it.previousIndex(), equalTo(2))
        assertThat(it.nextIndex(), equalTo(0))
        assertThat(it.previousIndex(), equalTo(2))
    }

    @Test
    fun testSet() {
        val list = arrayListOf(1, 2)
        val it = list.ringListIterator()
        assertThat(it.next(), equalTo(1))
        assertThat(it.next(), equalTo(2))
        it.set(5)
        assertThat(list, equalTo(listOf(1, 5)))
        it.hasNext()
        it.set(3)
        assertThat(list, equalTo(listOf(1, 3)))
    }

    @Test
    fun testAddRemove() {
        val list = arrayListOf(1, 2)
        val it = list.ringListIterator(1)
        assertThat(it.previous(), equalTo(1))
        it.remove()
        assertThat(list, equalTo(listOf(2)))
        it.hasPrevious()
        it.add(5)
        assertThat(list, equalTo(listOf(5, 2)))
    }

    @Test
    fun testAdd() {
        val list = arrayListOf(1, 2, 3)
        val it = list.ringListIterator()
        it.next()
        it.next()
        it.add(5)
        assertThat(it.next(), equalTo(3))
        assertThat(list, equalTo(listOf(1, 2, 5, 3)))
    }
}
