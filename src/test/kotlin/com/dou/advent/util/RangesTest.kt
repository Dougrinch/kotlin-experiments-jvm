package com.dou.advent.util

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test

class RangesTest {

    @Test
    fun testRanges() {
        assertThat(
            0..10 intersectWith 3..5, equalTo(
                IntersectionResult(
                    listOf(3..5), listOf(0..2, 6..10), listOf()
                )
            )
        )
        assertThat(
            0..10 intersectWith 3..10, equalTo(
                IntersectionResult(
                    listOf(3..10), listOf(0..2), listOf()
                )
            )
        )

        assertThat(
            3..5 intersectWith 0..10, equalTo(
                IntersectionResult(
                    listOf(3..5), listOf(), listOf(0..2, 6..10)
                )
            )
        )

        assertThat(
            3..5 intersectWith 6..10, equalTo(
                IntersectionResult(
                    listOf(), listOf(3..5), listOf(6..10)
                )
            )
        )

        assertThat(
            3..5 intersectWith 5..10, equalTo(
                IntersectionResult(
                    listOf(5..5), listOf(3..4), listOf(6..10)
                )
            )
        )

        assertThat(
            5..10 intersectWith 1..8, equalTo(
                IntersectionResult(
                    listOf(5..8), listOf(9..10), listOf(1..4)
                )
            )
        )
    }
}
