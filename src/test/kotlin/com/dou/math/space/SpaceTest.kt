package com.dou.math.space

import com.dou.closeTo
import com.dou.math.DoubleOps
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import kotlin.math.PI

class SpaceTest : Space3D<Double>, DoubleOps {

    @Test
    fun test() {
        val p1 = point(1.0, 2.0, 3.0)
        val t1 = shiftBy(vector(4.0, -2.0, 0.0))
        val p2 = p1 * t1 * rotateByAround(PI / 2, vector(0.0, 1.0, 0.0)) * scaleBy(0.5)

        with(p2) {
            assertThat(x, closeTo(1.5))
            assertThat(y, closeTo(0.0))
            assertThat(z, closeTo(-2.5))
        }
    }
}
