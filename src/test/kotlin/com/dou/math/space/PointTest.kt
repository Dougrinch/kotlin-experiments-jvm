package com.dou.math.space

import com.dou.math.*
import kotlin.reflect.typeOf

inline fun <T, reified N : TypedNumber> point(element: (Int) -> T): Point<T, N, *, *> {
    return ref<N>().point(element)
//    return when (typeOf<N>()) {
//        typeOf<N2>() -> Point2D(element(0), element(1)) as Point<T, N, *, *>
//        typeOf<N3>() -> Point3D(element(0), element(1), element(2)) as Point<T, N, *, *>
//        else -> throw UnsupportedOperationException()
//    }
}

@JvmName("pn")
inline fun <T, reified N : TypedNumber> Ref<N>.point(element: (Int) -> T): Point<T, N, *, *> {
    return when (typeOf<N>()) {
        typeOf<N2>() -> Point2D(element(0), element(1)) as Point<T, N, *, *>
        typeOf<N3>() -> Point3D(element(0), element(1), element(2)) as Point<T, N, *, *>
        else -> throw UnsupportedOperationException()
    }
}

@JvmName("p2")
inline fun <T> Ref<N2>.point(element: (Int) -> T): Point<T, N2, *, *> {
    return Point2D(element(0), element(1))
}

@JvmName("p3")
inline fun <T> Ref<N3>.point(element: (Int) -> T): Point<T, N2, *, *> {
    return Point2D(element(0), element(1))
}

inline fun <reified N : TypedNumber> Point<Int, N, *, *>.add(p2: Point<Int, N, *, *>): Point<Int, N, *, *> {
    return point { this[it] + p2[it] }
}

interface Ref<N>
object RefImpl : Ref<Nothing>

inline fun <reified N> ref(): Ref<N> = RefImpl as Ref<N>

//inline fun <T, reified N : TypedNumber> point(element: (Int) -> T): Point<T, N, *, *> {
//
//}

fun main() {
    val p1 = point<Int, N2> { 2 * it + 1 }
    println(p1)
    val p2 = point<Int, N2> { 2 * it - 1 }
    println(p2)
    println(p1.add(p2))
}
