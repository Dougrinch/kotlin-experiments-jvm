package com.dou.math.complex

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class ComplexTest {

    @Test
    fun testAddition() {
        val c1 = Complex(3.0, 2.0)
        val c2 = Complex(1.0, 7.0)
        assertThat(c1 + c2, equalTo(Complex(4.0, 9.0)))
    }

    @Test
    fun testSubtraction() {
        val c1 = Complex(3.0, 2.0)
        val c2 = Complex(1.0, 7.0)
        assertThat(c1 - c2, equalTo(Complex(2.0, -5.0)))
    }

    @Test
    fun testMultiplication() {
        val c1 = Complex(3.0, 2.0)
        val c2 = Complex(1.0, 7.0)
        assertThat(c1 * c2, equalTo(Complex(-11.0, 23.0)))
    }

    @Test
    fun testDivision() {
        val c1 = Complex(3.0, 2.0)
        val c2 = Complex(1.0, 7.0)
        assertThat(c1 / c2, equalTo(Complex(0.34, -0.38)))
    }
}
