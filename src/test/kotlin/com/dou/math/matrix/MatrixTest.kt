package com.dou.math.matrix

import com.dou.math.IntOps
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class MatrixTest : IntOps {

    @Test
    fun `test row matrix`() {
        val m = row(1, 2, 3)

        assertThat((0..2).map { m[it] }, equalTo(listOf(1, 2, 3)))
        assertThat((0..2).map { m[0, it] }, equalTo(listOf(1, 2, 3)))
        assertThat(m.rows, equalTo(listOf(row(1, 2, 3))))
        assertThat(m.columns, equalTo(listOf(column(1), column(2), column(3))))
    }

    @Test
    fun `test column matrix`() {
        val m = column(1, 2, 3)

        assertThat((0..2).map { m[it] }, equalTo(listOf(1, 2, 3)))
        assertThat((0..2).map { m[it, 0] }, equalTo(listOf(1, 2, 3)))
        assertThat(m.rows, equalTo(listOf(row(1), row(2), row(3))))
        assertThat(m.columns, equalTo(listOf(column(1, 2, 3))))
    }

    @Test
    fun `test matrix addition`() {
        val m1 = Matrix(
            row(1, 2),
            row(3, 4)
        )

        val m2 = Matrix(
            row(5, 6),
            row(7, 8)
        )

        val expected = Matrix(
            row(6, 8),
            row(10, 12)
        )

        assertThat(m1 + m2, equalTo(expected))
    }

    @Test
    fun `test matrix multiplication`() {
        val m1 = Matrix(
            row(1, 2),
            row(3, 4)
        )

        val m2 = Matrix(
            row(5, 6),
            row(7, 8)
        )

        val expected = Matrix(
            row(19, 22),
            row(43, 50)
        )

        assertThat(m1 * m2, equalTo(expected))
    }

    @Test
    fun `test columns`() {
        val m = Matrix(
            row(1, 2),
            row(3, 4)
        )

        assertThat(m.columns, equalTo(listOf(column(1, 3), column(2, 4))))
    }

    @Test
    fun `test rows`() {
        val m = Matrix(
            row(1, 2),
            row(3, 4)
        )

        assertThat(m.rows, equalTo(listOf(row(1, 2), row(3, 4))))
    }

    @Test
    fun `test tensor product`() {
        val m1 = Matrix(
            row(1, 2),
            row(3, 4)
        )

        val m2 = Matrix(
            row(0, 5),
            row(6, 7)
        )

        val expected = Matrix(
            row(0, 5, 0, 10),
            row(6, 7, 12, 14),
            row(0, 15, 0, 20),
            row(18, 21, 24, 28)
        )

        assertThat(m1 x m2, equalTo(expected))
    }
}
