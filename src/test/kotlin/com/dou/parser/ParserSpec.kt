package com.dou.parser

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail

class ParserSpec {

    @Test
    fun testDelegation() {
        val str1 = string("str")
        val str2 by string("str")
        val str3 = parser { string("str") }
        val str4 by parser { string("str") }
        val str5 by str2
        val str6 by parser { str2 }

        str1 applying "str" parsesTo "str"
        str2 applying "str" parsesTo "str"
        str3 applying "str" parsesTo "str"
        str4 applying "str" parsesTo "str"
        str5 applying "str" parsesTo "str"
        str6 applying "str" parsesTo "str"
    }

    @Test
    fun testRegex() {
        val p by "{" - regex("a*") - "}" map { it.length }
        p applying "{aaa}" parsesTo 3
        p applying "{}" parsesTo 0
    }

    @Test
    fun testStack() {
        val stackParser = ParserImpl(null) { ctx ->
            Success(0) { ctx.pathString }
        }

        val int by stackParser map { it }
        val str by int
        val transparent1 = str
        val str2 by string("text") or parser { transparent1 }
        val transparent2 = parser { str2 }
        val str3 by transparent2

        str3 applying "" parsesTo "str3.str2.(text or null).str.int"
        str3 applying "te" parsesTo Error("text expected", "1:0")
        str3 applying "text" parsesTo "text"
    }

    @Test
    fun testManyParserName() {
        digit.many(separator = ",", exact = 3) applying "a,2,3" failedAt "many(digit).0.digit.regex.regex"
        digit.many(separator = ",", exact = 3) applying "1,b,3" failedAt "many(digit).1.(, - digit).digit.regex.regex"
        digit.many(separator = ",", exact = 3) applying "1,2,c" failedAt "many(digit).2.(, - digit).digit.regex.regex"
    }

    @Test
    fun testErrorsPropagation() {
        val ints by int.many(",")
        "[" - ints - "]" applying "[1,2 3]" parsesTo Error("One of , or ] expected", "1:4")
    }

    @Test
    fun testLazyValueConstruction() {
        var valueCalculated = 0

        val int = regex("-?\\d+") map {
            valueCalculated += 1
            it.toInt()
        }

        int.slice() applying "-158" parsesTo "-158"
        assertThat(valueCalculated, equalTo(0))

        int applying "-158" parsesTo -158
        assertThat(valueCalculated, equalTo(1))

        int - { succeed(Unit) } parseAs Unit applying "200" parsesTo Unit
        assertThat(valueCalculated, equalTo(2))

        int - succeed(Unit) parseAs Unit applying "200" parsesTo Unit
        assertThat(valueCalculated, equalTo(2))
    }

    @Test
    fun richValueTest() {
        (string("a") or string("b")).many().slice() applying "aaba" parsesTo "aaba"

        string("abra") or string("cadabra") applying "abra" parsesTo "abra"
        string("abra") or string("cadabra") applying "cadabra" parsesTo "cadabra"

        (string("ab") or string("cde")).many(exact = 3) applying "ababab" parsesTo listOf("ab", "ab", "ab")
        (string("ab") or string("cde")).many(exact = 3) applying "cdecdecde" parsesTo listOf("cde", "cde", "cde")
        (string("ab") or string("cde")).many(exact = 3) applying "ababcde" parsesTo listOf("ab", "ab", "cde")
        (string("ab") or string("cde")).many(exact = 3) applying "cdecdeab" parsesTo listOf("cde", "cde", "ab")

        int applying "18" parsesTo 18

        (int - "," - int - "," - int) applying "18,-42,+58" parsesTo (18 to -42 to 58)

        (int - "," - int).map { (a, b) -> a + b }.many(separator = ",").let { p ->
            p applying "1,2,3,4,5,6" parsesTo listOf(3, 7, 11)
        }

        int.many(separator = ",").map { it.sum() }.many(separator = ";").let { p ->
            p applying "1,2;3;4,5,6" parsesTo listOf(3, 3, 15)
        }

        (int - "," - { n -> int.many(separator = ",", exact = n) } - "," - int.many(separator = ","))
            .let { p ->
                p applying "3,1,2,3,4" parsesTo (3 to listOf(1, 2, 3) to listOf(4))
                p applying "2,1,2,3,4" parsesTo (2 to listOf(1, 2) to listOf(3, 4))
            }

        digit.many(separator = ",") - regex("\\w+") applying "1,2,3word" parsesTo (listOf(1, 2, 3) to "word")
        digit.many(separator = ",") - "," - regex("\\w+") applying "1,2,3,word" parsesTo (listOf(1, 2, 3) to "word")
    }

    private infix fun <A> Parser<A>.applying(input: String): ApplyingContext<A> {
        return ApplyingContext(this, input)
    }

    private infix fun <A> ApplyingContext<A>.parsesTo(result: A) {
        assertThat(p.parse(input), equalTo(result))
    }

    private infix fun <A> ApplyingContext<A>.parsesTo(error: Error) {
        val th: ParsingException
        try {
            val result = p.parse(input)
            fail("$error expected but parses to $result")
        } catch (e: ParsingException) {
            th = e
        }
        assertThat(th.error, equalTo(error.message))
        assertThat("${th.location.line}:${th.location.column}", equalTo(error.at))
    }

    private infix fun <A> ApplyingContext<A>.failedAt(path: String) {
        val pr = p.parse(Context(null, null, Location(input, 0, input.length)))
        assertThat((pr as Failure).errors.single().second.pathString, equalTo(path))
    }

    private class ApplyingContext<A>(val p: Parser<A>, val input: String)

    private data class Error(val message: String, val at: String)
}
